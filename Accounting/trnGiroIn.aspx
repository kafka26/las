<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnGiroIn.aspx.vb" Inherits="Accounting_IncomingGiroRealization" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="SaddleBrown" Text=".: Incoming Giro Realization" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Incoming Giro Realization :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w30"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=5></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w31"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w32"><asp:ListItem Value="cb.cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">Bank Account</asp:ListItem>
<asp:ListItem Value="cb.cashbanknote">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w33"></asp:TextBox></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w34"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w35"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w37"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w38"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w40"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w41"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w42">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1" __designer:wfdid="w43"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w44" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2" __designer:wfdid="w45"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w46" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w48"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server" __designer:wfdid="w50"></asp:LinkButton></TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="8" DataKeyNames="cashbankoid" AllowPaging="True" __designer:wfdid="w51" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="cashbankoid" DataNavigateUrlFormatString="~\Accounting\trnGiroIn.aspx?oid={0}" DataTextField="cashbankno" HeaderText="Cash/Bank No." SortExpression="cashbankno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cashbankdate" HeaderText="Real Date" SortExpression="cashbankdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Bank Account" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status" SortExpression="cashbankstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note" SortExpression="cashbanknote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:BoundField DataField="divname" HeaderText="Business Unit">
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Left" />
        <ItemStyle HorizontalAlign="Left" />
    </asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' /><asp:Label
                ID="lblOidGVMst" runat="server" Text='<%# eval("cashbankoid") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w52"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel2" __designer:wfdid="w53"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image12" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w54"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress> </TD><TD align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <Triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</Triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Incoming Giro Realization Header" __designer:wfdid="w280" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" __designer:wfdid="w281"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w282" Visible="False"></asp:Label> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w283" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit" __designer:wfdid="w285" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w286" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="giropaymstoid" runat="server" __designer:wfdid="w284" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblInfoNo" runat="server" Text="Cash/Bank No." __designer:wfdid="w287"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" CssClass="inpTextDisabled" Width="200px" __designer:wfdid="w288" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Realization Date" Width="96px" __designer:wfdid="w226"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankdate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w290" AutoPostBack="True" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w291"></asp:ImageButton> <asp:Label id="lblInfoDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w292"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Currency" __designer:wfdid="w293"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w294">
            </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Total Amount" __designer:wfdid="w295"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w296" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Bank Account" __designer:wfdid="w297"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w298" AutoPostBack="True">
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w299"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankstatus" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w300" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w301"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanknote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w302" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w303" TargetControlID="cashbankdate" PopupButtonID="btnDate" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w304" TargetControlID="cashbankdate" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Incoming Giro Realization Detail" __designer:wfdid="w305" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w306" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="seq" runat="server" __designer:wfdid="w307" Visible="False">1</asp:Label> <asp:Label id="payargirooid" runat="server" __designer:wfdid="w308" Visible="False"></asp:Label> <asp:Label id="custoid" runat="server" __designer:wfdid="w309" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:ImageButton id="btnClearGiro" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w310" Visible="False"></asp:ImageButton></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="refoid" runat="server" __designer:wfdid="w311" Visible="False"></asp:Label> <asp:Label id="refacctgoid" runat="server" __designer:wfdid="w312" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="BGM No." __designer:wfdid="w313"></asp:Label> <asp:Label id="Label21" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w314"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="refno" runat="server" CssClass="inpTextDisabled" Width="280px" __designer:wfdid="w315" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchGiro" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w316"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Due Date" __designer:wfdid="w317"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payargiroduedate" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w318" Enabled="False" ToolTip="MM/DD/YYYY"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Giro No." __designer:wfdid="w319"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payargirorefno" runat="server" CssClass="inpTextDisabled" Width="280px" __designer:wfdid="w320" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="Customer" __designer:wfdid="w321"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="300px" __designer:wfdid="w322" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Amount" __designer:wfdid="w323"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payargiroamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w324" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w325"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payargironote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w326" MaxLength="100"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w327"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w328"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w329" DataKeyNames="seq" PageSize="5" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="refno" HeaderText="BGM No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payargiroduedate" HeaderText="Due Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payargirorefno" HeaderText="Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payargiroamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payargironote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w330"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w331"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w332"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w333"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w334" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w335" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w336" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w337"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w338" AlternateText="Show COA"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w339" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w340"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Incoming Giro Realization :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListGiro" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListGiro" runat="server" CssClass="modalBox" Width="800px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListGiro" runat="server" Font-Size="Medium" Font-Bold="True" Font-Underline="False">List Of Incoming Giro Posting</asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListGiro" runat="server" CssClass="Label" Width="100%" DefaultButton="btnFindListGiro">Filter : <asp:DropDownList id="FilterDDLListGiro" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="cashbankno">BGM No.</asp:ListItem>
<asp:ListItem Value="cashbankrefno">Giro No.</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListGiro" runat="server" CssClass="inpText" Width="200px"></asp:TextBox> <asp:ImageButton id="btnFindListGiro" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListGiro" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListGiro" runat="server" ForeColor="#333333" Width="99%" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:TemplateField><HeaderTemplate>
<asp:CheckBox id="cbHdrListGiro" runat="server" AutoPostBack="True" OnCheckedChanged="cbHdrListGiro_CheckedChanged"></asp:CheckBox>
</HeaderTemplate>
<ItemTemplate>
<asp:CheckBox id="cbListGiro" runat="server" ToolTip='<%# eval("cashbankoid") %>' Checked='<%# eval("checkvalue") %>'></asp:CheckBox>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:BoundField DataField="cashbankno" HeaderText="BGM No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankrefno" HeaderText="Giro No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankduedate" HeaderText="Due Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankgroup" HeaderText="Group">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lbAddToListGiro" runat="server">[ Add To List ]</asp:LinkButton> <asp:LinkButton id="lbSelectAllListGiro" runat="server">[ Select All To List ]</asp:LinkButton> <asp:LinkButton id="lkbCloseListGiro" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListGiro" runat="server" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListGiro" runat="server" TargetControlID="btnHideListGiro" PopupControlID="pnlListGiro" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListGiro" Drag="True"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCOAPosting" runat="server" CssClass="modalBox" Visible="False"
                Width="600px">
                <table style="width: 100%">
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:Label ID="lblCOAPosting" runat="server" Font-Bold="True" Font-Size="Medium"
                                Text="COA - Posting Rate :"></asp:Label>
                            <asp:DropDownList ID="DDLRateType" runat="server" AutoPostBack="True" CssClass="inpText"
                                Font-Bold="True" Font-Size="Medium" Width="125px">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:GridView ID="gvCOAPosting" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" Width="99%">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:LinkButton ID="lkbCloseCOAPosting" runat="server">[ CLOSE ]</asp:LinkButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnHideCOAPosting" runat="server" Visible="False" />
            <ajaxToolkit:ModalPopupExtender ID="mpeCOAPosting" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting"
                TargetControlID="btnHideCOAPosting">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

