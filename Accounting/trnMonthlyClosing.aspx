﻿<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMonthlyClosing.aspx.vb" Inherits="Accounting_trnMonthlyClosing" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text=".: Monthly Closing" CssClass="Title" Font-Italic="False"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List Monthly Closing :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE><TR><TD class="Label" align=left>Filter : </TD><TD align=left><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText"></asp:DropDownList></TD><TD align=left><asp:TextBox id="FilterText" runat="server" CssClass="inpText"></asp:TextBox></TD><TD align=left><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/find.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD align=left><asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left></TD><TD style="HEIGHT: 10px" align=left></TD></TR></TABLE></asp:Panel> <FIELDSET style="WIDTH: 98%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 250px; TEXT-ALIGN: left; BORDER-BOTTOM-STYLE: none" id="Fieldset3"><DIV id="Div3"></DIV><DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%"><asp:GridView id="gvMst" runat="server" BorderColor="#DEDFDE" Font-Size="X-Small" Width="98%" BorderWidth="1px" BorderStyle="Solid" CellPadding="4" AutoGenerateColumns="False" AllowSorting="True">
<RowStyle BackColor="Lavender" Font-Size="X-Small"></RowStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" Text="Data tidak ditemukan !!" CssClass="Important"></asp:Label> 
</EmptyDataTemplate>

<HeaderStyle BackColor="RoyalBlue" Font-Size="X-Small" ForeColor="Navy"></HeaderStyle>

<AlternatingRowStyle BackColor="White" Font-Size="X-Small"></AlternatingRowStyle>
</asp:GridView> </DIV></FIELDSET> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=3></TD></TR><TR><TD style="WIDTH: 14%" align=left><asp:Label id="Label6" runat="server" Text="Business Unit"></asp:Label></TD><TD style="WIDTH: 1%" align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:DropDownList id="bussinessunit" runat="server" CssClass="inpText" Width="300px" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label7" runat="server" Text="Period"></asp:Label></TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:DropDownList id="DDLMonth" runat="server" CssClass="inpText" Width="125px" AutoPostBack="True"></asp:DropDownList>&nbsp;<asp:DropDownList id="DDLYear" runat="server" CssClass="inpText" Width="75px" AutoPostBack="True"></asp:DropDownList> <asp:Label id="Label2" runat="server" Font-Bold="True" Text="# Last Monthly Closing :"></asp:Label> <asp:Label id="lblLastClosing" runat="server" ForeColor="Red" Font-Bold="False"></asp:Label> <asp:Label id="Label3" runat="server" Font-Bold="True" Text="# Next Monthly Closing :"></asp:Label> <asp:Label id="lblNextClosing" runat="server" ForeColor="Red" Font-Bold="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label8" runat="server" Text="Status"></asp:Label></TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:Label id="closingstatus" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label9" runat="server" Text="Value In"></asp:Label></TD><TD align=center colSpan=1>:</TD><TD align=left colSpan=1><asp:DropDownList id="DDLCurrency" runat="server" CssClass="inpText" Width="50px" AutoPostBack="True"><asp:ListItem>IDR</asp:ListItem>
<asp:ListItem>USD</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD align=left></TD><TD align=center colSpan=1></TD><TD align=left colSpan=1><asp:ImageButton id="btnCalculate" runat="server" ImageUrl="~/Images/calculate.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD align=left colSpan=3><asp:Label id="lastclosemonth" runat="server" Visible="False"></asp:Label><asp:Label id="lastcloseyear" runat="server" Visible="False"></asp:Label><asp:Label id="nextclosemonth" runat="server" Visible="False"></asp:Label><asp:Label id="nextcloseyear" runat="server" Visible="False"></asp:Label> <asp:Label id="lblEff" runat="server" Text="Effective Days :" Visible="False"></asp:Label> <asp:TextBox id="effectivedays" runat="server" CssClass="inpText" Width="50px" Visible="False" MaxLength="2"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 5px" align=left colSpan=3></TD></TR><TR><TD align=left colSpan=3><asp:GridView id="gvDtl" runat="server" Width="100%" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#FFFBD6"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Name">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:TemplateField HeaderText="Start Balance"><EditItemTemplate>
<asp:TextBox id="TextBox1" runat="server" Text='<%# Bind("amtopen") %>'></asp:TextBox>
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label1" runat="server" Text="<%# GetOpen() %>"></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Debet"><EditItemTemplate>
<asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("amtdebet") %>'></asp:TextBox>
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label2" runat="server" Text="<%# GetDebet() %>"></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Font-Strikeout="False"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Credit"><EditItemTemplate>
<asp:TextBox id="TextBox3" runat="server" Text='<%# Bind("amtcredit") %>'></asp:TextBox>
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label3" runat="server" Text="<%# GetCredit() %>"></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:TemplateField HeaderText="Final Balance"><EditItemTemplate>
<asp:TextBox id="TextBox4" runat="server" Text='<%# Bind("amtbalance") %>'></asp:TextBox>
</EditItemTemplate>
<ItemTemplate>
<asp:Label id="Label4" runat="server" Text="<%# GetClose() %>"></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:TemplateField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<PagerStyle HorizontalAlign="Right" Wrap="False" CssClass="gvhdr" ForeColor="White" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmptyDetail" runat="server" CssClass="Important" Text="No Monthly Closing data."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" align=left colSpan=3></TD></TR><TR><TD class="Label" align=left colSpan=3><asp:Panel id="pnlNeedPost" runat="server" Width="100%" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="FONT-WEIGHT: bold" align=left><asp:Label id="lblCounter" runat="server" Font-Bold="True" Text="0"></asp:Label>&nbsp;following transaction must be POST before Monthly Closing can be executed :</TD></TR><TR><TD style="HEIGHT: 5px" align=left></TD></TR><TR><TD align=left><asp:GridView id="gvPostList" runat="server" Width="50%" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="seq" HeaderText="No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="Transaction">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="jml" HeaderText="Count">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteImageUrl="~/Images/del.jpeg" ShowDeleteButton="True" ButtonType="Image" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 10px" align=left colSpan=3></TD></TR><TR><TD style="COLOR: #585858" align=left colSpan=3>Proccessed By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left colSpan=3><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPosting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnOpen" runat="server" ImageUrl="~/Images/revise.png" ImageAlign="AbsMiddle" __designer:wfdid="w1" OnClick="btnOpen_Click"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                                </div>
                                                <div id="processMessage" class="processMessage">
                                                    <span style="font-weight: bold; font-size: 10pt; color: purple">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                                                        Please Wait .....</span><br />
                                                </div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Monthly Closing :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" ImageUrl="~/Images/error.jpg" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKValidasi_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" Drag="True" TargetControlID="bePopUpMsg"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>

