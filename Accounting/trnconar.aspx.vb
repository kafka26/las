''Prgmr:mumun | LastUpdt:20.09.2014
'Imports System.Data
'Imports System.Data.SqlClient
'Imports System.Windows.Forms
'Imports System.Data.OleDb
'Imports CrystalDecisions.Shared
'Imports CrystalDecisions.CrystalReports.Engine
'Imports ClassFunction
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trnConAR
    Inherits System.Web.UI.Page

#Region "Variabel"
    'Public ConnStr As String = ConfigurationSettings.AppSettings("CONN")
    'Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_Conn"))
    'Dim xCmd As New SqlCommand("", conn)
    'Dim xreader As SqlDataReader
    'Dim sSql As String = ""
    'Dim cProc As New ClassProcedure
    'Dim cKon As New Koneksi
    'Dim Report As ReportDocument
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function validatingData() As String
        Dim sMsg As String = ""
        If suppoid.Text = "" Then
            sMsg &= "- Please Fill Customer !!<BR>"
        End If
        If trnbelino.Text = "" Then
            sMsg &= "-  Please Fill Invoice No !!<BR>"
        End If
        If amtbeli.Text = "" Or ToDouble(amtbeli.Text) <= 0 Then
            sMsg &= "- Total Invoice must be >= 0 !!<BR>"
        End If
        If CDate(invoicedate.Text) = "1/1/1900" Then
            sMsg &= "- Tanggal invoice salah !!<BR>"
        End If
		If trnapnote.Text.Trim.Length > 100 Then
			sMsg &= "- maximal note is 100 character !!<BR>"
        End If
        If lblPosting.Text = "POST" Then
            sSql = "select(ISNULL(cg.custcreditlimitrupiah,0)-ISNULL(cg.custcreditlimitusagerupiah,0)) CR from QL_mstcustgroup cg INNER JOIN QL_mstcust c on c.custgroupoid=cg.custgroupoid"
            Dim CR As Double = ToDouble(GetStrData(sSql))
            If CR = 0 Then
                sMsg &= "- Credit Limit = 0, tambah credit limit dulu!!<BR>"
            End If
        End If
        Return sMsg
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Prosedure"
	Private Sub ReAmount()
		If ToDouble(trntaxpct.Text) > 10 Then
			showMessage("Nilai Tax tidak boleh lebih dari 10 % ", 2)
		End If
		dpp.Text = ToMaskEdit(ToDouble(dpp.Text), 2)
		amttax.Text = ToMaskEdit(ToDouble(dpp.Text) * (ToDouble(trntaxpct.Text) / 100), 2)
		amtbeli.Text = ToMaskEdit(ToDouble(dpp.Text) + ToDouble(amttax.Text), 2)
		amtbayar.Text = ToMaskEdit(ToDouble(amtbayar.Text), 2)
		totalAP.Text = ToMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtbayar.Text), 2)
	End Sub

    Private Sub initDDL()
        ' Init Payment Term
        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' ORDER BY gendesc"
        FillDDL(trnpaytype, sSql)
        'ddl outlet
        'FillDDLOutlet(DDLOutlet, Session("UserID"))
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        FillDDL(DDLOutlet, sSql)
        'Fill A/P Account
        FillDDLAcctg(DDLacctgCode, "VAR_AR", DDLOutlet.SelectedValue)
        'Isi Kode sales
        sSql = "select distinct salescode,salescode+'-'+personname from ql_mstperson p inner join QL_mstlevel g on g.leveloid =p.leveloid where upper(g.leveldesc)='SALES' and p.activeflag='ACTIVE'"
        FillDDL(salescode, sSql)
    End Sub

    Public Sub binddata(ByVal sfilter As String)
		Dim sStatus As String = ""
        If cbStatus.Checked Then
            If FilterDDLStatus.SelectedValue <> "ALL" Then
                sStatus = " AND trnjualstatus='" & FilterDDLStatus.SelectedValue & "'"
            End If
		End If
		sSql = "SELECT 'False' AS checkvalue,'" & CompnyCode & "' outlet, bm.cmpcode, bm.trnjualmstoid, bm.trnjualno, bm.trnjualdate, getdate() AS payduedate, s.custoid, s.custname, bm.trnjualamtnetto AS amttrans, bm.Trnjualnote, bm.trnjualstatus, bm.trnjualtype AS typejual FROM QL_trnjualmst bm INNER JOIN QL_mstcust s ON bm.custoid=s.custoid /*--left JOIN QL_conar ap ON bm.cmpcode=ap.cmpcode AND bm.trnjualmstoid=ap.refoid*/ INNER JOIN QL_mstdivision o ON o.cmpcode=bm.cmpcode WHERE bm.trnjualmstoid < 0 /*ap.payrefoid=0 --AND ap.trnartype='AR BALANCE'*/ AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(sfilter) & "%' " & sStatus & "  ORDER BY bm.trnjualmstoid ASC"

		'sSql = "SELECT 'False' AS checkvalue,'" & CompnyCode & "' outlet, ap.cmpcode, bm.trnjualmstoid, bm.trnjualno, ap.trnardate trnjualdate, ap.payduedate, s.custoid, s.custname, ap.amttrans, bm.Trnjualnote, bm.trnjualstatus, bm.trnjualtype AS typejual FROM QL_conar ap INNER JOIN QL_mstcust s ON ap.custoid=s.custoid INNER JOIN QL_trnjualmst bm ON bm.cmpcode=ap.cmpcode AND bm.trnjualmstoid=ap.refoid AND bm.trnjualmstoid < 0 INNER JOIN QL_mstdivision o ON o.cmpcode=ap.cmpcode WHERE ap.payrefoid=0 AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(sfilter) & "%' " & sStatus & " AND ap.trnartype='AR BALANCE' "
        Session("TblTrn") = cKon.ambiltabel(sSql, "QL_trnbelimst")
        gvTrnConap.DataSource = Session("TblTrn")
        gvTrnConap.DataBind()
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblTrn") IsNot Nothing Then
            Dim dt As DataTable = Session("TblTrn")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTrnConap.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTrnConap.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTrnConap.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "trnjualmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Public Sub fillTextBox(ByVal sOutlet As String, ByVal vID As String)
        Try
            sSql = "SELECT b.trnjualamt AS accumpayment, B.cmpcode, B.trnjualmstoid,B.custoid AS trncustoid,B.trnjualstatus,ISNULL(C.acctgoid,0) AS acctgoid,B.trnjualdate,B.trnjualno,B.trnpaytype,B.trnjualamt,B.trntaxpct,B.trntaxamt,B.trnjualamtnetto,B.trnjualnote,C.conaroid,C.payduedate,ISNULL(P.payaroid,0)paymentoid,ISNULL(C1.payrefno,'')payrefno,ISNULL(C1.amtbayar,0)amtbayar,B.upduser,B.updtime,ISNULL(C1.paymentdate,'01/01/1900') AS paymentdate,ISNULL(C1.paymentacctgoid,0) paymentacctgoid,B.trnjualres1,b.trnjualtype AS typejual, b.salescode FROM QL_trnjualmst B LEFT JOIN QL_conar C ON B.trnjualmstoid = C.refoid AND C.reftype='ql_trnjualmst' LEFT OUTER JOIN QL_trnpayar P ON  B.trnjualmstoid = P.refoid AND P.reftype='QL_trnjualmst' LEFT OUTER JOIN QL_conar C1 ON    C1.reftype='QL_trnpayar' AND B.trnjualmstoid = C1.refoid AND P.payaroid = C1.payrefoid WHERE B.trnjualmstoid=" & vID & " AND B.cmpcode='" & sOutlet & "' ORDER BY B.trnjualmstoid DESC"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLOutlet.SelectedValue = xreader("cmpcode").ToString
                trnbelimstoid.Text = Trim(xreader("trnjualmstoid").ToString)
                suppoid.Text = Trim(xreader("trncustoid").ToString)
                lblPosting.Text = Trim(xreader("trnjualstatus").ToString)
                acctgoid.Text = Trim(xreader("acctgoid").ToString)
                invoicedate.Text = Format(CDate(Trim(xreader("trnjualdate").ToString)), "MM/dd/yyyy")
                trnbelino.Text = (xreader("trnjualno").ToString)
                trnpaytype.SelectedValue = Trim(xreader("trnpaytype").ToString)
                dpp.Text = ToMaskEdit(ToDouble((xreader("trnjualamt").ToString)), 2)
                trntaxpct.Text = ToMaskEdit(ToDouble((xreader("trntaxpct").ToString)), 2)
                amttax.Text = ToMaskEdit(ToDouble((xreader("trntaxamt").ToString)), 2)
                amtbeli.Text = ToMaskEdit(ToDouble((xreader("trnjualamtnetto").ToString)), 2)
                salescode.SelectedValue = Trim(xreader("salescode").ToString)
                trnapnote.Text = Trim(xreader("trnjualnote").ToString)
                conapoid.Text = Trim(xreader("conaroid").ToString)
                If trnpaytype.SelectedItem.Text = "CASH" Then
                    payduedate.Text = CDate(invoicedate.Text)
                Else
                    sSql = "SELECT gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND genoid=" & trnpaytype.SelectedValue
                    Dim DueDate As Integer = GetStrData(sSql)
                    payduedate.Text = Format(CDate(invoicedate.Text).AddDays(DueDate), "MM/dd/yyyy")
                End If
                'payduedate.Text = Format(CDate(Trim(xreader("payduedate").ToString)), "MM/dd/yyyy")
                paymentoid.Text = Trim(xreader("paymentoid").ToString)
                paymentdate.Text = Format(CDate(Trim(xreader("paymentdate").ToString)), "MM/dd/yyyy")
                paymentacctgoid.Text = Trim(xreader("paymentacctgoid").ToString)
                payrefno.Text = Trim(xreader("payrefno").ToString)
                amtbayar.Text = ToMaskEdit(ToDouble((xreader("accumpayment").ToString)), 2)
                'rate.Text = ToMaskEdit(ToDouble((xreader("rate").ToString)), 2)
                ReAmount()
                If Trim(xreader("trnjualstatus").ToString).ToUpper = "Post" Or Trim(xreader("trnjualstatus").ToString).ToUpper = "Post" Then
                    btnPost1.Visible = False : btnSave1.Visible = False
                    btnDelete1.Visible = False
                End If
                UpdUser.Text = Trim(xreader("upduser").ToString)
                UpdTime.Text = Trim(xreader("updtime").ToString)
                DDLacctgCode.SelectedValue = xreader("trnjualres1").ToString
                DDLType.SelectedValue = Trim(xreader("typejual").ToString)
                ' Other data
                acctgCode.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = " & acctgoid.Text)
                payacctg.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = '" & paymentacctgoid.Text & "'")
                suppcode.Text = cKon.ambilscalar("SELECT custname FROM QL_mstcust WHERE custoid = " & suppoid.Text)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.ToString, 1)
        Finally
            DDLOutlet.CssClass = "inpTextDisabled" : DDLOutlet.Enabled = False
        End Try
    End Sub

    Private Sub generateID()
        sSql = "SELECT ISNULL(MIN(payaroid),0) FROM QL_trnpayar WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND payaroid < 0"
        Session("vIDPayap") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(conaroid),0) FROM QL_conar WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND conaroid < 0"
        Session("vIDConap") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(cashbankoid),0) FROM QL_trncashbankmst WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND cashbankoid < 0"
        Session("vIDCashBank") = cKon.ambilscalar(sSql)
        paymentoid.Text = Session("vIDPayap") - 1
        conapoid.Text = Session("vIDConap") - 1
        cashbankoid.Text = Session("vIDCashBank") - 1
    End Sub

    Private Sub generateGLID()
        sSql = "SELECT ISNULL(MIN(glmstoid),0) FROM ql_trnglmst WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND glmstoid < 0"
        Session("vIDGLMst") = cKon.ambilscalar(sSql)
        sSql = "SELECT ISNULL(MIN(gldtloid),0) FROM ql_trngldtl WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND gldtloid < 0"
        Session("vIDGLDtl") = cKon.ambilscalar(sSql)
        trnglmstoid.Text = Session("vIDGLMst") - 1
        trngldtloid.Text = Session("vIDGLDtl") - 1
    End Sub

    Public Sub bindDataSupplier()

        sSql = "SELECT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND activeflag='ACTIVE' ORDER BY custcode, custname"
        FillGV(gvListCust, sSql, "QL_mstcust")

        'Dim dt As New DataTable
        'dt = cKon.ambiltabel("select distinct custoid,custcode, custname,0 coa_hutang, '' coa, '' fakturpajak, custnpwp npwp, custname namanpwp, custaddr alamatnpwp FROM QL_mstcust s INNER JOIN QL_mstacctg a ON a.cmpcode=s.cmpcode WHERE s.cmpcode = 'CORP' AND (custcode LIKE '%" & Tchar(suppcode.Text) & "%' OR custname LIKE '%" & Tchar(suppcode.Text) & "%') ORDER BY custname ", "QL_mstcust")
        'Session("TblCust") = dt
        'gvSupplier.DataSource = Session("TblCust")
        'gvSupplier.DataBind()
    End Sub

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptConarXls.rpt"))
            Else
                Report.Load(Server.MapPath(folderReport & "rptConarXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='CORP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            Report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(Report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ARInitBalanceReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "ARInitBalanceReport")
            End If
            Report.Close()
            Report.Dispose()
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            'showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
			Session("CompnyCode") = CompnyCode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnconar.aspx")
        End If
        If checkPagePermission("~\Accounting\trnconar.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & "- A/R Initial Balance"
        Session("oid") = Request.QueryString("oid")
     
        btnDelete1.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        btnPost1.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            initDDL()
            Dim CutOffDate As Date
			sSql = "SELECT ISNULL(genother1,'') FROM QL_mstgen WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND gengroup='CUTOFDATE'"
            If cKon.ambilscalar(sSql) = "" Then
                showMessage("- Please set CUT OFF DATE first", 2)
                lblPosting.Text = "IN PROCESS"
                CutOffDate = "1/1/1900"
            Else
                CutOffDate = cKon.ambilscalar(sSql)
            End If
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                btnDelete1.Visible = True
				fillTextBox(CompnyCode, Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                generateID()
                invoicedate.Text = Format(CutOffDate.AddDays(-1), "MM/dd/yyyy")
                invoicedate_TextChanged(sender, e)
                paymentdate.Text = Format(GetServerTime, "MM/dd/yyyy")
                btnDelete1.Visible = False
                UpdUser.Text = Session("UserID")
                UpdTime.Text = Now
                TabContainer1.ActiveTabIndex = 0
                lblPosting.Text = "IN PROCESS"
            End If
		End If
		If lblPosting.Text.ToUpper = "POST" Or lblPosting.Text.ToUpper = "CLOSED" Then
			btnPost1.Visible = False : btnSave1.Visible = False : btnDelete1.Visible = False
		Else
			btnPost1.Visible = True : btnSave1.Visible = True : btnDelete1.Visible = True
		End If
		generateGLID()
    End Sub

    Protected Sub btnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        bindDataSupplier()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        'bindDataSupplier(Session("CompnyCode"), "", "")
        'gvSupplier.Visible = True
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFilter.SelectedIndex = -1
        FilterConap.Text = ""
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvTrnConap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrnConap.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "MM/dd/yyyy")
			'e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "MM/dd/yyyy")
			e.Row.Cells(7).Text = ToMaskEdit(ToDouble((e.Row.Cells(7).Text)), 2)
			'If e.Row.Cells(7).Text = "POST" Then
			'	e.Row.Cells(8).Enabled = "false"
			'End If
        End If
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterConap.Text = ""
    End Sub

    Protected Sub trntaxpct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		If trntaxpct.Text = 0 Then
			trntaxpct.Text = 0
			amttax.Text = 0
		Else
			ReAmount()
		End If
    End Sub

    Protected Sub amtbeli_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit((ToDouble(e.Row.Cells(4).Text)), 2)
            e.Row.Cells(3).Text = ToMaskEdit((ToDouble(e.Row.Cells(3).Text)), 2)

        End If
    End Sub

    Protected Sub gvTrnConap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTrnConap.PageIndexChanging
        gvTrnConap.PageIndex = e.NewPageIndex
        gvTrnConap.DataSource = Session("TblTrn")
        gvTrnConap.DataBind()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub trnpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trnpaytype.SelectedItem.Text = "CASH" Then
            payduedate.Text = CDate(invoicedate.Text)
        Else
            sSql = "SELECT gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND genoid=" & trnpaytype.SelectedValue
            Dim DueDate As Integer = GetStrData(sSql)
            payduedate.Text = Format(CDate(invoicedate.Text).AddDays(DueDate), "MM/dd/yyyy")

        End If
    End Sub

    Protected Sub invoicedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles invoicedate.TextChanged
        Try
            sSql = "select genother1 from ql_mstgen where genoid=" & trnpaytype.SelectedValue & " AND genggroup='CUTOFDATE'"
            payduedate.Text = Format(CDate(invoicedate.Text).AddDays(ToDouble(GetStrData(sSql))), "MM/dd/yyyy")

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ImbEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid.Text = ""
        suppcode.Text = ""
        LblFakturPajak.Text = ""
        'npwp.Text = ""
        namanpwp.Text = ""
        alamatnpwp.Text = ""
    End Sub

    Protected Sub imbPayAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sVar() As String = {"VAR_CASH", "VAR_BANK"}
        FillGVAcctg(gvAcctg, sVar, Session("CompnyCode"), " AND (acctgcode LIKE '%" & TChar(payacctg.Text) & "%' OR acctgdesc LIKE '%" & TChar(payacctg.Text) & "%')")
        gvAcctg.Visible = True
    End Sub

    Protected Sub gvAcctg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        paymentacctgoid.Text = gvAcctg.SelectedDataKey.Item("acctgoid").ToString
        payacctg.Text = gvAcctg.SelectedDataKey.Item("acctgdesc").ToString
        gvAcctg.Visible = False
    End Sub

    Protected Sub ImbErasePayAcc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        paymentacctgoid.Text = ""
        payacctg.Text = ""
        gvAcctg.Visible = False
    End Sub

    Protected Sub gvAcctg_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        paymentacctgoid.Text = gvAcctg.SelectedDataKey.Item("acctgoid").ToString
        payacctg.Text = gvAcctg.SelectedDataKey.Item("acctgcode").ToString + " - " + gvAcctg.SelectedDataKey.Item("acctgdesc").ToString
        gvAcctg.Visible = False
    End Sub

    Protected Sub gvTrnConap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTrnConap.SelectedIndexChanged
        Response.Redirect("trnconar.aspx?cmpcode=" & gvTrnConap.SelectedDataKey.Item("cmpcode").ToString & "&oid=" & gvTrnConap.SelectedDataKey.Item("trnjualmstoid").ToString & "")
    End Sub

#End Region

    Protected Sub imgFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub imgViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cbStatus.Checked = False
        FilterDDLStatus.SelectedItem.Text = "ALL"
        ddlFilter.SelectedIndex = -1
        FilterConap.Text = ""
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub btnPost1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPosting.Text = "POST"
        btnSave1_Click(sender, e)
    End Sub

    Protected Sub btnDelete1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans
        Try
			'ql_trnbelimst
            sSql = "DELETE QL_trnjualmst WHERE cmpcode='" & DDLOutlet.SelectedValue & "' and trnjualmstoid=" & trnbelimstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit() : xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback() : xCmd.Connection.Close()
            showMessage(ex.Message, 1) : Exit Sub
        End Try
        Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub btnCancel1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sValue As String = validatingData()
        If sValue <> "" Then
            lblPosting.Text = "IN PROCESS"
            ShowMessage(sValue, 2)
            Exit Sub
        End If
        generateID()
        generateGLID()
        If Session("oid") = Nothing And Session("oid") = "" Then
            sSql = "SELECT ISNULL(MIN(trnjualmstoid),0) FROM QL_trnjualmst WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND trnjualmstoid < 0"
            Session("vIDTrnBeli") = cKon.ambilscalar(sSql)
            trnbelimstoid.Text = Session("vIDTrnBeli") - 1

            sSql = "SELECT COUNT(*) FROM QL_trnjualmst WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND trnjualno='" & Tchar(trnbelino.Text) & "'"
            If CheckDataExists(sSql) Then
                ShowMessage("- Invoice Number has been used before", 2)
                lblPosting.Text = "IN PROCESS"
                Exit Sub
            End If
        End If

		sSql = "SELECT curroid FROM QL_mstcurr WHERE currcode='IDR'"
		Dim Curroid As Integer = cKon.ambilscalar(sSql)
		Dim CutDateOff As Date
		CutDateOff = CDate(payduedate.Text)
		cRate.SetRateValue(Curroid, Format(CutDateOff, "MM/dd/yyyy"))
        Dim RateUSD As Double = cRate.GetRateMonthlyUSDValue
        Dim RateIDR As Double = cRate.GetRateMonthlyIDRValue
        Dim Rate1Oid As Double = cRate.GetRateDailyOid
        Dim Rate2Oid As Double = cRate.GetRateMonthlyOid

        Dim objTrans As SqlClient.SqlTransaction
		If conn.State = ConnectionState.Closed Then
			conn.Open()
		End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans

        Try
            If Session("oid") = Nothing And Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnjualmst (cmpcode,trnjualmstoid,periodacctg,trnjualno,trnjualdate,trnjualtype,custoid,domstoid,trnpaytype,jualratetoidr,jualratetousd,trnjualamt,trnjualamtidr,trnjualamtusd,trntaxtype,trntaxpct,trntaxamt,trnjualamtnetto,trnjualamtnettoidr,trnjualamtnettousd,trnjualnote, salescode,trnjualstatus,trnjualres1,crtuser,crttime,upduser,updtime,curroid) " & _
             " VALUES " & _
             " ('" & DDLOutlet.SelectedValue & "'," & trnbelimstoid.Text & ",'" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "','" & Tchar(trnbelino.Text.Trim) & "','" & CDate(invoicedate.Text) & "','" & DDLType.SelectedValue & "', " & suppoid.Text & ",0," & trnpaytype.SelectedValue & ",'" & ToDouble(cRate.GetRateDailyIDRValue) & "'," & ToDouble(cRate.GetRateDailyUSDValue) & ",'" & ToDouble(dpp.Text) & "','" & ToDouble(dpp.Text) * cRate.GetRateMonthlyIDRValue & "','" & ToDouble(dpp.Text) * cRate.GetRateMonthlyUSDValue & "'," & IIf(ToDouble(trntaxpct.Text) = 0, "'NT'", "'EXC'") & ",'" & ToDouble(trntaxpct.Text) & "','" & ToDouble(amttax.Text) & "'," & ToDouble(amtbeli.Text) & ",'" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "','" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "','" & Tchar(trnapnote.Text.Trim) & "','" & Tchar(salescode.SelectedValue) & "','" & lblPosting.Text & "','" & DDLacctgCode.SelectedValue & "','" & Session("UserId") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP," & Curroid & ") "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE QL_trnjualmst SET curroid=" & Curroid & ", periodacctg='" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "', trnjualno='" & Tchar(trnbelino.Text.Trim) & "', trnjualdate='" & CDate(invoicedate.Text) & "', custoid=" & suppoid.Text & ", trnjualamt=" & ToDouble(dpp.Text) & ",trnjualamtidr='" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "', trnjualamtusd='" & ToDouble(dpp.Text) * cRate.GetRateMonthlyUSDValue & "',trntaxpct=" & ToDouble(trntaxpct.Text) & ", trntaxamt=" & ToDouble(amttax.Text) & ", trnjualamtnettoidr='" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "',trnjualamtnettousd='" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "',  trnjualamtnetto=" & ToDouble(amtbeli.Text) & ", trnjualnote='" & Tchar(trnapnote.Text.Trim) & "', trnjualstatus='" & lblPosting.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, trntaxtype=" & IIf(ToDouble(trntaxpct.Text) = 0, "'NT'", "'EXC'") & ", trnpaytype=" & trnpaytype.SelectedValue & ",trnjualres1='" & DDLacctgCode.SelectedValue & "', trnjualtype='" & DDLType.SelectedValue & "', salescode='" & salescode.SelectedValue & "' WHERE cmpcode='" & DDLOutlet.SelectedValue & "' AND trnjualmstoid=" & trnbelimstoid.Text

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

			' ql_conap
			If lblPosting.Text = "POST" Then
				sSql = " INSERT INTO QL_conar(cmpcode,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payduedate,amttrans,amtbayar,trnarnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother,paybankoid)" & _
				  " VALUES " & _
				  "('" & DDLOutlet.SelectedValue & "'," & conapoid.Text & ",'QL_trnjualmst', " & trnbelimstoid.Text & ",0," & suppoid.Text & "," & DDLacctgCode.SelectedValue & ",'" & lblPosting.Text & "','AR BALANCE','" & CDate(invoicedate.Text) & "','" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "',0,'01/01/1900','" & CDate(payduedate.Text) & "'," & ToDouble(amtbeli.Text) & ",0,'" & Tchar(trnapnote.Text.Trim) & "','" & Session("UserId") & "',CURRENT_TIMESTAMP,'" & Session("UserId") & "',CURRENT_TIMESTAMP,'" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "',0,'" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "',0,0,0)"
				xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                ' Update Credit limit Customer
                sSql = "SELECT custgroupoid from QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid=" & suppoid.Text & ""
                Dim group As Integer = cKon.ambilscalar(sSql)
                sSql = "SELECT currcode from QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & Curroid & ""
                Dim curr As String = GetStrData(sSql)
                If curr = "IDR" Then
                    sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah + (" & ToDouble(amtbeli.Text) * cRate.GetRateDailyIDRValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusageusd = custcreditlimitusageusd + (" & ToDouble(amtbeli.Text) * cRate.GetRateDailyUSDValue & ") WHERE cmpcode='" & CompnyCode & "' AND custgroupoid=" & group & ""
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
			End If
			objTrans.Commit()
			xCmd.Connection.Close()
		Catch ex As Exception
			lblPosting.Text = "IN PROCESS"
			objTrans.Rollback() : conn.Close()
			showMessage(ex.ToString, 1)
			Exit Sub
		End Try
        If multiplepost.Text = "false" Then
            Response.Redirect("trnconar.aspx?awal=true")
        End If
    End Sub

    Protected Sub ibRePost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibRePost.Click
        statusRePost.Text = "REPOST"
        btnSave1_Click(sender, e)
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Printreport("Excel")
    End Sub

    Protected Sub ibselectall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblTrn") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrn")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrn")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    If dtTbl.Rows(C1)("trnjualstatus") = "POST" Then
                        objView(0)("CheckValue") = "False"
                        dtTbl.Rows(C1)("CheckValue") = "False"
                    Else
                        objView(0)("CheckValue") = "True"
                        dtTbl.Rows(C1)("CheckValue") = "True"
                    End If
                    objView.RowFilter = ""
                Next
                'objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                'Session("TblMat") = objTbl
                Session("TblTrn") = dtTbl
                gvTrnConap.DataSource = Session("TblTrn")
                gvTrnConap.DataBind()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        multiplepost.Text = "true"
        UpdateCheckedValue()
        If Session("TblTrn") IsNot Nothing Then
            Dim dv As DataView = Session("TblTrn").DefaultView
            dv.RowFilter = "checkvalue='True'"
            For C1 As Integer = 0 To dv.Count - 1
                Session("oid") = dv(C1)("trnjualmstoid").ToString
				fillTextBox(CompnyCode, Session("oid"))
                lblPosting.Text = "POST"
                btnSave1_Click(sender, e)
            Next
            dv.RowFilter = ""
        End If
        Response.Redirect("trnconar.aspx?awal=true")
    End Sub

    Protected Sub ibuncheckall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblTrn") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrn")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrn")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnjualmstoid=" & dtTbl.Rows(C1)("trnjualmstoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                'objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                'Session("TblMat") = objTbl
                Session("TblTrn") = dtTbl
                gvTrnConap.DataSource = Session("TblTrn")
                gvTrnConap.DataBind()
            Else
                Session("WarningListMat") = "No material data can't be selected!"
                showMessage(Session("WarningListMat"), 2)
            End If
        Else
            Session("WarningListMat") = "No material data can't be selected!"
            showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnconap.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        bindDataSupplier()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        bindDataSupplier()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        bindDataSupplier()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        'If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
        '    btnClearCust_Click(Nothing, Nothing)
        'End If
        suppoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        suppcode.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)

        'suppoid.Text = gvSupplier.SelectedDataKey(0).ToString()
        'suppcode.Text = gvSupplier.SelectedDataKey(2).ToString()
        'acctgoid.Text = gvSupplier.SelectedDataKey("coa_hutang").ToString().Trim
        'acctgCode.Text = gvSupplier.SelectedDataKey("coa").ToString().Trim
        'sSql = "SELECT acctgoid,acctgcode + '-' + acctgdesc as desk FROM QL_mstacctg WHERE acctgoid='" & acctgoid.Text & "'"
        ''Dim otable As DataTable = CreateDataTableFromSQL(sSql)
        'Dim otable As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg1")
        'If otable.Rows.Count > 0 Then
        '    acctgoid.Text = otable.Rows(0).Item("acctgoid")
        '    acctgCode.Text = otable.Rows(0).Item("desk")
        'End If
        'LblFakturPajak.Text = gvSupplier.SelectedDataKey.Item("fakturpajak").ToString
        'If LblFakturPajak.Text = "Y" Then
        '    npwp.Text = gvSupplier.SelectedDataKey.Item("npwp").ToString
        '    namanpwp.Text = gvSupplier.SelectedDataKey.Item("namanpwp").ToString
        '    alamatnpwp.Text = gvSupplier.SelectedDataKey.Item("alamatnpwp").ToString
        'End If
    End Sub

    Protected Sub gvAcctg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

    End Sub
End Class
