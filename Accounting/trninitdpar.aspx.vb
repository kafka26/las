'Created By Vriska On 4 October 2014
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trninitdpar
    Inherits System.Web.UI.Page
#Region "Variables"
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim sSql As String = ""
	Dim cKon As New Koneksi
	Dim cRate As New ClassRate
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"

    Public Function GetIDCB() As String
        Return Eval("cmpcode") & "," & Eval("dparoid")
    End Function

    Public Function getCheckBoxValue(ByVal nRowNum As Integer, ByVal nCol As Integer) As Boolean
        Dim sReturn As Boolean
        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(nRowNum)
        If (row.RowType = DataControlRowType.DataRow) Then
            Dim cc As System.Web.UI.ControlCollection = row.Cells(nCol).Controls
            For Each myControl As System.Web.UI.Control In cc
                If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                    sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                    Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                    If cbcheck Then
                        sReturn = True
                    ElseIf Not cbcheck Then
                        sReturn = False
                    End If
                End If
            Next
        End If
        getCheckBoxValue = sReturn
    End Function

#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitAllDDL()
        'FillDDL(DDLBusUnit, sSql)
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(outlet, sSql)
        FillDDLAcctg(trndparacctgoid, "VAR_DP_AR", outlet.SelectedValue)
        If trndparacctgoid.Items.Count < 1 Then
            showMessage("Isi/Buat account VAR_DP_AR di master accounting!!", 2)
        End If
    End Sub

    Private Sub BindSupplier(ByVal sPlus As String)
        sSql = "SELECT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND activeflag='ACTIVE' ORDER BY custcode, custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
        'sSql = "SELECT c.* FROM QL_mstcust c " & _
        '    "WHERE c.cmpcode='CORP' " & sPlus & " ORDER BY c.custcode"
        'FillGV(gvCust, sSql, "QL_mstcreditcard")
    End Sub

    Private Sub BindData(ByVal sPLus As String)
        Try
            Dim tgle As Date = CDate(dateAwal.Text)
            tgle = CDate(dateAkhir.Text)
        Catch ex As Exception
            'QLMsgBox1.ShowMessage("Invalid format date !!", 2, "")
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(dateAwal.Text) > CDate(dateAkhir.Text) Then
            'QLMsgBox1.ShowMessage("Second/End Period must be greater than First Period !!", 2, "")
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If

        'sSql = "SELECT ar.dparoid,ar.dparno,ar.dpardate,ar.dparamt, " & _
        '       "ar.dparstatus,ar.dparnote, 0 dparflag,dparpaytype giroNo,ar.cmpcode,ar.cmpcode as outlet,custname,dparpaytype, 0 AS selected " & _
        '       "FROM QL_trndpar ar " & _
        '       "INNER JOIN QL_mstcust s ON ar.custoid=s.custoid " & _
        '       "INNER JOIN QL_mstdivision o ON ar.cmpcode=o.cmpcode " & _
        '       "WHERE dparoid<=0 AND dparstatus<>'DELETE' " & sPLus & " " & _
        '       "and ar.dpardate between '" & toDate(dateAwal.Text) & "' and '" & toDate(dateAkhir.Text) & "' " & _
        '       "" & IIf(statuse.SelectedValue = "ALL", "", " and ar.dparstatus='" & statuse.SelectedValue & "'") & " "
        'If Session("CompnyCode") <> CompnyCode Then
        '    sSql &= " AND ar.cmpcode LIKE '" & Session("CompnyCode") & "%'"
        'Else
        '    sSql &= " AND ar.cmpcode LIKE '%'"
        'End If
        'sSql += "ORDER BY trndparno"

		sSql = "SELECT ar.dparoid,ar.dparno,ar.dpardate,ar.dparamt, ar.dparstatus,ar.dparnote,0 dparflag,dparpaytype giroNo,ar.cmpcode,ar.cmpcode as outlet,custname,dparpaytype, 0 AS selected FROM QL_trndpar ar INNER JOIN QL_mstcust s ON ar.custoid=s.custoid INNER JOIN QL_mstdivision o ON ar.cmpcode=o.cmpcode WHERE dparoid<=0 AND dparstatus<>'DELETE'" & sPLus & " " & _
			"and ar.dpardate between '" & CDate(dateAwal.Text) & "' and '" & CDate(dateAkhir.Text) & "' " & _
			"" & IIf(statuse.SelectedValue = "ALL", "", " and ar.dparstatus='" & statuse.SelectedValue & "'") & " "
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND ar.cmpcode LIKE '" & Session("CompnyCode") & "%'"
        Else
            sSql &= " AND ar.cmpcode LIKE '%'"
        End If
        sSql += " ORDER BY dparno"

        Dim objTable As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
        Session("TblDPARInit") = objTable
        gvMst.DataSource = Session("TblDPARInit")
        gvMst.DataBind()

        UnabledCheckBox()
    End Sub

    Private Sub DDLPayreftypeChange()
        If payreftype.SelectedValue = "CASH" Then
            tr1.Visible = False
            tr2.Visible = False
        Else
            tr1.Visible = True
            tr2.Visible = True
			payduedate.Text = Format(GetServerTime, "MM/dd/yyyy")
        End If
    End Sub

    Private Sub FillTextBox(ByVal iOid As Integer, ByVal vOutlet As String)
        'sSql = "SELECT ar.dpapoid,ar.cashbankoid,ar.dpapno,ar.dpapdate,ar.suppoid,suppname, " & _
        '       "ar.dpappaytype,0,ar.dpapduedate,ar.dpappayrefno,ar.dpappayacctgoid," & _
        '       "ar.dpapamt,ar.dpapnote,ar.dpapstatus,ar.upduser, " & _
        '       "ar.updtime,ar.createuser,ar.createtime,ar.cmpcode " & _
        '       "FROM QL_trndpap ar INNER JOIN ql_mstsupp s ON ar.suppoid=s.suppoid " & _
        '       "WHERE ar.cmpcode='" & vOutlet & "' AND ar.dpapoid=" & iOid
        sSql = "SELECT ar.dparoid,ar.cashbankoid,ar.dparno,ar.dpardate,ar.custoid,custname, " & _
               "ar.dparpaytype,0,ar.dparduedate,ar.dparpayrefno,ar.dparpayacctgoid," & _
               "ar.dparamt,ar.dparnote,ar.dparstatus,ar.upduser, " & _
               "ar.updtime,ar.createuser,ar.createtime,ar.cmpcode, ar.acctgoid " & _
               "FROM QL_trndpar ar INNER JOIN ql_mstcust s ON ar.custoid=s.custoid " & _
               "WHERE ar.cmpcode='" & vOutlet & "' AND ar.dparoid=" & iOid
        Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
        If dtMst.Rows.Count < 1 Then
            'QLMsgBox1.ShowMessage("can't load data DP AR Balance !!<BR>Info :<BR>" & sSql, 2, "")
            showMessage("Can't load data DP AR Balance !!", 2)
            Exit Sub
        Else
            trndparoid.Text = dtMst.Rows(0)("dparoid").ToString
            outlet.SelectedValue = dtMst.Rows(0)("cmpcode").ToString
            trndparno.Text = dtMst.Rows(0)("dparno").ToString
            trndpardate.Text = Format(CDate(dtMst.Rows(0)("dpardate").ToString), "MM/dd/yyyy")
            custname.Text = dtMst.Rows(0)("custname").ToString
            custoid.Text = dtMst.Rows(0)("custoid").ToString
            payreftype.Text = dtMst.Rows(0)("dparpaytype").ToString
            payduedate.Text = Format(CDate(dtMst.Rows(0)("dparduedate").ToString), "MM/dd/yyyy")
            payrefno.Text = dtMst.Rows(0)("dparpayrefno").ToString
            trndparacctgoid.SelectedValue = dtMst.Rows(0)("acctgoid").ToString
            trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("dparamt").ToString), 2)
            trndparamt.Text = ToMaskEdit(ToDouble(trndparamt.Text), 2)
            trndparnote.Text = dtMst.Rows(0)("dparnote").ToString
            trndparstatus.Text = dtMst.Rows(0)("dparstatus").ToString

            If payreftype.SelectedValue = "CASH" Then
                tr1.Visible = False
                tr2.Visible = False
            Else
                tr1.Visible = True
                tr2.Visible = True
            End If

            create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
            update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "

            If trndparstatus.Text <> "POST" Then
                btnSave1.Visible = True : btnDelete1.Visible = True : btnposting1.Visible = True
            Else
                btnSave1.Visible = False : btnDelete1.Visible = False : btnposting1.Visible = False
            End If
        End If
        outlet.Enabled = False
        outlet.CssClass = "inpTextDisabled"

        If trndparstatus.Text <> "In Process" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPosting2.Visible = False
            trndpardate.CssClass = "inpTextDisabled"
            trndpardate.ReadOnly = True
            imbDPARDate.Visible = False
            trndparno.Enabled = False
            outlet.Enabled = False
            custname.Enabled = False
            imbSearchCust.Visible = False
            imbClearCust.Visible = False
            trndparacctgoid.Enabled = False
            payreftype.Enabled = False
            payduedate.CssClass = "inpTextDisabled"
            payduedate.ReadOnly = True
            imbDueDate.Visible = False
            payrefno.Enabled = False
            trndparamt.Enabled = False
            trndparnote.Enabled = False
        End If

    End Sub

    Public Sub UnabledCheckBox()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Dim irowne As Int16 = 0
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 + (gvMst.PageIndex * 10) To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus".ToString)) = "POST" Or Trim(objRow(C1)("trndparstatus".ToString) = "DELETE") Then
                        Try
                            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(irowne)
                            If (row.RowType = DataControlRowType.DataRow) Then
                                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                                For Each myControl As System.Web.UI.Control In cc
                                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                        CType(myControl, System.Web.UI.WebControls.CheckBox).Enabled = False
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    irowne += 1
                Next
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub CheckAll()

        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus").ToString) <> "POST" And Trim(objRow(C1)("trndparstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub UncheckAll()
        Try
            Dim objTable As DataTable
            Dim objRow() As DataRow
            objTable = Session("TblDPARInit")
            objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            If objRow.Length() > 0 Then
                For C1 As Integer = 0 To objRow.Length() - 1
                    If Trim(objRow(C1)("trndparstatus").ToString) <> "POST" And Trim(objRow(C1)("trndparstatus").ToString) <> "DELETE" Then
                        Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
                        If (row.RowType = DataControlRowType.DataRow) Then
                            Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                            For Each myControl As System.Web.UI.Control In cc
                                If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                    CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = False
                                End If
                            Next
                        End If
                    End If
                Next
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub UpdateCheckedPost()
        'Untuk mendapat cek box value multi select
        Dim dv As DataView = Session("TblDPARInit").DefaultView
        dv.AllowEdit = True
        For C1 As Integer = 0 To gvMst.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvMst.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(1).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbcheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        Dim sOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        Dim a As String() = sOid.Split(",")
                        dv.RowFilter = "trndparoid=" & a(1) & " AND cmpcode='" & a(0) & "'"
                        If cbcheck = True Then
                            dv(0)("selected") = 1
                        Else
                            dv(0)("selected") = 0
                        End If
                        dv.RowFilter = ""
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptInitDPAR.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptInitDPARXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='CORP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            Report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(Report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPARInitBalanceReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DPARInitBalanceReport")
            End If
            Report.Close()
            Report.Dispose()
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            'showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then
			Response.Redirect("~/Other/login.aspx")
		End If
		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("~\Accounting\trninitDPAR.aspx")
		End If
		If checkPagePermission("~\Accounting\trninitDPAR.aspx", Session("Role")) = False Then
			Response.Redirect("~\Other\NotAuthorize.aspx")
		End If
		Session("oid") = Request.QueryString("oid")
        Page.Title = CompnyName & " - DP AR Inittial Balance"
        Session("outlet") = Request.QueryString("cmpcode")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan menghapus data ini ?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")
        btnPosting2.Attributes.Add("OnClick", "javascript:return confirm('Apakah anda yakin akan mem-posting data ini ?');")

        If Not Page.IsPostBack Then
			dateAwal.Text = Format(GetServerTime, "MM/dd/yyyy")
			dateAkhir.Text = Format(GetServerTime, "MM/dd/yyyy")
            BindData("")
            InitAllDDL()
            DDLPayreftypeChange()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), Session("outlet"))
                TabContainer1.ActiveTabIndex = 1
            Else
				trndpardate.Text = Format(GetServerTime, "MM/dd/yyyy")
				payduedate.Text = Format(GetServerTime, "MM/dd/yyyy")
                TabContainer1.ActiveTabIndex = 0
                create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
                update.Text = ""
                trndparstatus.Text = "In Process"
                btnDelete1.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If

        End If
    End Sub

    Protected Sub imbSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindSupplier("")
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
        'BindSupplier(" and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')")
        'gvCust.Visible = True
    End Sub

    Protected Sub imbClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        custoid.Text = ""
        custname.Text = ""
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim sMsg As String = ""

        If trndparno.Text = "" Then
            sMsg &= "-  DP No. harus diisi !!<BR>"
        End If

        If custoid.Text = "" Then
            sMsg &= "- Customer harus diisi !!<BR>"
        End If

        If Not IsDate(CDate(trndpardate.Text)) Then
            sMsg &= "- Tanggal harus diisi !!<BR>"
        End If
        If IsDate(CDate(trndpardate.Text)) = False Then
            sMsg &= "- Tanggal salah !!<BR>"
        End If

        If payduedate.Visible Then
            If Not IsDate(CDate(payduedate.Text)) Then
                sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
            End If
            If IsDate(CDate(payduedate.Text)) = False Then
                sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
            End If
        End If
        If payduedate.Visible And (IsDate(CDate(payduedate.Text))) And (IsDate(CDate(trndpardate.Text))) Then
            If CDate(payduedate.Text) < CDate(trndpardate.Text) Then
                sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
            End If
        End If

        If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
            sMsg &= "-  Ref No. harus diisi !!<BR>"
        End If

        If ToDouble(trndparamt.Text) <= 0 Then
            sMsg &= "- Total DP harus >  0 !!<BR>"
        End If
        If trndparnote.Text.Trim.Length > 200 Then
            sMsg &= "- Note, maksimal 200 karakter !!<BR>"
        End If

        If CheckDataExists("SELECT COUNT(*) FROM QL_trndpar WHERE cmpcode='" & outlet.SelectedValue & "' AND dparno='" & Tchar(trndparno.Text) & "' AND dparoid<>'" & Session("oid") & "'") = True Then
            sMsg &= "-  DP No Outlet sudah ada !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
			trndparstatus.Text = "In Process"
			Exit Sub
        End If

        Session("DPARInit") = GetStrData("SELECT isnull(MIN(dparoid)-1,-1) FROM QL_trndpar WHERE cmpcode='" & outlet.SelectedValue & "'")

        sSql = "select count(-1) from QL_trndpar where dparno='" & trndparno.Text & "' and dparstatus='POST' and  cmpcode='" & outlet.SelectedValue & "'"
        If GetStrData(sSql) > 0 Then
            showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
            trndparstatus.Text = "In Process" : Exit Sub
        End If


        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If payreftype.SelectedValue = "CASH" Then
                payduedate.Text = "01/01/1900"
            End If

            If Session("oid") = Nothing Or Session("oid") = "" Then
				Dim lastrate As Integer
                lastrate = GetStrData("select top 1 rateoid from ql_mstrate where curroid=1 order by rateoid desc")
                '"dpappayacctgoid,dpappaytype,cashbankacctgoid,payduedate,payrefno," & _
                '"dpapamt,taxtype,taxoid,taxpct,taxamt,dpapnote,dpapflag,dpapacumamt,dpapstatus, " & _
                sSql = "INSERT INTO QL_trndpar(cmpcode,dparoid, periodacctg,dparno,dpardate,custoid,cashbankoid,dparpayacctgoid,dparpaytype,dparduedate,dparpayrefno,dparamt,dparnote,dparaccumamt,dparstatus,createuser,createtime,upduser,updtime,acctgoid, curroid, rateoid,rate2oid,giroacctgoid) VALUES ('" & outlet.SelectedValue & "'," & Session("DPARInit") & ", '" & GetDateToPeriodAcctg(CDate(trndpardate.Text)) & "','" & Tchar(trndparno.Text) & "', '" & CDate(trndpardate.Text) & "','" & custoid.Text & "',0, " & trndparacctgoid.SelectedValue & ",'" & payreftype.SelectedValue & "','" & CDate(payduedate.Text) & "','" & Tchar(payrefno.Text) & "'," & ToDouble(trndparamt.Text) & ", '" & Tchar(trndparnote.Text) & "',0,'" & trndparstatus.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "', CURRENT_TIMESTAMP," & trndparacctgoid.SelectedValue & ",1, " & lastrate & ",60,0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            Else
                sSql = "UPDATE QL_trndpar SET dparno='" & Tchar(trndparno.Text) & "', periodacctg='" & GetDateToPeriodAcctg(CDate(trndpardate.Text)) & "', dpardate='" & CDate(trndpardate.Text) & "', " & _
                        "dparpayacctgoid=" & trndparacctgoid.SelectedValue & ",dparamt=" & ToDouble(trndparamt.Text) & ", " & _
                        "dparpaytype='" & payreftype.SelectedValue & "',dparduedate='" & CDate(payduedate.Text) & "', " & _
                        "dparpayrefno='" & Tchar(payrefno.Text) & "', " & _
                        "dparnote='" & Tchar(trndparnote.Text) & "',custoid='" & custoid.Text & "'," & _
                        "dparaccumamt=0,dparstatus='" & trndparstatus.Text & "',upduser='" & Session("UserID") & "', " & _
                        "updtime=CURRENT_TIMESTAMP, acctgoid=" & trndparacctgoid.SelectedValue & " " & _
                        "WHERE cmpcode='" & outlet.SelectedValue & "' AND dparoid=" & trndparoid.Text
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit() : conn.Close()
        Catch ex As Exception
            objTrans.Rollback() : conn.Close() : trndparstatus.Text = "In Process"
            showMessage(ex.ToString, 2)
			Exit Sub
        End Try

        If Session("TblDPARInitdtl") Is Nothing Then
			If trndparstatus.Text = "POST" Then
				Session("SavedInfo") &= "Data have been posted with DP AR No. = " & trndparno.Text & " !"
			End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("trninitdpar.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trndpar SET trndparstatus='DELETE',syncflag='' WHERE cmpcode='" & outlet.SelectedValue & "' AND trndparoid=" & trndparoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            'QLMsgBox1.ShowMessage(ex.Message, 2, "")
            showMessage(ex.Message, 2)
            Exit Sub
        End Try
        Response.Redirect("trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckAll.Click
        CheckAll()
    End Sub

    Protected Sub btnUncheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUncheckAll.Click
        UncheckAll()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosting.Click
        Dim sErrorku As String = ""
        If Not Session("TblDPARInit") Is Nothing Then
            Dim dt As DataTable = Session("TblDPARInit")
            If dt.Rows.Count > 0 Then
                UpdateCheckedPost()
                Dim dv As DataView = dt.DefaultView

                dv.RowFilter = "selected=1"
                If dv.Count = 0 Then
                    sErrorku &= "Please select DP AR first!"
                End If
                dv.RowFilter = ""
                dv.RowFilter = "selected=1 AND trndparstatus='POST'"
                If dv.Count > 0 Then
                    sErrorku &= "DP AR have been POSTED before!"
                End If
                dv.RowFilter = ""

                If sErrorku <> "" Then
                    'QLMsgBox1.ShowMessage(sErrorku, 2, "")
                    showMessage(sErrorku, 2)
                    dv.RowFilter = ""
                    Exit Sub
                End If

                Dim parsingOID As Integer = 0
                Dim sCmpcode As String = ""
                Dim dtAwal As DataTable = Session("TblDPARInit")
                Dim dvAwal As DataView = dtAwal.DefaultView
                Dim iSeq As Integer = dvAwal.Count + 1
                dvAwal.AllowEdit = True
                dvAwal.AllowNew = True
                dv.RowFilter = "selected=1 AND trndparstatus='In Process'"
                'poststatx.Text = "SPECIAL"

                Dim seq As Integer = 0
                For C1 As Integer = 0 To dv.Count - 1
                    parsingOID = dv(C1)("trndparoid").ToString
                    sCmpcode = dv(C1)("cmpcode").ToString
                    filltextboxSELECTED(sCmpcode, parsingOID)
                    If Not (Session("errmsg") Is Nothing Or Session("errmsg") = "") Then
                        sErrorku &= "Can't POST DP AP No " & dv(C1)("trndparno").ToString & " with reason:<BR>" & Session("errmsg")
                    End If
                    seq = C1
                Next

                If seq = dv.Count - 1 Then
                    showMessage("Data Telah diPosting <BR>", 2)
                    'QLMsgBox1.ShowMessage("Data Telah di Posting <BR> ", 3, "REDIR")
                    'Session("QLMsgBox") = QLMsgBox1
                    Response.Redirect("trninitdpar.aspx?awal=true")
                End If

            End If
        End If
    End Sub

    Private Sub filltextboxSELECTED(ByVal sCmpcode As String, ByVal selectedoid As Integer)
        If Not selectedoid = Nothing Or selectedoid <> 0 Then
            Dim sMsg As String = ""
            Dim vpayid As Integer = selectedoid
            Session("oid") = vpayid.ToString
            Try
                sSql = "SELECT ar.trndparoid,ar.cashbankoid,ar.trndparno,ar.trndpardate,ar.custoid,custname, " & _
                "ar.payreftype,ar.cashbankacctgoid,ar.payduedate,ar.payrefno,ar.trndparacctgoid," & _
                "ar.trndparamt,ar.trndparnote,ar.trndparstatus,ar.upduser, " & _
                "ar.updtime,ar.createuser,ar.createtime,ar.cmpcode " & _
                "FROM QL_trndpar ar INNER JOIN ql_mstcust s ON ar.custoid=s.custoid " & _
                "WHERE ar.cmpcode='" & sCmpcode & "' AND ar.trndparoid=" & selectedoid
                Dim dtMst As DataTable = cKon.ambiltabel(sSql, "QL_trndpar")
                If dtMst.Rows.Count < 1 Then
                    showMessage("Can't load data DP AR Balance !!", 2)
                    'QLMsgBox1.ShowMessage("can't load data DP AR Balance !!<BR>Info :<BR>" & sSql, 2, "")
                    Exit Sub
                Else
                    trndparoid.Text = dtMst.Rows(0)("trndparoid").ToString
                    outlet.SelectedValue = dtMst.Rows(0)("cmpcode").ToString
                    trndparno.Text = dtMst.Rows(0)("trndparno").ToString
					trndpardate.Text = Format(CDate(dtMst.Rows(0)("trndpardate").ToString), "MM/dd/yyyy")
                    custname.Text = dtMst.Rows(0)("custname").ToString
                    custoid.Text = dtMst.Rows(0)("custoid").ToString
                    payreftype.Text = dtMst.Rows(0)("payreftype").ToString
					payduedate.Text = Format(CDate(dtMst.Rows(0)("payduedate").ToString), "MM/dd/yyyy")
                    payrefno.Text = dtMst.Rows(0)("payrefno").ToString
                    DDLPayreftypeChange()
                    trndparacctgoid.SelectedValue = dtMst.Rows(0)("trndparacctgoid").ToString
                    trndparamt.Text = ToMaskEdit(ToDouble(dtMst.Rows(0)("trndparamt").ToString), 2)
                    trndparnote.Text = dtMst.Rows(0)("trndparnote").ToString
                    trndparstatus.Text = dtMst.Rows(0)("trndparstatus").ToString
                    create.Text = "Created By <B>" & dtMst.Rows(0)("createuser").ToString & "</B> On <B>" & dtMst.Rows(0)("createtime").ToString & "</B> "
                    update.Text = "; Last Updated By <B>" & dtMst.Rows(0)("upduser").ToString & "</B> On <B>" & dtMst.Rows(0)("updtime").ToString & "</B> "
                    If trndparstatus.Text <> "POST" Then
                        btnSave1.Visible = True : btnDelete1.Visible = True : btnposting1.Visible = True
                    Else
                        btnSave1.Visible = False : btnDelete1.Visible = False : btnposting1.Visible = False
                    End If
                End If
                outlet.Enabled = False
                outlet.CssClass = "inpTextDisabled"

                Session("TblDPARInitdtl") = dtMst

            Catch ex As Exception
                xreader.Close() : conn.Close()
                sMsg &= "Gagal menampilkan data DP AR Balance.<BR>Info:" & ex.Message & "<BR>" & sSql & "<BR><BR>"
            End Try

        Else
            showMessage("DP AR Balance salah !", 2)
            'QLMsgBox1.ShowMessage("DP AR Balance salah !", 2, "")
            Exit Sub
        End If

        TabContainer1.ActiveTabIndex = 0

        btnPosting2_Click(Nothing, Nothing)

        TabContainer1.ActiveTabIndex = 0

    End Sub

    Protected Sub btnPosting2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPosting2.Click
        trndparstatus.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
        gvMst.PageIndex = e.NewPageIndex
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(4).Text = Format(CDate(e.Row.Cells(4).Text), "MM/dd/yyyy")
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
		dateAwal.Text = Format(Now, "MM/dd/yyyy")
		dateAkhir.Text = Format(Now, "MM/dd/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    'Protected Sub gvCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    custoid.Text = gvCust.SelectedDataKey("custoid").ToString
    '    custname.Text = gvCust.SelectedDataKey("custname").ToString
    '    gvCust.Visible = False
    'End Sub

    'Protected Sub gvCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCust.PageIndexChanging
    '    gvCust.PageIndex = e.NewPageIndex
    '    BindSupplier(" and (custname like '%" & Tchar(custname.Text) & "%' or custcode like '%" & Tchar(custname.Text) & "%')")
    '    gvCust.Visible = True
    'End Sub

    Protected Sub payreftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payreftype.SelectedIndexChanged
        DDLPayreftypeChange()
    End Sub
#End Region

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
		dateAwal.Text = Format(Now, "MM/01/yyyy")
		dateAkhir.Text = Format(Now, "MM/dd/yyyy")
        FilterDDL.SelectedIndex = 0 : FilterText.Text = "" : statuse.SelectedIndex = 0
        BindData(" AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%' ")
    End Sub

    Protected Sub ImageButton3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""

        If trndparno.Text = "" Then
            sMsg &= "-  DP No. harus diisi !!<BR>"
        End If

        If custoid.Text = "" Then
            sMsg &= "- Customer harus diisi !!<BR>"
        End If

        If Not IsDate(CDate(trndpardate.Text)) Then
            sMsg &= "- Tanggal harus diisi !!<BR>"
        End If
        If IsDate(CDate(trndpardate.Text)) = False Then
            sMsg &= "- Tanggal salah !!<BR>"
        End If

        If payduedate.Visible Then
            If Not IsDate(CDate(payduedate.Text)) Then
                sMsg &= "- Tanggal Jatuh Tempo harus diisi !!<BR>"
            End If
            If IsDate(CDate(payduedate.Text)) = False Then
                sMsg &= "- Tanggal Jatuh Tempo salah !!<BR>"
            End If
        End If
        If payduedate.Visible And (IsDate(CDate(payduedate.Text))) And (IsDate(CDate(trndpardate.Text))) Then
            If CDate(payduedate.Text) < CDate(trndpardate.Text) Then
                sMsg &= "- Tanggal jatuh tempo harus >= tanggal transaksi !!<BR>"
            End If
        End If

        If payrefno.Text = "" And payreftype.SelectedValue <> "CASH" Then
            sMsg &= "-  Ref No. harus diisi !!<BR>"
        End If

        If ToDouble(trndparamt.Text) <= 0 Then
            sMsg &= "- Total DP harus >  0 !!<BR>"
        End If
        If trndparnote.Text.Length > 100 Then
            sMsg &= "- Note, maksimal 100 karakter !!<BR>"
        End If

        If CheckDataExists("SELECT COUNT(*) FROM QL_trndpar WHERE cmpcode='" & outlet.SelectedValue & "' AND dparno='" & Tchar(trndparno.Text) & "' AND dparoid <> '" & Session("oid") & "'") = True Then
            sMsg &= "-  DP No Outlet sudah ada !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
			trndparstatus.Text = "In Process"
			Exit Sub
        End If

        Dim DparOid As Integer = GetStrData("SELECT ISNULL(MIN(dparoid),0) FROM ql_trndpar WHERE cmpcode='" & outlet.SelectedValue & "' AND dparoid < 0")
        Session("DPARInit") = DparOid - 1
        Dim MinOid As Integer = Session("DPARInit")
        'Session("DPARInit") = GetStrData("SELECT isnull(MIN(dparoid)-1,-1) FROM QL_trndpar WHERE cmpcode='" & outlet.SelectedValue & "'")

        sSql = "select count(-1) from QL_trndpar where dparno='" & trndparno.Text & "' and dparstatus='POST' and  cmpcode='" & outlet.SelectedValue & "'"
        If GetStrData(sSql) > 0 Then
            showMessage("Data ini sudah di Posting !, Tekan batal untuk lihat Data di List Information", 2)
            trndparstatus.Text = "In Process" : Exit Sub
        End If

		sSql = "SELECT curroid FROM QL_mstcurr WHERE currcode='IDR'"
		cRate.SetRateValue(cKon.ambilscalar(sSql), Format(GetServerTime(), "MM/dd/yyyy"))
		Dim RateUSD As Double = cRate.GetRateMonthlyUSDValue
		Dim RateIDR As Double = cRate.GetRateMonthlyIDRValue

        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans

        Try
            If payreftype.SelectedValue = "CASH" Then
				payduedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            End If

			If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trndpar(cmpcode,dparoid,periodacctg,dparno,dpardate,custoid,cashbankoid,dparpayacctgoid,dparpaytype,dparduedate,dparpayrefno,dparamt,dparnote,dparaccumamt,dparstatus,createuser,createtime,upduser,updtime,acctgoid, curroid, rateoid,rate2oid,giroacctgoid,dparamtidr,dparamtusd) VALUES ('" & outlet.SelectedValue & "'," & Session("DPARInit") & ",'" & GetDateToPeriodAcctg(CDate(trndpardate.Text)) & "','" & Tchar(trndparno.Text) & "', '" & CDate(trndpardate.Text) & "','" & custoid.Text & "',0, " & trndparacctgoid.SelectedValue & ",'" & payreftype.SelectedValue & "','" & CDate(payduedate.Text) & "','" & Tchar(payrefno.Text) & "'," & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyIDRValue & ", '" & Tchar(trndparnote.Text) & "',0,'" & trndparstatus.Text & "', '" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "', CURRENT_TIMESTAMP,150,'" & cRate.GetRateDailyOid & "', " & cRate.GetRateMonthlyOid & ",60,0,'" & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyIDRValue & "'," & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
			Else
				sSql = "UPDATE QL_trndpar SET dparno='" & Tchar(trndparno.Text) & "', " & _
				  " dpardate='" & CDate(trndpardate.Text) & "', " & _
				  " dparpayacctgoid=" & trndparacctgoid.SelectedValue & "," & _
				  " dparamt=" & ToDouble(trndparamt.Text) & ", " & _
				  " dparpaytype='" & payreftype.SelectedValue & "'," & _
				  " dparduedate='" & CDate(payduedate.Text) & "', " & _
				  " dparpayrefno='" & Tchar(payrefno.Text) & "'," & _
				  " rateoid='" & cRate.GetRateDailyOid & "'," & _
				  " rate2oid='" & cRate.GetRateMonthlyOid & "', " & _
				  " dparnote='" & Tchar(trndparnote.Text) & "'," & _
				  " custoid='" & custoid.Text & "'," & _
				  " dparamtidr=" & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyIDRValue & "," & _
				  " dparamtusd=" & ToDouble(trndparamt.Text) * cRate.GetRateMonthlyUSDValue & ", " & _
				  " dparaccumamt=0,dparstatus='" & trndparstatus.Text & "'," & _
				  " upduser='" & Session("UserID") & "', " & _
				  " updtime=CURRENT_TIMESTAMP " & _
				  " WHERE cmpcode='" & outlet.SelectedValue & "' AND dparoid=" & trndparoid.Text
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
			End If
			objTrans.Commit()
			conn.Close()
        Catch ex As Exception
			objTrans.Rollback()
			conn.Close()
			trndparstatus.Text = "In Process"
            showMessage(ex.ToString, 2)
			Exit Sub
        End Try

        If Session("TblDPARInitdtl") Is Nothing Then
            If trndparstatus.Text = "POST" Then
                showMessage("Data have been posted with DP AP No. = " & trndparno.Text & " !", 2)
			Else
				Response.Redirect("trninitdpar.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("~\accounting\trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnposting1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        trndparstatus.Text = "POST"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnDelete1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trndpar WHERE cmpcode='" & outlet.SelectedValue & "' AND dparoid=" & trndparoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 2)
            'QLMsgBox1.ShowMessage(ex.Message, 2, "")
            Exit Sub
        End Try
        Response.Redirect("trninitdpar.aspx?awal=true")
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Printreport("Excel")
    End Sub

    Protected Sub trndparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		trndparamt.Text = ToMaskEdit(ToDouble(trndparamt.Text), 2)
	End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        'If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
        '    btnClearCust_Click(Nothing, Nothing)
        'End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindSupplier("")
        mpeListCust.Show()
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindSupplier("")
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindSupplier("")
        mpeListCust.Show()
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub
End Class
