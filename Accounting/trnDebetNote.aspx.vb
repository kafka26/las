' DEBET NOTE :  -A/P : Max Amount = A/P Balance
'               +A/R : No Max Amount
Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports ClassProcedure

Partial Class Accounting_trnDebetNote
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Dim cKon As New Koneksi
    Dim cRate As New ClassRate()
#End Region

#Region "Functions"
    Private Function IsValidPeriod(ByVal sDateAwal As String, ByVal sDateAkhir As String) As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(sDateAwal, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2, "") : Return False
        End If
        If Not IsValidDate(sDateAkhir, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2, "") : Return False
        End If
        If CDate(sDateAwal) > CDate(sDateAkhir) Then
            showMessage("Period 2 must be more than Period 1 !", 2, "") : Return False
        End If
        Return True
    End Function

    Public Function ConvertDtToTDF(ByVal dt As DataTable) As String
        Dim dr As DataRow, ary() As Object, i As Integer
        Dim iCol As Integer

        'Output Column Headers
        For iCol = 0 To dt.Columns.Count - 1
            Response.Write(dt.Columns(iCol).ToString & vbTab)
        Next
        Response.Write(vbCrLf)

        'Output Data
        For Each dr In dt.Rows
            ary = dr.ItemArray
            For i = 0 To UBound(ary)
                Response.Write(ary(i).ToString & vbTab)
            Next
            Response.Write(vbCrLf)
        Next
        Return ""
    End Function
#End Region

#Region "Procedures"
    Sub GenerateDNNo()
		Dim sNo As String = "DN-" & Format(CDate(dndate.Text), "yyMM") & "-"
        sSql = "SELECT isnull(max(abs(replace(dnno,'" & sNo & "',''))),0)+1  FROM ql_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnno LIKE '" & sNo & "%'"
        dnno.Text = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

    Private Sub CheckDNStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trndebetnote WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND dnstatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnDebetNote.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "' "
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbDNInProcess.Visible = True
            lkbDNInProcess.Text = "You have " & GetStrData(sSql) & " In Process Debet Note data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
	End Sub

	Private Sub BindDataSuppCust()
		If reftype.SelectedValue.ToUpper = "AP" Then
			lblListSupp.Text = "List Of Supplier"
            sSql = "SELECT DISTINCT oid,code,name FROM ( "
            sSql &= "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid AS Oid,s.suppcode AS code,s.suppname AS name,ap.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount,ap.amttrans apamt,ap.trnapdate,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid<>0 And ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid = ap2.refoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS appaidamt, (ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode And ap.reftype=ap2.reftype And ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS apdncnamt FROM QL_conap ap INNER JOIN (SELECT bm.cmpcode, bm.trnbelimstoid oid, bm.trnbelino transno, 'QL_trnbelimst' AS reftype FROM QL_trnbelimst bm UNION ALL SELECT apm.cmpcode, apm.approductmstoid oid, apm.approductno transno, 'QL_trnapproductmst' AS reftype FROM QL_trnapproductmst apm) AS Tbl_Trans ON Tbl_Trans.cmpcode=ap.cmpcode AND Tbl_Trans.oid=ap.refoid AND Tbl_Trans.reftype=ap.reftype INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid WHERE ap.cmpcode='" & bus_unit.SelectedValue & "' AND ISNULL(ap.payrefoid,0)=0 "
            sSql &= ") AS a WHERE a.apamt>a.appaidamt+a.apdncnamt AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%' ORDER BY code "
		Else
			lblListSupp.Text = "List Of Customer"
            sSql = "SELECT DISTINCT oid, code, name FROM ( "
            sSql &= "SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid AS oid,c.custcode AS code,c.custname AS name,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS araccount,ar.amttrans aramt,ar.trnardate,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 And ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS arpaidamt, (ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid <> 0 And ar.cmpcode = ar2.cmpcode And ar.reftype = ar2.reftype AND ar.refoid = ar2.refoid AND ar2.trnartype IN ('DNAR','CNAR', 'SRET')),0.0)) AS ardncnamt FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid WHERE ar.cmpcode='" & bus_unit.SelectedValue & "' AND ISNULL(ar.payrefoid,0)=0 "
            sSql &= ") AS a WHERE a.aramt>a.arpaidamt+a.ardncnamt AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text.Trim) & "%' ORDER BY code "

		End If
        Dim dtSuppCust As DataTable = cKon.ambiltabel(sSql, "suppcust")
        If dtSuppCust.Rows.Count > 0 Then
            Session("suppcust") = dtSuppCust
            gvSuppCust.DataSource = dtSuppCust
            gvSuppCust.DataBind()
            gvSuppCust.Visible = True
            cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
        Else
            showMessage("No data Customer..!!", 2, "")
        End If

    End Sub

	Public Sub BindData(ByVal sSqlPlus As String)
        sSql = "SELECT * FROM ( "
        sSql &= "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,'A/P' AS tipe,s.suppname AS suppcustname,bm.trnbelino AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN QL_trnbelimst BM ON bm.cmpcode=dn.cmpcode AND bm.trnbelimstoid=dn.refoid INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid WHERE dn.reftype='QL_trnbelimst' UNION ALL SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,'A/R' AS tipe,c.custname AS suppcustname,jm.trnjualno AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN QL_trnjualmst jm ON jm.cmpcode=dn.cmpcode AND dn.refoid=jm.trnjualmstoid INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid WHERE dn.reftype='ql_trnjualmst' UNION ALL SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,'A/P' AS tipe,c.suppname AS suppcustname,jm.approductno AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN QL_trnapproductmst jm ON jm.cmpcode=dn.cmpcode AND dn.refoid=jm.approductmstoid INNER JOIN QL_mstsupp c ON c.suppoid=dn.suppcustoid WHERE dn.reftype='QL_trnapproductmst' UNION ALL SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,'A/R' AS tipe,c.custname AS suppcustname,jm.transitemno AS aparno,dn.dnamt,dn.dnstatus,dn.createuser,dn.reftype,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN QL_trntransitemmst jm ON jm.cmpcode=dn.cmpcode AND dn.refoid=jm.transitemmstoid INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid WHERE dn.reftype='QL_trntransitemmst' "
        sSql &= ") AS a WHERE a.cmpcode='" & CompnyCode & "'"
        sSql &= sSqlPlus & " ORDER BY a.dnoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trndebetnote")
        gvMst.DataSource = Session("TblMst")
        gvMst.DataBind()
        lblViewInfo.Visible = False : lkbDNInProcess.Visible = False
    End Sub

	Public Sub FillTextBox(ByVal iOid As String)
		Try
            sSql = "SELECT * FROM ( "
            sSql &= "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,s.suppname AS suppcustname,dn.reftype tipe,dn.refoid,Tbl_Trans.transno AS aparno,dn.curroid,dn.rateoid,dn.rate2oid,ap.trnapdate apardate,ap.amttrans aparamt,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid<>0 AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar,ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trndebetnote dn1 ON dn1.cmpcode=ap2.cmpcode AND dn1.dnoid=ap2.payrefoid AND ap2.trnaptype='DNAP' AND ap2.payrefoid<>0 AND ap2.payrefoid<>dn.dnoid WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid),0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnaptype IN ('CNAP', 'RETPI') AND ap2.payrefoid<>0),0.0) AS amtdncn,dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN (SELECT bm.cmpcode, bm.trnbelimstoid oid, bm.trnbelino transno, 'QL_trnbelimst' AS reftype FROM QL_trnbelimst bm UNION ALL SELECT apm.cmpcode, apm.approductmstoid oid, apm.approductno transno, 'QL_trnapproductmst' AS reftype FROM QL_trnapproductmst apm) AS Tbl_Trans ON Tbl_Trans.cmpcode=dn.cmpcode AND Tbl_Trans.oid=dn.refoid AND Tbl_Trans.reftype=dn.reftype INNER JOIN QL_mstsupp s ON s.suppoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid INNER JOIN QL_conap ap ON ap.cmpcode=dn.cmpcode AND ap.reftype=dn.reftype AND ap.refoid=dn.refoid AND ap.payrefoid=0 UNION ALL "
            sSql &= "SELECT dn.cmpcode,dv.divname,dn.dnoid,dn.dnno,dn.dndate,dn.suppcustoid,c.custname AS suppcustname,dn.reftype AS tipe,dn.refoid, (CASE dn.reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst jm WHERE jm.trnjualmstoid=dn.refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst jm WHERE transitemmstoid=dn.refoid) ELSE '' END) AS aparno,dn.curroid,dn.rateoid,dn.rate2oid,ar.trnardate apardate,ar.amttrans aparamt,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar,ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trndebetnote dn1 ON dn1.cmpcode=ar2.cmpcode AND dn1.dnoid=ar2.payrefoid AND ar2.trnartype='DNAR' AND ar2.payrefoid<>0 AND ar2.payrefoid<>dn.dnoid WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid),0.0)+ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnartype IN ('CNAR', 'SRET') AND ar2.payrefoid<>0),0.0) AS amtdncn,dn.dnamt,dn.dbacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) dbacctgdesc,dn.cracctgoid,dn.dnnote,dn.dnstatus,dn.createuser,dn.createtime,dn.upduser,dn.updtime FROM QL_trndebetnote dn INNER JOIN QL_mstdivision dv ON dv.divcode=dn.cmpcode INNER JOIN QL_mstcust c ON c.custoid=dn.suppcustoid INNER JOIN QL_mstacctg a ON a.acctgoid=dn.dbacctgoid INNER JOIN QL_conar ar ON ar.cmpcode=dn.cmpcode AND ar.reftype=dn.reftype AND ar.refoid=dn.refoid AND ar.payrefoid=0 WHERE dn.reftype IN ('QL_trnjualmst','QL_trntransitemmst') "
            sSql &= ") AS a WHERE a.dnoid=" & iOid & ""

            Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_trndebetnote")
            If dtData.Rows.Count < 1 Then
                showMessage("Failed to load transaction data.", 1, "")
            Else
                bus_unit.SelectedValue = dtData.Rows(0)("cmpcode").ToString
                dnoid.Text = dtData.Rows(0)("dnoid").ToString
                dnno.Text = dtData.Rows(0)("dnno").ToString
                dndate.Text = Format(CDate(dtData.Rows(0)("dndate").ToString), "MM/dd/yyyy")
                If dtData.Rows(0)("tipe").ToString = "QL_trnbelimst" Then
                    reftype.SelectedValue = "ap"
                Else
                    reftype.SelectedValue = "ar"
                End If
                reftype_SelectedIndexChanged(Nothing, Nothing)
                supp_cust_oid.Text = dtData.Rows(0)("suppcustoid").ToString
                supp_cust_name.Text = dtData.Rows(0)("suppcustname").ToString
                aparreftype.Text = dtData.Rows(0)("tipe").ToString
                refoid.Text = dtData.Rows(0)("refoid").ToString
                aparno.Text = dtData.Rows(0)("aparno").ToString
                apardate.Text = Format(CDate(dtData.Rows(0)("apardate").ToString), "MM/dd/yyyy")
                curroid.SelectedValue = dtData.Rows(0)("curroid").ToString
                aparrateoid.Text = dtData.Rows(0)("rateoid").ToString
                aparrate2oid.Text = dtData.Rows(0)("rate2oid").ToString
                aparamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("aparamt").ToString), 4)
                aparpaidamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("amtbayar").ToString) + ToDouble(dtData.Rows(0)("amtdncn").ToString), 4)
                aparbalance.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("aparamt").ToString) - (ToDouble(dtData.Rows(0)("amtbayar").ToString) + ToDouble(dtData.Rows(0)("amtdncn").ToString)), 4)
                maxamount.Text = ToMaskEdit(ToDouble(aparbalance.Text), 4)
                dnamt.Text = ToMaskEdit(ToDouble(dtData.Rows(0)("dnamt").ToString), 4)
                dnamt_TextChanged(Nothing, Nothing)
                InitCOADebet(dtData.Rows(0)("dbacctgoid").ToString, dtData.Rows(0)("dbacctgdesc").ToString)
                coadebet.SelectedValue = dtData.Rows(0)("dbacctgoid").ToString
                coacredit.SelectedValue = dtData.Rows(0)("cracctgoid").ToString
                dnnote.Text = dtData.Rows(0)("dnnote").ToString
                dnstatus.Text = dtData.Rows(0)("dnstatus").ToString
                create.Text = "Created By <B>" & dtData.Rows(0)("createuser").ToString & "</B> On <B>" & dtData.Rows(0)("createtime").ToString & "</B> "
                upduser.Text = dtData.Rows(0)("upduser").ToString
                updtime.Text = dtData.Rows(0)("updtime").ToString
                If dnstatus.Text = "In Process" Then
                    btnSave.Visible = True
                    btnPost.Visible = True
                    btnDelete.Visible = True
                    lbldnno.Text = "Draft No"
                    dnoid.Visible = True
                    dnno.Visible = False
                Else
                    btnSave.Visible = False
                    btnPost.Visible = False
                    btnDelete.Visible = False
                    lbldnno.Text = "DN No"
                    dnoid.Visible = False
                    dnno.Visible = True
                End If
            End If

        Catch ex As Exception
			showMessage("Error loading transaction data:<BR>" & ex.Message, 1, "")
			Exit Sub
		End Try
	End Sub

	Private Sub ResetAPARData()
		lblaparno.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P No", "A/R No")
		refoid.Text = 0 : aparno.Text = ""
		aparreftype.Text = "" : apardate.Text = ""
		cProc.DisposeGridView(gvAPAR) : Session("apar") = Nothing : gvAPAR.Visible = False
		lblaparcurr.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Currency", "A/R Currency")
		curroid.SelectedIndex = 0 : aparrateoid.Text = "" : aparrate2oid.Text = ""
		lblaparamt.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Amount", "A/R Amount")
		aparamt.Text = ""
		lblaparpaidamt.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Paid Amt", "A/R Paid Amt")
		aparpaidamt.Text = ""
		lblaparbalance.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance", "A/R Balance")
		aparbalance.Text = "" : dnamt.Text = ""
		lblaparafter.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance After DN", "A/R Balance After DN")
		aparbalanceafter.Text = ""
		coadebet.Items.Clear()
		coacredit.SelectedIndex = IIf(coacredit.Items.Count > 0, 0, -1)
		maxamount.Text = "0"
		lblmaxamt.Visible = (reftype.SelectedValue.ToUpper = "AP")
		maxamount.Visible = (reftype.SelectedValue.ToUpper = "AP")
	End Sub

	Private Sub InitCOADebet(ByVal iAcctgOid As Integer, ByVal sAcctgCodeDesc As String)
		coadebet.Items.Clear()
		coadebet.Items.Add(New ListItem(sAcctgCodeDesc, iAcctgOid))
	End Sub

	Private Sub InitCOACredit(ByVal sCmpcode As String)
		' COA Credit
		Dim sVarErr As String = "" : coacredit.Items.Clear()
		If reftype.SelectedValue.ToUpper = "AP" Then
			If IsInterfaceExists("VAR_DEBET_NOTE_AP", sCmpcode) Then
				FillDDLAcctg(coacredit, "VAR_DEBET_NOTE_AP", sCmpcode)
			Else
				sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DEBET_NOTE_AP"
			End If
			If sVarErr <> "" Then
				showMessage(GetInterfaceWarning(sVarErr, ""), 2, "")
			End If
		Else
			If IsInterfaceExists("VAR_DEBET_NOTE_AR", sCmpcode) Then
				FillDDLAcctg(coacredit, "VAR_DEBET_NOTE_AR", sCmpcode)
			Else
				sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DEBET_NOTE_AR"
			End If
			If sVarErr <> "" Then
				showMessage(GetInterfaceWarning(sVarErr, ""), 2, "")
			End If
		End If
	End Sub

	Public Sub InitAllDDL()
		' Bussiness Unit
		sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE' "
		If Session("CompnyCode") <> CompnyCode Then
			sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
		End If
		FillDDL(bus_unit, sSql)
		InitCOACredit(bus_unit.SelectedValue)

		' Fill DDL Currency
		sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
		FillDDL(curroid, sSql)
	End Sub

	Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer, ByVal sState As String)
		Dim strCaption As String = CompnyName
		If iType = 1 Then ' Error
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		ElseIf iType = 2 Then ' Warning
			imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
		ElseIf iType = 3 Then ' Information
			imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
		Else
			imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
		End If
		Dim sAsliTemp As String = sMessage.Replace("<br />", vbCrLf).Replace("<BR>", vbCrLf)
		Dim sTemp() As String = sAsliTemp.Split(vbCrLf)

		If sTemp.Length > 25 Then
			lblMessage.Text = "<textarea class='inpText' readonly='true' style='height:250px;width:99%;'>" & sAsliTemp & "</textarea>"
		Else
			lblMessage.Text = sMessage
		End If
		lblCaption.Text = strCaption : lblState.Text = sState
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
	End Sub
#End Region

#Region "Events"
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("UserID") = "" Then Response.Redirect("~\Other\login.aspx")
		If Not (checkPagePermission("~\accounting\trnDebetNote.aspx", Session("Role"))) Then
			Response.Redirect("~\other\NotAuthorize.aspx")
		End If

		If Request.QueryString("awal") = "true" Then
			' Simpan session ke variabel temporary supaya tidak hilang
			Dim userId As String = Session("UserID")
			Dim xsetAcc As DataTable = Session("SpecialAccess")
			Dim appLimit As Decimal = Session("ApprovalLimit")
			Dim xsetRole As DataTable = Session("Role")
			Dim cmpcode As String = Session("CompnyCode")
			' Clear all session
			Session.Clear()
			' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
			Session("UserID") = userId
			Session("SpecialAccess") = xsetAcc
			Session("ApprovalLimit") = appLimit
			Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
			Response.Redirect("trnDebetNote.aspx")
		End If

		Session("oid") = Request.QueryString("oid")
		Me.btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data ?');")
		Me.btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data ?');")
		Page.Title = CompnyName & " - Debet Note"

		If Not IsPostBack Then
			CheckDNStatus()
			InitAllDDL()
			If Session("oid") <> Nothing And Session("oid") <> "" Then
				FillTextBox(Session("oid"))
				TabContainer1.ActiveTabIndex = 1
			Else
				Date1.Text = Format(GetServerTime, "MM/01/yyyy")
				Date2.Text = Format(GetServerTime, "MM/dd/yyyy")
				dndate.Text = Format(GetServerTime, "MM/dd/yyyy")
				dnoid.Text = GenerateID("QL_TRNDEBETNOTE", CompnyCode)
				dnstatus.Text = "In Process"
				btnDelete.Visible = False : btnShowCOA.Visible = False
				create.Text = "Created By <B>" & Session("UserID") & "</B> On <B>" & GetServerTime() & "</B>"
				upduser.Text = "-" : updtime.Text = "-"
				TabContainer1.ActiveTabIndex = 0
			End If
		End If
	End Sub

	Protected Sub btnOKPopUp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOKPopUp.Click
		cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
		Select Case lblState.Text
			Case "REDIR"
				Response.Redirect("~\accounting\trnDebetNote.aspx?awal=true")
		End Select
	End Sub

	Protected Sub bus_unit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles bus_unit.SelectedIndexChanged
		reftype_SelectedIndexChanged(Nothing, Nothing)
	End Sub

	Protected Sub reftype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reftype.SelectedIndexChanged
		lblsupp_cust.Text = IIf(reftype.SelectedValue.ToUpper = "AP", "Supplier", "Customer")
		supp_cust_oid.Text = "" : supp_cust_name.Text = ""
		cProc.DisposeGridView(gvSuppCust) : gvSuppCust.Visible = False
		ResetAPARData()
		InitCOACredit(bus_unit.SelectedValue)
	End Sub

	Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSC.Click
		FilterDDLListSupp.SelectedIndex = -1
		FilterTextListSupp.Text = ""
		gvSuppCust.SelectedIndex = -1
		BindDataSuppCust()
    End Sub

	Protected Sub btnClearSC_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSC.Click
		gvSuppCust.Visible = False
		cProc.DisposeGridView(gvSuppCust)
		Session("suppcust") = Nothing
		supp_cust_oid.Text = ""
		supp_cust_name.Text = ""
		ResetAPARData()
	End Sub

	Protected Sub btnFindAPAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindAPAR.Click
		If supp_cust_oid.Text = "" Then
			showMessage("Please select " & lblsupp_cust.Text & " first.", 2, "") : Exit Sub
		End If
		If reftype.SelectedValue.ToUpper = "AP" Then
            sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.curroid,a.currcode,a.amttrans,a.amtbayar+a.amtdncn AS amtbayar,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance  FROM ( "
            sSql &= "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,bm.trnbelino AS transno,s.suppcode,s.suppname,ap.acctgoid ,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,c.curroid,c.currcode,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid <> 0 And ap.cmpcode = ap2.cmpcode And ap.reftype = ap2.reftype And ap.refoid = ap2.refoid AND ISNULL(pay.payapres1,'') <> 'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0  AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS amtdncn FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid  INNER JOIN QL_trnbelimst bm ON ap.cmpcode=bm.cmpcode AND ap.refoid=bm.trnbelimstoid INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=bm.curroid AND c.cmpcode=bm.cmpcode WHERE ap.cmpcode='" & bus_unit.SelectedValue & "' AND ap.reftype='QL_trnbelimst' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" & supp_cust_oid.Text & " UNION ALL "
            sSql &= "SELECT ap.cmpcode,ap.reftype,ap.refoid,ap.suppoid,bm.approductno AS transno,s.suppcode,s.suppname,ap.acctgoid ,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ap.amttrans,c.curroid,c.currcode,CONVERT(VARCHAR(10),ap.trnapdate,101) AS transdate,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 INNER JOIN QL_trnpayap pay ON pay.cmpcode=ap2.cmpcode AND pay.payapoid=ap2.payrefoid WHERE ap2.payrefoid <> 0 And ap.cmpcode = ap2.cmpcode And ap.reftype = ap2.reftype And ap.refoid = ap2.refoid AND ISNULL(pay.payapres1,'') <> 'Lebih Bayar' AND ap2.trnaptype LIKE 'PAYAP%'),0.0)) AS amtbayar,(ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid <> 0  AND ap.cmpcode=ap2.cmpcode AND ap.reftype=ap2.reftype AND ap.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS amtdncn FROM QL_conap ap INNER JOIN QL_mstsupp s ON ap.suppoid=s.suppoid INNER JOIN QL_trnapproductmst bm ON ap.cmpcode=bm.cmpcode AND ap.refoid=bm.approductmstoid INNER JOIN QL_mstacctg a ON a.acctgoid=ap.acctgoid INNER JOIN QL_mstcurr c ON c.curroid=bm.curroid AND c.cmpcode=bm.cmpcode WHERE ap.cmpcode='" & bus_unit.SelectedValue & "' AND ap.reftype='QL_trnapproductmst' AND ISNULL(ap.payrefoid,0)=0 AND ap.suppoid=" & supp_cust_oid.Text & " "
            sSql &= ") AS a "
            sSql &= " WHERE (a.amttrans - (a.amtbayar + a.amtdncn))<>0 AND a.transno LIKE '%" & Tchar(aparno.Text.Trim) & "%' ORDER BY a.transno "
        Else
            sSql = "SELECT a.cmpcode,a.reftype,a.refoid,a.acctgoid,a.acctgaccount,a.transno,a.transdate,a.curroid,(SELECT cr.currcode FROM QL_mstcurr cr WHERE cr.curroid=a.curroid) currcode,a.amttrans,a.amtbayar+a.amtdncn AS amtbayar,a.amttrans-(a.amtbayar+a.amtdncn) AS amtbalance FROM (SELECT ar.cmpcode,ar.reftype,ar.refoid,ar.custoid,c.custcode,c.custname,ar.acctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS acctgaccount,ar.amttrans, (CASE ar.reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst jm WHERE jm.trnjualmstoid=ar.refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst jm WHERE transitemmstoid=ar.refoid) ELSE '' END) AS transno, ar.trnardate AS transdate, 1 AS curroid,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 INNER JOIN QL_trnpayar pay ON pay.cmpcode=ar2.cmpcode AND pay.payaroid=ar2.payrefoid WHERE ar2.payrefoid<>0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ISNULL(pay.payarres1,'') <> 'Lebih Bayar' AND ar2.trnartype LIKE 'PAYAR%'),0.0)) AS amtbayar,(ISNULL((SELECT SUM(ar2.amtbayar) FROM QL_conar ar2 WHERE ar2.payrefoid <> 0 AND ar.cmpcode=ar2.cmpcode AND ar.reftype=ar2.reftype AND ar.refoid=ar2.refoid AND ar2.trnartype IN ('DNAR','CNAR', 'SRET')),0.0)) AS amtdncn FROM QL_conar ar INNER JOIN QL_mstcust c ON ar.custoid=c.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=ar.acctgoid WHERE ar.cmpcode='" & bus_unit.SelectedValue & "' AND ISNULL(ar.payrefoid,0)=0 AND ar.custoid=" & supp_cust_oid.Text & " "
            sSql &= ") AS a "
            sSql &= "WHERE (a.amttrans - (a.amtbayar + a.amtdncn))<>0 AND a.transno LIKE '%" & Tchar(aparno.Text.Trim) & "%' ORDER BY a.transno"
        End If
		Dim dtAPAR As DataTable = cKon.ambiltabel(sSql, "apar")
		Session("apar") = dtAPAR
		gvAPAR.DataSource = dtAPAR
		gvAPAR.DataBind()
		gvAPAR.Visible = True
	End Sub

	Protected Sub btnClearAPAR_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAPAR.Click
		ResetAPARData()
	End Sub

	Protected Sub gvAPAR_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvAPAR.PageIndexChanging
		gvAPAR.PageIndex = e.NewPageIndex
		Dim dtAPAR As DataTable = Session("apar")
		gvAPAR.DataSource = dtAPAR
		gvAPAR.DataBind()
	End Sub

	Protected Sub gvAPAR_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvAPAR.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
			e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
			e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
		ElseIf e.Row.RowType = DataControlRowType.Header Then
			e.Row.Cells(1).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P No", "A/R No")
			e.Row.Cells(2).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Date", "A/R Date")
			e.Row.Cells(4).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Amount", "A/R Amount")
			e.Row.Cells(6).Text = IIf(reftype.SelectedValue.ToUpper = "AP", "A/P Balance", "A/R Balance")
		End If
	End Sub

	Protected Sub gvAPAR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvAPAR.SelectedIndexChanged
		Dim sRef As String = gvAPAR.SelectedDataKey("reftype").ToString
		Dim iRef As Integer = gvAPAR.SelectedDataKey("refoid").ToString
		Dim dtAPAR As DataTable = Session("apar")
		Dim dvSelectAPAR As DataView = dtAPAR.DefaultView
		dvSelectAPAR.RowFilter = "reftype='" & sRef & "' AND refoid=" & iRef
		If dvSelectAPAR.Count < 1 Then
			showMessage("Invalid " & reftype.SelectedItem.Text & " selection.", 2, "")
		Else
			refoid.Text = iRef
			aparreftype.Text = sRef
			aparno.Text = dvSelectAPAR(0)("transno").ToString
			apardate.Text = Format(CDate(dvSelectAPAR(0)("transdate").ToString), "MM/dd/yyyy")
			curroid.SelectedValue = dvSelectAPAR(0)("curroid").ToString
			cRate.SetRateValue(CInt(curroid.SelectedValue), apardate.Text)
			aparrateoid.Text = cRate.GetRateDailyOid
			aparrate2oid.Text = cRate.GetRateMonthlyOid
			aparamt.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amttrans").ToString), 4)
			aparpaidamt.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbayar").ToString), 4)
			aparbalance.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)

			InitCOADebet(dvSelectAPAR(0)("acctgoid").ToString, dvSelectAPAR(0)("acctgaccount").ToString)
			coacredit.SelectedIndex = 0
			lblmaxamt.Visible = (reftype.SelectedValue.ToUpper = "AP")
			maxamount.Visible = (reftype.SelectedValue.ToUpper = "AP")
			If maxamount.Visible Then
				maxamount.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)
			Else
				maxamount.Text = "0"
			End If
			dnamt.Text = ""
			aparbalanceafter.Text = ToMaskEdit(ToDouble(dvSelectAPAR(0)("amtbalance").ToString), 4)
		End If
		dvSelectAPAR.RowFilter = ""
		gvAPAR.Visible = False
		cProc.DisposeGridView(gvAPAR)
		Session("apar") = Nothing
	End Sub

	Protected Sub dnamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dnamt.TextChanged
		dnamt.Text = ToMaskEdit(ToDouble(dnamt.Text), 4)
		aparbalanceafter.Text = ToMaskEdit(ToDouble(aparbalance.Text) + (ToDouble(dnamt.Text) * IIf(reftype.SelectedValue.ToUpper = "AP", -1, 1)), 4)
	End Sub

	Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
		Response.Redirect("~\accounting\trnDebetNote.aspx?awal=true")
	End Sub

	Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
		'showTableCOA(no.Text, cmpcode, gvakun)
		'pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
	End Sub

	Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        dnstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
	End Sub

	Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
		Dim sMsg As String = ""

		If bus_unit.Items.Count < 1 Then sMsg &= "- Please create Bussines Unit data first.<BR>"
		If dndate.Text = "" Then sMsg &= "- Please fill DN Date first.<BR>"
		Dim sRefe As String = ""
		If dndate.Text <> "" Then If Not IsValidDate(dndate.Text, "MM/dd/yyyy", sRefe) Then sMsg &= "- DN Date is invalid. " & sRefe & "<BR>"
		If ToDouble(supp_cust_oid.Text) = 0 Then sMsg &= "- Please select " & lblsupp_cust.Text & " first.<BR>"
		If refoid.Text = "" Then
			sMsg &= "- Please select " & reftype.SelectedItem.Text & " first.<BR>"
		Else
			If ToDouble(dnamt.Text) <= 0 Then
				sMsg &= "- Debet Note amount must be greater than 0.<BR>"
			Else
				If reftype.SelectedValue.ToUpper = "AP" Then
					If ToDouble(dnamt.Text) > ToDouble(maxamount.Text) Then
						sMsg &= "- Maximum Debet Note amount is " & ToMaskEdit(ToDouble(maxamount.Text), 4) & "<BR>"
					End If
				End If
            End If
            cRate.SetRateValue(CInt(curroid.SelectedValue), apardate.Text)
			If dnstatus.Text = "Post" Then
				cRate.SetRateValue(CInt(curroid.SelectedValue), apardate.Text)
				If cRate.GetRateDailyLastError <> "" Then sMsg &= "- " & cRate.GetRateDailyLastError & ".<BR>"
				If cRate.GetRateMonthlyLastError <> "" Then sMsg &= "- " & cRate.GetRateMonthlyLastError & ".<BR>"
			End If
		End If
		If reftype.SelectedValue.ToUpper = "AP" Then
			If coacredit.Items.Count < 1 Then sMsg &= "- " & GetInterfaceWarning("VAR_DEBET_NOTE_AP", "") & ".<BR>"
		Else
			If coacredit.Items.Count < 1 Then sMsg &= "- " & GetInterfaceWarning("VAR_DEBET_NOTE_AR", "") & ".<BR>"
		End If

		If sMsg <> "" Then
			showMessage(sMsg, 2, "")
			dnstatus.Text = "In Process" : Exit Sub
		End If

		Dim sDraftNo As String = dnoid.Text : Dim sPostNo As String = dnno.Text
		If Session("oid") = Nothing Or Session("oid") = "" Then
			sSql = "SELECT COUNT(*) FROM QL_trndebetnote WHERE dnoid=" & dnoid.Text
			If CheckDataExists(sSql) Then
				dnoid.Text = GenerateID("QL_TRNDEBETNOTE", CompnyCode)
			End If
		Else
			Dim sStatusInfo As String = GetMultiUserStatus("QL_trndebetnote", "dnoid", dnoid.Text, "dnstatus", updtime.Text, "Post")
			If sStatusInfo <> "" Then
				showMessage(sStatusInfo, 2, "")
				dnstatus.Text = "In Process" : Exit Sub
			End If
		End If

		If dnstatus.Text = "Post" Then
			GenerateDNNo()
			If isPeriodAcctgClosed(bus_unit.SelectedValue, dndate.Text) Then
                showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(dndate.Text))).ToUpper & " " & Year(CDate(dndate.Text)).ToString & " anymore because the period has been closed. Please select another period!", 2, "") : dnstatus.Text = "In Process"
                Exit Sub
			End If
		End If
		Dim sDateNow As Date = GetServerTime.Date
		Dim sPeriodAcctgNow As String = GetDateToPeriodAcctg(GetServerTime)

		Dim iConap As Integer = GenerateID("QL_CONAP", CompnyCode)
		Dim iConar As Integer = GenerateID("QL_CONAR", CompnyCode)
		Dim iGLMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
		Dim iGLDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)

		Dim objTrans As SqlClient.SqlTransaction
		If conn.State = ConnectionState.Closed Then conn.Open()
		objTrans = conn.BeginTransaction()
		xCmd.Transaction = objTrans

		Try
			If Session("oid") = Nothing Or Session("oid") = "" Then
				sSql = "INSERT INTO QL_trndebetnote (cmpcode,dnoid,dnno,dndate,reftype,refoid,suppcustoid,dbacctgoid,cracctgoid,curroid,rateoid,rate2oid,dnamt,dnamtidr,dnamtusd,dntaxtype,dntaxamt,dntaxamtidr,dntaxamtusd,dntotal,dntotalidr,dntotalusd,dnnote,dnstatus,createuser,createtime,upduser,updtime) VALUES " & _
				 "('" & bus_unit.SelectedValue & "'," & dnoid.Text & ",'" & dnno.Text & "','" & CDate(dndate.Text) & "','" & aparreftype.Text & "'," & refoid.Text & "," & _
				 "" & supp_cust_oid.Text & "," & coadebet.SelectedValue & "," & coacredit.SelectedValue & "," & curroid.SelectedValue & "," & aparrateoid.Text & "," & aparrate2oid.Text & "," & _
				 "" & ToDouble(dnamt.Text) & "," & ToDouble(dnamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(dnamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
				 "'" & dntaxtype.SelectedValue & "'," & ToDouble(dntaxamt.Text) & "," & ToDouble(dntaxamt.Text) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(dntaxamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
				 "" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) & "," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
				 "" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'" & Tchar(dnnote.Text) & "','" & dnstatus.Text & "'," & _
				 "'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

				sSql = "UPDATE QL_mstoid SET lastoid=" & dnoid.Text & " WHERE tablename='QL_TRNDEBETNOTE' and cmpcode='" & CompnyCode & "'"
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
			Else
				' SELECT OLD DATA 
				sSql = "SELECT reftype FROM QL_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
				xCmd.CommandText = sSql
				Dim sOldType As String = xCmd.ExecuteScalar
				sSql = "SELECT refoid FROM QL_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
				xCmd.CommandText = sSql
				Dim sOldOid As String = xCmd.ExecuteScalar
                If reftype.SelectedValue.ToUpper = "AP" Then
                    sSql = "DELETE FROM QL_conap WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnaptype='DNAP' AND payrefoid=" & dnoid.Text
                Else
                    sSql = "DELETE FROM QL_conar WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnartype='DNAR' AND payrefoid=" & dnoid.Text
                End If
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                If reftype.SelectedValue.ToUpper = "AP" Then
                    sSql = " UPDATE QL_TRNBELIMST SET trnbelimststatus='Approved', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnbelimstoid=" & sOldOid
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                Else
                    If sOldType.ToUpper = "QL_TRNJUALMST" Then
                        sSql = " UPDATE QL_trnjualmst SET trnjualstatus='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnjualmstoid=" & sOldOid
                    ElseIf sOldType.ToUpper = "QL_TRNTRANSITEMMST" Then
                        sSql = " UPDATE QL_trntransitemmst SET transitemmststatus='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND transitemmstoid=" & sOldOid
                    End If
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

				sSql = "UPDATE QL_trndebetnote SET dnno='" & dnno.Text & "',dndate='" & CDate(dndate.Text) & "',reftype='" & aparreftype.Text & "',refoid=" & refoid.Text & "," & _
				 "suppcustoid=" & supp_cust_oid.Text & ",dbacctgoid=" & coadebet.SelectedValue & ",cracctgoid=" & coacredit.SelectedValue & ",curroid=" & curroid.SelectedValue & "," & _
				 "rateoid=" & aparrateoid.Text & ",rate2oid=" & aparrate2oid.Text & ",dnamt=" & ToDouble(dnamt.Text) & ",dnamtidr=" & ToDouble(dnamt.Text) * cRate.GetRateMonthlyIDRValue & "," & _
				 "dnamtusd=" & ToDouble(dnamt.Text) * cRate.GetRateMonthlyUSDValue & ",dntaxtype='" & dntaxtype.SelectedValue & "',dntaxamt=" & ToDouble(dntaxamt.Text) & "," & _
				 "dntaxamtidr=" & ToDouble(dntaxamt.Text) * cRate.GetRateMonthlyIDRValue & ",dntaxamtusd=" & ToDouble(dntaxamt.Text) * cRate.GetRateMonthlyUSDValue & "," & _
				 "dntotal=" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) & ",dntotalidr=" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
				 "dntotalusd=" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",dnnote='" & Tchar(dnnote.Text) & "',dnstatus='" & dnstatus.Text & "'," & _
				 "upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP " & _
				 "WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
			End If

			If reftype.SelectedValue.ToUpper = "AP" Then
				sSql = "INSERT INTO QL_conap (cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno," & _
				 "paybankoid,payduedate,amttrans,amtbayar,trnapnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES " & _
				 "('" & bus_unit.SelectedValue & "'," & iConap & ",'" & aparreftype.Text & "'," & refoid.Text & "," & dnoid.Text & "," & supp_cust_oid.Text & "," & coadebet.SelectedValue & "," & _
				 "'" & dnstatus.Text & "','DN" & reftype.SelectedValue.ToUpper & "','1/1/1900','" & GetDateToPeriodAcctg(CDate(dndate.Text)) & "'," & coacredit.SelectedValue & ",'" & CDate(dndate.Text) & "'," & _
				 "'" & dnno.Text & "',0,'" & CDate(dndate.Text) & "',0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) & ",'DN No. " & dnno.Text & " for A/P No. " & aparno.Text & "'," & _
				 "'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & _
				 "0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",0)"
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

				sSql = "UPDATE QL_mstoid SET lastoid=" & iConap & " WHERE tablename='QL_CONAP' and cmpcode='" & CompnyCode & "'"
				xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                If ToDouble(aparbalanceafter.Text) <= 0 Then
                    Dim sRef As String = aparreftype.Text.ToUpper
                    sSql = " UPDATE QL_TRNBELIMST SET trnbelimststatus='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnbelimstoid=" & refoid.Text
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If
            Else
                sSql = "INSERT INTO QL_conar (cmpcode,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno," & _
                 "paybankoid,payduedate,amttrans,amtbayar,trnarnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES " & _
                 "('" & bus_unit.SelectedValue & "'," & iConar & ",'" & aparreftype.Text & "'," & refoid.Text & "," & dnoid.Text & "," & supp_cust_oid.Text & "," & coadebet.SelectedValue & "," & _
                 "'" & dnstatus.Text & "','DN" & reftype.SelectedValue.ToUpper & "','1/1/1900','" & GetDateToPeriodAcctg(CDate(dndate.Text)) & "'," & coacredit.SelectedValue & ",'" & CDate(dndate.Text) & "'," & _
                 "'" & dnno.Text & "',0,'" & CDate(dndate.Text) & "',0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * -1 & ",'DN No. " & dnno.Text & " for A/R No. " & aparno.Text & "'," & _
                 "'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue * -1 & "," & _
                 "0," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue * -1 & ",0)"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iConar & " WHERE tablename='QL_CONAR' and cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            If dnstatus.Text = "Post" Then
                ' Insert GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type,rateoid,rate2oid,glrateidr,glrate2idr,glrateusd,glrate2usd) VALUES " & _
                 "('" & bus_unit.SelectedValue & "'," & iGLMstOid & ",'" & CDate(dndate.Text) & "','" & GetDateToPeriodAcctg(CDate(dndate.Text)) & "','DN No. " & dnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
                 "'" & Session("UserID") & "',CURRENT_TIMESTAMP,''," & cRate.GetRateDailyOid & "," & cRate.GetRateMonthlyOid & "," & cRate.GetRateDailyIDRValue & "," & cRate.GetRateMonthlyIDRValue & "," & cRate.GetRateDailyUSDValue & "," & cRate.GetRateMonthlyUSDValue & ")"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1,glother2) VALUES " & _
                 "('" & bus_unit.SelectedValue & "'," & iGLDtlOid & ",1," & iGLMstOid & "," & coadebet.SelectedValue & ",'D'," & ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text) & "," & _
                 "'" & dnno.Text & "','DN No. " & dnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
                 "" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'QL_trndebetnote','" & dnoid.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                iGLDtlOid += 1

                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1,glother2) VALUES " & _
                 "('" & bus_unit.SelectedValue & "'," & iGLDtlOid & ",2," & iGLMstOid & "," & coacredit.SelectedValue & ",'C'," & ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text) & "," & _
                 "'" & dnno.Text & "','DN No. " & dnno.Text & " for " & reftype.SelectedItem.Text & " No. " & aparno.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & _
                 "" & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyIDRValue & "," & (ToDouble(dnamt.Text) + ToDouble(dntaxamt.Text)) * cRate.GetRateMonthlyUSDValue & ",'QL_trndebetnote','" & dnoid.Text & "')"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit() : conn.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnSave_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.ToString, 1, "")
                    dnstatus.Text = "In Process"
                    Exit Sub
                End If
            Else
                showMessage(exSql.ToString, 1, "")
                dnstatus.Text = "In Process"
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback() : conn.Close()
            dnstatus.Text = "In Process"
            showMessage(ex.ToString, 1, "") : Exit Sub
        End Try

		If sDraftNo <> dnoid.Text Then
			showMessage("Draft No. have been regenerated because have been used by another data. Your new Draft No. is " & dnoid.Text & ".<BR>", 3, "REDIR")
		Else
			If sPostNo <> dnno.Text Then
				showMessage("Data have been posted with DN No. " & dnno.Text & ".", 3, "REDIR")
			Else
				Response.Redirect("~\Accounting\trnDebetNote.aspx?awal=true")
			End If
		End If
	End Sub

	Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
		If dnoid.Text.Trim = "" Then
			showMessage("Please select Debet Note data first!", 2, "") : Exit Sub
		Else
			Dim sStatusInfo As String = GetMultiUserStatus("QL_trndebetnote", "dnoid", dnoid.Text, "dnstatus", updtime.Text, "Post")
			If sStatusInfo <> "" Then
				showMessage(sStatusInfo, 2, "")
				dnstatus.Text = "In Process" : Exit Sub
			End If
		End If
        Dim sColName As String = "" : Dim sStatus As String = "" : Dim sColStatus As String = ""
		Dim objTrans As SqlClient.SqlTransaction
		If conn.State = ConnectionState.Closed Then conn.Open()
		objTrans = conn.BeginTransaction()
		xCmd.Transaction = objTrans

		Try
			' SELECT OLD DATA 
			sSql = "SELECT reftype FROM QL_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
			xCmd.CommandText = sSql
			Dim sOldType As String = xCmd.ExecuteScalar
			sSql = "SELECT refoid FROM QL_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
			xCmd.CommandText = sSql
			Dim sOldOid As String = xCmd.ExecuteScalar
            If sOldType.ToUpper = "QL_TRNBELIMST" Then
                sColName = "trnbelimstoid" : sStatus = "Approved" : sColStatus = "trnbelimststatus"
            ElseIf sOldType.ToUpper = "QL_TRNAPPRODUCTMST" Then
                sColName = "approductmstoid" : sStatus = "Post" : sColStatus = "approductmststatus"
            End If
            If sOldType.ToUpper = "QL_TRNBELIMST" Or sOldType.ToUpper = "QL_TRNAPPRODUCTMST" Then
                sSql = "DELETE FROM QL_conap WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnaptype='DNAP' AND payrefoid=" & dnoid.Text
            Else
                sSql = "DELETE FROM QL_conar WHERE cmpcode='" & bus_unit.SelectedValue & "' AND trnartype='DNAR' AND payrefoid=" & dnoid.Text
            End If
			xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

            If sOldType.ToUpper = "QL_TRNBELIMST" Or sOldType.ToUpper = "QL_TRNAPPRODUCTMST" Then
                sSql = "UPDATE " & sOldType.ToUpper & " SET " & sColStatus & "='" & sStatus & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & bus_unit.SelectedValue & "' AND " & sColName & "=" & sOldOid
                xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
            End If

			sSql = "DELETE FROM QL_trndebetnote WHERE cmpcode='" & bus_unit.SelectedValue & "' AND dnoid=" & dnoid.Text
			xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

			objTrans.Commit() : conn.Close()
		Catch ex As Exception
			objTrans.Rollback() : conn.Close()
			showMessage(ex.Message, 1, "") : Exit Sub
		End Try
		Response.Redirect("~\accounting\trnDebetNote.aspx?awal=true")
	End Sub

	Protected Sub gvMst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMst.PageIndexChanging
		gvMst.PageIndex = e.NewPageIndex
		gvMst.DataSource = Session("TblMst")
		gvMst.DataBind()
	End Sub

	Protected Sub gvMst_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMst.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
			e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 4)
		End If
	End Sub

	Protected Sub gvMst_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvMst.Sorting
		'Retrieve the table from the session object.
		Dim dt = TryCast(Session("TblMst"), DataTable)
		If dt IsNot Nothing Then
			'Sort the data.
			dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
			gvMst.DataSource = Session("TblMst")
			gvMst.DataBind()
		End If
	End Sub

	Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
		Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
		If chkPeriod.Checked Then
			If IsValidPeriod(Date1.Text, Date2.Text) Then
				sSqlPlus &= " AND a.dndate>='" & Date1.Text & " 00:00:00' AND a.dndate<='" & Date2.Text & " 23:59:59'"
			Else
				Exit Sub
			End If
		End If
		sSqlPlus &= " AND a.reftype LIKE '" & FilterType.SelectedValue & "' AND a.dnstatus LIKE '" & FilterStatus.SelectedValue & "'"
		If checkPagePermission("~\Accounting\trnDebetNote.aspx", Session("SpecialAccess")) = False Then
			sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
		End If
		BindData(sSqlPlus)
	End Sub

	Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAll.Click
		Dim sSqlPlus As String = ""
		FilterDDL.SelectedIndex = -1 : FilterText.Text = ""
		chkPeriod.Checked = False
		Date1.Text = Format(GetServerTime(), "MM/01/yyyy")
		Date2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		FilterType.SelectedIndex = 0
		FilterStatus.SelectedIndex = 0
		If checkPagePermission("~\Accounting\trnDebetNote.aspx", Session("SpecialAccess")) = False Then
			sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
		End If
		BindData(sSqlPlus)
	End Sub

	Protected Sub lkbDNInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbDNInProcess.Click
		Dim nDays As Integer = 7
		FilterText.Text = "" : FilterDDL.SelectedIndex = -1
		chkPeriod.Checked = False
		Date1.Text = Format(GetServerTime(), "MM/01/yyyy")
		Date2.Text = Format(GetServerTime(), "MM/dd/yyyy")
		FilterType.SelectedIndex = 0
		FilterStatus.SelectedIndex = 0
		Dim sSqlPlus As String = ""
		If checkPagePermission("~\Accounting\trnDebetNote.aspx", Session("SpecialAccess")) = False Then
			sSqlPlus &= " AND a.createuser='" & Session("UserID") & "' "
		End If
		sSqlPlus &= " AND DATEDIFF(DAY, a.updtime, GETDATE()) > " & nDays & " AND a.dnstatus='In Process' "
		BindData(sSqlPlus)
	End Sub

	Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
		BindDataSuppCust()
		mpeListSupp.Show()
	End Sub

	Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
		FilterDDLListSupp.SelectedIndex = -1
		FilterTextListSupp.Text = ""
		gvSuppCust.SelectedIndex = -1
		BindDataSuppCust()
		mpeListSupp.Show()
	End Sub

	Protected Sub gvSuppCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSuppCust.PageIndexChanging
		gvSuppCust.PageIndex = e.NewPageIndex
		Dim dtSuppCust As DataTable = Session("suppcust")
		gvSuppCust.DataSource = dtSuppCust
		gvSuppCust.DataBind()
		mpeListSupp.Show()
	End Sub

	Protected Sub gvSuppCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) 
		supp_cust_oid.Text = gvSuppCust.SelectedDataKey("oid").ToString
		supp_cust_name.Text = gvSuppCust.SelectedDataKey("name").ToString
		gvSuppCust.Visible = False
		cProc.DisposeGridView(gvSuppCust)
		Session("suppcust") = Nothing
		ResetAPARData()
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub

	Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub
#End Region

End Class