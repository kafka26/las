Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_DownPaymentAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If dpapdate.Text = "" Then
            sError &= "- Please fill DP DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(dpapdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DP DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select DP ACCOUNT field!<BR>"
        End If
        If dpappayacctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If dpappaytype.SelectedValue <> "BKK" Then
            If dpapduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(dpapduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(dpapdate.Text) > CDate(dpapduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than DP DATE!<BR>"
                    End If
                End If
            End If
        End If
        If dpappaytype.SelectedValue = "BGK" Then
            If dpappayrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If dpaptakegiro.Text = "" Then
                sError &= "- Please fill DATE TAKE GIRO field!<BR>"
            Else
                If Not IsValidDate(dpaptakegiro.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
                Else
                    If CDate(dpapdate.Text) > CDate(dpaptakegiro.Text) Then
                        sError &= "- DATE TAKE GIRO must be more or equal than DP DATE!<BR>"
                    End If
                End If
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If dpapamt.Text = "" Then
            sError &= "- Please fill DP AMOUNT field!<BR>"
        Else
            If ToDouble(dpapamt.Text) <= 0 Then
                sError &= "- DP AMOUNT must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("dpapamt", "QL_trndpap", ToDouble(dpapamt.Text), sErrReply) Then
                    sError &= "- DP AMOUNT must be less than MAX DP AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If addacctgoid1.SelectedValue <> 0 Then
            If ToDouble(addacctgamt1.Text) = 0 Then
                sError &= "- Additional Cost Amount 1 can't be equal to 0!<BR>"
            End If
        End If
        If addacctgoid2.SelectedValue <> 0 Then
            If ToDouble(addacctgamt2.Text) = 0 Then
                sError &= "- Additional Cost Amount 2 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            If ToDouble(addacctgamt3.Text) = 0 Then
                sError &= "- Additional Cost Amount 3 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            dpapstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub ReAmountDP()
        dpapnett.Text = ToMaskEdit(ToDouble(dpapamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), 4)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trndpap WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND dpapstatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Down Payment A/P data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDPAccount()
        End If
        InitDDLAdd()
    End Sub

    Private Sub CurDDL(ByVal Curr As String)
        ' Fill DDL Currency
        sSql = "SELECT DISTINCT a.curroid, currcode FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' " & Curr & " "
        FillDDL(curroid, sSql)
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
    End Sub

    Private Sub InitDDLDPAccount()
        ' Fill DDL DP Account
        FillDDLAcctg(acctgoid, "VAR_DPAP", DDLBusUnit.SelectedValue)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT dpapoid, dpapno, CONVERT(VARCHAR(10), dpapdate, 101) AS dpapdate, suppname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE dpappaytype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS dpappaytype, dpapstatus, dpapnote, 'False' AS checkvalue, cashbankno FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=dp.cashbankoid WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " dp.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " dp.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " AND dpapoid > 0 ORDER BY dp.dpapoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trndpap")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "dpapoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateCashBankNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If dpapdate.Text <> "" Then
                If IsValidDate(dpapdate.Text, "MM/dd/yyyy", sErr) Then
                    If dpappayacctgoid.SelectedValue <> "" Then
                        Dim sNo As String = dpappaytype.SelectedValue & "-" & Format(CDate(dpapdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & dpappayacctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT suppoid, suppcode, suppname, suppaddr FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' AND activeflag='ACTIVE' ORDER BY suppcode, suppname"
        FillGV(gvListSupp, sSql, "QL_mstsupp")
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal
        lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal
        dpapduedate.Visible = bVal
        imbDueDate.Visible = bVal
        lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub EnableAddInfoGiro(ByVal bVal As Boolean)
        lblDTG.Visible = bVal
        lblWarnDTG.Visible = bVal
        lblSeptDTG.Visible = bVal
        dpaptakegiro.Visible = bVal
        imbDTG.Visible = bVal
        lblInfoDTG.Visible = bVal
        lblWarnRefNo.Visible = bVal
    End Sub

    Private Sub GenerateNo()
		Dim sNo As String = "DPAP-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dpapno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapno LIKE '%" & sNo & "%'"
        dpapno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT dp.cmpcode, dp.dpapoid, dp.periodacctg, dpapno, dpapdate, dp.suppoid, suppname, dp.acctgoid, dp.cashbankoid, cashbankno, dpappaytype, dpappayacctgoid, dpappayrefno, dpapduedate, dp.curroid, dp.dpapamt, dpapnote, dpapstatus, dp.createuser, dp.createtime, dp.upduser, dp.updtime, dpaptakegiro, dp.addacctgoid1, dp.addacctgamt1, dp.addacctgoid2, dp.addacctgamt2, dp.addacctgoid3, dp.addacctgamt3 FROM QL_trndpap dp INNER JOIN QL_mstsupp s ON s.suppoid=dp.suppoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.dpapoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                dpapoid.Text = Trim(xreader("dpapoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                dpapno.Text = Trim(xreader("dpapno").ToString)
                dpapdate.Text = Format(xreader("dpapdate"), "MM/dd/yyyy")
                suppoid.Text = Trim(xreader("suppoid").ToString)
                suppname.Text = Trim(xreader("suppname").ToString)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                dpappaytype.SelectedValue = Trim(xreader("dpappaytype").ToString)
                dpappaytype_SelectedIndexChanged(Nothing, Nothing)
                dpappayrefno.Text = Trim(xreader("dpappayrefno").ToString)
                dpapduedate.Text = Format(xreader("dpapduedate"), "MM/dd/yyyy")
                If dpapduedate.Text = "01/01/1900" Then
                    dpapduedate.Text = ""
                End If
                dpaptakegiro.Text = Format(xreader("dpaptakegiro"), "MM/dd/yyyy")
                If dpaptakegiro.Text = "01/01/1900" Then
                    dpaptakegiro.Text = ""
                End If
                dpappayacctgoid.SelectedValue = Trim(xreader("dpappayacctgoid").ToString)
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
                dpapamt.Text = ToMaskEdit(ToDouble(Trim(xreader("dpapamt").ToString)), 4)
                addacctgoid1.SelectedValue = xreader("addacctgoid1").ToString
                addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
                addacctgamt1.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt1").ToString)), 4)
                addacctgoid2.SelectedValue = xreader("addacctgoid2").ToString
                addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
                addacctgamt2.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt2").ToString)), 4)
                addacctgoid3.SelectedValue = xreader("addacctgoid3").ToString
                addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
                addacctgamt3.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt3").ToString)), 4)
                ReAmountDP()
                dpapnote.Text = Trim(xreader("dpapnote").ToString)
                dpapstatus.Text = Trim(xreader("dpapstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        dpappaytype.CssClass = "inpTextDisabled" : dpappaytype.Enabled = False
        dpappayacctgoid.CssClass = "inpTextDisabled" : dpappayacctgoid.Enabled = False
        If dpapstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            If dpapnote.Text.Contains("DPAP Lebih Bayar Dari") Then
                btnShowCOA.Visible = False
            Else
                btnShowCOA.Visible = True
            End If
            lblTrnNo.Text = "DP No."
            dpapoid.Visible = False
            dpapno.Visible = True
        End If
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("dpapoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptDPAP.rpt"))
            Dim sWhere As String = ""
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dpapdate>='" & FilterPeriod1.Text & " 00:00:00' AND dpapdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dpapstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.dpapoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DownPaymentAPPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
    End Sub

    Private Sub PrintR(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptDPAP.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptDPAPXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='CORP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            Report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(Report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DPAPReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "DPAPReport")
            End If
            Report.Close()
            Report.Dispose()
        Catch ex As Exception
            Report.Close()
            Report.Dispose()
            'showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                'curroid.SelectedValue = sCurrOid
            Else
                curroid.Items.Clear()
                showMessage("Please define Currency for selected Payment Account!", 2)
            End If
        Else
            curroid.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnDPAP.aspx")
        End If
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Down Payment A/P"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            CurDDL("")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dpapoid.Text = GenerateID("QL_TRNDPAP", CompnyCode)
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                dpapdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                dpapstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                dpappaytype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, dp.updtime, GETDATE()) > " & nDays & " AND dpapstatus='In Process' "
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click

        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text.Trim) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND dpapdate > ='" & FilterPeriod1.Text & " 00:00:00' AND dpapdate < ='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND dpapstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnDPAP.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDPAccount() : InitDDLAdd()
        dpappaytype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dpapdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpapdate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = ""
        suppname.Text = ""
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub dpappaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpappaytype.SelectedIndexChanged
        dpappayrefno.Text = "" : dpapduedate.Text = "" : dpaptakegiro.Text = ""
        If dpappaytype.SelectedValue = "BKK" Then
            EnableAddInfo(False)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dpappayacctgoid, "VAR_CASH", DDLBusUnit.SelectedValue)
        ElseIf dpappaytype.SelectedValue = "BBK" Then
            EnableAddInfo(True)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dpappayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        Else
            EnableAddInfo(True)
            EnableAddInfoGiro(True)
            FillDDLAcctg(dpappayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        End If
        Dim oid As Integer = dpappayacctgoid.SelectedValue
        SetCOACurrency(dpappayacctgoid.SelectedValue)
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dpappayacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpappayacctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If

        sSql = "SELECT DISTINCT COUNT(c.curroid) FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' AND a.acctgoid='" & dpappayacctgoid.SelectedValue & "'"
        Dim sCurrOid As Integer = Integer.Parse(GetStrData(sSql))
        If sCurrOid <> 0 Then
            CurDDL("AND a.acctgoid='" & dpappayacctgoid.SelectedValue & "'")
        Else
            CurDDL("")
            'showMessage("Please define Currency for selected Payment Account!", 2)
        End If

        'SetCOACurrency(dpappayacctgoid.SelectedValue)
    End Sub

    Protected Sub dpapamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpapamt.TextChanged
        dpapamt.Text = ToMaskEdit(ToDouble(dpapamt.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trndpap WHERE dpapoid=" & dpapoid.Text
                If CheckDataExists(sSql) Then
                    dpapoid.Text = GenerateID("QL_TRNDPAP", CompnyCode)
                    isRegenOid = True
                End If
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno='" & cashbankno.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateCashBankNo()
                End If
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpap", "dpapoid", dpapoid.Text, "dpapstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    dpapstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iSeq As Integer = 1

            Dim cRate As New ClassRate()
            Dim iGiroAcctgOid As Integer = 0
            periodacctg.Text = GetDateToPeriodAcctg(CDate(dpapdate.Text))
            If dpapstatus.Text = "Post" Then
                GenerateNo()
                cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If dpappaytype.SelectedValue = "BGK" Then
                    If Not IsInterfaceExists("VAR_GIRO", DDLBusUnit.SelectedValue) Then
                        sVarErr &= "VAR_GIRO"
                    Else
                        iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & dpapdate.Text & "', '" & dpappaytype.SelectedValue & "', 'DPAP', " & dpappayacctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(dpapnett.Text) & ", " & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyUSDValue & ", " & suppoid.Text & ", '" & IIf(dpappaytype.SelectedValue <> "BKK", dpapduedate.Text, "1/1/1900") & "', '" & Tchar(dpappayrefno.Text) & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(dpappaytype.SelectedValue = "BGK", dpaptakegiro.Text, "1/1/1900") & "', " & iGiroAcctgOid & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "INSERT INTO QL_trndpap (cmpcode, dpapoid, periodacctg, dpapno, dpapdate, suppoid, acctgoid, cashbankoid, dpappaytype, dpappayacctgoid, dpappayrefno, dpapduedate, curroid, rateoid, rate2oid, dpapamt, dpapaccumamt, dpapnote, dpapstatus, createuser, createtime, upduser, updtime, dpaptakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, dpapamtidr, dpapamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & dpapoid.Text & ", '" & periodacctg.Text & "', '" & dpapno.Text & "', '" & dpapdate.Text & "', " & suppoid.Text & ", " & acctgoid.SelectedValue & ", " & cashbankoid.Text & ", '" & dpappaytype.SelectedValue & "', " & dpappayacctgoid.SelectedValue & ", '" & Tchar(dpappayrefno.Text) & "', '" & IIf(dpappaytype.SelectedValue <> "BKK", dpapduedate.Text, "1/1/1900") & "', " & curroid.SelectedValue & ", " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & ToDouble(dpapamt.Text) & ", 0, '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(dpappaytype.SelectedValue = "BGK", dpaptakegiro.Text, "1/1/1900") & "', " & iGiroAcctgOid & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ", " & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & dpapoid.Text & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & dpapdate.Text & "', cashbanktype='" & dpappaytype.SelectedValue & "', acctgoid=" & dpappayacctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(dpapnett.Text) & ", cashbankamtidr=" & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamtusd=" & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & suppoid.Text & ", cashbankduedate='" & IIf(dpappaytype.SelectedValue <> "BKK", dpapduedate.Text, "1/1/1900") & "', cashbankrefno='" & Tchar(dpappayrefno.Text) & "', cashbanknote='" & Tchar(dpapnote.Text) & "', cashbankstatus='" & dpapstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & IIf(dpappaytype.SelectedValue = "BGK", dpaptakegiro.Text, "1/1/1900") & "', giroacctgoid=" & iGiroAcctgOid & ",addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndpap SET periodacctg='" & periodacctg.Text & "', dpapno='" & dpapno.Text & "', dpapdate='" & dpapdate.Text & "', suppoid=" & suppoid.Text & ", acctgoid=" & acctgoid.SelectedValue & ", dpappaytype='" & dpappaytype.SelectedValue & "', dpappayacctgoid=" & dpappayacctgoid.SelectedValue & ", dpappayrefno='" & Tchar(dpappayrefno.Text) & "', dpapduedate='" & IIf(dpappaytype.SelectedValue <> "BKK", dpapduedate.Text, "1/1/1900") & "', curroid=" & curroid.SelectedValue & ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", dpapamt=" & ToDouble(dpapamt.Text) & ", dpapnote='" & Tchar(dpapnote.Text) & "', dpapstatus='" & dpapstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, dpaptakegiro='" & IIf(dpappaytype.SelectedValue = "BGK", dpaptakegiro.Text, "1/1/1900") & "', giroacctgoid=" & iGiroAcctgOid & ",addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & ", dpapamtidr=" & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyIDRValue & ", dpapamtusd=" & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyUSDValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & dpapoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If dpapstatus.Text = "Post" Then
                    Dim sDate As String = dpapdate.Text
                    If dpappaytype.SelectedValue = "BBK" Then
                        sDate = dpapduedate.Text
                    End If
                    Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'DP A/P|No=" & dpapno.Text & "', '" & dpapstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    ' Insert Into GL Dtl
                    ' Uang Muka
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'D', " & ToDouble(dpapamt.Text) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dpapamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trndpap " & dpapoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    ' Additional Cost 1+
                    If ToDouble(addacctgamt1.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid1.SelectedValue & ", 'D', " & ToDouble(addacctgamt1.Text) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - DP A/P|No=" & dpapno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Additional Cost 2+
                    If ToDouble(addacctgamt2.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid2.SelectedValue & ", 'D', " & ToDouble(addacctgamt2.Text) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - DP A/P|No=" & dpapno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Additional Cost 3+
                    If ToDouble(addacctgamt3.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid3.SelectedValue & ", 'D', " & ToDouble(addacctgamt3.Text) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - DP A/P|No=" & dpapno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Cash/Bank/Giro
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & IIf(dpappaytype.SelectedValue = "BGK", iGiroAcctgOid, dpappayacctgoid.SelectedValue) & ", 'C', " & ToDouble(dpapnett.Text) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dpapnett.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trndpap " & dpapoid.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    ' Additional Cost 1-
                    If ToDouble(addacctgamt1.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid1.SelectedValue & ", 'C', " & Math.Abs(ToDouble(addacctgamt1.Text)) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt1.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt1.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - DP A/P|No=" & dpapno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Additional Cost 2-
                    If ToDouble(addacctgamt2.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid2.SelectedValue & ", 'C', " & Math.Abs(ToDouble(addacctgamt2.Text)) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt2.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt2.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - DP A/P|No=" & dpapno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Additional Cost 3-
                    If ToDouble(addacctgamt3.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid3.SelectedValue & ", 'C', " & Math.Abs(ToDouble(addacctgamt3.Text)) & ", '" & dpapno.Text & "', '" & Tchar(dpapnote.Text) & "', '" & dpapstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt3.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt3.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - DP A/P|No=" & dpapno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit()
                xCmd.Connection.Close()
            Catch ex As Exception
                objTrans.Rollback()
                xCmd.Connection.Close()
                dpapstatus.Text = "In Process"
                showMessage(ex.Message & sSql, 1)
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & dpapoid.Text & ".<BR>"
            End If
            If dpapstatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with DP No. = " & dpapno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dpapoid.Text = "" Then
            showMessage("Please select Down Payment A/P data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpap", "dpapoid", dpapoid.Text, "dpapstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                dpapstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid IN (SELECT cashbankoid FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & dpapoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & dpapoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnDPAP.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        dpapstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(dpapno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpap " & dpapoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(dpapno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpap " & dpapoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintR("Excel")
    End Sub
#End Region

End Class