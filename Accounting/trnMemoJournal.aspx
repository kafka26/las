<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMemoJournal.aspx.vb" Inherits="Accounting_MemorialJournal" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" ForeColor="SaddleBrown" Text=".: Memorial Journal" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Memorial Journal :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" __designer:wfdid="w28" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=5></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w29"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w30"><asp:ListItem Value="glother1">Memo No.</asp:ListItem>
<asp:ListItem Value="glother3">Note</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w31"></asp:TextBox></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w32"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w33"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w34"></asp:ImageButton>&nbsp;<asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w35"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w36"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w37"></asp:ImageButton>&nbsp;<asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w38"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w39"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w40">
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1" __designer:wfdid="w41"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" __designer:wfdid="w42"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2" __designer:wfdid="w43"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear" __designer:wfdid="w44"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w45"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w46"></asp:ImageButton> <asp:ImageButton id="btnPrint" onclick="btnPrint_Click" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w47"></asp:ImageButton></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server" __designer:wfdid="w48"></asp:LinkButton></TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" DataKeyNames="glmstoid" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None" __designer:wfdid="w49" AllowSorting="True" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="glmstoid" DataNavigateUrlFormatString="~\Accounting\trnMemoJournal.aspx?oid={0}" DataTextField="glother1" HeaderText="Memo No." SortExpression="glother1">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="gldate" HeaderText="Memo Date" SortExpression="gldate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glflag" HeaderText="Status" SortExpression="glflag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glother3" HeaderText="Note" SortExpression="glother3">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox><asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("glmstoid") %>' Visible="False"></asp:Label>
</ItemTemplate>

<HeaderStyle CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w50"></asp:Label></TD><TD align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Memorial Journal Header" __designer:wfdid="w339" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data" Width="56px" __designer:wfdid="w340"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w341" Visible="False"></asp:Label> <asp:Label id="glmstoid" runat="server" __designer:wfdid="w342" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblInfoNo" runat="server" Text="Memo No." __designer:wfdid="w347"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="glother1" runat="server" CssClass="inpTextDisabled" Width="200px" __designer:wfdid="w348" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label14" runat="server" Text="Currency" __designer:wfdid="w345"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w346">
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Total Debit" Width="61px" __designer:wfdid="w353"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tbTotalDebit" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w354" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Memo Date" __designer:wfdid="w349"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="gldate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w350" AutoPostBack="True" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w351" Height="16px"></asp:ImageButton> <asp:Label id="Label7" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w352"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" Width="73px" __designer:wfdid="w357"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="glother3" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w358" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Total Credit" Width="71px" __designer:wfdid="w355"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="tbTotalCredit" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w356" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w359"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="glflag" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w360" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w361" TargetControlID="gldate" PopupButtonID="imbDate" Format="MM/dd/yyyy">
            </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w362" TargetControlID="gldate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
            </ajaxToolkit:MaskedEditExtender> <asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w344" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Memorial Journal Detail" __designer:wfdid="w363" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w364" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="glseq" runat="server" __designer:wfdid="w365" Visible="False">1</asp:Label> <asp:Label id="gldtloid" runat="server" __designer:wfdid="w366" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Account Group" __designer:wfdid="w367"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctggroup" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w368" AutoPostBack="True">
            </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label12" runat="server" Text="Flag" __designer:wfdid="w369"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="gldbcr" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w370">
                <asp:ListItem Value="D">DEBIT</asp:ListItem>
                <asp:ListItem Value="C">CREDIT</asp:ListItem>
            </asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Account" __designer:wfdid="w371"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w372">
            </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Amount" __designer:wfdid="w373"></asp:Label> <asp:Label id="Label10" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w374"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="glamt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w375" MaxLength="12"></asp:TextBox></TD></TR><TR><TD id="TD2" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label15" runat="server" Text="Division" __designer:wfdid="w376"></asp:Label></TD><TD id="TD5" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD6" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="groupoid" runat="server" CssClass="inpText" Width="305px" __designer:wfdid="w377">
            </asp:DropDownList></TD><TD id="TD4" class="Label" align=left runat="server" Visible="false"></TD><TD id="TD3" class="Label" align=center runat="server" Visible="false"></TD><TD id="TD1" class="Label" align=left runat="server" Visible="false"></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w378"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="glnote" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w379" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w380" TargetControlID="glamt" ValidChars="1234567890,.">
                </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w381"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w382"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w383" GridLines="None" AutoGenerateColumns="False" CellPadding="4" PageSize="5" DataKeyNames="glseq">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glamtdb" HeaderText="Debit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glamtcr" HeaderText="Credit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="groupdesc" HeaderText="Division">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w384"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w385"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update&nbsp;By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w386"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w387"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w388" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w389" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w390" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w391"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w392" AlternateText="Show COA"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w393" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="progressBackgroundFilter" class="progressBackgroundFilter"></DIV><DIV id="processMessage" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w394"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Memorial Journal :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" Font-Bold="True" ForeColor="White" Font-Size="Small"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" Width="24px" Height="24px" ImageUrl="~/Images/error.jpg"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" Drag="True" PopupDragHandleControlID="lblCaption" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" Visible="False" CausesValidation="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlCOAPosting" runat="server" CssClass="modalBox" Visible="False"
                Width="600px">
                <table style="width: 100%">
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:Label ID="lblCOAPosting" runat="server" Font-Bold="True" Font-Size="Medium"
                                Text="COA - Posting Rate :"></asp:Label>
                            <asp:DropDownList ID="DDLRateType" runat="server" AutoPostBack="True" CssClass="inpText"
                                Font-Bold="True" Font-Size="Medium" Width="125px">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3">
                            <asp:GridView ID="gvCOAPosting" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" Width="99%">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="Label" colspan="3" style="height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:LinkButton ID="lkbCloseCOAPosting" runat="server">[ CLOSE ]</asp:LinkButton></td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button ID="btnHideCOAPosting" runat="server" Visible="False" />
            <ajaxToolkit:ModalPopupExtender ID="mpeCOAPosting" runat="server" BackgroundCssClass="modalBackground"
                Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting"
                TargetControlID="btnHideCOAPosting">
            </ajaxToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

