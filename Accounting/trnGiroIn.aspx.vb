Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_IncomingGiroRealization
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"

    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If refoid.Text = "" Then
            sError &= "- Please select BGM NO. field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sError &= "- Please fill REALIZATION DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- REALIZATION DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select BANK ACCOUNT field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetSumDetail() As DataTable
        Dim dtRet As DataTable = New DataTable()
        dtRet.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtRet.Columns.Add("acctgamt", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtidr", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtusd", Type.GetType("System.Double"))
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            Dim sOid As String = ""
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If Not sOid.Contains("[" & dt.Rows(C1)("refacctgoid").ToString & "]") Then
                    sOid &= "[" & dt.Rows(C1)("refacctgoid").ToString & "],"
                End If
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1).Replace("[", "").Replace("]", "")
                Dim sSplit() As String = sOid.Split(",")
                For C1 As Integer = 0 To sSplit.Length - 1
                    Dim dr As DataRow = dtRet.NewRow
                    Dim iOid As Integer = CInt(sSplit(C1))
                    dr("acctgoid") = iOid
                    dr("acctgamt") = Math.Abs(ToDouble(dt.Compute("SUM(payargiroamt)", "refacctgoid=" & iOid).ToString))
                    dr("acctgamtidr") = Math.Abs(ToDouble(dt.Compute("SUM(payargiroamtidr)", "refacctgoid=" & iOid).ToString))
                    dr("acctgamtusd") = Math.Abs(ToDouble(dt.Compute("SUM(payargiroamtusd)", "refacctgoid=" & iOid).ToString))
                    dtRet.Rows.Add(dr)
                Next
            End If
        End If
        Return dtRet
    End Function
#End Region

#Region "Procedures"

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='GIRO IN'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Incoming Giro Realization data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
        InitDDLBank()
    End Sub

    Private Sub InitDDLBank()
        FillDDLAcctg(acctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
    End Sub

    Private Sub GenerateNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    If acctgoid.SelectedValue <> "" Then
                        Dim sNo As String = "BBM-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cashbankoid, cashbankno, CONVERT(VARCHAR(10), cashbankdate, 101) AS cashbankdate, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, cashbankstatus, cashbanknote, 'False' AS checkvalue, divname FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid INNER JOIN QL_mstdivision div ON div.cmpcode=cb.cmpcode WHERE cashbankgroup='GIRO IN'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, cashbankdate) DESC, cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindListGiro()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT 'False' AS checkvalue, cashbankoid, cashbankno, cashbankrefno, cashbankduedate, cashbankamt, (CASE cashbankgroup WHEN 'MUTATIONTO' THEN cb.personoid ELSE s.custoid END) AS custoid, (CASE cashbankgroup WHEN 'MUTATIONTO' THEN '' ELSE custname END) AS custname, cb.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, cashbankamtidr, cashbankamtusd, 'QL_trncashbankmst' AS tblname, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbanktakegiroreal' AS fieldtakegiro,cb.cashbankgroup FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON s.custoid=personoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.giroacctgoid WHERE cb.cmpcode='" & CompnyCode & "' AND cashbanktype='BGM' AND cashbankgroup NOT IN ('RECEIPT GIRO', 'PAYAR GIRO') AND cashbankstatus='Post' AND ISNULL(cashbankres1, '')='' AND cb.curroid=" & curroid.SelectedValue & " AND cb.cashbankduedate<=CONVERT(DATETIME, '" & cashbankdate.Text & " 23:59:59') "
        sSql &= " UNION ALL "
		sSql &= "SELECT 'False' AS checkvalue, cashbankgloid AS cashbankoid, cashbankno, cashbankglrefno AS cashbankrefno, cashbankglduedate AS cashbankduedate, cashbankglamt AS cashbankamt,s.custoid,s.custname AS custname, cb.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, cashbankglamtidr AS cashbankamtidr, cashbankglamtusd AS cashbankamtusd, 'QL_trncashbankgl' AS tblname, 'cashbankgloid' AS fieldoid, 'cashbankglgiroflag' AS fieldstatus, 'cashbankgltakegiroreal' AS fieldtakegiro,cb.cashbankgroup FROM QL_trncashbankgl gl INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid INNER JOIN QL_mstcust s ON s.custoid=cb.personoid INNER JOIN QL_mstacctg a ON a.acctgoid=cb.giroacctgoid WHERE gl.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGM' AND cashbankgroup='RECEIPT GIRO' AND cashbankstatus='Post' AND cashbankglgiroflag='' AND cb.curroid=" & curroid.SelectedValue & " AND cb.cashbankduedate<=CONVERT(DATETIME, '" & cashbankdate.Text & " 23:59:59')"
        sSql &= " UNION ALL "
		sSql &= "SELECT 'False' AS checkvalue, payaroid AS cashbankoid, cashbankno, payarrefno AS cashbankrefno, payarduedate AS cashbankduedate, payaramt AS cashbankamt, s.custoid AS custoid,c.custname AS custname, pay.giroacctgoid AS acctgoid, ('(' + acctgcode + ') ' + acctgdesc) AS acctgdesc, payaramtidr AS cashbankamtidr, payaramtusd AS cashbankamtusd, 'QL_trnpayar' AS tblname, 'payaroid' AS fieldoid, 'payargiroflag' AS fieldstatus, 'payartakegiroreal' AS fieldtakegiro,cb.cashbankgroup FROM QL_trnpayar pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstcust s ON s.custoid=cb.personoid INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid INNER JOIN QL_mstcust c ON c.custoid=pay.custoid WHERE pay.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbanktype='BGM' AND cashbankgroup='PAYAR GIRO' AND cashbankstatus='Post' AND payargiroflag='' AND cb.curroid=" & curroid.SelectedValue & " AND cb.cashbankduedate<=CONVERT(DATETIME, '" & cashbankdate.Text & " 23:59:59')"
        sSql &= ") AS tbl_GiroIn ORDER BY cashbankduedate"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "tbl_GiroIn")
        If dt.Rows.Count > 0 Then
            Session("TblListGiro") = dt
            Session("TblListGiroView") = Session("TblListGiro")
            gvListGiro.DataSource = Session("TblListGiroView")
            gvListGiro.DataBind()
            cProc.SetModalPopUpExtender(btnHideListGiro, pnlListGiro, mpeListGiro, True)
        Else
            showMessage("There is no Incoming Giro Posting data available for this time!", 2)
        End If
    End Sub

    Private Sub UpdateCheckedListGiro()
        If Session("TblListGiro") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListGiro")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListGiro.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListGiro.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListGiro") = dt
        End If
        If Session("TblListGiroView") IsNot Nothing Then
            Dim dt As DataTable = Session("TblListGiroView")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvListGiro.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvListGiro.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                            sOid = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If dv.Count = 1 Then
                        If cbCheck = True Then
                            dv(0)("checkvalue") = "True"
                        Else
                            dv(0)("checkvalue") = "False"
                        End If
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblListGiroView") = dt
        End If
    End Sub

    Private Sub CreateTblDetail()
        Dim dtlTable As DataTable = New DataTable("TabelGiroIn")
        dtlTable.Columns.Add("seq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("payargirorefno", Type.GetType("System.String"))
        dtlTable.Columns.Add("refacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("refno", Type.GetType("System.String"))
        dtlTable.Columns.Add("custoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("custname", Type.GetType("System.String"))
        dtlTable.Columns.Add("payargiroduedate", Type.GetType("System.String"))
        dtlTable.Columns.Add("payargiroamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payargironote", Type.GetType("System.String"))
        dtlTable.Columns.Add("payargiroamtidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payargiroamtusd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("tblname", Type.GetType("System.String"))
        dtlTable.Columns.Add("fieldoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fieldstatus", Type.GetType("System.String"))
        dtlTable.Columns.Add("fieldtakegiro", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        seq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                seq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        refoid.Text = ""
        payargirorefno.Text = ""
        refacctgoid.Text = ""
        refno.Text = ""
        custoid.Text = ""
        custname.Text = ""
        payargiroduedate.Text = ""
        payargiroamt.Text = ""
        payargironote.Text = ""
        gvDtl.SelectedIndex = -1
        CountTotalAmt()
        btnSearchGiro.Visible = True
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        cashbankdate.Enabled = bVal : cashbankdate.CssClass = sCss : btnDate.Visible = bVal
        curroid.Enabled = bVal : curroid.CssClass = sCss
        acctgoid.Enabled = bVal : acctgoid.CssClass = sCss
    End Sub

    Private Sub CountTotalAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("payargiroamt").ToString)
            Next
        End If
        cashbankamt.Text = ToMaskEdit(dVal, 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, acctgoid, curroid, cashbankamt, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime FROM QL_trncashbankmst cb WHERE cashbankoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
                cashbankamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankamt").ToString)), 4)
                cashbanknote.Text = Trim(xreader("cashbanknote").ToString)
                cashbankstatus.Text = Trim(xreader("cashbankstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        btnShowCOA.Visible = False
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            btnShowCOA.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If

        sSql = "SELECT 0 AS seq, pay.refoid, payargirorefno, refacctgoid, cashbankno AS refno, pay.custoid, custname, CONVERT(VARCHAR(10), payargiroduedate, 101) AS payargiroduedate, payargiroamt, payargironote, payargiroamtidr, payargiroamtusd, (CASE pay.reftype WHEN 'Pay A/R Giro In' THEN 'QL_trncashbankmst' ELSE pay.reftype END) AS tblname, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payaroid' WHEN 'QL_trncashbankgl' THEN 'cashbankgloid' ELSE 'cashbankoid' END) AS fieldoid, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payargiroflag' WHEN 'QL_trncashbankgl' THEN 'cashbankglgiroflag' ELSE 'cashbankres1' END) AS fieldstatus, (CASE pay.reftype WHEN 'QL_trnpayar' THEN 'payartakegiroreal' WHEN 'QL_trncashbankgl' THEN 'cashbankgltakegiroreal' ELSE 'cashbanktakegiroreal' END) AS fieldtakegiro FROM QL_trnpayargiro pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.refoid LEFT JOIN QL_mstcust s ON s.custoid=pay.custoid WHERE pay.cashbankoid=" & sOid
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trnpayargiro")
        For C1 As Integer = 0 To dtTable.Rows.Count - 1
            dtTable.Rows(C1)("seq") = C1 + 1
        Next
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptGiroIn.rpt"))
            Dim sWhere As String = "WHERE cb.cashbankgroup='GIRO IN'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cb.cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cb.cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cb.cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "IncomingGiroRealizationPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnGiroIn.aspx")
        End If
        If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Incoming Giro Realization"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                GenerateNo()
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("WarningListGiro") Is Nothing And Session("WarningListGiro") <> "" Then
            If lblPopUpMsg.Text = Session("WarningListGiro") Then
                Session("WarningListGiro") = Nothing
                cProc.SetModalPopUpExtender(btnHideListGiro, pnlListGiro, mpeListGiro, True)
            End If
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, cb.updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' "
        If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnGiroIn.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLBank()
        If i_u.Text = "New Data" Then
            GenerateNo()
        End If
    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankdate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateNo()
        End If
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateNo()
        End If
    End Sub

    Protected Sub btnSearchGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchGiro.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If cashbankdate.Text = "" Then
            showMessage("Please fill Realization Date first!", 2)
            Exit Sub
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                showMessage("Realization Date is invalid! " & sErr, 2)
                Exit Sub
            End If
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If acctgoid.SelectedValue = "" Then
            showMessage("Please select Bank Account first!", 2)
            Exit Sub
        End If
        FilterTextListGiro.Text = "" : FilterDDLListGiro.SelectedIndex = -1
        gvListGiro.SelectedIndex = -1 : Session("TblListGiro") = Nothing
        Session("TblListGiroView") = Nothing : gvListGiro.DataSource = Nothing : gvListGiro.DataBind()
        BindListGiro()
    End Sub

    Protected Sub btnFindListGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListGiro.Click
        UpdateCheckedListGiro()
        Dim dt As DataTable = Session("TblListGiro")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = FilterDDLListGiro.SelectedValue & " LIKE '%" & Tchar(FilterTextListGiro.Text) & "%'"
        If dv.Count > 0 Then
            Session("TblListGiroView") = dv.ToTable
            gvListGiro.DataSource = Session("TblListGiroView")
            gvListGiro.DataBind()
            mpeListGiro.Show()
        Else
            dv.RowFilter = ""
            Session("WarningListGiro") = "Incoming Giro Posting data can't be found!"
            showMessage(Session("WarningListGiro"), 2)
        End If
    End Sub

    Protected Sub btnAllListGiro_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListGiro.Click
        UpdateCheckedListGiro()
        FilterTextListGiro.Text = "" : FilterDDLListGiro.SelectedIndex = -1 : gvListGiro.SelectedIndex = -1
        Session("TblListGiroView") = Session("TblListGiro")
        gvListGiro.DataSource = Session("TblListGiroView")
        gvListGiro.DataBind()
        mpeListGiro.Show()
    End Sub

    Protected Sub gvListGiro_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListGiro.PageIndexChanging
        UpdateCheckedListGiro()
        gvListGiro.PageIndex = e.NewPageIndex
        gvListGiro.DataSource = Session("TblListGiroView")
        gvListGiro.DataBind()
        mpeListGiro.Show()
    End Sub

    Protected Sub gvListGiro_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvListGiro.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "MM/dd/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 4)
        End If
    End Sub

    Protected Sub cbHdrListGiro_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        For C1 As Integer = 0 To gvListGiro.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvListGiro.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = sender.Checked
                    End If
                Next
            End If
        Next
        mpeListGiro.Show()
    End Sub

    Protected Sub lbAddToListGiro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbAddToListGiro.Click
        UpdateCheckedListGiro()
        Dim dt As DataTable = Session("TblListGiro")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "checkvalue='True'"
        If dv.Count > 0 Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim iSeq As Integer = objTable.Rows.Count + 1
            Dim objView As DataView = objTable.DefaultView
            For C1 As Integer = 0 To dv.Count - 1
                objView.RowFilter = "refoid=" & dv(C1)("cashbankoid")
                If objView.Count = 0 Then
                    Dim rv As DataRowView = objView.AddNew()
                    rv.BeginEdit()
                    rv("seq") = iSeq
                    rv("refoid") = dv(C1)("cashbankoid")
                    rv("payargirorefno") = dv(C1)("cashbankrefno").ToString
                    rv("refacctgoid") = dv(C1)("acctgoid")
                    rv("refno") = dv(C1)("cashbankno").ToString
                    rv("custoid") = dv(C1)("custoid")
                    rv("custname") = dv(C1)("custname").ToString
                    rv("payargiroduedate") = Format(dv(C1)("cashbankduedate"), "MM/dd/yyyy")
                    rv("payargiroamt") = ToDouble(dv(C1)("cashbankamt").ToString)
                    rv("payargiroamtidr") = ToDouble(dv(C1)("cashbankamtidr").ToString)
                    rv("payargiroamtusd") = ToDouble(dv(C1)("cashbankamtusd").ToString)
                    rv("payargironote") = ""
                    rv("tblname") = dv(C1)("tblname").ToString
                    rv("fieldoid") = dv(C1)("fieldoid").ToString
                    rv("fieldstatus") = dv(C1)("fieldstatus").ToString
                    rv("fieldtakegiro") = dv(C1)("fieldtakegiro").ToString
                    rv.EndEdit()
                    iSeq += 1
                End If
                objView.RowFilter = ""
            Next
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListGiro, pnlListGiro, mpeListGiro, False)
        Else
            dv.RowFilter = ""
            Session("WarningListGiro") = "Please select some Incoming Giro Posting data first!"
            showMessage(Session("WarningListGiro"), 2)
        End If
    End Sub

    Protected Sub lbSelectAllListGiro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbSelectAllListGiro.Click
        UpdateCheckedListGiro()
        Dim dt As DataTable = Session("TblListGiro")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = ""
        If dv.Count > 0 Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDetail()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim iSeq As Integer = objTable.Rows.Count + 1
            Dim objView As DataView = objTable.DefaultView
            For C1 As Integer = 0 To dv.Count - 1
                objView.RowFilter = "cashbankoid=" & dv(C1)("cashbankoid")
                If objView.Count = 0 Then
                    Dim rv As DataRowView = objView.AddNew()
                    rv.BeginEdit()
                    rv("seq") = iSeq
                    rv("refoid") = dv(C1)("cashbankoid")
                    rv("payargirorefno") = dv(C1)("cashbankrefno").ToString
                    rv("refacctgoid") = dv(C1)("acctgoid")
                    rv("refno") = dv(C1)("cashbankno").ToString
                    rv("custoid") = dv(C1)("custoid")
                    rv("custname") = dv(C1)("custname").ToString
                    rv("payargiroduedate") = Format(dv(C1)("cashbankduedate"), "MM/dd/yyyy")
                    rv("payargiroamt") = ToDouble(dv(C1)("cashbankamt").ToString)
                    rv("payargiroamtidr") = ToDouble(dv(C1)("cashbankamtidr").ToString)
                    rv("payargiroamtusd") = ToDouble(dv(C1)("cashbankamtusd").ToString)
                    rv("payargironote") = ""
                    rv.EndEdit()
                    iSeq += 1
                End If
                objView.RowFilter = ""
            Next
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            dv.RowFilter = ""
            cProc.SetModalPopUpExtender(btnHideListGiro, pnlListGiro, mpeListGiro, False)
        Else
            dv.RowFilter = ""
            Session("WarningListGiro") = "Please select some Incoming Giro Posting data first!"
            showMessage(Session("WarningListGiro"), 2)
        End If
    End Sub

    Protected Sub lkbCloseListGiro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListGiro.Click
        cProc.SetModalPopUpExtender(btnHideListGiro, pnlListGiro, mpeListGiro, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            Dim objTable As DataTable = Session("TblDtl")
            Dim objRow As DataRow
            objRow = objTable.Rows(seq.Text - 1)
            objRow.BeginEdit()
            objRow("payargironote") = payargironote.Text
            objRow.EndEdit()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            seq.Text = gvDtl.SelectedDataKey.Item("seq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "seq=" & seq.Text
                refoid.Text = dv.Item(0).Item("refoid").ToString
                payargirorefno.Text = dv.Item(0).Item("payargirorefno").ToString
                refacctgoid.Text = dv.Item(0).Item("refacctgoid").ToString
                refno.Text = dv.Item(0).Item("refno").ToString
                custoid.Text = dv.Item(0).Item("custoid").ToString
                custname.Text = dv.Item(0).Item("custname").ToString
                payargiroduedate.Text = dv.Item(0).Item("payargiroduedate").ToString
                payargiroamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("payargiroamt").ToString), 4)
                payargironote.Text = dv.Item(0).Item("payargironote").ToString
                btnSearchGiro.Visible = False
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("seq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                Dim sNo As String = cashbankno.Text
                GenerateNo()
                If sNo <> cashbankno.Text Then
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            payargirooid.Text = GenerateID("QL_TRNPAYARGIRO", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim cRate As New ClassRate()
            Dim dtSum As DataTable = Nothing
            periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
            Dim sDate As String = cashbankdate.Text
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim dTotalIDR As Double = 0, dTotalUSD As Double = 0
            If cashbankstatus.Text = "Post" Then
                If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sDate) Then
                    showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : cashbankstatus.Text = "In Process" : Exit Sub
                End If
                cRate.SetRateValue(CInt(curroid.SelectedValue), sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                dtSum = GetSumDetail()
                dTotalIDR = ToDouble(dtSum.Compute("SUM(acctgamtidr)", "").ToString)
                dTotalUSD = ToDouble(dtSum.Compute("SUM(acctgamtusd)", "").ToString)
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', 'BBM', 'GIRO IN', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(cashbankamt.Text) & ", " & dTotalIDR & ", " & dTotalUSD & ", 0, '" & cashbankdate.Text & "', '', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(cashbankamt.Text) & ", cashbankamtidr=" & dTotalIDR & ", cashbankamtusd=" & dTotalUSD & ", cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trncashbankmst SET cashbankres1='', cashbanktakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trncashbankmst', 'Pay A/R Giro In'))"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trncashbankgl SET cashbankglgiroflag='', cashbankgltakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankgloid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trncashbankgl'))"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trnpayar SET payargiroflag='', payartakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND payaroid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trnpayar'))"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trnpayargiro (cmpcode, payargirooid, cashbankoid, custoid, reftype, refoid, refacctgoid, payargirorefno, payargiroduedate, payargiroamt, payargiroamtidr, payargiroamtusd, payargironote, payargirostatus, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(payargirooid.Text)) & ", " & cashbankoid.Text & ", " & objTable.Rows(c1)("custoid").ToString & ", 'Pay A/R Giro In', " & objTable.Rows(c1)("refoid").ToString & ", " & objTable.Rows(c1)("refacctgoid").ToString & ", '" & objTable.Rows(c1)("payargirorefno").ToString & "', '" & objTable.Rows(c1)("payargiroduedate").ToString & "', " & ToDouble(objTable.Rows(c1)("payargiroamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("payargiroamtidr").ToString) & ", " & ToDouble(objTable.Rows(c1)("payargiroamtusd").ToString) & ", '" & Tchar(objTable.Rows(c1)("payargironote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE " & objTable.Rows(c1)("tblname").ToString & " SET " & objTable.Rows(c1)("fieldstatus").ToString & "='Closed', " & objTable.Rows(c1)("fieldtakegiro").ToString & "='" & cashbankdate.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & objTable.Rows(c1)("fieldoid").ToString & "=" & objTable.Rows(c1)("refoid").ToString
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
			'UPDATE SAVING PROMO AND COMMISSION CALCULATION
                        'If cashbankstatus.Text = "Post" Then
                        'START COMMISSION 
                        'CHECK IF DP AR
                        'sSql = "select count(-1) from QL_tis_trninvinsentiveratehist where transtype='INHOLD' and refname='QL_trnpayar' and refoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BLM' and cashbankgroup='AR' and giroacctgoid=(select dparoid from QL_trndpar where cashbankoid=" & objTable.Rows(c1)("refoid").ToString & "))"
                        'xCmd.CommandText = sSql
                        'If xCmd.ExecuteScalar > 0 Then
                        'sSql = "select histoid,invoiceoid,commissionamts1,commissionamts2 from QL_tis_trninvinsentiveratehist where transtype='INHOLD' and refname='QL_trnpayar' and refoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BLM' and cashbankgroup='AR' and giroacctgoid=(select dparoid from QL_trndpar where cashbankoid=" & objTable.Rows(c1)("refoid").ToString & "))"
                        'Dim dset As New DataSet
                        'Dim adapter As New SqlDataAdapter
                        'xCmd.CommandText = sSql
                        'adapter.SelectCommand = xCmd
                        'adapter.Fill(dset)
                        'Dim dtb As DataTable = dset.Tables(0)
                        'For xx1 As Integer = 0 To dtb.Rows.Count - 1
                        ' sSql = "UPDATE QL_tis_trninvinsentiveratehist SET transtype='IN',refdate='" & cashbankdate.Text & "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE histoid=" & dtb.Rows(xx1).Item("histoid") & ""
                        'xCmd.CommandText = sSql
                        'xCmd.ExecuteNonQuery()
                        'sSql = "UPDATE QL_tis_trninvinsentiverate SET commissionamts1=commissionamts1+" & dtb.Rows(xx1).Item("commissionamts1") & ", commissionamts2=commissionamts2+" & dtb.Rows(xx1).Item("commissionamts2") & ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND invoiceoid=" & dtb.Rows(xx1).Item("invoiceoid") & ""
                        'xCmd.CommandText = sSql
                        'xCmd.ExecuteNonQuery()
                        'Next
                        'Else
                        'CHECK IF GIRO
                        'sSql = "select count(-1) from QL_tis_trninvinsentiveratehist where transtype='INHOLD' and refname='QL_trnpayar' and refoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BGM' and cashbankgroup='AR' and cashbankrefno='" & objTable.Rows(c1)("payargirorefno").ToString & "')"
                        'xCmd.CommandText = sSql
                        'If xCmd.ExecuteScalar > 0 Then
                        'sSql = "select histoid,invoiceoid,commissionamts1,commissionamts2 from QL_tis_trninvinsentiveratehist where transtype='INHOLD' and refname='QL_trnpayar' and refoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BGM' and cashbankgroup='AR' and cashbankrefno='" & objTable.Rows(c1)("payargirorefno").ToString & "')"
                        'Dim dset As New DataSet
                        'Dim adapter As New SqlDataAdapter
                        'xCmd.CommandText = sSql
                        'adapter.SelectCommand = xCmd
                        'adapter.Fill(dset)
                        'Dim dtb As DataTable = dset.Tables(0)
                        'For xx1 As Integer = 0 To dtb.Rows.Count - 1
                        '    sSql = "UPDATE QL_tis_trninvinsentiveratehist SET transtype='IN',refdate='" & cashbankdate.Text & "',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE histoid=" & dtb.Rows(xx1).Item("histoid") & ""
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        '    sSql = "UPDATE QL_tis_trninvinsentiverate SET commissionamts1=commissionamts1+" & dtb.Rows(xx1).Item("commissionamts1") & ", commissionamts2=commissionamts2+" & dtb.Rows(xx1).Item("commissionamts2") & ",upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & CompnyCode & "' AND invoiceoid=" & dtb.Rows(xx1).Item("invoiceoid") & ""
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        'Next
                        'End If
                        'End If
                        'END COMMISSION 

                        'START PROMO 
                        'CHECK IF DP AR
                        'sSql = "select count(-1) from QL_tis_trnspromohist where transtype='INHOLD' and usageoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BLM' and cashbankgroup='AR' and giroacctgoid=(select dparoid from QL_trndpar where cashbankoid=" & objTable.Rows(c1)("refoid").ToString & "))"
                        'xCmd.CommandText = sSql
                        'If xCmd.ExecuteScalar > 0 Then
                        'sSql = "select transoid from QL_tis_trnspromohist where transtype='INHOLD' and usageoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BLM' and cashbankgroup='AR' and giroacctgoid=(select dparoid from QL_trndpar where cashbankoid=" & objTable.Rows(c1)("refoid").ToString & "))"
                        'Dim dset As New DataSet
                        'Dim adapter As New SqlDataAdapter
                        'xCmd.CommandText = sSql
                        'adapter.SelectCommand = xCmd
                        'adapter.Fill(dset)
                        'Dim dtb As DataTable = dset.Tables(0)
                        'For xx1 As Integer = 0 To dtb.Rows.Count - 1
                        '    sSql = "UPDATE QL_tis_trnspromohist SET transtype='IN',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE transoid=" & dtb.Rows(xx1).Item("transoid") & ""
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        'Next
                        'Else
                        'CHECK IF GIRO
                        'sSql = "select count(-1) from QL_tis_trnspromohist where transtype='INHOLD' and usageoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BGM' and cashbankgroup='AR' and cashbankrefno='" & objTable.Rows(c1)("payargirorefno").ToString & "')"
                        'xCmd.CommandText = sSql
                        'If xCmd.ExecuteScalar > 0 Then
                        'sSql = "select transoid from QL_tis_trnspromohist where transtype='INHOLD' and usageoid in (select cashbankoid from QL_trncashbankmst where cashbanktype='BGM' and cashbankgroup='AR' and cashbankrefno='" & objTable.Rows(c1)("payargirorefno").ToString & "')"
                        'Dim dset As New DataSet
                        'Dim adapter As New SqlDataAdapter
                        'xCmd.CommandText = sSql
                        'adapter.SelectCommand = xCmd
                        'adapter.Fill(dset)
                        'Dim dtb As DataTable = dset.Tables(0)
                        'For xx1 As Integer = 0 To dtb.Rows.Count - 1
                        '    sSql = "UPDATE QL_tis_trnspromohist SET transtype='IN',upduser='" & Session("UserID") & "',updtime=CURRENT_TIMESTAMP WHERE transoid=" & dtb.Rows(xx1).Item("transoid") & ""
                        '    xCmd.CommandText = sSql
                        '    xCmd.ExecuteNonQuery()
                        'Next
                        'End If
                        'End If
                        'END PROMO 
                        'End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(payargirooid.Text)) & " WHERE tablename='QL_TRNPAYARGIRO' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If cashbankstatus.Text = "Post" Then
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Giro In|No=" & cashbankno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 0, 0, 0, 0)"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    Dim iSeq As Integer = 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'D', " & ToDouble(cashbankamt.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dTotalIDR & ", " & dTotalUSD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1
                    iSeq += 1
                    For c1 As Int16 = 0 To dtSum.Rows.Count - 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & dtSum.Rows(c1)("acctgoid").ToString & ", 'C', " & ToDouble(dtSum.Rows(c1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dtSum.Rows(c1)("acctgamtidr").ToString) & ", " & ToDouble(dtSum.Rows(c1)("acctgamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Bank No. have been regenerated because being used by another data. Your new Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select Incoming Giro Realization data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "UPDATE QL_trncashbankmst SET cashbankres1='', cashbanktakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trncashbankmst', 'Pay A/R Giro In'))"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trncashbankgl SET cashbankglgiroflag='', cashbankgltakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankgloid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trncashbankgl'))"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_trnpayar SET payargiroflag='', payartakegiroreal='01/01/1900' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND payaroid IN (SELECT refoid FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND reftype IN ('QL_trnpayar'))"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnpayargiro WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnGiroIn.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub
#End Region

End Class