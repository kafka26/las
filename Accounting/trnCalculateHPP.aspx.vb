Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_CalculateHPP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetSelectedOid() As String
        Dim sRet As String = ""
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        If CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = True Then
                            sRet &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                        End If
                    End If
                Next
            End If
        Next
        If sRet <> "" Then
            sRet = Left(sRet, sRet.Length - 1)
        End If
        Return sRet
    End Function

    Private Function GetSummaryGiro(ByVal sOids As String) As DataTable
        Dim dtRet As DataTable = Nothing
        sSql = "SELECT cashbankoid, cashbankno, cashbankamt, cashbankamtidr, cashbankamtusd, acctgoid, curroid, giroacctgoid, '' AS newcashbankno FROM QL_trncashbankmst WHERE cashbankoid IN (" & sOids & ")"
        dtRet = cKon.ambiltabel(sSql, "QL_girosummary")
        Return dtRet
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub InitDDL()
        ' Init DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
    End Sub

    Private Sub BindGiroData()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT 0 AS nmr, cb.cashbankoid, cb.cashbankno, cb.cashbankrefno, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN '' ELSE s.suppname END) AS suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cb.cashbanktakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) AS cashbankduedate, cashbankamt, cb.createuser, cb.cashbankamtidr, cb.cashbankamtusd, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN cb.personoid ELSE s.suppoid END) AS suppoid, cb.cashbanknote, cb.giroacctgoid, cb.acctgoid, cb.curroid, '' AS giroflagnote, 'QL_trncashbankmst' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbankres2' AS fieldnote, 'cashbanktakegiroreal' AS fieldtakegiro FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON s.suppoid=(CASE cb.cashbankgroup WHEN 'EXPENSE' THEN cb.refsuppoid ELSE cb.personoid END) WHERE cb.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cb.cashbanktype='BGK' AND cashbankgroup NOT IN ('EXPENSE GIRO', 'PAYAP GIRO') AND cb.cashbankstatus='Post' AND ISNULL(cb.cashbankres1, '')='' AND cb.cashbankdate<=CONVERT(DATETIME, '" & tbDate.Text & " 23:59:59')"
        sSql &= " UNION ALL "
        sSql &= "SELECT 0 AS nmr, cashbankgloid AS cashbankoid, cb.cashbankno, cashbankglrefno AS cashbankrefno, suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cashbankgltakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cashbankglduedate, 101) AS cashbankduedate, cashbankglamt AS cashbankamt, cb.createuser, cashbankglamtidr AS cashbankamtidr, cashbankglamtusd AS cashbankamtusd, refsuppoid AS suppoid, cashbankglnote AS cashbanknote, 0 AS giroacctgoid, cb.acctgoid, cb.curroid, '' AS giroflagnote, 'QL_trncashbankgl' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankgloid' AS fieldoid, 'cashbankglgiroflag' AS fieldstatus, 'cashbankglgironote' AS fieldnote, 'cashbankgltakegiroreal' AS fieldtakegiro FROM QL_trncashbankgl gl INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=gl.cmpcode AND cb.cashbankoid=gl.cashbankoid INNER JOIN QL_mstsupp s ON s.suppoid=refsuppoid WHERE gl.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cb.cashbanktype='BGK' AND cashbankgroup='EXPENSE GIRO' AND cb.cashbankstatus='Post' AND cashbankglgiroflag='' AND cb.cashbankdate<=CONVERT(DATETIME, '" & tbDate.Text & " 23:59:59')"
        sSql &= " UNION ALL "
        sSql &= "SELECT 0 AS nmr, payapoid AS cashbankoid, cb.cashbankno, payaprefno AS cashbankrefno, suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), payaptakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), payapduedate, 101) AS cashbankduedate, payapamt AS cashbankamt, cb.createuser, payapamtidr AS cashbankamtidr, payapamtusd AS cashbankamtusd, cb.personoid AS suppoid, payapnote AS cashbanknote, pay.giroacctgoid, cb.acctgoid, cb.curroid, '' AS giroflagnote, 'QL_trnpayap' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'payapoid' AS fieldoid, 'payapgiroflag' AS fieldstatus, 'payapgironote' AS fieldnote, 'payaptakegiroreal' AS fieldtakegiro FROM QL_trnpayap pay INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=pay.cmpcode AND cb.cashbankoid=pay.cashbankoid INNER JOIN QL_mstsupp s ON s.suppoid=cb.personoid WHERE pay.cmpcode='" & DDLBusUnit.SelectedValue & "' AND cb.cashbanktype='BGK' AND cashbankgroup='PAYAP GIRO' AND cb.cashbankstatus='Post' AND payapgiroflag='' AND cb.cashbankdate<=CONVERT(DATETIME, '" & tbDate.Text & " 23:59:59')"
        sSql &= ") AS QL_girodetail ORDER BY CONVERT(DATETIME, cashbanktakegiro), cashbankno"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_girodetail")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
        Next
        dt.AcceptChanges()
        Session("TblDataGiro") = dt
        gvDtl.DataSource = Session("TblDataGiro")
        gvDtl.DataBind()
    End Sub

    Private Sub UpdateNote()
        If Session("TblDataGiro") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDataGiro")
            For C1 As Integer = 0 To gvDtl.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim sNote As String = ""
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(10).Controls
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                            sNote = CType(myControl, System.Web.UI.WebControls.TextBox).Text
                        End If
                    Next
                    dt.Rows(C1)("giroflagnote") = sNote
                End If
            Next
            dt.AcceptChanges()
            Session("TblDataGiro") = dt
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnGiroOutFlag.aspx")
        End If
        If checkPagePermission("~\Accounting\trnGiroOutFlag.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Outgoing Giro Flag"
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POSTING this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            tbDate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("Success") Is Nothing And Session("Success") <> "" Then
            Response.Redirect("~\Accounting\trnGiroOutFlag.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnShowData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowData.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select BUSINESS UNIT first", 2)
            Exit Sub
        End If
        Dim sErr As String = ""
        If tbDate.Text = "" Then
            sErr = "Please fill DATE first!"
        Else
            If Not IsValidDate(tbDate.Text, "MM/dd/yyyy", sErr) Then
                sErr = "DATE is invalid! " & sErr
            Else
                If CDate(tbDate.Text & " 23:59:59") > CDate(Format(GetServerTime(), "MM/dd/yyyy 23:59:59")) Then
                    sErr = "DATE must be less or equal than TODAY!"
                End If
            End If
        End If
        If sErr <> "" Then
            showMessage(sErr, 2)
            Exit Sub
        End If
        BindGiroData()
    End Sub

    Protected Sub cbHeader_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bCheck As Boolean = False
        If sender.Checked Then
            bCheck = True
        End If
        For C1 As Integer = 0 To gvDtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvDtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        CType(myControl, System.Web.UI.WebControls.CheckBox).Checked = bCheck
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 4)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnGiroOutFlag.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosting.Click
        Dim sOids As String = GetSelectedOid()
        If sOids = "" Then
            showMessage("Please Select Some Detail Data First!", 2)
            Exit Sub
        End If
        UpdateNote()
        Dim dtGiro As DataTable = Session("TblDataGiro")
        Dim dvGiro As DataView = dtGiro.DefaultView
        dvGiro.RowFilter = "nmr IN (" & sOids & ")"
        Dim sOidPost As String = ""
        Dim sErrMsg As String = ""
        For C1 As Integer = 0 To dvGiro.Count - 1
            If CDate(dvGiro(C1)("cashbanktakegiro").ToString) > CDate(tbDate.Text) Then
                sErrMsg &= "- Date Take Giro for Outgoing Giro Data No. " & dvGiro(C1)("nmr") & " must be less or equal than Filter Date!<BR>"
            End If
            If CDate(dvGiro(C1)("cashbankduedate").ToString) <= CDate(tbDate.Text) Then
                dvGiro(C1)("girostatus") = "Post"
            End If
        Next
        If sErrMsg <> "" Then
            showMessage(sErrMsg, 2)
            dvGiro.RowFilter = "" : Exit Sub
        End If
        dvGiro.RowFilter = ""
        dvGiro.RowFilter = "nmr IN (" & sOids & ") AND girostatus='Post'"
        Dim sOidAcctg As String = ""
        If dvGiro.Count > 0 Then
            For C1 As Integer = 0 To dvGiro.Count - 1
                If Not sOidAcctg.Contains("[" & dvGiro(C1)("acctgoid").ToString & "]") Then
                    sOidAcctg &= "[" & dvGiro(C1)("acctgoid").ToString & "];"
                End If
            Next
        End If
        dvGiro.RowFilter = ""
        If sOidAcctg <> "" Then
            sOidAcctg = Left(sOidAcctg, sOidAcctg.Length - 1)
            Dim sSplitOid() As String = sOidAcctg.Replace("[", "").Replace("]", "").Split(";")
            Dim sNo As String = "BBK-" & Format(CDate(tbDate.Text), "yyMM") & "-"
            Dim iNoSeq As Integer = 0
            For C1 As Integer = 0 To sSplitOid.Length - 1
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' AND acctgoid=" & sSplitOid(C1)
                If GetStrData(sSql) <> "" Then
                    iNoSeq = ToInteger(GetStrData(sSql))
                Else
                    iNoSeq = 1
                End If
                dvGiro.RowFilter = "nmr IN (" & sOids & ") AND girostatus='Post' AND acctgoid=" & sSplitOid(C1)
                For C2 As Integer = 0 To dvGiro.Count - 1
                    dvGiro(C2)("postingcashbankno") = GenNumberString(sNo, "", iNoSeq + C2, DefaultFormatCounter)
                Next
                dvGiro.RowFilter = ""
            Next
        End If
        Dim cashbankoid As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
        Dim payapgirooid As Integer = GenerateID("QL_TRNPAYAPGIRO", CompnyCode)
        Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
        Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
        Dim sPeriod As String = GetDateToPeriodAcctg(CDate(tbDate.Text))
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            dvGiro.RowFilter = "nmr IN (" & sOids & ") AND girostatus=''"
            For C1 As Integer = 0 To dvGiro.Count - 1
                sSql = "UPDATE " & dvGiro(C1)("tblname").ToString & " SET " & dvGiro(C1)("fieldstatus").ToString & "='Post', " & dvGiro(C1)("fieldnote").ToString & "='" & Tchar(dvGiro(C1)("giroflagnote").ToString) & "', " & dvGiro(C1)("fieldtakegiro").ToString & "='" & tbDate.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & dvGiro(C1)("fieldoid").ToString & "=" & dvGiro(C1)("cashbankoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next
            dvGiro.RowFilter = ""
            dvGiro.RowFilter = "nmr IN (" & sOids & ") AND girostatus='Post'"
            For C1 As Integer = 0 To dvGiro.Count - 1
                ' Update BGK Status
                sSql = "UPDATE " & dvGiro(C1)("tblname").ToString & " SET " & dvGiro(C1)("fieldstatus").ToString & "='Closed', " & dvGiro(C1)("fieldnote").ToString & "='" & Tchar(dvGiro(C1)("giroflagnote").ToString) & "', " & dvGiro(C1)("fieldtakegiro").ToString & "='" & tbDate.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & dvGiro(C1)("fieldoid").ToString & "=" & dvGiro(C1)("cashbankoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert BBK (Cashbank)
                sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid & ", '" & sPeriod & "', '" & dvGiro(C1)("postingcashbankno").ToString & "', '" & tbDate.Text & "', 'BBK', 'GIRO OUT', " & dvGiro(C1)("acctgoid") & ", " & dvGiro(C1)("curroid") & ", " & ToDouble(dvGiro(C1)("cashbankamt").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtusd").ToString) & ", 0, '" & tbDate.Text & "', '', 'Pencairan Cash/Bank No. " & dvGiro(C1)("cashbankno").ToString & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert BBK (Pay Giro)
                sSql = "INSERT INTO QL_trnpayapgiro (cmpcode, payapgirooid, cashbankoid, suppoid, reftype, refoid, refacctgoid, payapgirorefno, payapgiroduedate, payapgiroamt, payapgiroamtidr, payapgiroamtusd, payapgironote, payapgirostatus, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & payapgirooid & ", " & cashbankoid & ", " & dvGiro(C1)("suppoid") & ", '" & dvGiro(C1)("tblname").ToString & "', " & dvGiro(C1)("cashbankoid") & ", " & dvGiro(C1)("giroacctgoid") & ", '" & dvGiro(C1)("cashbankrefno").ToString & "', '" & dvGiro(C1)("cashbankduedate").ToString & "', " & ToDouble(dvGiro(C1)("cashbankamt").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtusd").ToString) & ", 'Pencairan Cash/Bank No. " & dvGiro(C1)("cashbankno").ToString & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert Into GL Mst
                sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & tbDate.Text & "', '" & sPeriod & "', 'Giro Out|No=" & dvGiro(C1)("postingcashbankno").ToString & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', 0, 0, 0, 0, 0, 0)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                ' Insert Into GL Dtl
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", 1, " & iGlMstOid & ", " & dvGiro(C1)("giroacctgoid") & ", 'D', " & ToDouble(dvGiro(C1)("cashbankamt").ToString) & ", '" & dvGiro(C1)("postingcashbankno").ToString & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvGiro(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGlDtlOid += 1
                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", 2, " & iGlMstOid & ", " & dvGiro(C1)("acctgoid") & ", 'C', " & ToDouble(dvGiro(C1)("cashbankamt").ToString) & ", '" & dvGiro(C1)("postingcashbankno").ToString & "', '', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvGiro(C1)("cashbankamtidr").ToString) & ", " & ToDouble(dvGiro(C1)("cashbankamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                iGlDtlOid += 1
                iGlMstOid += 1
                payapgirooid += 1
                cashbankoid += 1
            Next
            sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid - 1 & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstoid SET lastoid=" & payapgirooid - 1 & " WHERE tablename='QL_TRNPAYAPGIRO' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            dvGiro.RowFilter = ""
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch exSql As SqlException
            objTrans.Rollback() : conn.Close()
            If ToDouble(Session("ErrorCounter")) < 5 Then
                If exSql.Number = 2627 Then
                    Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                    btnPosting_Click(sender, e)
                    Exit Sub
                Else
                    showMessage(exSql.Message, 1)
                    Exit Sub
                End If
            Else
                showMessage(exSql.Message, 1)
                Exit Sub
            End If
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("Success") = "Selected data have been posted successfully."
        showMessage(Session("Success"), 3)
    End Sub
#End Region

End Class