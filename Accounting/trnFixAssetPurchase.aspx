<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFixAssetPurchase.aspx.vb" Inherits="Accounting_trnFixAssetPurchase" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset Purchase"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                    Width="100%"
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0"><asp:View id="View3" runat="server"><asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 60px" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Date" OnCheckedChanged="cbPeriode_CheckedChanged"></asp:CheckBox><asp:Label id="Label36" runat="server" Text="Date" Visible="False"></asp:Label></TD><TD align=left colSpan=3><asp:TextBox id="txtPeriode1" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 60px" align=left><asp:Label id="Label1" runat="server" Text="Filter"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLfilter" runat="server" Width="120px" CssClass="inpText"><asp:ListItem Value="trnbelifano">No</asp:ListItem>
<asp:ListItem Value="Suppname">Supplier</asp:ListItem>
<asp:ListItem Value="m.trnbelifamstnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="200px" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 60px" align=left><asp:Label id="Label4" runat="server" Text="Status"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLstatus" runat="server" Width="120px" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem>IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; <asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/btnfind.bmp"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 60px" align=right><ajaxToolkit:CalendarExtender id="ce2" runat="server" TargetControlID="txtPeriode1" Format="MM/dd/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" TargetControlID="txtPeriode2" Format="MM/dd/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" CultureName="en-US" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPeriode1" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" CultureName="en-US" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPeriode2" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=4><asp:GridView id="GVFixedAsset" runat="server" Width="100%" OnSelectedIndexChanged="GVFixedAsset_SelectedIndexChanged" PageSize="8" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="cmpcode,trnbelifamstoid" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVFixedAsset_PageIndexChanging" OnRowDataBound="GVFixedAsset_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifano" HeaderText="FA. No">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Compny Code" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadate" HeaderText="Date" SortExpression="trnbelifadate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Pay Term">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifaamtnetto" DataFormatString="{0:#,##0.00}" HeaderText="Netto Amt">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifamstnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifamststatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" Text="Click button Find or View All to view data" CssClass="Important"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                                List of Fixed Asset Purchase :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 100%" vAlign=top colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0"><asp:View id="View1" runat="server"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD id="TD7" align=left><asp:Label id="lblNo" runat="server" Text="Draft No." Visible="False"></asp:Label></TD><TD id="TD2" align=left Visible="false"><asp:Label id="fixmstoid" runat="server" Visible="False"></asp:Label><asp:Label id="CutofDate" runat="server" Visible="False"></asp:Label></TD><TD id="TD3" align=left Visible="true"><asp:Label id="Rateoid" runat="server" Visible="False"></asp:Label><asp:Label id="Rate2oid" runat="server" Visible="False"></asp:Label></TD><TD id="TD1" align=left Visible="true"></TD></TR><TR><TD align=left><asp:Label id="Label27" runat="server" Text="No."></asp:Label></TD><TD align=left><asp:TextBox id="FixAssetNo" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="20"></asp:TextBox> <asp:Label id="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD align=left><asp:Label id="Label5" runat="server" Text="Asset Date"></asp:Label></TD><TD align=left><asp:TextBox id="fixDate" runat="server" CssClass="inpText" Width="75px" OnTextChanged="fixDate_TextChanged" AutoPostBack="True"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label14" runat="server" Text="(MM/dd/yyyy)" CssClass="Important"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label6" runat="server" Text="Supplier"></asp:Label></TD><TD align=left><asp:TextBox id="Supp" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="200"></asp:TextBox> <asp:ImageButton id="BtnCariSupp" onclick="BtnCariSupp_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:ImageButton id="btnHapusSupp" onclick="btnHapusSupp_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:Label id="lblSuppOid" runat="server" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label7" runat="server" Text="Ref No"></asp:Label> </TD><TD align=left><asp:TextBox id="RefNo" runat="server" CssClass="inpText" MaxLength="16" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label2x" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label39" runat="server" ForeColor="DarkBlue" Text="Currrency" Width="63px"></asp:Label></TD><TD align=left><asp:DropDownList id="CurrDDL" runat="server" CssClass="inpText" Width="80px" OnSelectedIndexChanged="CurrDDL_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD align=left><asp:Label id="Label20" runat="server" Text="Pay Term"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLpayterm" runat="server" CssClass="inpText" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label40" runat="server" Text="Daily Rate to IDR" Width="104px"></asp:Label></TD><TD align=left><asp:TextBox id="RateToIDR" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD align=left><asp:Label id="Label41" runat="server" Text="Daily Rate To USD"></asp:Label></TD><TD align=left><asp:TextBox id="RateToUSD" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label42" runat="server" Text="Monthly Rate to IDR" Width="120px"></asp:Label></TD><TD align=left><asp:TextBox id="Rate2ToIDR" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD><TD align=left><asp:Label id="Label46" runat="server" Text="Monthly Rate To USD" Width="128px"></asp:Label></TD><TD align=left><asp:TextBox id="Rate2ToUsd" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False">0</asp:TextBox></TD></TR><TR><TD style="HEIGHT: 25px" align=left><asp:Label id="Label28" runat="server" ForeColor="DarkBlue" Text="Type asset" Width="81px"></asp:Label></TD><TD style="HEIGHT: 25px" align=left><asp:DropDownList id="fixgroup" runat="server" CssClass="inpText" Width="154px" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="HEIGHT: 25px" align=left><asp:Label id="Label21" runat="server" Text="Total Disc Detail" Width="96px"></asp:Label></TD><TD style="HEIGHT: 25px" align=left><asp:TextBox id="TotalDiscDetail" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" OnTextChanged="TotalDiscDetail_TextChanged" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label3" runat="server" Text="Total Detail"></asp:Label></TD><TD align=left><asp:TextBox id="TotalDetail" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" OnTextChanged="TotalDetail_TextChanged" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label22" runat="server" Text="Disc hdr"></asp:Label></TD><TD align=left><asp:TextBox id="DiscHdr" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Disc Type"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLtype1" runat="server" CssClass="inpText" Width="75px" OnSelectedIndexChanged="DDLtype1_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>AMT</asp:ListItem>
<asp:ListItem>PCT</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterType1" runat="server" CssClass="inpText" Width="150px" MaxLength="16" OnTextChanged="FilterType1_TextChanged" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label23" runat="server" Text="Taxable"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLtax" runat="server" CssClass="inpText" OnSelectedIndexChanged="DDLtax_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>NON TAX</asp:ListItem>
<asp:ListItem>INCLUSIVE</asp:ListItem>
<asp:ListItem>EXCLUSIVE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 20px" align=left><asp:Label id="Label15" runat="server" Text="DPP"></asp:Label></TD><TD style="HEIGHT: 20px" align=left><asp:TextBox id="DPP" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" AutoPostBack="True"></asp:TextBox></TD><TD style="HEIGHT: 20px" align=left><asp:Label id="Label24" runat="server" Text="Netto"></asp:Label></TD><TD style="HEIGHT: 20px" align=left><asp:TextBox id="Netto" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" OnTextChanged="Netto_TextChanged" AutoPostBack="True"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label17" runat="server" Text="Tax"></asp:Label></TD><TD align=left><asp:TextBox id="Tax" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="10" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label25" runat="server" Text="Status"></asp:Label></TD><TD align=left><asp:TextBox id="Status" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label19" runat="server" Text="Note"></asp:Label></TD><TD align=left><asp:TextBox id="Note1" runat="server" CssClass="inpText" Width="300px" MaxLength="150"></asp:TextBox></TD><TD align=left><asp:Label id="lblPOST" runat="server" Visible="False"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLoutlet" runat="server" Visible="False" CssClass="inpTextDisabled" Width="170px" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" Enabled="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="Outlet" runat="server" Text="Outlet" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label11" runat="server" Font-Bold="True" Text="Detail :" Width="56px" Font-Strikeout="False" Font-Underline="True"></asp:Label></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="fixDate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" CultureName="en-US" ErrorTooltipEnabled="True" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"></ajaxToolkit:MaskedEditExtender></TD><TD align=left></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" PopupButtonID="btnDate" Format="MM/dd/yyyy" TargetControlID="fixDate"></ajaxToolkit:CalendarExtender></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label26" runat="server" ForeColor="DarkBlue" Text="Code"></asp:Label></TD><TD align=left><asp:TextBox id="Code" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="200"></asp:TextBox> <asp:ImageButton id="btnSearchCode" onclick="btnSearchCode_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:ImageButton id="btnHapusCode" onclick="btnHapusCode_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" Height="16px"></asp:ImageButton> <asp:Label id="lblItemOid" runat="server" ForeColor="DarkBlue" Visible="False"></asp:Label> <asp:Label id="lblCode" runat="server" ForeColor="DarkBlue" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Description"></asp:Label></TD><TD align=left><asp:TextBox id="Desc" runat="server" CssClass="inpText" Width="300px" MaxLength="200"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label30" runat="server" ForeColor="DarkBlue" Text="Qty"></asp:Label></TD><TD align=left><asp:TextBox id="Qty" runat="server" CssClass="inpText" Width="50px" MaxLength="10" OnTextChanged="Qty_TextChanged" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label37" runat="server" Text="Price"></asp:Label></TD><TD align=left><asp:TextBox id="Price" runat="server" CssClass="inpText" Width="150px" MaxLength="12" OnTextChanged="Price_TextChanged" AutoPostBack="True"></asp:TextBox> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label31" runat="server" ForeColor="DarkBlue" Text="Disc Type"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLtype2" runat="server" CssClass="inpText" Width="75px" OnSelectedIndexChanged="DDLtype2_SelectedIndexChanged" AutoPostBack="True"><asp:ListItem>AMT</asp:ListItem>
<asp:ListItem>PCT</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterType2" runat="server" CssClass="inpText" Width="150px" OnTextChanged="FilterType2_TextChanged" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label10" runat="server" Text="Disc Dtl"></asp:Label></TD><TD align=left><asp:TextBox id="discDtl" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="16" AutoPostBack="True" ReadOnly="True">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label32" runat="server" Text="Sub Total"></asp:Label></TD><TD align=left><asp:TextBox id="subTotal" runat="server" CssClass="inpTextDisabled" Width="150px" Enabled="False" MaxLength="18" AutoPostBack="True"></asp:TextBox></TD><TD align=left><asp:Label id="Label8" runat="server" Text="Depreciation"></asp:Label></TD><TD align=left><asp:TextBox id="fixdepmonth" runat="server" CssClass="inpTextDisabled" Width="50px" Enabled="False" MaxLength="2" AutoPostBack="True">0</asp:TextBox> <asp:Label id="Label9" runat="server" ForeColor="Red" Font-Size="X-Small" Text="(month)"></asp:Label> <asp:Label id="Label33" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label35" runat="server" Text="Final Value"></asp:Label></TD><TD align=left><asp:TextBox id="FinalValue" runat="server" CssClass="inpText" Width="150px" MaxLength="16" OnTextChanged="FinalValue_TextChanged" AutoPostBack="True">0</asp:TextBox></TD><TD align=left><asp:Label id="AddToList" runat="server" Text="addtolist" Visible="False"></asp:Label></TD><TD align=left><asp:Label id="Label12" runat="server" ForeColor="Red" Font-Bold="True" Text="* Set to  -1, If have no Depreciation !!"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label34" runat="server" Text="Note"></asp:Label></TD><TD align=left><asp:TextBox id="Note2" runat="server" CssClass="inpText" Width="300px" MaxLength="100"></asp:TextBox></TD><TD align=left colSpan=2><asp:ImageButton id="btnAddtoList" onclick="btnAddtoList_Click1" runat="server" ImageUrl="~/Images/addtolist.png"></asp:ImageButton> <asp:ImageButton id="btnClear" onclick="btnClear_Click1" runat="server" ImageUrl="~/Images/clear.png"></asp:ImageButton> <asp:Label id="fadtloid" runat="server" Text="fadtloid" Visible="False"></asp:Label> <asp:Label id="famstoid" runat="server" Text="famstoid" Visible="False"></asp:Label></TD></TR><TR><TD align=right colSpan=4><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="cmpcode,trnbelifadtloid,itemfacode,itemfadesc,trnbelifadtlqty,trnbelifadtlprice,trnbelifadtldiscamt,trnbelifadtlamtnetto,trnbelifadtldep,trnbelifadtllastvalue,trnbelifadtlnote,trnbelifadtldiscvalue,trnbelifadtldisctype" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="8" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged" OnRowDeleting="GVFixedAssetdtl_RowDeleting">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifadtlseq" HeaderText="Nomer" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemfacode" HeaderText="FA Code">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="itemfadesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlqty" HeaderText="Quantity">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlprice" DataFormatString="{0:#,##0.00}" HeaderText="Price">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldiscamt" DataFormatString="{0:#,##0.00}" HeaderText="Discount">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlamtnetto" DataFormatString="{0:#,##0.00}" HeaderText="Sub Total">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldep" HeaderText="Depreciation">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtllastvalue" DataFormatString="{0:#,##0.00}" HeaderText="Final Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtlnote" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="trnbelifadtldiscvalue" DataFormatString="{0:#,##0.00}" HeaderText="Disc Value" Visible="False"></asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="trnbelifadtldisctype" HeaderText="Disc Type" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/btnsave.bmp"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" onclick="btnCancel_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" __designer:wfdid="w1"></asp:ImageButton> <asp:ImageButton id="btnDelete" onclick="btnDelete_Click" runat="server" ImageUrl="~/Images/btndelete.bmp"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnshowCOA" onclick="btnshowCOA_Click" runat="server" ImageUrl="~/Images/showCOA.png" Visible="False"></asp:ImageButton><ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" TargetControlID="FilterType1" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" TargetControlID="Qty" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" TargetControlID="Price" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" TargetControlID="FilterType2" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender5" runat="server" TargetControlID="FinalValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR><TR></TR><TR></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListSupp" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Visible="False" Width="650px" CssClass="modalBox"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="suppname">Supp Name</asp:ListItem>
<asp:ListItem Value="suppcode">Supp Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" Width="150px" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSupplier" runat="server" ForeColor="#333333" Width="98%" PageSize="5" OnSelectedIndexChanged="gvSupplier_SelectedIndexChanged" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="suppoid,suppcode,suppname,supptype,coa" AllowPaging="True" GridLines="None" OnPageIndexChanging="gvSupplier_PageIndexChanging" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code" SortExpression="suppcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListMat" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListMat" runat="server" Visible="False" Width="500px" CssClass="modalBox"><TABLE style="WIDTH: 100%"><TR><TD style="WIDTH: 897px" class="Label" align=center colSpan=3><asp:Label id="lblTitleListMat" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Item" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 897px; HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD style="WIDTH: 897px" class="Label" align=center colSpan=3><asp:Panel id="pnlFindListMat" runat="server" Width="100%" DefaultButton="btnFindListMat">Filter : <asp:DropDownList id="FilterDDLListMat" runat="server" Width="100px" CssClass="inpText"><asp:ListItem Value="i.itemcode">Code</asp:ListItem>
<asp:ListItem Value="i.itemShortDescription">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListMat" runat="server" Width="200px" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListMat" onclick="btnFindListMat_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListMat" onclick="btnAllListMat_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="WIDTH: 897px" class="Label" vAlign=top align=left colSpan=3><asp:GridView id="GVmstitem" runat="server" ForeColor="#333333" Width="100%" PageSize="5" OnSelectedIndexChanged="GVmstitem_SelectedIndexChanged1" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="itemoid,itemcode,itemShortDescription" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVmstitem_PageIndexChanging" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="itemcode" HeaderText="Code" SortExpression="itemcode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="itemShortDescription" HeaderText="Description" SortExpression="itemShortDescription">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label114" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="WIDTH: 897px" align=center colSpan=3>&nbsp;<asp:LinkButton id="lkbCloseListMat" onclick="lkbCloseListMat_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE><ajaxToolkit:ModalPopupExtender id="mpeListMat" runat="server" TargetControlID="btnHideListMat" PopupDragHandleControlID="lblTitleListMat" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListMat"></ajaxToolkit:ModalPopupExtender></asp:Panel> <asp:Button id="btnHideListMat" runat="server" ForeColor="Transparent" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="UpdatePanel6" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPosting2" runat="server" Width="760px" CssClass="modalBox" Visible="False" BorderWidth="2px" BorderStyle="Solid"><TABLE style="WIDTH: 560px"><TR><TD style="HEIGHT: 25px" align=center colSpan=3><asp:Label id="lblPosting2" runat="server" Font-Bold="True" Text="COA - Posting"></asp:Label></TD></TR><TR><TD colSpan=3><asp:GridView id="gvakun" runat="server" Width="750px" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle CssClass="gvrow"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="acctgcode" HeaderText="Kode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Nama COA">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="glnote" HeaderText="Catatan">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="debet" HeaderText="Debet">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="kredit" HeaderText="Kredit">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<PagerStyle CssClass="gvfooter"></PagerStyle>

<AlternatingRowStyle CssClass="gvalternate"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCancel2" runat="server" Font-Bold="True" Font-Size="Small" OnClick="lkbCancel2_Click">[ CLOSE ]</asp:LinkButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePosting2" runat="server" TargetControlID="btnHidePosting2" PopupControlID="pnlPosting2" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblPosting2"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHidePosting2" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt"> Form Fixed Asset Purchase :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" PopupDragHandleControlID="lblCaption" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlPopUpMsg" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
</asp:Content>