Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_CashBankExpense
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid2.Text = "" Then
            sError &= "- Please select EXPENSE ACCOUNT field!<BR>"
        End If
        If cashbankglamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(cashbankglamt.Text) = 0 Then
                sError &= "- AMOUNT must be not equal to 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("cashbankglamt", "QL_trncashbankgl", ToDouble(cashbankglamt.Text), sErrReply) Then
                    sError &= "- AMOUNT must be less than MAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        'If groupoid.SelectedValue = "" Then
        '    sError &= "- Please select DIVISION field!<BR>"
        'End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sError &= "- Please fill EXPENSE DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- EXPENSE DATE is invalid. " & sErr & "<BR>"
            Else
                If CDate(cashbankdate.Text) > Today Then
                    sError &= "- EXPENSE DATE must less or equal then TODAY!<BR>"
                End If
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        Dim sErrReply As String = ""
        If Not isLengthAccepted("cashbankdpp", "QL_trncashbankmst", ToDouble(cashbankamt.Text), sErrReply) Then
            sError &= "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If cashbanktype.SelectedValue = "BBK" Or cashbanktype.SelectedValue = "BGK" Then
            If cashbankrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If cashbankduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than EXPENSE DATE<BR>"
                    End If
                End If
            End If
        ElseIf cashbanktype.SelectedValue = "BLK" Then
            If giroacctgoid.Text = "" Then
                sError &= "- Please select DP NO. field!<BR>"
            End If
        End If
        If cashbanktype.SelectedValue = "BGK" Then
            If cashbanktakegiro.Text = "" Then
                sError &= "- Please fill DATE TAKE GIRO field!<BR>"
            Else
                If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
                Else
                    If CDate(cashbanktakegiro.Text) > CDate(cashbankduedate.Text) Then
                        sError &= "- DATE TAKE GIRO must be less or equal than DUE DATE<BR>"
                    End If
                End If
            End If
            'If suppoid.Text = "" Then
            '    sError &= "- Please select SUPPLIER field!<BR>"
            'End If
        End If
        'If personoid.SelectedValue = "" Then
        '    sError &= "- Please select PIC field!<BR>"
        'End If
        If Not isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", ToDouble(cashbanktaxamt.Text), sErrReply) Then
            sError &= "- TAX AMOUNT must be less than MAX TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If cashbankothertaxamt.Text <> "" Then
            If Not isLengthAccepted("cashbankothertaxamt", "QL_trncashbankmst", ToDouble(cashbankothertaxamt.Text), sErrReply) Then
                sError &= "- OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
            End If
        End If
        If ToDouble(cashbankgrandtotal.Text) < 0 Then
            sError &= "- GRAND TOTAL must be more than 0!<BR>"
        Else
            If cashbanktype.SelectedValue = "BLK" Then
                If ToDouble(cashbankgrandtotal.Text) > ToDouble(dpapamt.Text) Then
                    sError &= "- GRAND TOTAL must be less than DP AMOUNT!<BR>"
                End If
            End If
        End If
        If Not isLengthAccepted("cashbankamt", "QL_trncashbankmst", ToDouble(cashbankgrandtotal.Text), sErrReply) Then
            sError &= "- GRAND TOTAL must be less than MAX GRAND TOTAL (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function

    Private Sub CheckTax()
        If cashbanktaxtype.SelectedValue = "TAX" Then
            cashbanktaxpct.Text = 0
        Else
            cashbanktaxpct.Text = 0
        End If
    End Sub
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckExpenseStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='EXPENSE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnExpense.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbExpenseInProcess.Visible = True
            lkbExpenseInProcess.Text = "You have " & GetStrData(sSql) & " In Process Cash/Bank Expense data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLPerson() : InitDDLDiv()
        End If
        ' Fill DDL Currency
        'sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        'FillDDL(curroid, sSql)
    End Sub

    Private Sub CurDDL(ByVal Curr As String)
        ' Fill DDL Currency
        sSql = "SELECT DISTINCT a.curroid, currcode FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' " & Curr & " "
        FillDDL(curroid, sSql)
    End Sub

    Private Sub InitDDLDiv()
        'sSql = "SELECT groupoid, groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        'FillDDL(groupoid, sSql)
    End Sub

    Private Sub InitDDLPerson()
        ' Fill DDL PIC
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        FillDDLWithAdditionalText(personoid, sSql, "None", "0")
        Try
            personoid.SelectedValue = GetStrData("SELECT personoid FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cb.cashbankoid, cb.cashbankno, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, /*p.personname,*/ cb.cashbankstatus, cb.cashbanknote, 'False' AS checkvalue, ISNULL(suppname, '') AS suppname, '' AS CBType, cb.cashbankamt FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid /*INNER JOIN QL_mstperson p ON p.cmpcode=cb.cmpcode AND p.personoid=cb.personoid*/ LEFT JOIN (SELECT s.suppoid, s.suppname, 'Expense' supptype FROM QL_mstsupp s UNION ALL SELECT s.custoid suppoid, s.custname suppname, 'PPH' supptype FROM QL_mstcust s) s ON s.suppoid=cb.refsuppoid AND ISNULL(cashbankres3,'Expense')=supptype WHERE cb.cashbankgroup='EXPENSE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, cb.cashbankdate) DESC, cb.cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateExpenseNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindSupplierData()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT suppoid, suppcode, suppname, suppaddr, (CASE supptaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable FROM QL_mstsupp WHERE cmpcode='" & CompnyCode & "') Supp"
        sSql &= " WHERE " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppcode"
        FillGV(gvListSupp, sSql, "QL_mstsupp")
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT custoid suppoid, custcode suppcode, custname suppname, custaddr suppaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS supptaxable FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND custoid IN (SELECT DISTINCT m.custoid FROM QL_trnjualmst m WHERE m.trnjualstatus IN ('Post', 'Approved', 'Closed') AND trnjualmstoid>0 AND trnjualno LIKE 'LAT%')) Supp"
        sSql &= " WHERE " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppcode"
        FillGV(gvListSupp, sSql, "QL_mstsupp")
    End Sub

    Private Sub BindListCOA()
        Dim yy As String = "AND " & FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%'"
        If RblExpenseType.SelectedValue <> "" Then
            If RblExpenseType.SelectedItem.Text = "PENGGAJIAN" Then
                yy = "AND (acctgcode LIKE '%62101%' or acctgcode LIKE '%614%' or acctgcode LIKE '%62109%' or acctgcode LIKE '%53104%')"
            End If
        End If
        FillGVAcctg(gvListCOA, "VAR_EXPENSE", DDLBusUnit.SelectedValue, yy)
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        cashbankglseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                cashbankglseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        acctgoid2.Text = ""
        acctgcode.Text = ""
        acctgdesc.Text = ""
        cashbankglamt.Text = ""
        cashbankglnote.Text = ""
        groupoid.SelectedIndex = -1
        gvDtl.SelectedIndex = -1
        paycommoid.Text = ""
        paycommamt.Text = ""
        paycommno.Text = ""
        reftype.Text = ""
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.CssClass = sCss : DDLBusUnit.Enabled = bVal
        RblExpense.Enabled = bVal
    End Sub

    Private Sub CountTotalAmt()
        Dim dVal As Double = 0 : Dim dValMinus As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")

            dVal = ToDouble(dt.Compute("SUM(cashbankglamt)", "cashbankglamt>0").ToString)
            dValMinus = ToDouble(dt.Compute("SUM(cashbankglamt)", "cashbankglamt<0").ToString)
        End If
        cashbankamt.Text = ToMaskEdit(dVal, 4)
        lblDeduction.Visible = (dValMinus < 0)
        lblDeductionSign.Visible = (dValMinus < 0)
        totaldeduction.Visible = (dValMinus < 0)
        totaldeduction.Text = ToMaskEdit(dValMinus, 4)
        If cashbanktaxtype.SelectedValue = "TAX" Then
            cashbanktaxamt.Text = "" 'ToMaskEdit((ToDouble(cashbankamt.Text) * ToDouble(cashbanktaxpct.Text)) / 100, 4)
        Else
            cashbanktaxamt.Text = ""
        End If
        cashbankgrandtotal.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(cashbanktaxamt.Text) + ToDouble(cashbankothertaxamt.Text) + ToDouble(dValMinus), 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cb.cmpcode, cashbankoid, cb.periodacctg, cashbankno, cashbankdate, cashbanktype, cb.acctgoid, cb.curroid, cashbankamt, cb.personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, cb.createuser, cb.createtime, cb.upduser, cb.updtime, cashbanktakegiro, ISNULL(cb.refsuppoid, 0) AS suppoid, ISNULL(suppname, '') AS suppname, cashbanktaxtype, cashbanktaxpct, cashbanktaxamt, cashbankothertaxamt, cashbankdpp, ISNULL(giroacctgoid, 0) AS giroacctgoid, (CASE cashbanktype WHEN 'BLK' THEN (ISNULL((SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode=cb.cmpcode AND dp.dpapoid=ISNULL(cb.giroacctgoid, 0)), 0.0) + cashbankamt) ELSE 0.0 END) AS cashbankresamt, ISNULL(cashbankres3, 'Expense') cashbankres3 FROM QL_trncashbankmst cb LEFT JOIN (SELECT s.suppoid, s.suppname, 'Expense' supptype FROM QL_mstsupp s UNION ALL SELECT s.custoid suppoid, s.custname suppname, 'PPH' supptype FROM QL_mstcust s) s ON s.suppoid=cb.refsuppoid AND ISNULL(cashbankres3,'Expense')=supptype WHERE cb.cashbankoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbanktype.SelectedValue = Trim(xreader("cashbanktype").ToString)
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                If cashbankduedate.Text = "01/01/1900" Then
                    cashbankduedate.Text = ""
                End If
                cashbanktakegiro.Text = Format(xreader("cashbanktakegiro"), "MM/dd/yyyy")
                If cashbanktakegiro.Text = "01/01/1900" Then
                    cashbanktakegiro.Text = ""
                End If
                cashbankrefno.Text = Trim(xreader("cashbankrefno").ToString)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                cashbankgrandtotal.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankamt").ToString)), 4)
                personoid.SelectedValue = Trim(xreader("personoid").ToString)
                cashbanknote.Text = Trim(xreader("cashbanknote").ToString)
                cashbankstatus.Text = Trim(xreader("cashbankstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                suppoid.Text = Trim(xreader("suppoid").ToString)
                suppname.Text = Trim(xreader("suppname").ToString)
                cashbanktaxtype.SelectedValue = Trim(xreader("cashbanktaxtype").ToString)
                cashbanktaxpct.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbanktaxpct").ToString)), 4)
                cashbanktaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbanktaxamt").ToString)), 4)
                cashbankothertaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankothertaxamt").ToString)), 4)
                cashbankamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankdpp").ToString)), 4)
                giroacctgoid.Text = xreader("giroacctgoid").ToString
                Session("lastdpapoid") = giroacctgoid.Text
                Session("lastcashbanktype") = cashbanktype.SelectedValue
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
                RblExpense.SelectedValue = Trim(xreader("cashbankres3").ToString)
                RblExpense_SelectedIndexChanged(Nothing, Nothing)
                cashbankrefno.CssClass = "inpTextDisabled" : cashbankrefno.Enabled = False
                dpapamt.Text = ToMaskEdit(ToDouble(xreader("cashbankresamt").ToString), 4)
                If RblExpense.SelectedValue = "PPH" Then
                    dpapamt.Text = ToMaskEdit(ToDouble(GetStrData("SELECT trnjualamt FROM QL_trnjualmst WHERE trnjualmstoid=" & giroacctgoid.Text)), 2)
                End If
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        cashbanktype.CssClass = "inpTextDisabled" : cashbanktype.Enabled = False
        acctgoid.CssClass = "inpTextDisabled" : acctgoid.Enabled = False
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            btnShowCOA.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, (CASE reftype WHEN 'QL_trnpaycommmst' THEN (SELECT paycommno FROM QL_trnpaycommmst pay WHERE pay.paycommoid=cbgl.refoid) WHEN 'QL_trnpayspvmst' THEN (SELECT t_spv_comm_calc_no FROM QL_t_spv_comm_calc pay WHERE pay.t_spv_comm_calc_id=cbgl.refoid) WHEN 'QL_r_salary' THEN (select 'GAJI ' + salary_type + ' BULAN ' + cast(r_salary_month as varchar(30)) + ' ' + cast(r_salary_year as varchar(30)) from QL_r_salary r where r.r_salaryoid=cbgl.refoid) ELSE '' END) refno, (CASE reftype WHEN 'QL_trnpaycommmst' THEN (SELECT paycommamt FROM QL_trnpaycommmst pay WHERE pay.paycommoid=cbgl.refoid) WHEN 'QL_trnpayspvmst' THEN (SELECT t_spv_comm_calc_amt FROM QL_t_spv_comm_calc pay WHERE pay.t_spv_comm_calc_id=cbgl.refoid) WHEN 'QL_r_salary' THEN (select total_salary from QL_r_salary where r_salaryoid=cbgl.refoid) ELSE 0.0 END) refamt, cbgl.reftype, cbgl.refoid FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" & sOid & " ORDER BY cashbankgloid"
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankgl")
        For C1 As Integer = 0 To dtTable.Rows.Count - 1
            dtTable.Rows(C1)("cashbankglseq") = C1 + 1
        Next
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail() : CountTotalAmt()
        Session("lastdpapamt") = ToDouble(cashbankgrandtotal.Text)
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptExpense.rpt"))
            Dim sWhere As String = "WHERE cashbankgroup='EXPENSE'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnExpense.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ExpensePrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnExpense.aspx?awal=true")
    End Sub

    Private Sub BindDPData()
        lblListDP.Text = "List Of Down Payment"
        sSql = "SELECT * FROM ("
        sSql &= "SELECT dp.dpapoid, dp.dpapno, (CONVERT(VARCHAR(10), dp.dpapdate, 101)) AS dpapdate, dp.acctgoid AS dpapacctgoid, a.acctgdesc, dp.dpapnote, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) AS dpapamt, dpapstatus, dp.suppoid, dp.curroid, dpapaccumamt FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "') tbl "
        sSql &= " WHERE " & FilterDDLListDP.SelectedValue & " LIKE '%" & Tchar(FilterTextListDP.Text) & "%' AND dpapstatus='Post' AND suppoid=" & suppoid.Text & " AND curroid=" & curroid.SelectedValue & " AND dpapamt > dpapaccumamt ORDER BY dpapoid"
        FillGV(gvListDP, sSql, "QL_trndpap")
    End Sub

    Private Sub BindInvoiceData()
        lblListDP.Text = "List Of Sales Invoice"
        sSql = "SELECT * FROM ("
        sSql &= "SELECT dp.trnjualmstoid dpapoid, dp.trnjualno dpapno, (CONVERT(VARCHAR(10), dp.trnjualdate, 101)) AS dpapdate, con.acctgoid AS dpapacctgoid, a.acctgdesc, dp.trnjualnote dpapnote, trnjualamt AS dpapamt, trnjualstatus, dp.custoid FROM QL_trnjualmst dp INNER JOIN QL_conar con ON con.refoid=dp.trnjualmstoid AND con.reftype='QL_trnjualmst' AND con.payrefoid=0 INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "' AND dp.trnjualmstoid>0 AND trnjualno LIKE 'LAT%') tbl"
        sSql &= " WHERE " & FilterDDLListDP.SelectedValue & " LIKE '%" & Tchar(FilterTextListDP.Text) & "%' AND trnjualstatus IN ('Post', 'Approved', 'Closed') AND custoid=" & suppoid.Text & " ORDER BY dpapoid DESC"
        FillGV(gvListDP, sSql, "QL_trndpap")
    End Sub

    Private Sub EnableCurrency(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then sCss = "inpTextDisabled"
        curroid.Enabled = bVal : curroid.CssClass = sCss
        If bVal = True Then
            If curroid.Items.Count = 0 Then
                ' Fill DDL Currency
                sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                FillDDL(curroid, sSql)
            End If
        End If
    End Sub

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                'curroid.SelectedValue = sCurrOid
            Else
                If curroid.Enabled = False Then
                    curroid.Items.Clear()
                    showMessage("Please define Currency for selected Payment Account!", 2)
                End If
            End If
        Else
            curroid.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnExpense.aspx")
        End If
        If checkPagePermission("~\Accounting\trnExpense.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Cash/Bank Expense"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckExpenseStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            CurDDL("")
            CheckTax()
            cashbanktaxtype_SelectedIndexChanged(Nothing, Nothing)
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnExpense.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbExpenseInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbExpenseInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, cb.updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' "
        If checkPagePermission("~\Accounting\trnExpense.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cb.cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cb.cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cb.cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If cbType.Checked Then
            sSqlPlus &= " AND cb.cashbanktype='" & ddlType.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnExpense.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbType.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnExpense.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        'InitDDLPerson()
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateExpenseNo()
        End If
        btnClearCOA_Click(Nothing, Nothing)
        InitDDLDiv()
    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankdate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateExpenseNo()
        End If
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbanktype.SelectedIndexChanged
        Dim sVar As String = "VAR_CASH"
        Dim bVal As Boolean = False
        Dim bVal2 As Boolean = True
        Dim bVal3 As Boolean = False
        Dim sCss As String = "inpText"
        Dim sText As String = "Ref. No."
        Dim sText2 As String = "Due Date"
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : cashbankduedate.Text = "" : dpapamt.Text = ""
        EnableCurrency(False)

        If cashbanktype.SelectedValue = "BBK" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BGK" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BLK" Then
            sVar = "VAR_DPAP" : bVal2 = False
            sCss = "inpTextDisabled" : sText = "DP No."
            sText2 = "DP Amount" : bVal3 = True : EnableCurrency(True)
        End If

        FillDDLAcctg(acctgoid, sVar, DDLBusUnit.SelectedValue)
        SetCOACurrency(acctgoid.SelectedValue)
        lblWarnDueDate.Visible = bVal : cashbankduedate.Visible = bVal
        imbDueDate.Visible = bVal : lblInfoDueDate.Visible = bVal
        cashbankduedate.Text = "" : lblDueDate.Visible = bVal3
        lblSeptDueDate.Visible = bVal3 : lblWarnRefNo.Visible = bVal3
        acctgoid.Enabled = bVal2 : acctgoid.CssClass = sCss
        btnSearchDP.Visible = Not bVal2 : btnClearDP.Visible = Not bVal2
        cashbankrefno.Enabled = bVal2 : cashbankrefno.CssClass = sCss
        dpapamt.Visible = Not bVal2 : lblRefNo.Text = sText : lblDueDate.Text = sText2
        lblDTG.Visible = bVal : lblWarnDTG.Visible = bVal : cashbanktakegiro.Visible = bVal
        lblSeptDTG.Visible = bVal : cashbanktakegiro.Visible = bVal
        imbDTG.Visible = bVal : lblInfoDTG.Visible = bVal
        If i_u.Text = "New Data" Then
            GenerateExpenseNo()
        End If
        RblExpense_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateExpenseNo()
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' AND a.acctgoid='" & acctgoid.SelectedValue & "'"
        Dim sCurrOid As Integer = Integer.Parse(GetStrData(sSql))
        If sCurrOid <> 0 Then
            CurDDL("AND a.acctgoid='" & acctgoid.SelectedValue & "'")
        Else
            CurDDL("")
            'showMessage("Please define Currency for selected Payment Account!", 2)
        End If
        'SetCOACurrency(acctgoid.SelectedValue)
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        If RblExpense.SelectedValue = "Expense" Then
            BindSupplierData()
        Else
            BindCustomerData()
        End If
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = "" : suppname.Text = "" : cashbanktaxtype.SelectedIndex = -1 : cashbanktaxpct.Text = "" : CountTotalAmt()
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        If RblExpense.SelectedValue = "Expense" Then
            BindSupplierData()
        Else
            BindCustomerData()
        End If
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        If RblExpense.SelectedValue = "Expense" Then
            BindSupplierData()
        Else
            BindCustomerData()
        End If
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        cashbanktaxtype.SelectedValue = gvListSupp.SelectedDataKey.Item("supptaxable").ToString
        If cashbanktaxtype.SelectedValue = "TAX" Then
            cashbanktaxpct.Text = "" 'ToMaskEdit(GetTaxValue(), 4)
        Else
            cashbanktaxpct.Text = ""
        End If
        CountTotalAmt()
        CheckTax()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub cashbankothertaxamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankothertaxamt.TextChanged
        cashbankgrandtotal.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(cashbanktaxamt.Text) + ToDouble(cashbankothertaxamt.Text), 4)
    End Sub

    Protected Sub btnSearchCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCOA.Click
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
    End Sub

    Protected Sub btnClearCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCOA.Click
        acctgoid2.Text = ""
        acctgdesc.Text = ""
        acctgcode.Text = ""
    End Sub

    Protected Sub btnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCOA.Click
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub btnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCOA.Click
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCOA.PageIndexChanging
        gvListCOA.PageIndex = e.NewPageIndex
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCOA.SelectedIndexChanged
        If acctgoid2.Text <> gvListCOA.SelectedDataKey.Item("acctgoid").ToString Then
            btnClearCOA_Click(Nothing, Nothing)
        End If
        acctgoid2.Text = gvListCOA.SelectedDataKey.Item("acctgoid").ToString
        acctgcode.Text = gvListCOA.SelectedDataKey.Item("acctgcode").ToString
        acctgdesc.Text = gvListCOA.SelectedDataKey.Item("acctgdesc").ToString
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub lkbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCOA.Click
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            cashbanktaxtype.CssClass = "inpTextDisabled"
            cashbanktaxtype.Enabled = False
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = New DataTable("TabelExpense")
                dtlTable.Columns.Add("cashbankglseq", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
                dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbankglamt", Type.GetType("System.Double"))
                dtlTable.Columns.Add("cashbankglnote", Type.GetType("System.String"))
                'dtlTable.Columns.Add("groupoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("groupdesc", Type.GetType("System.String"))
                dtlTable.Columns.Add("refno", Type.GetType("System.String"))
                dtlTable.Columns.Add("refamt", Type.GetType("System.String"))
                dtlTable.Columns.Add("reftype", Type.GetType("System.String"))
                dtlTable.Columns.Add("refoid", Type.GetType("System.Int32"))
                Session("TblDtl") = dtlTable
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND refoid=" & ToInteger(paycommoid.Text) & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' "
                'AND groupoid=" & groupoid.SelectedValue
            Else
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND refoid=" & ToInteger(paycommoid.Text) & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' AND cashbankglseq<>" & cashbankglseq.Text & " "
                'AND groupoid=" & groupoid.SelectedValue
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                cashbankglseq.Text = objTable.Rows.Count + 1
                objRow("cashbankglseq") = cashbankglseq.Text
            Else
                objRow = objTable.Rows(cashbankglseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("acctgoid") = acctgoid2.Text
            objRow("acctgcode") = acctgcode.Text
            objRow("acctgdesc") = acctgdesc.Text
            objRow("cashbankglamt") = ToDouble(cashbankglamt.Text)
            objRow("cashbankglnote") = cashbankglnote.Text
            objRow("refno") = paycommno.Text
            objRow("refamt") = ToDouble(paycommamt.Text)
            objRow("reftype") = RblExpenseType.SelectedValue
            objRow("refoid") = ToInteger(paycommoid.Text)
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalAmt()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
        cashbanktaxtype.CssClass = "inpText"
        cashbanktaxtype.Enabled = True
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            cashbankglseq.Text = gvDtl.SelectedDataKey.Item("cashbankglseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "cashbankglseq=" & cashbankglseq.Text
                acctgoid2.Text = dv.Item(0).Item("acctgoid").ToString
                acctgcode.Text = dv.Item(0).Item("acctgcode").ToString
                acctgdesc.Text = dv.Item(0).Item("acctgdesc").ToString
                cashbankglamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("cashbankglamt").ToString), 4)
                cashbankglnote.Text = dv.Item(0).Item("cashbankglnote").ToString
                paycommoid.Text = dv.Item(0).Item("refoid").ToString
                paycommno.Text = dv.Item(0).Item("refno").ToString
                paycommamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("refamt").ToString), 4)
                RblExpenseType.SelectedValue = dv.Item(0).Item("reftype").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("cashbankglseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        CountTotalAmt()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                Dim sNo As String = cashbankno.Text
                GenerateExpenseNo()
                If sNo <> cashbankno.Text Then
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            cashbankgloid.Text = GenerateID("QL_TRNCASHBANKGL", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim cRate As New ClassRate()
            Dim iTaxAcctgOid As Integer = 0
            Dim iOtherTaxAcctgOid As Integer = 0
            Dim iGiroAcctgOid As Integer = ToInteger(giroacctgoid.Text)
            periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
            Dim sDueDate As String = ""
            If cashbanktype.SelectedValue = "BKK" Or cashbanktype.SelectedValue = "BLK" Then
                sDueDate = cashbankdate.Text
            Else
                sDueDate = cashbankduedate.Text
            End If
            Dim sDTG As String = "1/1/1900"

            If cashbanktakegiro.Text <> "" Then
                sDTG = cashbanktakegiro.Text
            End If

            Dim sDate As String = cashbankdate.Text
            If cashbanktype.SelectedValue = "BBK" Then
                sDate = cashbankduedate.Text
            End If
            If cRate.GetRateDailyLastError <> "" Then
                showMessage(cRate.GetRateDailyLastError, 2)
                Exit Sub
            End If
            If cRate.GetRateMonthlyLastError <> "" Then
                showMessage(cRate.GetRateMonthlyLastError, 2)
                Exit Sub
            End If
            cRate.SetRateValue(CInt(curroid.SelectedValue), cashbankdate.Text)
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            If cashbankstatus.Text = "Post" Then
                If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sDate) Then
                    showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If ToDouble(cashbanktaxamt.Text) > 0 Then
                    If Not IsInterfaceExists("VAR_PPN_IN", DDLBusUnit.SelectedValue) Then
                        sVarErr = "VAR_PPN_IN"
                    Else
                        iTaxAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_IN", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If ToDouble(cashbankothertaxamt.Text) > 0 Then
                    If Not IsInterfaceExists("VAR_OTHER_TAX_EXPENSE", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_OTHER_TAX_EXPENSE"
                    Else
                        iOtherTaxAcctgOid = GetAcctgOID(GetVarInterface("VAR_OTHER_TAX_EXPENSE", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If cashbanktype.SelectedValue = "BGK" Then
                    If Not IsInterfaceExists("VAR_GIRO", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_GIRO"
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
                If cashbanktype.SelectedValue = "BGK" Then
                    iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO", DDLBusUnit.SelectedValue), CompnyCode)
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans

            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, refsuppoid, cashbanktaxtype, cashbanktaxpct, cashbanktaxamt, cashbankothertaxamt, cashbankdpp, cashbankresamt,cashbankgiroreal,cashbanktakegiroreal, cashbankres3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'EXPENSE', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(cashbankgrandtotal.Text) & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", " & personoid.SelectedValue & ", '" & sDueDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sDTG & "', " & iGiroAcctgOid & ", " & ToInteger(suppoid.Text) & ", '" & cashbanktaxtype.SelectedValue & "', " & ToDouble(cashbanktaxpct.Text) & ", " & ToDouble(cashbanktaxamt.Text) & ", " & ToDouble(cashbankothertaxamt.Text) & ", " & ToDouble(cashbankamt.Text) & ", " & ToDouble(dpapamt.Text) & ",'" & sDueDate & "','" & sDTG & "', '" & RblExpense.SelectedValue & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbanktype.SelectedValue = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " & ToDouble(cashbankgrandtotal.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(cashbankgrandtotal.Text) & ", cashbankamtidr=" & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamtusd=" & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & personoid.SelectedValue & ", cashbankduedate='" & sDueDate & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & sDTG & "', giroacctgoid=" & iGiroAcctgOid & ", refsuppoid=" & ToInteger(suppoid.Text) & ", cashbanktaxtype='" & cashbanktaxtype.SelectedValue & "', cashbanktaxpct=" & ToDouble(cashbanktaxpct.Text) & ", cashbanktaxamt=" & ToDouble(cashbanktaxamt.Text) & ", cashbankothertaxamt=" & ToDouble(cashbankothertaxamt.Text) & ", cashbankdpp=" & ToDouble(cashbankamt.Text) & ", cashbankresamt=" & ToDouble(dpapamt.Text) & ",cashbankgiroreal='" & sDate & "', cashbanktakegiroreal='" & sDTG & "', cashbankres3='" & RblExpense.SelectedValue & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " & ToDouble(Session("lastdpapamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & Session("lastdpapoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If cashbanktype.SelectedValue = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " & ToDouble(cashbankgrandtotal.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trncashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglduedate, cashbankglrefno, cashbankglnote, cashbankglstatus, createuser, createtime, upduser, updtime, reftype, refoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(cashbankgloid.Text)) & ", " & cashbankoid.Text & ", " & objTable.Rows(c1)("acctgoid").ToString & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", '" & IIf(cashbanktype.SelectedValue <> "BKK", cashbankduedate.Text, "1/1/1900") & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(objTable.Rows(c1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & objTable.Rows(c1)("reftype").ToString & "', " & objTable.Rows(c1)("refoid").ToString & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(cashbankgloid.Text)) & " WHERE tablename='QL_TRNCASHBANKGL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If cashbankstatus.Text = "Post" Then
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cash/Bank Expense|No=" & cashbankno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' DEBET
                    ' Detail Expense
                    Dim iSeq As Integer = 1
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(C1)("acctgoid").ToString & ", 'D', " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) & ", '" & cashbankno.Text & "', '" & Tchar(objTable.Rows(C1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "'," & groupoid.SelectedValue & " )"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            iSeq += 1
                        End If
                    Next
                    ' PPN In
                    If ToDouble(cashbanktaxamt.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTaxAcctgOid & ", 'D', " & ToDouble(cashbanktaxamt.Text) & ", '" & cashbankno.Text & "', '', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbanktaxamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbanktaxamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Tax C/B Expense No. " & cashbankno.Text & "', 'K')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Other Tax
                    If ToDouble(cashbankothertaxamt.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iOtherTaxAcctgOid & ", 'D', " & ToDouble(cashbankothertaxamt.Text) & ", '" & cashbankno.Text & "', '', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankothertaxamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankothertaxamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Other Tax C/B Expense No. " & cashbankno.Text & "', 'K')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' CREDIT
                    objTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(C1)("acctgoid").ToString & ", 'C', " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) & ", '" & cashbankno.Text & "', '" & Tchar(objTable.Rows(C1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            iSeq += 1
                        End If
                    Next
                    If cashbanktype.SelectedValue = "BGK" Then
                        ' Hutang Giro
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iGiroAcctgOid & ", 'C', " & ToDouble(cashbankgrandtotal.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        ' Kas/Bank
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'C', " & ToDouble(cashbankgrandtotal.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message & sSql, 1)
                xCmd.Connection.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnExpense.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnExpense.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select Cash/Bank Expense data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLK" Then
                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " & ToDouble(Session("lastdpapamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & Session("lastdpapoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnExpense.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub btnSearchDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDP.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If suppoid.Text = "" Then
            showMessage("Please select " & IIf(RblExpense.SelectedValue = "Expense", "Supplier", "Customer") & " first!", 2)
            Exit Sub
        End If
        FilterDDLListDP.SelectedIndex = -1
        FilterTextListDP.Text = ""
        gvListDP.SelectedIndex = -1
        If RblExpense.SelectedValue = "Expense" Then
            BindDPData()
        Else
            BindInvoiceData()
        End If
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, True)
    End Sub

    Protected Sub btnClearDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDP.Click
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : acctgoid.SelectedIndex = -1 : dpapamt.Text = ""
        cashbankrefno.CssClass = "inpText" : cashbankrefno.Enabled = True
    End Sub

    Protected Sub btnFindListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDP.Click
        If RblExpense.SelectedValue = "Expense" Then
            BindDPData()
        Else
            BindInvoiceData()
        End If
        mpeListDP.Show()
    End Sub

    Protected Sub btnAllListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListDP.Click
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDP.PageIndexChanging
        gvListDP.PageIndex = e.NewPageIndex
        If RblExpense.SelectedValue = "Expense" Then
            BindDPData()
        Else
            BindInvoiceData()
        End If
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDP.SelectedIndexChanged
        If giroacctgoid.Text <> gvListDP.SelectedDataKey.Item("dpapoid").ToString Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        giroacctgoid.Text = gvListDP.SelectedDataKey.Item("dpapoid").ToString
        cashbankrefno.Text = gvListDP.SelectedDataKey.Item("dpapno").ToString
        If RblExpense.SelectedValue = "Expense" Then
            acctgoid.SelectedValue = gvListDP.SelectedDataKey.Item("dpapacctgoid").ToString
        End If
        dpapamt.Text = ToMaskEdit(ToDouble(gvListDP.SelectedDataKey.Item("dpapamt").ToString), 4)
        cashbankrefno.CssClass = "inpTextDisabled" : cashbankrefno.Enabled = False
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub lkbCloseListDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDP.Click
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub
#End Region

    Protected Sub cashbanktaxtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckTax()
    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
        End If
    End Sub

    Protected Sub RblExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If RblExpense.SelectedValue = "PPH" Then
            lblSuppCust.Text = "Supplier" : lblRefNo.Text = "Ref. No." : lblSuppCust.Text = "Customer" : lblRefNo.Text = "Invoice No."
            btnSearchDP.Visible = True : btnClearDP.Visible = True
        End If
    End Sub

#Region "Sales & SPV Comm"
    Private Sub BindDataPayComm()
        If RblExpenseType.SelectedItem.Text = "KOMISI SALES" Then
            lblListComm.Text = "List Of Sales Commission"
            sSql = "SELECT * FROM ("
            sSql &= "SELECT pay.paycommoid, pay.cmpcode, p.personoid, personname, pay.paycommno, CONVERT(VARCHAR(10), paycommdate, 101) paycommdate, (paycommamt - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankgl gl WHERE gl.refoid=pay.paycommoid AND reftype='QL_trnpaycommmst' AND gl.cashbankoid<>" & ToInteger(Session("oid")) & "), 0.0)) paycommamt, paycommnote, 'QL_trnpaycommmst' reftype FROM QL_trnpaycommmst pay INNER JOIN QL_mstperson p ON p.personoid=pay.personoid WHERE pay.personoid=" & ToInteger(personoid.SelectedValue) & " AND pay.paycommstatus='Post'"
            sSql &= ") dt"
            sSql &= " WHERE paycommamt<>0 AND " & FilterDDLListComm.SelectedValue & " LIKE '%" & Tchar(FilterTextListComm.Text) & "%'"
        ElseIf RblExpenseType.SelectedItem.Text = "PENGGAJIAN" Then
            lblListComm.Text = "List Of Perhitungan Gaji"
            sSql = "SELECT * FROM ("
            sSql &= "SELECT pay.r_salaryoid paycommoid, pay.cmpcode, p.personoid, personname, 'GAJI ' + salary_type + ' BULAN ' + cast(r_salary_month as varchar(30)) + ' ' + cast(r_salary_year as varchar(30)) paycommno, CONVERT(VARCHAR(10), pay.createtime, 101) paycommdate, (total_salary - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankgl gl WHERE gl.refoid=pay.r_salaryoid AND reftype='QL_r_salary' AND gl.cashbankoid<>0), 0.0)) paycommamt, '' paycommnote, 'QL_trnpaycommmst' reftype FROM QL_r_salary pay INNER JOIN QL_mstperson p ON p.personoid=pay.personoid WHERE r_salary_year=" & CDate(cashbankdate.Text).Year & " AND r_salary_month=" & CDate(cashbankdate.Text).Month & " AND pay.personoid=" & ToInteger(personoid.SelectedValue) & ""
            sSql &= ") dt"
            sSql &= " WHERE paycommamt<>0 AND " & FilterDDLListComm.SelectedValue & " LIKE '%" & Tchar(FilterTextListComm.Text) & "%'"
        Else
            lblListComm.Text = "List Of Sales SPV Commission"
            sSql = "SELECT * FROM ("
            sSql &= "SELECT pay.t_spv_comm_calc_id paycommoid, p.cmpcode, p.personoid, personname, pay.t_spv_comm_calc_no paycommno, CONVERT(VARCHAR(10), t_spv_comm_calc_date, 101) paycommdate, (t_spv_comm_calc_amt - ISNULL((SELECT SUM(gl.cashbankglamt) FROM QL_trncashbankgl gl WHERE gl.refoid=pay.t_spv_comm_calc_id AND reftype='QL_trnpayspvmst' AND gl.cashbankoid<>" & ToInteger(Session("oid")) & "), 0.0)) paycommamt, t_spv_comm_calc_note paycommnote, 'QL_trnpayspvmst' reftype FROM QL_t_spv_comm_calc pay INNER JOIN QL_mstperson p ON p.personoid=pay.m_person_id WHERE pay.m_person_id=" & ToInteger(personoid.SelectedValue) & " AND pay.t_spv_comm_calc_status='Post'"
            sSql &= ") dt"
            sSql &= " WHERE paycommamt<>0 AND " & FilterDDLListComm.SelectedValue & " LIKE '%" & Tchar(FilterTextListComm.Text) & "%'"
        End If
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnpaycommmst")
        Session("TblPayComm") = dt
        gvListComm.DataSource = Session("TblPayComm")
        gvListComm.DataBind()
    End Sub

    Protected Sub btnFindListComm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindDataPayComm()
        mpeListComm.Show()
    End Sub

    Protected Sub btnAllListComm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListComm.SelectedIndex = -1
        FilterTextListComm.Text = ""
        BindDataPayComm()
        mpeListComm.Show()
    End Sub

    Protected Sub gvListComm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        paycommoid.Text = gvListComm.SelectedDataKey.Item("paycommoid").ToString
        If gvListComm.SelectedDataKey.Item("paycommoid").ToString <> paycommoid.Text Then
            btnEraseComm_Click(Nothing, Nothing)
        End If
        paycommno.Text = gvListComm.SelectedDataKey.Item("paycommno").ToString
        paycommamt.Text = ToMaskEdit(ToDouble(gvListComm.SelectedDataKey.Item("paycommamt").ToString), 2)
        cashbankglamt.Text = ToMaskEdit(ToDouble(gvListComm.SelectedDataKey.Item("paycommamt").ToString), 2)
        reftype.Text = gvListComm.SelectedDataKey.Item("reftype").ToString
        cProc.SetModalPopUpExtender(btnHideListComm, pnlListComm, mpeListComm, False)
    End Sub

    Protected Sub gvListComm_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        End If
    End Sub

    Protected Sub gvListComm_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListComm.PageIndex = e.NewPageIndex
        gvListComm.DataSource = Session("TblPayComm")
        gvListComm.DataBind()
        mpeListComm.Show()
    End Sub

    Protected Sub btnSearchComm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If RblExpenseType.SelectedValue = "" Then
            showMessage("Please Select TYPE TRANSACTION first!", 2)
            Exit Sub
        End If
        BindDataPayComm()
        cProc.SetModalPopUpExtender(btnHideListComm, pnlListComm, mpeListComm, True)
    End Sub

    Protected Sub btnEraseComm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        paycommoid.Text = ""
        paycommno.Text = ""
        paycommamt.Text = ""
        reftype.Text = ""
        acctgoid2.Text = ""
        acctgcode.Text = ""
        acctgdesc.Text = ""
    End Sub

    Protected Sub lkbCloseListCom_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListComm, pnlListComm, mpeListComm, False)
    End Sub
#End Region
End Class