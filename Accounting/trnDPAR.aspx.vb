Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_DownPaymentAR
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If dpardate.Text = "" Then
            sError &= "- Please fill DP DATE field!<BR>"
            sErr = "Empty"
        Else
            If Not IsValidDate(dpardate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- DP DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If custoid.Text = "" Then
            sError &= "- Please select Customer field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select DP ACCOUNT field!<BR>"
        End If
        If dparpayacctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If dparpaytype.SelectedValue <> "BKM" Then
            If dparduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(dparduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(dpardate.Text) > CDate(dparduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than DP DATE!<BR>"
                    End If
                End If
            End If
        End If
        If dparpaytype.SelectedValue = "BGM" Then
            If dparpayrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If dpartakegiro.Text = "" Then
                sError &= "- Please fill DATE TAKE GIRO field!<BR>"
            Else
                If Not IsValidDate(dpartakegiro.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
                Else
                    If CDate(dpardate.Text) > CDate(dpartakegiro.Text) Then
                        sError &= "- DATE TAKE GIRO must be more or equal than DP DATE!<BR>"
                    End If
                End If
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If dparamt.Text = "" Then
            sError &= "- Please fill DP AMOUNT field!<BR>"
        Else
            If ToDouble(dparamt.Text) <= 0 Then
                sError &= "- DP AMOUNT must be more than 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("dparamt", "QL_trndpar", ToDouble(dparamt.Text), sErrReply) Then
                    sError &= "- DP AMOUNT must be less than MAX DP AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If addacctgoid1.SelectedValue <> 0 Then
            If ToDouble(addacctgamt1.Text) = 0 Then
                sError &= "- Additional Cost Amount 1 can't be equal to 0!<BR>"
            End If
        End If
        If addacctgoid2.SelectedValue <> 0 Then
            If ToDouble(addacctgamt2.Text) = 0 Then
                sError &= "- Additional Cost Amount 2 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            If ToDouble(addacctgamt3.Text) = 0 Then
                sError &= "- Additional Cost Amount 3 can't be equal to 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            dparstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub ReAmountDP()
        dparnett.Text = ToMaskEdit(ToDouble(dparamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), 4)
    End Sub

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trndpar WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND dparstatus='In Process'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Down Payment A/R data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDPAccount()
        End If
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)

        InitDDLAdd()
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
    End Sub

    Private Sub InitDDLDPAccount()
        ' Fill DDL DP Account
        FillDDLAcctg(acctgoid, "VAR_DP_AR", DDLBusUnit.SelectedValue)
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT dparoid, dparno, CONVERT(VARCHAR(10), dpardate, 101) AS dpardate, custname, (acctgcode + ' - ' + acctgdesc) AS acctgdesc, (CASE dparpaytype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'BANK' ELSE 'GIRO' END) AS dparpaytype, dparstatus, dparnote, 'False' AS checkvalue, cashbankno FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid INNER JOIN QL_trncashbankmst cb ON cb.cashbankoid=dp.cashbankoid WHERE"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " dp.cmpcode='" & Session("CompnyCode") & "'"
        Else
            sSql &= " dp.cmpcode LIKE '%'"
        End If
        sSql &= sSqlPlus & " ORDER BY dp.dparoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trndpar")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "dparoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateCashBankNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If dpardate.Text <> "" Then
                If IsValidDate(dpardate.Text, "MM/dd/yyyy", sErr) Then
                    If dparpayacctgoid.SelectedValue <> "" Then
						Dim sNo As String = dparpaytype.SelectedValue & "-" & Format(CDate(dpardate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & dparpayacctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT custoid, custcode, custname, custaddr FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' AND activeflag='ACTIVE' ORDER BY custcode, custname"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal
        lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal
        dparduedate.Visible = bVal
        imbDueDate.Visible = bVal
        lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub EnableAddInfoGiro(ByVal bVal As Boolean)
        lblDTG.Visible = bVal
        lblWarnDTG.Visible = bVal
        lblSeptDTG.Visible = bVal
        dpartakegiro.Visible = bVal
        imbDTG.Visible = bVal
        lblInfoDTG.Visible = bVal
        lblWarnRefNo.Visible = bVal
    End Sub

    Private Sub GenerateNo()
		Dim sNo As String = "DPAR-" & Format(GetServerTime(), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dparno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparno LIKE '%" & sNo & "%'"
        dparno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT dp.cmpcode, dparoid, dp.periodacctg, dparno, dpardate, dp.custoid, custname, dp.acctgoid, dp.cashbankoid, cashbankno, dparpaytype, dparpayacctgoid, dparpayrefno, dparduedate, dp.curroid, dparamt, dparnote, dparstatus, dp.createuser, dp.createtime, dp.upduser, dp.updtime, dpartakegiro, dp.addacctgoid1, dp.addacctgamt1, dp.addacctgoid2, dp.addacctgamt2, dp.addacctgoid3, dp.addacctgamt3 FROM QL_trndpar dp INNER JOIN QL_mstcust s ON s.custoid=dp.custoid INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dparoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                dparoid.Text = Trim(xreader("dparoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                dparno.Text = Trim(xreader("dparno").ToString)
                dpardate.Text = Format(xreader("dpardate"), "MM/dd/yyyy")
                custoid.Text = Trim(xreader("custoid").ToString)
                custname.Text = Trim(xreader("custname").ToString)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                dparpaytype.SelectedValue = Trim(xreader("dparpaytype").ToString)
                dparpaytype_SelectedIndexChanged(Nothing, Nothing)
                dparpayrefno.Text = Trim(xreader("dparpayrefno").ToString)
                dparduedate.Text = Format(xreader("dparduedate"), "MM/dd/yyyy")
                If dparduedate.Text = "01/01/1900" Then
                    dparduedate.Text = ""
                End If
                dpartakegiro.Text = Format(xreader("dpartakegiro"), "MM/dd/yyyy")
                If dpartakegiro.Text = "01/01/1900" Then
                    dpartakegiro.Text = ""
                End If
                dparpayacctgoid.SelectedValue = Trim(xreader("dparpayacctgoid").ToString)
                dparamt.Text = ToMaskEdit(ToDouble(Trim(xreader("dparamt").ToString)), 4)
                addacctgoid1.SelectedValue = xreader("addacctgoid1").ToString
                addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
                addacctgamt1.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt1").ToString)), 4)
                addacctgoid2.SelectedValue = xreader("addacctgoid2").ToString
                addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
                addacctgamt2.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt2").ToString)), 4)
                addacctgoid3.SelectedValue = xreader("addacctgoid3").ToString
                addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
                addacctgamt3.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt3").ToString)), 4)
                ReAmountDP()
                dparnote.Text = Trim(xreader("dparnote").ToString)
                dparstatus.Text = Trim(xreader("dparstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        dparpaytype.CssClass = "inpTextDisabled" : dparpaytype.Enabled = False
        dparpayacctgoid.CssClass = "inpTextDisabled" : dparpayacctgoid.Enabled = False
        If dparstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            If dparnote.Text.Contains("DPAR Lebih Bayar Dari") Then
                btnShowCOA.Visible = False
            Else
                btnShowCOA.Visible = True
            End If
            lblTrnNo.Text = "DP No."
            dparoid.Visible = False
            dparno.Visible = True
        End If
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("dparoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptDPAR.rpt"))
            Dim sWhere As String = ""
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dpardate>='" & FilterPeriod1.Text & " 00:00:00' AND dpardate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dparstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= IIf(sWhere.Trim = "", " WHERE ", " AND ") & " dp.dparoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "DownPaymentARPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    End Sub

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                curroid.SelectedValue = sCurrOid
            Else
                curroid.Items.Clear()
                showMessage("Please define Currency for selected Payment Account!", 2)
            End If
        Else
            curroid.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnDPAR.aspx")
        End If
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Down Payment A/R"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                dparoid.Text = GenerateID("QL_TRNDPAR", CompnyCode)
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                dpardate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                dparstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                dparpaytype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, dp.updtime, GETDATE()) > " & nDays & " AND dparstatus='In Process' "
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND dpardate>='" & FilterPeriod1.Text & " 00:00:00' AND dpardate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND dparstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnDPAR.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND dp.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLDPAccount() : InitDDLAdd()
        dparpaytype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dpardate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dpardate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = ""
        custname.Text = ""
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub dparpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpaytype.SelectedIndexChanged
        dparpayrefno.Text = "" : dparduedate.Text = "" : dpartakegiro.Text = ""
        If dparpaytype.SelectedValue = "BKM" Then
            EnableAddInfo(False)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_CASH", DDLBusUnit.SelectedValue)
        ElseIf dparpaytype.SelectedValue = "BBM" Then
            EnableAddInfo(True)
            EnableAddInfoGiro(False)
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        Else
            EnableAddInfo(True)
            EnableAddInfoGiro(True)
            FillDDLAcctg(dparpayacctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        End If
        SetCOACurrency(dparpayacctgoid.SelectedValue)
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
    End Sub

    Protected Sub dparpayacctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparpayacctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateCashBankNo()
        End If
        SetCOACurrency(dparpayacctgoid.SelectedValue)
    End Sub

    Protected Sub dparamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dparamt.TextChanged
        dparamt.Text = ToMaskEdit(ToDouble(dparamt.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountDP()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 4)
        ReAmountDP()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trndpar WHERE dparoid=" & dparoid.Text
                If CheckDataExists(sSql) Then
                    dparoid.Text = GenerateID("QL_TRNDPAR", CompnyCode)
                    isRegenOid = True
                End If
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno='" & cashbankno.Text & "'"
                If CheckDataExists(sSql) Then
                    GenerateCashBankNo()
                End If
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpar", "dparoid", dparoid.Text, "dparstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    dparstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iSeq As Integer = 1

            Dim cRate As New ClassRate()
            Dim iGiroAcctgOid As Integer = 0
            periodacctg.Text = GetDateToPeriodAcctg(CDate(dpardate.Text))
            Dim sDate As String = dpardate.Text
            If dparpaytype.SelectedValue = "BBM" Then
                sDate = dparduedate.Text
            End If
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            If dparstatus.Text = "Post" Then
                'If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sDate) Then
                '    showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : Exit Sub
                'End If
                GenerateNo()
                cRate.SetRateValue(CInt(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If dparpaytype.SelectedValue = "BGM" Then
                    If Not IsInterfaceExists("VAR_GIRO_IN", DDLBusUnit.SelectedValue) Then
                        sVarErr &= "VAR_GIRO_IN"
                    Else
                        iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO_IN", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    Exit Sub
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Dim ValIDR As Integer = cRate.GetRateMonthlyUSDValue
            Dim ValUSD As Integer = cRate.GetRateMonthlyIDRValue
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & dpardate.Text & "', '" & dparpaytype.SelectedValue & "', 'DPAR', " & dparpayacctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(dparnett.Text) & ", " & ToDouble(dparnett.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dparnett.Text) * cRate.GetRateMonthlyUSDValue & ", " & custoid.Text & ", '" & IIf(dparpaytype.SelectedValue <> "BKM", dparduedate.Text, "1/1/1900") & "', '" & Tchar(dparpayrefno.Text) & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(dparpaytype.SelectedValue = "BGM", dpartakegiro.Text, "1/1/1900") & "', " & iGiroAcctgOid & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "INSERT INTO QL_trndpar (cmpcode, dparoid, periodacctg, dparno, dpardate, custoid, acctgoid, cashbankoid, dparpaytype, dparpayacctgoid, dparpayrefno, dparduedate, curroid, rateoid, rate2oid, dparamt, dparaccumamt, dparnote, dparstatus, createuser, createtime, upduser, updtime, dpartakegiro, giroacctgoid, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, dparamtidr, dparamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & dparoid.Text & ", '" & periodacctg.Text & "', '" & dparno.Text & "', '" & dpardate.Text & "', " & custoid.Text & ", " & acctgoid.SelectedValue & ", " & cashbankoid.Text & ", '" & dparpaytype.SelectedValue & "', " & dparpayacctgoid.SelectedValue & ", '" & Tchar(dparpayrefno.Text) & "', '" & IIf(dparpaytype.SelectedValue <> "BKM", dparduedate.Text, "1/1/1900") & "', " & curroid.SelectedValue & ", " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & ToDouble(dparamt.Text) & ", 0, '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(dparpaytype.SelectedValue = "BGM", dpartakegiro.Text, "1/1/1900") & "', " & iGiroAcctgOid & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ", " & ToDouble(dparamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dparamt.Text) * cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & dparoid.Text & " WHERE tablename='QL_TRNDPAR' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & dpardate.Text & "', cashbanktype='" & dparpaytype.SelectedValue & "', acctgoid=" & dparpayacctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(dparnett.Text) & ", cashbankamtidr=" & ToDouble(dparnett.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamtusd=" & ToDouble(dparnett.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & custoid.Text & ", cashbankduedate='" & IIf(dparpaytype.SelectedValue <> "BKM", dparduedate.Text, "1/1/1900") & "', cashbankrefno='" & Tchar(dparpayrefno.Text) & "', cashbanknote='" & Tchar(dparnote.Text) & "', cashbankstatus='" & dparstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & IIf(dparpaytype.SelectedValue = "BGM", dpartakegiro.Text, "1/1/1900") & "', giroacctgoid=" & iGiroAcctgOid & ",addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_trndpar SET periodacctg='" & periodacctg.Text & "', dparno='" & dparno.Text & "', dpardate='" & dpardate.Text & "', custoid=" & custoid.Text & ", acctgoid=" & acctgoid.SelectedValue & ", dparpaytype='" & dparpaytype.SelectedValue & "', dparpayacctgoid=" & dparpayacctgoid.SelectedValue & ", dparpayrefno='" & Tchar(dparpayrefno.Text) & "', dparduedate='" & IIf(dparpaytype.SelectedValue <> "BKM", dparduedate.Text, "1/1/1900") & "', curroid=" & curroid.SelectedValue & ", rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", dparamt=" & ToDouble(dparamt.Text) & ", dparnote='" & Tchar(dparnote.Text) & "', dparstatus='" & dparstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, dpartakegiro='" & IIf(dparpaytype.SelectedValue = "BGM", dpartakegiro.Text, "1/1/1900") & "', giroacctgoid=" & iGiroAcctgOid & ",addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & ", dparamtidr=" & ToDouble(dparamt.Text) * cRate.GetRateMonthlyIDRValue & ", dparamtusd=" & ToDouble(dparamt.Text) * cRate.GetRateMonthlyUSDValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & dparoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If dparstatus.Text = "Post" Then
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'DP A/R|No=" & dparno.Text & "', '" & dparstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' Cash/Bank/Giro
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & IIf(dparpaytype.SelectedValue = "BGM", iGiroAcctgOid, dparpayacctgoid.SelectedValue) & ", 'D', " & ToDouble(dparnett.Text) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparnett.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dparnett.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trndpar " & dparoid.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    ' Additional Cost 1-
                    If ToDouble(addacctgamt1.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid1.SelectedValue & ", 'D', " & Math.Abs(ToDouble(addacctgamt1.Text)) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt1.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt1.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - DP A/R|No=" & dparno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 2-
                    If ToDouble(addacctgamt2.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid2.SelectedValue & ", 'D', " & Math.Abs(ToDouble(addacctgamt2.Text)) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt2.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt2.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - DP A/R|No=" & dparno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 3-
                    If ToDouble(addacctgamt3.Text) < 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid3.SelectedValue & ", 'D', " & Math.Abs(ToDouble(addacctgamt3.Text)) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(ToDouble(addacctgamt3.Text)) * cRate.GetRateMonthlyIDRValue & ", " & Math.Abs(ToDouble(addacctgamt3.Text)) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - DP A/R|No=" & dparno.Text & "','K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Uang Muka
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'C', " & ToDouble(dparamt.Text) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dparamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(dparamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trndpar " & dparoid.Text & "')"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    ' Additional Cost 1+
                    If ToDouble(addacctgamt1.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid1.SelectedValue & ", 'C', " & ToDouble(addacctgamt1.Text) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - DP A/R|No=" & dparno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 2+
                    If ToDouble(addacctgamt2.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid2.SelectedValue & ", 'C', " & ToDouble(addacctgamt2.Text) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - DP A/R|No=" & dparno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 3+
                    If ToDouble(addacctgamt3.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid3.SelectedValue & ", 'C', " & ToDouble(addacctgamt3.Text) & ", '" & dparno.Text & "', '" & Tchar(dparnote.Text) & "', '" & dparstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - DP A/R|No=" & dparno.Text & "','M')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If

                objTrans.Commit() : xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        dparstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    dparstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback() : xCmd.Connection.Close()
                dparstatus.Text = "In Process"
                showMessage(ex.Message, 1)
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Draft No. have been regenerated because being used by another data. Your new Draft No. is " & dparoid.Text & ".<BR>"
            End If
            If dparstatus.Text = "Post" Then
                Session("SavedInfo") &= "Data have been posted with DP No. = " & dparno.Text & "."
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If dparoid.Text = "" Then
            showMessage("Please select Down Payment A/R data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trndpar", "dparoid", dparoid.Text, "dparstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                dparstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid IN (SELECT cashbankoid FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & dparoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trndpar WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & dparoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnDPAR.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        dparstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(dparno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpar " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(dparno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trndpar " & dparoid.Text & "' OR ISNULL(d.glother1, '')='QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub
#End Region

End Class