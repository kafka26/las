''Prgmr:mumun | LastUpdt:20.09.2014
''Update: Chafid | 04.12.2014

Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting
Partial Class Accounting_trnConAP
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim cRate As New ClassRate
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Function"
    Private Function validatingData() As String
        Dim sMsg As String = ""
        If suppoid.Text = "" Then
            sMsg &= "- Please Fill Supplier !!<BR>"
        End If
        If trnbelino.Text = "" Then
            sMsg &= "-  Please Fill Invoice No !!<BR>"
        End If 
        If amtbeli.Text = "" Or ToDouble(amtbeli.Text) <= 0 Then
            sMsg &= "- Total Invoice must be >= 0 !!<BR>"
        End If
        'If ToDouble(amtbayar.Text) >= ToDouble(amtbeli.Text) Then
        '	sMsg &= "- Total Payment must be < Total Invoice  !!<BR>"
        'End If
        If trnapnote.Text.Trim.Length > 200 Then
            sMsg &= "- maximal note is 200 character !!<BR>"
        End If
        If Not IsValidDate(invoicedate.Text, "MM/dd/yyyy", sMsg) Then
            sMsg &= "- Tanggal invoice salah !!<BR>"
        End If
        If ToDouble(trntaxpct.Text) > 100 Then
            sMsg &= "- Tax harus kurang dari atau sama dengan 100 !!<BR>"
        End If
        Return sMsg
    End Function

    Private Function IsValidPeriod() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""

        If invoicedate.Text = "" Then
            sError &= "- Please Fill Invoice Date field<BR>"
        Else
            If Not IsValidDate(invoicedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- INVOICE DATE invalid, " & sErr & "<BR>"
            End If
        End If

        If payduedate.Text = "" Then
            sError &= "- Please Fill Pay Due Date field<BR>"
        Else
            If Not IsValidDate(payduedate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- PAY DUE DATE invalid, " & sErr & "<BR>"
            Else
                If CDate(payduedate.Text) < CDate(invoicedate.Text) Then
                    sError &= "- Pay due Date must equal or more than invoice date <BR>"
                End If
            End If
		End If
		'If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
		'	showMessage("Period 2 must be more than Period 1 !", 2)
		'	Return False
		'End If

        If sError <> "" Then
			showMessage(sError, 2)
            lblPosting.Text = "IN PROCESS"
            Return False
        End If
        Return True
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Prosedure"
    
    Private Sub ReAmount()
        'rate.Text = ToMaskEdit(ToDouble(rate.Text), 2)
        If ToDouble(trntaxpct.Text) > 10 Then
            showMessage("Nilai Tax tidak boleh lebih dari 10 % ", 2)
        End If

        dpp.Text = ToMaskEdit(ToDouble(dpp.Text), 2)
        amttax.Text = ToMaskEdit(ToDouble(dpp.Text) * (ToDouble(trntaxpct.Text) / 100), 2)
        amtbeli.Text = ToMaskEdit(ToDouble(dpp.Text) + ToDouble(amttax.Text), 2)
        amtbayar.Text = ToMaskEdit(ToDouble(amtbayar.Text), 2)
        totalAP.Text = ToMaskEdit(ToDouble(amtbeli.Text) - ToDouble(amtbayar.Text), 2)
    End Sub

	Private Sub initDDL()
		' Init Payment Term
        'invoicedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLOutlet, sSql)
        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' ORDER BY gendesc"
		FillDDL(trnpaytype, sSql)
		sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
		FillDDL(CurroidDDL, sSql)
        cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), Format(GetServerTime, "MM/dd/yyyy"))
    End Sub

    Private Sub initAcctg()
        'sSql = "SELECT acctgoid,(+'('+acctgcode)+') '+acctgdesc  AS acctgdesc FROM QL_mstacctg WHERE acctgoid='" & acctgoid.Text & "'"
        'FillDDL(DDLacctgCode, sSql)
    End Sub

    Private Sub InitDDLVar()
        FillDDLAcctg(DDLacctgCode, "VAR_AP", CompnyCode)
    End Sub

    Public Sub binddata(ByVal sfilter As String)
		sSql = "SELECT 'False' AS checkvalue, bm.cmpcode,  bm.trnbelimstoid, bm.trnbelino, bm.trnbelidate, GETDATE() AS payduedate, s.suppoid, s.suppname, bm.trnbeliamtnetto AS amttrans, bm.trnbelimstnote, bm.trnbelimststatus, bm.trnbelitype AS typebeli FROM QL_trnbelimst bm /*--LEFT JOIN ql_conap ap ON bm.cmpcode=ap.cmpcode AND bm.trnbelimstoid=ap.refoid */ INNER JOIN QL_mstsupp s ON bm.suppoid=s.suppoid WHERE bm.cmpcode='" & CompnyCode & "' AND bm.trnbelimstoid < 0 AND " & ddlFilter.SelectedValue & " LIKE '%" & Tchar(FilterConap.Text) & "%' " & sfilter & " Order By bm.trnbelimstoid DESC"

        Session("TblTrn") = cKon.ambiltabel(sSql, "QL_trnbelimst")
        gvTrnConap.DataSource = Session("TblTrn")
        gvTrnConap.DataBind()
    End Sub

    Public Sub fillTextBox(ByVal sOutlet As String, ByVal vID As String)
        Try
            sSql = "SELECT b.trnbeliamt, B.cmpcode, B.trnbelimstoid,B.suppoid,B.trnbelimststatus,(SELECT s.apacctgoid FROM QL_mstsupp s WHERE s.suppoid=B.suppoid) AS acctgoid,B.trnbelidate,B.trnbelino,B.trnbelipaytype,B.trnbeliamt,B.trnbelimsttaxpct,B.trnbelimsttaxamt,B.trnbeliamtnetto,B.trnbelimstnote,GetDate() AS payduedate,ISNULL(P.payapoid,0)paymentoid,ISNULL(C1.payrefno,'')payrefno,ISNULL(C1.amtbayar,0)amtbayar,B.createuser,B.createtime,B.upduser,B.updtime,ISNULL(C1.paymentdate,'01/01/1900')paymentdate,ISNULL(C1.paymentacctgoid,0) paymentacctgoid, trnbeliref, B.trnbelitype AS typebeli,cr.curroid,cr.currcode +' - '+ cr.currdesc Currency,ISNULL(B.trnbeliamtnetto,0) AS Payment,(ISNULL(B.trnbeliamtnetto,0.00)-ISNULL(P.payapamt,0.00)) AS bayar,ISNULL(P.payapamt ,0.00) AS amttrans FROM ql_trnbelimst B /*LEFT JOIN ql_conap C ON B.trnbelimstoid = C.refoid */LEFT OUTER JOIN QL_trnpayap P ON  B.trnbelimstoid = P.refoid AND P.reftype='ql_trnbelimst' LEFT OUTER JOIN ql_conap C1 ON C1.reftype='QL_trnpayap' AND B.trnbelimstoid = C1.refoid AND P.payapoid = C1.payrefoid INNER JOIN QL_mstcurr cr ON cr.curroid=B.curroid AND cr.cmpcode=B.cmpcode WHERE B.trnbelimstoid=" & vID & " AND B.cmpcode='" & CompnyCode & "' ORDER BY B.trnbelimstoid DESC"

            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            'If conn.State = ConnectionState.Closed Then
            'conn.Open()
            'End If
            'xCmd = New SqlCommand(sSql, conn)
            'xreader = xCmd.ExecuteReader
            While xreader.Read
                CompnyCode = xreader("cmpcode").ToString
                trnbelimstoid.Text = Trim(xreader("trnbelimstoid").ToString)
                suppoid.Text = Trim(xreader("suppoid").ToString)
                lblPosting.Text = Trim(xreader("trnbelimststatus").ToString)
                acctgoid.Text = Trim(xreader("acctgoid").ToString)
                invoicedate.Text = Format(CDate(Trim(xreader("trnbelidate").ToString)), "MM/dd/yyyy")
                trnbelino.Text = (xreader("trnbelino").ToString)
                trnpaytype.SelectedValue = Trim(xreader("trnbelipaytype").ToString)
                dpp.Text = ToMaskEdit(ToDouble((xreader("trnbeliamt").ToString)), 2)
                trntaxpct.Text = ToMaskEdit(ToDouble((xreader("trnbelimsttaxpct").ToString)), 2)
                amttax.Text = ToMaskEdit(ToDouble((xreader("trnbelimsttaxamt").ToString)), 2)
                amtbeli.Text = ToMaskEdit(ToDouble((xreader("trnbeliamtnetto").ToString)), 2)
                amtbayar.Text = ToMaskEdit(ToDouble((xreader("bayar").ToString)), 2)
                trnapnote.Text = Trim(xreader("trnbelimstnote").ToString)
                conapoid.Text = Trim(xreader("trnbelimstoid").ToString)
                If trnpaytype.SelectedItem.Text = "CASH" Then
                    payduedate.Text = CDate(invoicedate.Text)
                Else
                    sSql = "SELECT gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND genoid=" & trnpaytype.SelectedValue
                    Dim DueDate As Integer = GetStrData(sSql)
                    payduedate.Text = Format(CDate(invoicedate.Text).AddDays(DueDate), "MM/dd/yyyy")
                End If
                paymentoid.Text = Trim(xreader("paymentoid").ToString)
                paymentdate.Text = Format(CDate(Trim(xreader("paymentdate").ToString)), "MM/dd/yyyy")
                paymentacctgoid.Text = Trim(xreader("acctgoid").ToString)
                payrefno.Text = Trim(xreader("payrefno").ToString)
                CurroidDdl.SelectedValue = Trim(xreader("curroid").ToString)
                DDLacctgCode.SelectedValue = Trim(xreader("acctgoid").ToString)
                totalAP.Text = ToMaskEdit(ToDouble((xreader("amttrans").ToString)), 2)
                ReAmount()
                CrtUser.Text = Trim(xreader("createuser").ToString)
                CrtTime.Text = Trim(xreader("createtime").ToString)
                UpdUser.Text = Trim(xreader("upduser").ToString)
                UpdTime.Text = Trim(xreader("updtime").ToString)
                ' Other data
                acctgCode.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = " & acctgoid.Text)
                payacctg.Text = cKon.ambilscalar("SELECT acctgcode + '-' + acctgdesc FROM ql_mstacctg WHERE acctgoid = '" & paymentacctgoid.Text & "'")
                suppcode.Text = cKon.ambilscalar("SELECT suppname FROM ql_mstsupp WHERE suppoid = " & suppoid.Text)
                'DDLType_SelectedIndexChanged(Nothing, Nothing)
				'DDLacctgCode.SelectedValue = Trim(xreader("trnbeliref").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
        Finally
			If lblPosting.Text = "Approved" Or lblPosting.Text = "Closed" Or lblPosting.Text = "Post" Then
				btnPost1.Visible = False
				btnSave1.Visible = False
				btnDelete1.Visible = False
				ibRePost.Visible = False
			Else
				btnPost1.Visible = True
				btnSave1.Visible = True
				btnDelete1.Visible = True
				ibRePost.Visible = False
			End If
            'DDLOutlet.CssClass = "inpTextDisabled"
            'DDLOutlet.Enabled = False
        End Try
    End Sub

    Private Sub generateID()
        sSql = "SELECT ISNULL(MIN(payapoid),0) FROM ql_trnpayap WHERE cmpcode='" & CompnyCode & "' AND payapoid < 0"
        Session("vIDPayap") = cKon.ambilScalar(sSql)
        sSql = "SELECT ISNULL(MIN(conapoid),0) FROM ql_conap WHERE cmpcode='" & CompnyCode & "' AND conapoid < 0"
        Session("vIDConap") = cKon.ambilscalar(sSql)
        'sSql = "SELECT ISNULL(MIN(cashbankoid),0) FROM ql_trncashbankmst WHERE cmpcode='" & CompnyCode & "' AND cashbankoid < 0"
        'Session("vIDCashBank") = cKon.ambilScalar(sSql)
        paymentoid.Text = Session("vIDPayap") - 1
        conapoid.Text = Session("vIDConap") - 1
        'cashbankoid.Text = Session("vIDCashBank") - 1
    End Sub

    Private Sub generateGLID()
        'sSql = "SELECT ISNULL(MIN(glmstoid),0) FROM ql_trnglmst WHERE cmpcode='" & CompnyCode & "' AND glmstoid < 0"
        'Session("vIDGLMst") = cKon.ambilScalar(sSql)
        'sSql = "SELECT ISNULL(MIN(gldtloid),0) FROM ql_trngldtl WHERE cmpcode='" & CompnyCode & "' AND gldtloid < 0"
        'Session("vIDGLDtl") = cKon.ambilScalar(sSql)
        'trnglmstoid.Text = Session("vIDGLMst") - 1
        'trngldtloid.Text = Session("vIDGLDtl") - 1
    End Sub

    Public Sub bindDataSupplier()
        'Dim dt As New DataTable

        sSql = "select suppoid,suppcode,supptype,suppname,suppaddr,apacctgoid AS coa_hutang, acctgcode AS coa FROM QL_mstsupp s inner join ql_mstacctg a ON a.acctgoid=s.apacctgoid WHERE s.cmpcode = '" & CompnyCode & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppname "

        Dim objTbl As DataTable = cKon.ambiltabel(sSql, "QL_mstsupp")
        Session("TblSupp") = objTbl
        gvSupplier.DataSource = objTbl
        gvSupplier.DataBind()

        'Session("TblSupp") = dt
        'gvSupplier.DataSource = Session("TblSupp")
        'gvSupplier.DataBind()
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblTrn") IsNot Nothing Then
            Dim dt As DataTable = Session("TblTrn")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTrnConap.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTrnConap.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTrnConap.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "trnbelimstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport(Optional ByVal sType As String = "")
        Try
            If sType = "" Then
                report.Load(Server.MapPath(folderReport & "rptConap.rpt"))
            Else
                report.Load(Server.MapPath(folderReport & "rptConapXls.rpt"))
            End If
            'Dim sWhere As String = " WHERE cmpcode='PRKP' AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
            'If cbCategory.Checked Then
            '    sWhere &= " AND acctggrp1='" & FilterDDLCategory.SelectedValue & "'"
            'End If
            'If cbStatus.Checked Then
            '    If FilterDDLStatus.SelectedValue <> "ALL" Then
            '        sWhere &= " AND activeflag='" & FilterDDLStatus.SelectedValue & "'"
            '    End If
            'End If
            'If checkPagePermission("~\Master\mstAcctg.aspx", Session("SpecialAccess")) = False Then
            '    sWhere &= " AND createuser='" & Session("UserID") & "'"
            'End If
            Dim sWhere As String = ""
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            If sType = "" Then
                report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "APInitBalanceReport")
            Else
                report.ExportToHttpResponse(ExportFormatType.Excel, Response, True, "APInitBalanceReport")
            End If
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
    End Sub 'OK

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
			Session("CompnyCode") = cmpcode
			Session("sCmpcode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnconap.aspx")
        End If
        If checkPagePermission("~\Accounting\trnconap.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - A/P Initial Balance "
        Session("oid") = Request.QueryString("oid")
        btnPost1.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Post this data?');")
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to delete this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to Post this data?');")
        Page.Title = "A/P Initial Balance"
        If Not Page.IsPostBack Then
            initDDL()
			InitDDLVar()
            'cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), invoicedate.text)
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            Dim sDate As Date = Format(CUTOFFDATE.AddDays(-1), "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                btnDelete1.Visible = True
				fillTextBox(Session("sCmpcode"), Session("oid"))
                TabContainer1.ActiveTabIndex = 1
                'initAcctg()
            Else
                generateID()
                If CUTOFFDATE = "1/1/1900" Then
                    invoicedate.Text = Session("CutOffDate")
                Else
					invoicedate.Text = Format(CUTOFFDATE.AddDays(-1), "MM/dd/yyyy")
                End If
                trnpaytype_SelectedIndexChanged(Nothing, Nothing)
				btnDelete1.Visible = False
				CrtUser.Text = Session("UserID")
                CrtTime.Text = GetServerTime()
                UpdUser.Text = "-"
                UpdTime.Text = "-"
                TabContainer1.ActiveTabIndex = 0
                lblPosting.Text = "IN PROCESS"
            End If
		End If
		If lblPosting.Text = "Approved" Or lblPosting.Text = "Closed" Or lblPosting.Text = "Post" Then
			btnPost1.Visible = False
			btnSave1.Visible = False
			btnDelete1.Visible = False
			ibRePost.Visible = False
		Else
			btnPost1.Visible = True
			btnSave1.Visible = True
			btnDelete1.Visible = True
			ibRePost.Visible = False
		End If
        generateGLID()
    End Sub

    Protected Sub btnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        bindDataSupplier()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        suppoid.Text = gvSupplier.SelectedDataKey(0).ToString()
        suppcode.Text = gvSupplier.SelectedDataKey(2).ToString()
		acctgoid.Text = gvSupplier.SelectedDataKey("coa_hutang").ToString().Trim
        acctgCode.Text = gvSupplier.SelectedDataKey("coa").ToString().Trim
		'DDLacctgCode.SelectedValue = gvSupplier.SelectedDataKey("coa_hutang").ToString().Trim
		DDLType.SelectedValue = gvSupplier.SelectedDataKey("supptype").ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
        sSql = "SELECT acctgoid,acctgcode + '-' + acctgdesc as desk FROM QL_mstacctg WHERE acctgoid='" & acctgoid.Text & "'"
        FillDDL(DDLacctgCode, sSql)
        'Dim otable As DataTable = cKon.ambiltabel(sSql, "QL_mstacctg1")
		'Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_mstrate")
        'If otable.Rows.Count > 0 Then
        '   DDLacctgCode.SelectedValue = otable.Rows(0).Item("acctgoid")
        '	acctgCode.Text = otable.Rows(0).Item("desk")
        'End If

        'If ToDouble(acctgoid.Text) = 0 Then
        '    btnSearcAcctg.Visible = True
        'Else
        '    btnSearcAcctg.Visible = False
        'End If
        'gvSupplier.Visible = False

    End Sub

	Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
		'Dim objTrans As SqlClient.SqlTransaction
		'If conn.State = ConnectionState.Closed Then
		'    conn.Open()
		'End If
		'objTrans = conn.BeginTransaction()
		'xCmd.Connection = conn
		'xCmd.Transaction = objTrans
		'Try
		'    ' ql_trnbelimst
		'    sSql = "UPDATE ql_trnbelimst SET trnbelimststatus='DELETE' WHERE cmpcode='" & CompnyCode & "' and trnbelimstoid=" & trnbelimstoid.Text
		'    xCmd.CommandText = sSql
		'    xCmd.ExecuteNonQuery()

		'    ' ql_conap - Invoice
		'    sSql = "DELETE QL_conap WHERE cmpcode='" & CompnyCode & "' and conapoid=" & conapoid.Text
		'    xCmd.CommandText = sSql
		'    xCmd.ExecuteNonQuery()

		'    ' ql_trnpayap
		'    sSql = "DELETE ql_trnpayap WHERE cmpcode='" & CompnyCode & "' and payapoid=" & paymentoid.Text
		'    xCmd.CommandText = sSql
		'    xCmd.ExecuteNonQuery()

		'    ' ql_conap - Payment
		'    sSql = "DELETE QL_conap WHERE cmpcode='" & CompnyCode & "' and conapoid=" & conapoid.Text - 1
		'    xCmd.CommandText = sSql
		'    xCmd.ExecuteNonQuery()

		'    objTrans.Commit()
		'    xCmd.Connection.Close()
		'Catch ex As Exception
		'    objTrans.Rollback()
		'    xCmd.Connection.Close()
		'    showMessage(ex.Message, 1)
		'    Exit Sub
		'End Try
		'If lblPOST.Text <> "" Then
		'    Session("SavedInfo") &= "Data telah dihapus !!! Invoice No = " & trnbelino.Text
		'End If
		'If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
		'    showMessage(Session("SavedInfo"), 3)
		'Else
		'    Response.Redirect("~\Accounting\trninitdpap.aspx?awal=true")
		'End If
	End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlFilter.SelectedIndex = -1
        FilterConap.Text = ""
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub gvTrnConap_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrnConap.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = Format(CDate(e.Row.Cells(5).Text), "MM/dd/yyyy")
            'e.Row.Cells(6).Text = Format(CDate(e.Row.Cells(6).Text), "MM/dd/yyyy")
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble((e.Row.Cells(6).Text)), 2)
            If e.Row.Cells(7).Text = "Approved" Then
                e.Row.Cells(8).Enabled = "false"
            End If
        End If
    End Sub

    Protected Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterConap.Text = ""
    End Sub

    Protected Sub trntaxpct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmount()
    End Sub

    Protected Sub amtbeli_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
		ReAmount()
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblPosting.Text = "Approved"
		'btnSave_Click(sender, e)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowCOAPosting("CONAP=" & Tchar(trnbelino.Text), CompnyCode, gvakun)
        pnlPosting2.Visible = True : btnHidePosting2.Visible = True : mpePosting2.Show()
    End Sub

    Protected Sub gvakun_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvakun.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit((ToDouble(e.Row.Cells(4).Text)), 2)
            e.Row.Cells(3).Text = ToMaskEdit((ToDouble(e.Row.Cells(3).Text)), 2)
        End If
    End Sub

    Protected Sub gvTrnConap_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTrnConap.PageIndexChanging
        gvTrnConap.PageIndex = e.NewPageIndex
        UpdateCheckedValue()
        gvTrnConap.DataSource = Session("TblTrn")
        gvTrnConap.DataBind()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub trnpaytype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If trnpaytype.SelectedItem.Text = "CASH" Then
            payduedate.Text = CDate(invoicedate.Text)
        Else
            sSql = "SELECT gendesc FROM ql_mstgen WHERE cmpcode='" & CompnyCode & "' AND gengroup='PAYMENT TYPE' AND genoid=" & trnpaytype.SelectedValue
            Dim DueDate As Integer = GetStrData(sSql)
            payduedate.Text = Format(CDate(invoicedate.Text).AddDays(DueDate), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub invoicedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles invoicedate.TextChanged
        '    If invoicedate.text = "" Then
        '        invoicedate.text = Format(GetServerTime(), "MM/dd/yyyy")
        '    End If
    End Sub

    Protected Sub payduedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If payduedate.Text = "" Then
        '    payduedate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        'End If
    End Sub

    Protected Sub ImbEraseSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        suppoid.Text = ""
        suppcode.Text = ""
        'initDDL()
        'InitDDLVar()
        'gvSupplier.Visible = False
    End Sub

    Protected Sub imbPayAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sVar() As String = {"VAR_CASH", "VAR_BANK"}
        FillGVAcctg(gvAcctg, sVar, Session("CompnyCode"), " AND (acctgcode LIKE '%" & Tchar(payacctg.Text) & "%' OR acctgdesc LIKE '%" & Tchar(payacctg.Text) & "%')")
        gvAcctg.Visible = True
    End Sub

    Protected Sub gvAcctg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        paymentacctgoid.Text = gvAcctg.SelectedDataKey.Item("acctgoid").ToString
        payacctg.Text = gvAcctg.SelectedDataKey.Item("acctgdesc").ToString
        gvAcctg.Visible = False
    End Sub

    Protected Sub ImbErasePayAcc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        paymentacctgoid.Text = ""
        payacctg.Text = ""
        gvAcctg.Visible = False
    End Sub

    Protected Sub gvAcctg_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        paymentacctgoid.Text = gvAcctg.SelectedDataKey.Item("acctgoid").ToString
        payacctg.Text = gvAcctg.SelectedDataKey.Item("acctgcode").ToString + " - " + gvAcctg.SelectedDataKey.Item("acctgdesc").ToString
        gvAcctg.Visible = False
    End Sub

    Protected Sub gvTrnConap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvTrnConap.SelectedIndexChanged
        Response.Redirect("trnconap.aspx?cmpcode=" & gvTrnConap.SelectedDataKey.Item("cmpcode").ToString & "&oid=" & gvTrnConap.SelectedDataKey.Item("trnbelimstoid").ToString & "")
    End Sub

	Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
		gvSupplier.PageIndex = e.NewPageIndex
		bindDataSupplier()
		mpeListSupp.Show()
	End Sub

    Protected Sub btnSearcAcctg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub imgFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sStatus As String = ""
        If FilterDDLStatus.SelectedValue <> "All" Then
            sStatus = " AND bm.trnbelimststatus='" & FilterDDLStatus.SelectedValue & "'"
        Else
            sStatus = " AND bm.trnbelimststatus LIKE '%%'"
        End If
        binddata(sStatus)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub imgViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'FilterDDLStatus.SelectedItem.Text = "All"
        FilterConap.Text = ""
        ' :cbStatus.Checked = "false"
        binddata(FilterConap.Text)
        lblViewInfo.Visible = False
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sValue As String = validatingData()
        If sValue <> "" Then
            lblPosting.Text = "IN PROCESS"
            showMessage(sValue, 2)
            Exit Sub
        End If
		If IsValidPeriod() Then
			Dim CutDateOff As Date
			sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
			If Not IsDate(CDate(GetStrData(sSql))) Then
				showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
				Exit Sub
			Else
				CutDateOff = CDate(GetStrData(sSql))
			End If
			cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), Format(CutDateOff.AddDays(-1), "MM/dd/yyyy"))
			generateID()
			If Session("oid") = Nothing And Session("oid") = "" Then
				sSql = "SELECT ISNULL(MIN(trnbelimstoid),0) FROM ql_trnbelimst WHERE cmpcode='" & Session("CompnyCode") & "' AND trnbelimstoid < 0"
				Session("vIDTrnBeli") = cKon.ambilscalar(sSql)
				trnbelimstoid.Text = Session("vIDTrnBeli") - 1

				sSql = "SELECT COUNT(*) FROM QL_trnbelimst WHERE cmpcode='" & Session("CompnyCode") & "' AND trnbelino='" & Tchar(trnbelino.Text) & "' "
				If CheckDataExists(sSql) Then
					showMessage("- Invoice Number " & trnbelino.Text & " has been used before", 2)
					lblPosting.Text = "IN PROCESS"
					Exit Sub
				End If
			Else
				If lblPosting.Text <> "IN PROCESS" Then
					sSql = "select count(-1) FROM QL_trnpayap WHERE cmpcode='" & CompnyCode & "' AND reftype='QL_trnbelimst' AND refoid=" & trnbelimstoid.Text
					If GetStrData(sSql) > 0 Then
						showMessage("- Sorry you can't Post this invoice, cause there's already payment for this invoice !!", 2)
						Exit Sub
					End If
				End If
			End If

			If ToDouble(trntaxpct.Text) > 10 Then
				showMessage("Nilai Tax tidak boleh lebih dari 10 % ", 2)
				Exit Sub
			End If
			If trnapnote.Text.Length > 98 Then
				showMessage("Note tidak boleh lebih dari 100 Character ", 2)
				Exit Sub
			End If
			Dim objTrans As SqlClient.SqlTransaction
			If conn.State = ConnectionState.Closed Then
				conn.Open()
			End If
			objTrans = conn.BeginTransaction()
			xCmd.Connection = conn
			xCmd.Transaction = objTrans
			Try
				If Session("oid") = Nothing And Session("oid") = "" Then
					'Dim RateIDR As Double = cRate.GetRateDailyIDRValue
					'Dim RateUSD As Double = cRate.GetRateDailyUSDValue
					'Dim Rate2IDR As Double = cRate.GetRateMonthlyIDRValue
					'Dim Rate2USD As Double = cRate.GetRateMonthlyUSDValue
					sSql = "INSERT INTO QL_trnbelimst" & _
					"(cmpcode,trnbelimstoid,periodacctg,trnbelino,trnbelidate,trnbelitype,trnbeliref,suppoid,pomstoid,trnbelipaytype,curroid,rateoid,beliratetoidr,beliratetousd,rate2oid,belirate2toidr,belirate2tousd,trnbeliamt,trnbeliamtidr,trnbeliamtusd,trnbelimsttaxtype,trnbelimsttaxpct,trnbelimsttaxamt,trnbelimsttaxamtidr,trnbelimsttaxamtusd,trnbeliamtnetto,trnbeliamtnettoidr,trnbeliamtnettousd,trnbelimstnote,trnbelimststatus,createuser,createtime,upduser,updtime) VALUES " & _
					" ('" & Session("CompnyCode") & "', " & trnbelimstoid.Text & ",'" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "','" & Tchar(trnbelino.Text.Trim) & "','" & CDate(invoicedate.Text) & "','',''," & suppoid.Text & ",0,'" & trnpaytype.SelectedValue & "','" & CurroidDDL.SelectedValue & "','" & cRate.GetRateDailyOid & "','" & cRate.GetRateDailyIDRValue & "','" & cRate.GetRateDailyUSDValue & "','" & cRate.GetRateMonthlyOid & "','" & cRate.GetRateMonthlyIDRValue & "','" & cRate.GetRateMonthlyUSDValue & "','" & ToDouble(dpp.Text) & "','" & ToDouble(dpp.Text) * cRate.GetRateMonthlyIDRValue & "','" & ToDouble(dpp.Text) * cRate.GetRateMonthlyUSDValue & "'," & IIf(ToDouble(trntaxpct.Text) = 0, "'NONTAX'", "'EXCLUSIVE'") & ",'" & ToDouble(trntaxpct.Text) & "','" & ToDouble(amttax.Text) & "','" & ToDouble(amttax.Text) * cRate.GetRateMonthlyIDRValue & "','" & ToDouble(amttax.Text) * cRate.GetRateMonthlyUSDValue & "','" & ToDouble(amtbeli.Text) & "','" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "','" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "','" & Tchar(trnapnote.Text.Trim) & "','" & Tchar(lblPosting.Text) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
				Else
					sSql = "UPDATE QL_trnbelimst SET periodacctg='" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "', trnbelino='" & Tchar(trnbelino.Text.Trim) & "', trnbelidate='" & CDate(invoicedate.Text) & "', suppoid='" & suppoid.Text & "', trnbeliamt='" & ToDouble(dpp.Text) & "',trnbeliamtidr='" & ToDouble(dpp.Text) * cRate.GetRateMonthlyIDRValue & "',trnbeliamtusd='" & ToDouble(dpp.Text) * cRate.GetRateMonthlyUSDValue & "',trnbelimsttaxpct=" & ToDouble(trntaxpct.Text) & ", trnbelimsttaxamt=" & ToDouble(amttax.Text) & ", trnbelimsttaxamtidr='" & ToDouble(amttax.Text) * cRate.GetRateMonthlyIDRValue & "',trnbelimsttaxamtusd='" & ToDouble(amttax.Text) * cRate.GetRateMonthlyUSDValue & "',trnbeliamtnetto=" & ToDouble(amtbeli.Text) & ", trnbeliamtnettoidr='" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "',trnbeliamtnettousd='" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "',trnbelimstnote='" & Tchar(trnapnote.Text.Trim) & "', trnbelimststatus='" & lblPosting.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, trnbelimsttaxtype=" & IIf(ToDouble(trntaxpct.Text) = 0, "'NONTAX'", "'EXCLUSIVE'") & ", trnbelipaytype=" & trnpaytype.SelectedValue & ", trnbeliref='" & DDLacctgCode.SelectedValue & "' WHERE cmpcode='" & Session("CompnyCode") & "' AND trnbelimstoid=" & trnbelimstoid.Text
					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()

					'sSql = "DELETE FROM QL_conap WHERE cmpcode='" & Session("CompnyCode") & "' AND reftype='QL_trnbelimst' AND refoid=" & trnbelimstoid.Text
					'xCmd.CommandText = sSql
					'xCmd.ExecuteNonQuery()
					'If ToDouble(amtbayar.Text) > 0 Then
					'    sSql = "DELETE FROM QL_trnpayap WHERE cmpcode='" & Session("CompnyCode") & "' AND reftype='QL_trnbelimst' AND refoid=" & trnbelimstoid.Text
					'    xCmd.CommandText = sSql
					'    xCmd.ExecuteNonQuery()

					'    sSql = "DELETE FROM QL_conap WHERE cmpcode='" & Session("CompnyCode") & "' AND reftype='QL_trnpayap' AND refoid=" & trnbelimstoid.Text
					'    xCmd.CommandText = sSql
					'    xCmd.ExecuteNonQuery()
					'End If
				End If

				If lblPosting.Text = "Approved" Then
					sSql = " INSERT INTO QL_conap" & _
					   "(cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,curroid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnapnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) " & _
					   " VALUES('" & CompnyCode & "'," & conapoid.Text & ",'QL_Trnbelimst'," & trnbelimstoid.Text & ",0," & suppoid.Text & "," & DDLacctgCode.SelectedValue & "," & CurroidDDL.SelectedValue & ",'Post','AP BALANCE','" & CDate(invoicedate.Text) & "','" & GetDateToPeriodAcctg(CDate(invoicedate.Text)) & "','',0,'','','" & CDate(payduedate.Text) & "','" & ToDouble(amtbeli.Text) & "',0,'" & Tchar(trnapnote.Text) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyIDRValue & "',0,'" & ToDouble(amtbeli.Text) * cRate.GetRateMonthlyUSDValue & "',0,0)"

					xCmd.CommandText = sSql
					xCmd.ExecuteNonQuery()
				End If
				' ql_conap
				objTrans.Commit()
				xCmd.Connection.Close()
			Catch ex As Exception
				lblPosting.Text = ""
				objTrans.Rollback() : conn.Close()
				showMessage(ex.ToString, 1)
				Exit Sub
			End Try
			If lblPosting.Text = "Approved" Then
				Session("SavedInfo") &= "Data telah diposting !!! Invoice No = " & trnbelino.Text
			End If
			If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
				showMessage(Session("SavedInfo"), 3)
			Else
				Response.Redirect("~\Accounting\trnconap.aspx?awal=true")
			End If
			If multiplepost.Text = "false" Then
				Response.Redirect("trnconap.aspx?awal=true")
			End If
		End If
    End Sub

    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub btnDelete1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans
        Try
            ' ql_trnbelimst
            sSql = "UPDATE ql_trnbelimst SET trnbelimststatus='DELETE' WHERE cmpcode='" & CompnyCode & "' and trnbelimstoid=" & trnbelimstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            ' ql_conap - Invoice
            sSql = "DELETE QL_conap WHERE cmpcode='" & CompnyCode & "' and conapoid=" & conapoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            ' ql_trnpayap
            sSql = "DELETE ql_trnpayap WHERE cmpcode='" & CompnyCode & "' and payapoid=" & paymentoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            ' ql_conap - Payment
            sSql = "DELETE QL_conap WHERE cmpcode='" & CompnyCode & "' and conapoid=" & conapoid.Text - 1
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnCancel1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub btnDelete1_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Connection = conn
        xCmd.Transaction = objTrans
        Try
            ' ql_trnbelimst
            sSql = "DELETE FROM ql_trnbelimst WHERE cmpcode='" & CompnyCode & "' and trnbelimstoid=" & trnbelimstoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()

            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If lblPosting.Text <> "" Then
            Session("SavedInfo") &= "Data telah dihapus !!! Invoice No = " & trnbelino.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trninitdpap.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnPost1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPosting.Text = "Approved"
        btnSave1_Click(sender, e)
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        multiplepost.Text = "true"
        UpdateCheckedValue()
        If Session("TblTrn") IsNot Nothing Then
            Dim dv As DataView = Session("TblTrn").DefaultView
            dv.RowFilter = "checkvalue='True'"
            For C1 As Integer = 0 To dv.Count - 1
                Session("oid") = dv(C1)("trnbelimstoid").ToString
                fillTextBox(Session("sCmpcode"), Session("oid"))
                lblPosting.Text = "Approved"
                btnSave1_Click(sender, e)
            Next
            dv.RowFilter = ""
        End If
        'If lblPosting.Text = "Approved" Then
        '    Session("SavedInfo") &= " - Semua Data telah diposting"
        'End If
        'If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
        '    showMessage(Session("SavedInfo"), 3)
        'Else
        '    Response.Redirect("~\Accounting\trnconap.aspx?awal=true")
        'End If
        Response.Redirect("trnconap.aspx?awal=true")
    End Sub

    Protected Sub ibselectall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblTrn") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrn")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrn")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnbelimstoid=" & dtTbl.Rows(C1)("trnbelimstoid")
                    If dtTbl.Rows(C1)("trnbelimststatus") = "Approved" Then
                        objView(0)("CheckValue") = "False"
                        dtTbl.Rows(C1)("CheckValue") = "False"
                    Else
                        objView(0)("CheckValue") = "True"
                        dtTbl.Rows(C1)("CheckValue") = "True"
                    End If
                    objView.RowFilter = ""
                Next
                'objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                'Session("TblMat") = objTbl
                Session("TblTrn") = dtTbl
                gvTrnConap.DataSource = Session("TblTrn")
                gvTrnConap.DataBind()
            Else
                'Session("WarningListMat") = "No material data can't be selected!"
                'showMessage(Session("WarningListMat"), 2)
            End If
        Else
            'Session("WarningListMat") = "No material data can't be selected!"
            'showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub ibuncheckall_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If Not Session("TblTrn") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblTrn")
            If dtTbl.Rows.Count > 0 Then
                Dim objTbl As DataTable = Session("TblTrn")
                Dim objView As DataView = objTbl.DefaultView
                objView.AllowEdit = True
                objView.RowFilter = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    objView.RowFilter = "trnbelimstoid=" & dtTbl.Rows(C1)("trnbelimstoid")
                    objView(0)("CheckValue") = "False"
                    dtTbl.Rows(C1)("CheckValue") = "False"
                    objView.RowFilter = ""
                Next
                'objTbl.AcceptChanges()
                dtTbl.AcceptChanges()
                'Session("TblMat") = objTbl
                Session("TblTrn") = dtTbl
                gvTrnConap.DataSource = Session("TblTrn")
                gvTrnConap.DataBind()
            Else
                'Session("WarningListMat") = "No material data can't be selected!"
                'showMessage(Session("WarningListMat"), 2)
            End If
        Else
            'Session("WarningListMat") = "No material data can't be selected!"
            'showMessage(Session("WarningListMat"), 2)
        End If
    End Sub

    Protected Sub DDLType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim descacctg As String
        'Dim CompnyCode As String = System.Configuration.ConfigurationManager.AppSettings("CompanyCode")
        'DDLacctgCode.Items.Clear()
        'Dim sSql As String = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & Session("CompnyCode") & "' AND interfacevar='VAR_AP'"
        ' AND interfaceres1='PRKP'"
        'Dim sCode As String = GetStrData(sSql)
        'If sCode = "" Then
        '    sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_AP' "
        '    AND interfaceres1='All'"
        '    sCode = GetStrData(sSql)
        'End If
        'If sCode <> "" Then
        '    If DDLType.SelectedValue = "Raw" Then
        '        descacctg = "AND a.acctgdesc like '%- bahan%'"
        '    ElseIf DDLType.SelectedValue = "Service" Then
        '        descacctg = "AND a.acctgdesc like '%- jasa%'"
        '    Else
        '        descacctg = "AND a.acctgdesc like '%- non bahan%'"
        '    End If
        '    sSql = "SELECT DISTINCT a.acctgoid, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' " & descacctg & " AND a.activeflag='ACTIVE' AND ("
        '    Dim sSplitCode() As String = sCode.Split(",")
        '    For C1 As Integer = 0 To sSplitCode.Length - 1
        '        sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
        '        If C1 < sSplitCode.Length - 1 Then
        '            sSql &= " OR "
        '        End If
        '    Next
        '    sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) ORDER BY acctgdesc"
        '    FillDDL(DDLacctgCode, sSql)
        'End If
    End Sub

    Protected Sub ibRePost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibRePost.Click
        statusRePost.Text = "REPOST"
        btnSave1_Click(sender, e)
    End Sub

    Protected Sub btnExcel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        PrintReport("Excel")
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnconap.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        bindDataSupplier()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        bindDataSupplier()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
	End Sub
#End Region
End Class
