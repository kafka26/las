Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnPayAP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate()
    Dim cRate2 As New ClassRate()
    Public folderReport As String = "~/Report/"
    Const iMask = 4, iRoundDigit = 6
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDiffDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If ToInteger(diffacctgoid.SelectedValue) <= 0 Then
            sError &= "- Please select DIFF. ACCOUNT field!<BR>"
        End If
        If diffamount.Text = "" Then
            sError &= "- Please fill A/P DIFF. AMOUNT field!<BR>"
        Else
            If ToDouble(diffamount.Text) <= 0 Then
                sError &= "- A/P DIFF. AMOUNT must be more than 0!<BR>"
            Else
                'If ToDouble(diffamount.Text) > ToDouble(apbalance.Text) Then
                '    sError &= "- A/P DIFF. AMOUNT must be less or equal than A/P BALANCE!<BR>"
                'End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If aprawmstoid.Text = "" Then
            sError &= "- Please select A/P NO. field!<BR>"
        End If
        If payapamt.Text = "" Then
            sError &= "- Please fill A/P PAYMENT AMT. field!<BR>"
        Else
            If ToDouble(payapamt.Text) <= 0 Then
                sError &= "- A/P PAYMENT AMT. must be more than 0!<BR>"
            Else
                If ToDouble(payapamt.Text) < ToDouble(apbalance.Text) Then
                    If ToDouble(subtotalpayment.Text) > ToDouble(apbalance.Text) Then
                        sError &= "- TOTAL DIFFERENCE must be less or equal than " & ToMaskEdit(ToDouble(apbalance.Text) - ToDouble(payapamt.Text), iMask) & " (A/P BALANCE - A/P PAYMENT AMT.)!<BR>"
                    End If
                ElseIf ToDouble(payapamt.Text) > ToDouble(apbalance.Text) Then
                    If Math.Round(ToDouble(subtotalpayment.Text) - ToDouble(totaldiff.Text), iRoundDigit) <> Math.Round(ToDouble(apbalance.Text), iRoundDigit) Then
                        sError &= "- TOTAL DIFFERENCE must be " & ToMaskEdit(ToDouble(subtotalpayment.Text) - ToDouble(apbalance.Text), iMask) & " (SUB TOTAL PAYMENT - A/P BALANCE)!<BR>"
                    End If
                End If
            End If
        End If
        If apcurroid.SelectedValue <> "" And curroid.SelectedValue <> "" Then
            If apcurroid.SelectedValue <> curroid.SelectedValue Then
                If payapamtother.Text = "" Then
                    sError &= "- Please fill PAYMENT AMT. field!<BR>"
                Else
                    If ToDouble(payapamtother.Text) <= 0 Then
                        sError &= "- PAYMENT AMT. must be more than 0!<BR>"
                    End If
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sErr = "None"
            sError &= "- Please fill PAYMENT DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- PAYMENT DATE is invalid. " & sErr & "<BR>"
            Else
                If CDate(cashbankdate.Text) > Today Then
                    sError &= "- PAYMENT Date must less or equal than TODAY!<BR>"
                End If
            End If
        End If
        If suppoid.Text = "" Then
            sError &= "- Please select SUPPLIER field!<BR>"
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If ToDouble(payapnett.Text) <= 0 Then
            sError &= "- TOTAL PAYMENT must be more than 0!<BR>"
        Else
            If cashbanktype.SelectedValue = "BLK" Then
                If ToDouble(payapnett.Text) > ToDouble(dpapamt.Text) Then
                    sError &= "- TOTAL PAYMENT must be less than DP AMOUNT!<BR>"
                End If
            End If
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        Dim sErr2 As String = ""
        If cashbanktype.SelectedValue = "BBK" Or cashbanktype.SelectedValue = "BGK" Then
            If cashbankrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If cashbankduedate.Text = "" Then
                sErr2 = "None"
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr2) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If sErr = "" Then
                        If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                            sError &= "- DUE DATE must be more or equal than PAYMENT DATE!<BR>"
                        End If
                    End If
                End If
            End If
            If cashbanktype.SelectedValue = "BGK" Then
                If cashbanktakegiro.Text = "" Then
                    sError &= "- Please fill DATE TAKE GIRO field!<BR>"
                Else
                    Dim sErrGiro As String = ""
                    If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErrGiro) Then
                        sError &= "- DATE TAKE GIRO is invalid. " & sErrGiro & "<BR>"
                    End If
                End If
            End If
        ElseIf cashbanktype.SelectedValue = "BLK" Then
            If giroacctgoid.Text = "" Then
                sError &= "- Please select DP NO. field!<BR>"
            Else
                Dim sDPDate As String = GetStrData("SELECT CONVERT(VARCHAR(10), dpapdate, 101) dpapdate FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & giroacctgoid.Text)
                If CDate(cashbankdate.Text) < CDate(sDPDate) Then
                    sError &= "- PAYMENT DATE must be more or equal than DP DATE (DP DATE is " & sDPDate & ")!<BR>"
                End If
            End If
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!<BR>"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!<BR>"
            Else
                Dim sCode As String = "" : Dim sRefType As String = "" : Dim sRefColOid As String = "" : Dim sRefDate As String = ""
                For C1 As Integer = 0 To dtTbl.Rows.Count - 1
                    If Not sCode.Contains(dtTbl.Rows(C1)("apcurrcode").ToString) Then
                        sCode &= dtTbl.Rows(C1)("apcurrcode").ToString & ","
                    End If
                    If dtTbl.Rows(C1)("payapflag").ToString = "" Then
                        Dim dr() As DataRow = dtTbl.Select("dtlseq=" & dtTbl.Rows(C1)("dtlseq") & " AND payapflag='Kurang Bayar'")
                        If dr.Length > 0 Then
                            If ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) > ToDouble(dtTbl.Rows(C1)("apbalance").ToString) Then
                                sError &= "- TOTAL DIFFERENCE for A/P NO. " & dtTbl.Rows(C1)("aprawno").ToString & " must be less or equal than " & ToMaskEdit(ToDouble(dtTbl.Rows(C1)("apbalance").ToString) - ToDouble(dtTbl.Rows(C1)("payapamt").ToString), iMask) & " (A/P BALANCE - A/P PAYMENT AMT.)!<BR>"
                            End If
                        Else
                            dr = dtTbl.Select("dtlseq=" & dtTbl.Rows(C1)("dtlseq") & " AND payapflag='Lebih Bayar'")
                            If dr.Length > 0 Then
                                If Math.Round(ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString), iRoundDigit) - Math.Round(ToDouble(dtTbl.Rows(C1)("totaldiff").ToString), iRoundDigit) <> Math.Round(ToDouble(dtTbl.Rows(C1)("apbalance").ToString), iRoundDigit) Then
                                    'sError &= "- TOTAL DIFFERENCE for A/P NO. " & dtTbl.Rows(C1)("aprawno").ToString & " must be " & ToMaskEdit(ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("apbalance").ToString), iRoundDigit) & " (SUB TOTAL PAYMENT - A/P BALANCE)!<BR>"
                                End If
                            End If
                        End If
                    End If
                    sRefType = dtTbl.Rows(C1)("reftype").ToString
                    If sRefType.ToUpper = "QL_TRNBELIMST" Then
                        sRefColOid = "trnbelimstoid" : sRefDate = "trnbelidate"
                    ElseIf sRefType.ToUpper = "QL_TRNBELIMST_FA" Then
                        sRefColOid = "trnbelifamstoid" : sRefDate = "trnbelifadate"
                    ElseIf sRefType.ToUpper = "QL_TRNAPPRODUCTMST" Then
                        sRefColOid = "approductmstoid" : sRefDate = "approductdate"
                    End If
                    sSql = "SELECT CONVERT(VARCHAR(10), " & sRefDate & ",101) date FROM " & sRefType & " WHERE cmpcode='" & CompnyCode & "' AND " & sRefColOid & "=" & dtTbl.Rows(C1)("aprawmstoid").ToString
                    If CDate(cashbankdate.Text) < CDate(GetStrData(sSql)) Then
                        sError &= "- PAYMENT DATE must more or equal than INVOICE DATE! # " & dtTbl.Rows(C1)("aprawno").ToString & " #|(" & GetStrData(sSql) & ")<BR>"
                    End If
                Next
                If sCode <> "" Then
                    sCode = Left(sCode, sCode.Length - 1)
                    Dim sSplit() As String = sCode.Split(",")
                    If sSplit.Length > 1 Then
                        sError &= "- Every detail data must have same Currency!<BR>"
                    End If
                End If
            End If
        End If
        If addacctgoid1.SelectedValue <> 0 Then
            'If ToDouble(addacctgamt1.Text) <= 0 Then
            '    sError &= "- Additional Cost Amount 1 must be greater than 0!<BR>"
            'End If
        End If
        If addacctgoid2.SelectedValue <> 0 Then
            'If ToDouble(addacctgamt2.Text) <= 0 Then
            '    sError &= "- Additional Cost Amount 2 must be greater than 0!<BR>"
            'End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            'If ToDouble(addacctgamt3.Text) <= 0 Then
            '    sError &= "- Additional Cost Amount 3 must be greater than 0!<BR>"
            'End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If cashbanktype.SelectedValue = "BOK" Then
            If ToDouble(dpapamt.Text) < ToDouble(cashbankamt.Text) Then
                sError &= "- SI Amount < Total Payment!<BR>"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetSumDetail() As DataTable
        Dim dtRet As DataTable = New DataTable()
        dtRet.Columns.Add("acctgoid", Type.GetType("System.Int32"))
        dtRet.Columns.Add("acctgamt", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgflag", Type.GetType("System.String"))
        dtRet.Columns.Add("acctgamtidr", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtusd", Type.GetType("System.Double"))
        dtRet.Columns.Add("acctgamtother", Type.GetType("System.Double"))
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            dt.Columns.Add("payapamtpostidr", Type.GetType("System.Double"))
            dt.Columns.Add("payapamtpostusd", Type.GetType("System.Double"))
            Dim sOid As String = ""
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim sPlusMinus As String = "(#)"
                If dt.Rows(C1)("payapflag").ToString.ToUpper = "KURANG BAYAR" Then
                    sPlusMinus = "(-)"
                ElseIf dt.Rows(C1)("payapflag").ToString.ToUpper = "LEBIH BAYAR" Then
                    sPlusMinus = "(+)"
                End If
                If Not sOid.Contains(sPlusMinus & "[" & dt.Rows(C1)("apacctgoid").ToString & "]") Then
                    sOid &= sPlusMinus & "[" & dt.Rows(C1)("apacctgoid").ToString & "],"
                End If
                dt.Rows(C1)("payapamtpostidr") = ToDouble(dt.Rows(C1)("payapamtpost").ToString) * ToDouble(dt.Rows(C1)("apratetoidr").ToString)
                dt.Rows(C1)("payapamtpostusd") = ToDouble(dt.Rows(C1)("payapamtpost").ToString) * ToDouble(dt.Rows(C1)("apratetousd").ToString)
            Next
            If sOid <> "" Then
                sOid = Left(sOid, sOid.Length - 1).Replace("[", "").Replace("]", "")
                Dim sSplit() As String = sOid.Split(",")
                For C1 As Integer = 0 To sSplit.Length - 1
                    Dim dr As DataRow = dtRet.NewRow
                    Dim iOid As Integer = CInt(Right(sSplit(C1), sSplit(C1).Length - 3))
                    dr("acctgoid") = iOid
                    dr("acctgamt") = Math.Abs(ToDouble(dt.Compute("SUM(payapamtpost)", "apacctgoid=" & iOid & " AND payapflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgflag") = Left(sSplit(C1), 3)
                    dr("acctgamtidr") = Math.Abs(ToDouble(dt.Compute("SUM(payapamtpostidr)", "apacctgoid=" & iOid & " AND payapflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgamtusd") = Math.Abs(ToDouble(dt.Compute("SUM(payapamtpostusd)", "apacctgoid=" & iOid & " AND payapflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dr("acctgamtother") = Math.Abs(ToDouble(dt.Compute("SUM(payapamtother)", "apacctgoid=" & iOid & " AND payapflag='" & IIf(Left(sSplit(C1), 3) = "(#)", "", IIf(Left(sSplit(C1), 3) = "(+)", "Lebih Bayar", "Kurang Bayar")) & "'").ToString))
                    dtRet.Rows.Add(dr)
                Next
            End If
        End If
        Return dtRet
    End Function

    Private Function GetRateAP() As Integer
        GetRateAP = 0
        If Session("TblDtl") IsNot Nothing Then
            If Session("TblDtl").Rows.Count > 0 Then
                GetRateAP = CInt(Session("TblDtl").Rows(0)("apcurroid").ToString)
            End If
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        Dim sAsliTemp As String = sMessage.Replace("<br />", vbCrLf).Replace("<BR>", vbCrLf)
        Dim sTemp() As String = sAsliTemp.Split(vbCrLf)
        If sTemp.Length > 25 Then
            lblPopUpMsg.Text = "<textarea class='inpText' readonly='true' style='height:250px;width:99%;'>" & sAsliTemp & "</textarea>"
        Else
            lblPopUpMsg.Text = sMessage
        End If
        lblCaption.Text = strCaption
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='AP'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If GetStrData(sSql) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process A/P Payment data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitAllDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
       
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLCOAPayment()
        End If
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(curroid, sSql) Then
            ShowCurrSymbol(curroid.SelectedValue, 0)
        End If
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        If FillDDL(apcurroid, sSql) Then
            ShowCurrSymbol(apcurroid.SelectedValue, 1)
        End If

        InitDDLAdd()
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
    End Sub

    Private Sub ShowCurrSymbol(ByVal sCurr As String, ByVal iType As Int16)
        sSql = "SELECT (CASE WHEN currsymbol IS NULL THEN currcode WHEN currsymbol='' THEN currcode ELSE currsymbol END) AS currsymbol FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & sCurr
        If iType = 0 Then
            lblPayment.Text = GetStrData(sSql)
        Else
            lblAPPayment.Text = GetStrData(sSql)
            lblDiffPayment.Text = lblAPPayment.Text
        End If
        If curroid.SelectedValue.ToString <> apcurroid.SelectedValue.ToString Then
            payapamtother.Enabled = True : payapamtother.CssClass = "inpText"
        Else
            payapamtother.Enabled = False : payapamtother.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub InitDDLCOAPayment()
        Dim sVar As String = "VAR_CASH"
        Dim bVal As Boolean = False
        Dim bVal2 As Boolean = True
        Dim bVal3 As Boolean = False
        Dim sCss As String = "inpText"
        Dim sText As String = "Ref. No."
        Dim sText2 As String = "Due Date"
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : cashbankduedate.Text = "" : dpapamt.Text = ""
        EnableCurrency(False)
        If cashbanktype.SelectedValue = "BBK" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BGK" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BOK" Then
            sVar = "VAR_AR" : bVal2 = False : sCss = "inpTextDisabled"
            sText = "SI No." : sText2 = "SI Amount" : bVal3 = True : EnableCurrency(True)
        ElseIf cashbanktype.SelectedValue = "BLK" Then
            sVar = "VAR_DPAP" : bVal2 = False : sCss = "inpTextDisabled"
            sText = "DP No." : sText2 = "DP Amount" : bVal3 = True : EnableCurrency(True)
        End If
        ' Fill DDL COA Payment
        FillDDLAcctg(acctgoid, sVar, DDLBusUnit.SelectedValue) : SetCOACurrency(acctgoid.SelectedValue)
        lblWarnDueDate.Visible = bVal : cashbankduedate.Visible = bVal : imbDueDate.Visible = bVal : lblInfoDueDate.Visible = bVal : cashbankduedate.Text = ""
        lblDueDate.Visible = bVal3 : lblSeptDueDate.Visible = bVal3 : lblWarnRefNo.Visible = bVal3
        acctgoid.Enabled = bVal2 : acctgoid.CssClass = sCss : btnSearchDP.Visible = Not bVal2 : btnClearDP.Visible = Not bVal2 : cashbankrefno.Enabled = bVal2 : cashbankrefno.CssClass = sCss : dpapamt.Visible = Not bVal2
        lblRefNo.Text = sText : lblDueDate.Text = sText2
        If cashbanktype.SelectedValue = "BOK" Then
            FillDDLAcctg(acctgoid, New String() {"VAR_AR", "VAR_AR_OTHER"}, DDLBusUnit.SelectedValue)
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cashbankoid, cashbankno, CONVERT(VARCHAR(10), cashbankdate, 101) AS cashbankdate, suppname, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'TRANSFER' WHEN 'BGK' THEN 'GIRO/CHEQUE' WHEN 'BLK' THEN 'DOWN PAYMENT' ELSE 'BARTER' END) AS cashbanktype, cashbankstatus, cashbanknote, divname, 'False' AS checkvalue FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=personoid INNER JOIN QL_mstdivision div ON div.cmpcode=cb.cmpcode WHERE cashbankoid>0 AND cashbankgroup='AP'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, cb.cashbankdate) DESC, cb.cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub BindSupplierData()
        ' sSql = "SELECT DISTINCT apm.suppoid, suppcode, suppname, suppaddr, supptype FROM QL_trnaprawmst apm INNER JOIN QL_mstsupp s ON s.suppoid=apm.suppoid WHERE apm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND aprawmststatus='Approved' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppcode"
        sSql = "SELECT DISTINCT ap.suppoid,s.suppcode,s.suppname,s.suppaddr,s.supptype, g.gendesc AS paytype,s.custoid FROM QL_conap ap INNER JOIN QL_mstsupp s ON s.suppoid=ap.suppoid INNER JOIN QL_mstgen g ON g.genoid=supppaymentoid WHERE ap.cmpcode='" & DDLBusUnit.SelectedValue & "' /*AND ap.trnapstatus='Post'*/ AND ISNULL((SELECT pap.payapres1 FROM QL_trnpayap pap WHERE pap.cmpcode=ap.cmpcode AND pap.payapoid=ap.payrefoid),'')<>'Lebih Bayar' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' GROUP BY ap.suppoid,s.suppcode,s.suppname,s.suppaddr,s.supptype, g.gendesc,s.custoid HAVING SUM(ap.amttrans)>SUM(ap.amtbayar) ORDER BY suppcode"
        FillGV(gvListSupp, sSql, "QL_mstsupp")
    End Sub

    Private Sub InitDDLDiffAcctg()
        Dim sVar As String = ""
        If diffflag.SelectedIndex = 0 Then
            sVar = "VAR_LEBIH_BAYAR_AP"
        Else
            sVar = "VAR_KURANG_BAYAR_AP"
        End If
        FillDDLAcctg(diffacctgoid, sVar, DDLBusUnit.SelectedValue, "0", "-")
    End Sub

    Private Sub CreateTblDtlDiff()
        Dim dtlTable As DataTable = New DataTable("TabelAPPaymentDiff")
        dtlTable.Columns.Add("diffseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("diffflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("diffacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("diffaccount", Type.GetType("System.String"))
        dtlTable.Columns.Add("diffamount", Type.GetType("System.Double"))
        dtlTable.Columns.Add("diffamountother", Type.GetType("System.Double"))
        dtlTable.Columns.Add("diffnote", Type.GetType("System.String"))
        Session("TblDtlDiff") = dtlTable
    End Sub

    Private Sub ClearDetailDiff()
        diffseq.Text = "1"
        If Session("TblDtlDiff") Is Nothing = False Then
            Dim objTable As DataTable = Session("TblDtlDiff")
            diffseq.Text = objTable.Rows.Count + 1
        End If
        i_u3.Text = "New Detail"
        diffacctgoid.SelectedIndex = -1
        diffamount.Text = ""
        diffamountother.Text = ""
        diffnote.Text = ""
        gvDtlDiff.Columns(0).Visible = True
        gvDtlDiff.Columns(gvDtlDiff.Columns.Count - 1).Visible = True
        gvDtlDiff.SelectedIndex = -1
    End Sub

    Private Sub CountTotalDiff()
        Dim dVal As Double = 0
        If Session("TblDtlDiff") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtlDiff")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("diffamount").ToString)
            Next
        End If
        totaldiff.Text = ToMaskEdit(dVal, iMask)
        If diffflag.SelectedIndex = 0 Then
            subtotalpayment.Text = ToMaskEdit(ToDouble(payapamt.Text), iMask)
        Else
            subtotalpayment.Text = ToMaskEdit(ToDouble(payapamt.Text) + ToDouble(totaldiff.Text), iMask)
        End If
    End Sub

    Private Sub BindDPData()
        sSql = "SELECT dp.dpapoid, dp.dpapno, (CONVERT(VARCHAR(10), dp.dpapdate, 101)) AS dpapdate, dp.acctgoid AS dpapacctgoid, a.acctgdesc, dp.dpapnote, (dp.dpapamt - ISNULL(dp.dpapaccumamt, 0.0)) AS dpapamt FROM QL_trndpap dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid /*INNER JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid*/ WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListDP.SelectedValue & " LIKE '%" & Tchar(FilterTextListDP.Text) & "%' AND dp.dpapstatus='Post' AND dp.suppoid=" & suppoid.Text & " AND dp.curroid=" & curroid.SelectedValue & " AND dp.dpapamt > dp.dpapaccumamt ORDER BY dp.dpapoid"
        FillGV(gvListDP, sSql, "QL_trndpap")
    End Sub

    Private Sub BindSIData()
        sSql = "SELECT * FROM ("
        sSql &= "SELECT refoid trnjualmstoid, CASE reftype WHEN 'QL_trnjualmst' THEN (SELECT trnjualno FROM QL_trnjualmst m WHERE m.trnjualmstoid=refoid) WHEN 'QL_trntransitemmst' THEN (SELECT transitemno FROM QL_trntransitemmst m WHERE m.transitemmstoid=refoid) END trnjualno, CONVERT(VARCHAR(10), trnardate, 101) trnjualdate, '' trnjualnote, (amttrans - ISNULL((SELECT SUM(c.amtbayar) FROM QL_conar c LEFT JOIN QL_trnpayar pay ON pay.cmpcode=c.cmpcode AND c.refoid=pay.refoid AND c.reftype=pay.reftype WHERE c.payrefoid<>0 AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' AND c.reftype=jm.reftype AND c.refoid=jm.refoid),0)) trnjualamtnetto, trnardate, jm.reftype FROM QL_conar jm WHERE jm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND jm.custoid=" & custoid.Text & " AND jm.payrefoid=0 "
        sSql &= ") dt WHERE " & FilterDDLListSI.SelectedValue & " LIKE '%" & Tchar(FilterTextListSI.Text) & "%' ORDER BY trnardate"
        Dim dtData As DataTable = cKon.ambiltabel(sSql, "QL_conar")
        Dim dv As DataView = dtData.DefaultView
        dv.RowFilter = "trnjualamtnetto>0"
        Session("TblConar") = dv.ToTable
        gvListSI.DataSource = Session("TblConar")
        gvListSI.DataBind()
    End Sub

    Private Sub BindAPData()
        Dim sAdd As String = ""
        If Session("oid") IsNot Nothing And Session("oid") <> "" Then
            sAdd = " AND pay.cashbankoid<>" & Session("oid")
        End If
        sSql = "SELECT *, (apgrandtotal - appaidamt) ap_balance FROM (" & _
            "SELECT apm.cmpcode,apm.trnbelimstoid apoid,con.reftype,apm.trnbelino apno, '' as regno,(CONVERT(VARCHAR(10),apm.trnbelidate,101)) AS apdate, " & _
            "apm.trnbeliamtnetto apgrandtotal,apm.trnbelimstnote apnote,ISNULL(apm.trnbelimstres1,'') apmstres1, " & _
            "(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap LEFT JOIN QL_trnpayap pay ON ap.cmpcode=pay.cmpcode AND ap.payrefoid=pay.payapoid  " & _
            "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' " & sAdd & _
            "WHERE ap.payrefoid<>0 AND ap.cmpcode=con.cmpcode AND ap.reftype=con.reftype AND ap.refoid=con.refoid),0.0)) AS appaidamt, " & _
            "con.acctgoid AS apacctgoid,(a.acctgcode+' - '+a.acctgdesc) AS apaccount, " & _
            "(CONVERT(VARCHAR(10),apm.apdatetakegiro,101)) AS apdatetakegiro, " & _
            "apm.curroid,apm.belirate2toidr AS apratetoidr,(CASE WHEN ISNULL(apm.belirate2tousd, '0.00')='0.00' THEN ISNULL((SELECT rate2res2 FROM QL_mstrate2 r2 WHERE r2.rate2oid=apm.rate2oid), '0.00') ELSE apm.belirate2tousd END) AS apratetousd,apm.suppoid,'APRM' reftypedesc  " & _
            "FROM QL_trnbelimst apm INNER JOIN QL_conap con ON con.cmpcode=apm.cmpcode AND con.refoid=trnbelimstoid  " & _
            "INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid  " & _
            "WHERE con.reftype='QL_trnbelimst' AND ISNULL(con.payrefoid,0)=0 AND apm.trnbelimststatus='Approved' UNION ALL " & _
            "SELECT apm.cmpcode,apm.trnbelifamstoid apoid,con.reftype,apm.trnbelifano apno, '' as regno,(CONVERT(VARCHAR(10),apm.trnbelifadate,101)) AS apdate, " & _
            "apm.trnbelifaamtnetto apgrandtotal,apm.trnbelifamstnote apnote,ISNULL(apm.trnbelifamstres1,'') apmstres1, " & _
            "(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap LEFT JOIN QL_trnpayap pay ON ap.cmpcode=pay.cmpcode AND ap.payrefoid=pay.payapoid  " & _
            "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' " & sAdd & _
            "WHERE ap.payrefoid<>0 AND ap.cmpcode=con.cmpcode AND ap.reftype=con.reftype AND ap.refoid=con.refoid),0.0)) AS appaidamt, " & _
            "con.acctgoid AS apacctgoid,(a.acctgcode+' - '+a.acctgdesc) AS apaccount, " & _
            "(CONVERT(VARCHAR(10),'01/01/1900',101)) AS apdatetakegiro, " & _
            "apm.curroid,apm.belirate2toidr AS apratetoidr,(CASE WHEN ISNULL(apm.belirate2tousd, '0.00')='0.00' THEN ISNULL((SELECT rate2res2 FROM QL_mstrate2 r2 WHERE r2.rate2oid=apm.rate2oid), '0.00') ELSE apm.belirate2tousd END) AS apratetousd,apm.suppoid,'APFA' reftypedesc  " & _
            "FROM QL_trnbelimst_fa apm INNER JOIN QL_conap con ON con.cmpcode=apm.cmpcode AND con.refoid=trnbelifamstoid  " & _
            "INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid  " & _
            "WHERE con.reftype='QL_trnbelimst_fa' AND ISNULL(con.payrefoid,0)=0 AND apm.trnbelifamststatus='Post' UNION ALL " & _
            "SELECT apm.cmpcode,apm.approductmstoid apoid,con.reftype,apm.approductno apno, '' as regno,(CONVERT(VARCHAR(10),apm.approductdate,101)) AS apdate, " & _
            "apm.approductgrandtotal apgrandtotal,apm.approductmstnote apnote,ISNULL(apm.approductmstres1,'') apmstres1, " & _
            "(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap LEFT JOIN QL_trnpayap pay ON ap.cmpcode=pay.cmpcode AND ap.payrefoid=pay.payapoid  " & _
            "AND ISNULL(pay.payapres1,'')<>'Lebih Bayar' " & sAdd & _
            "WHERE ap.payrefoid<>0 AND ap.cmpcode=con.cmpcode AND ap.reftype=con.reftype AND ap.refoid=con.refoid),0.0)) AS appaidamt, " & _
            "con.acctgoid AS apacctgoid,(a.acctgcode+' - '+a.acctgdesc) AS apaccount, " & _
            "(CONVERT(VARCHAR(10),'01/01/1900',101)) AS apdatetakegiro, " & _
            "1 curroid,1 AS apratetoidr,1 AS apratetousd,apm.suppoid,'APPRES' reftypedesc  " & _
            "FROM QL_trnapproductmst apm INNER JOIN QL_conap con ON con.cmpcode=apm.cmpcode AND con.refoid=approductmstoid  " & _
            "INNER JOIN QL_mstacctg a ON a.acctgoid=con.acctgoid  " & _
            "WHERE con.reftype='QL_trnapproductmst' AND ISNULL(con.payrefoid,0)=0 AND apm.approductmststatus IN ('Post', 'Approved') " & _
            ") AS ap WHERE ap.cmpcode='" & DDLBusUnit.SelectedValue & "' /*AND apgrandtotal>appaidamt*/ " & _
            " AND ap.suppoid=" & suppoid.Text & _
            " AND " & FilterDDLListAP.SelectedValue & " LIKE '%" & Tchar(FilterTextListAP.Text) & "%' "
        If cashbanktakegiro.Text <> "" Then
            sSql &= " AND CONVERT(DATETIME, ap.apdatetakegiro)<=CONVERT(DATETIME, '" & cashbanktakegiro.Text & " 23:59:59')"
        End If
        sSql &= " ORDER BY ap.apoid"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_conap")
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = "appaidamt < apgrandtotal"
        gvListAP.DataSource = dv.ToTable
        gvListAP.DataBind()
    End Sub

    Private Sub CreateTblDtl()
        Dim dtlTable As DataTable = New DataTable("TabelAPPayment")
        dtlTable.Columns.Add("dtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("aprawmstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("aprawno", Type.GetType("System.String"))
        dtlTable.Columns.Add("regno", Type.GetType("System.String"))
        dtlTable.Columns.Add("apacctgoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("apaccount", Type.GetType("System.String"))
        dtlTable.Columns.Add("aprawgrandtotal", Type.GetType("System.Double"))
        dtlTable.Columns.Add("appaidamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("apbalance", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payapamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("apcurroid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("apcurrcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("payapamtother", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payapnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("subtotalpayment", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payapflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("totaldiff", Type.GetType("System.Double"))
        dtlTable.Columns.Add("payapamtpost", Type.GetType("System.Double"))
        dtlTable.Columns.Add("apratetoidr", Type.GetType("System.Double"))
        dtlTable.Columns.Add("apratetousd", Type.GetType("System.Double"))
        dtlTable.Columns.Add("reftype", Type.GetType("System.String"))
        Session("TblDtl") = dtlTable
    End Sub

    Private Sub ClearDetail()
        dtlseq.Text = "1"
        If cashbanktype.SelectedValue <> "BOK" Then
            If Session("TblDtl") Is Nothing = False Then
                Dim objTable As DataTable = Session("TblDtl")
                If objTable.Rows.Count > 0 Then
                    dtlseq.Text = objTable.Rows.Count + 1
                    EnableHeader(False)
                Else
                    EnableHeader(True)
                End If
            Else
                EnableHeader(True)
            End If
        End If
        i_u2.Text = "New Detail"
        aprawmstoid.Text = ""
        aprawno.Text = ""
        mrregno.Text = ""
        apacctgoid.Text = ""
        apaccount.Text = ""
        aprawgrandtotal.Text = ""
        appaidamt.Text = ""
        apbalance.Text = ""
        payapamt.Text = ""
        apcurroid.SelectedIndex = -1
        ShowCurrSymbol(apcurroid.SelectedValue, 1)
        apratetoidr.Text = ""
        apratetousd.Text = ""
        payapamtother.Text = ""
        payapamt_TextChanged(Nothing, Nothing)
        payapnote.Text = ""
        subtotalpayment.Text = ""
        totaldiff.Text = ""
        gvDtl.Columns(0).Visible = True
        gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = True
        gvDtl.SelectedIndex = -1
        Session("TblDtlDiff") = Nothing
        gvDtlDiff.DataSource = Nothing : gvDtlDiff.DataBind()
        ClearDetailDiff()
        Session("LastFlagIndex") = Nothing
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
        btnSearchSupp.Visible = bVal : btnClearSupp.Visible = bVal
        'cashbanktakegiro.Enabled = bVal : cashbanktakegiro.CssClass = sCss : imbDTG.Visible = bVal
    End Sub

    Private Sub CountTotalPayment()
        Dim dVal As Double = 0, dValOther As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                If Math.Abs(ToDouble(dt.Rows(C1)("payapamtother").ToString)) > 0 Then
                    dValOther += ToDouble(dt.Rows(C1)("payapamtother").ToString)
                Else
                    dVal += ToDouble(dt.Rows(C1)("payapamtpost").ToString)
                End If
            Next
        End If
        If dValOther > 0 Then
            cashbankamt.Text = ToMaskEdit(dValOther, iMask)
        Else
            cashbankamt.Text = ToMaskEdit(dVal, iMask)
        End If
    End Sub

    Private Sub ReAmountCost()
        payapnett.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), iMask)
    End Sub

    Private Sub generateNo()
        If DDLBusUnit.SelectedValue <> "" Then
            If cashbankdate.Text <> "" Then
                Dim sErr As String = ""
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    If acctgoid.SelectedValue <> "" Then
                        Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cb.cmpcode, cashbankoid, cb.periodacctg, cashbankdate, cashbankno, personoid, suppname, supptype, g.gendesc AS paytype, cb.curroid, cashbankamt, cashbanktype, cb.acctgoid, cashbankrefno, cashbankduedate, cashbanknote, cashbankstatus, cb.createuser, cb.createtime, cb.upduser, cb.updtime, cashbanktakegiro, ISNULL(giroacctgoid, 0) AS giroacctgoid, (CASE cashbanktype WHEN 'BLK' THEN (ISNULL((SELECT dpapamt - ISNULL(dp.dpapaccumamt, 0.0) FROM QL_trndpap dp WHERE dp.cmpcode=cb.cmpcode AND dp.dpapoid=ISNULL(cb.giroacctgoid, 0)), 0.0) + cashbankamt) WHEN 'BOK' THEN cb.cashbankdpp ELSE 0.0 END) AS cashbankdpp, cb.refsuppoid AS custoid, cb.addacctgoid1, cb.addacctgamt1, cb.addacctgoid2, cb.addacctgamt2, cb.addacctgoid3, cb.addacctgamt3, cashbankrate2toidr, cashbankrate2tousd, cashbankreftype FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON suppoid=personoid INNER JOIN QL_mstgen g ON g.genoid=supppaymentoid WHERE cb.cashbankoid=" & sOid
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xCmd = New SqlCommand(sSql, conn)
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                cashbankoid.Text = xreader("cashbankoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbankno.Text = xreader("cashbankno").ToString
                suppoid.Text = xreader("personoid").ToString
                suppname.Text = xreader("suppname").ToString
                supptype.Text = xreader("supptype").ToString
                paytype.Text = xreader("paytype").ToString
                cashbanktype.SelectedValue = xreader("cashbanktype").ToString
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                acctgoid.SelectedValue = xreader("acctgoid").ToString
                addacctgoid1.SelectedValue = xreader("addacctgoid1").ToString
                addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
                addacctgamt1.Text = ToMaskEdit(ToDouble(xreader("addacctgamt1").ToString), iMask)
                addacctgoid2.SelectedValue = xreader("addacctgoid2").ToString
                addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
                addacctgamt2.Text = ToMaskEdit(ToDouble(xreader("addacctgamt2").ToString), iMask)
                addacctgoid3.SelectedValue = xreader("addacctgoid3").ToString
                addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
                addacctgamt3.Text = ToMaskEdit(ToDouble(xreader("addacctgamt3").ToString), iMask)
                cashbankrefno.Text = xreader("cashbankrefno").ToString
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                cashbanknote.Text = xreader("cashbanknote").ToString
                cashbankstatus.Text = xreader("cashbankstatus").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                cashbanktakegiro.Text = Format(xreader("cashbanktakegiro"), "MM/dd/yyyy")
                If cashbanktakegiro.Text = "01/01/1900" Then
                    cashbanktakegiro.Text = ""
                End If
                giroacctgoid.Text = xreader("giroacctgoid").ToString
                custoid.Text = xreader("custoid").ToString
                Session("lastdpapoid") = giroacctgoid.Text
                dpapamt.Text = ToMaskEdit(ToDouble(xreader("cashbankdpp").ToString), iMask)
                Session("lastcashbanktype") = cashbanktype.SelectedValue
                curroid.SelectedValue = xreader("curroid").ToString
                porawrate2toidr.Text = ToDouble(xreader("cashbankrate2toidr").ToString)
                porawrate2tousd.Text = ToDouble(xreader("cashbankrate2tousd").ToString)
                ShowCurrSymbol(curroid.SelectedValue, 0)
                reftype_ar.Text = xreader("cashbankreftype").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            xreader.Close()
            conn.Close()
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnPost.Visible = False : btnShowCOA.Visible = False : EnableHeader(False)
            Exit Sub
        End Try
        curroid.Enabled = False : curroid.CssClass = "inpTextDisabled"
        cashbanktype.Enabled = False : cashbanktype.CssClass = "inpTextDisabled"
        acctgoid.Enabled = False : acctgoid.CssClass = "inpTextDisabled"
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnPost.Visible = False
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
            btnShowCOA.Visible = True
        End If
      
        sSql = "DECLARE @oid as INTEGER;" & _
            "SET @oid=" & sOid & ";" & _
            "SELECT apm.trnbelimstoid aprawmstoid, apm.trnbelino aprawno, '' as regno, pay.acctgoid AS apacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount, apm.trnbeliamtnetto aprawgrandtotal,(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode=pay2.cmpcode AND ap.payrefoid=pay2.payapoid AND pay2.cashbankoid<>pay.cashbankoid WHERE ap.payrefoid<>0 AND ap.cmpcode=pay.cmpcode AND ap.reftype=pay.reftype AND ap.refoid=pay.refoid AND ISNULL(pay2.payapres1, '')<>'Lebih Bayar' AND ap.trnaptype LIKE 'PAYAP%'), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND pay.cmpcode=ap2.cmpcode AND pay.reftype=ap2.reftype AND pay.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS appaidamt, pay.payapamt, 1 AS apcurroid,'IDR' AS apcurrcode, pay.payapamtother, pay.payapnote, 0.0 AS apbalance, 0.0 AS subtotalpayment, 0.0 AS totaldiff,0.0 AS payapamtpost,1 AS apratetoidr,1 AS apratetousd,pay.reftype FROM QL_trnpayap pay INNER JOIN QL_trnbelimst apm ON pay.cmpcode=apm.cmpcode AND pay.refoid=apm.trnbelimstoid AND pay.reftype='QL_trnbelimst' INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid=apm.curroid WHERE pay.cashbankoid=@oid AND ISNULL(pay.payapres1, '')='' " & _
            "UNION ALL " & _
            "SELECT apm.trnbelifamstoid aprawmstoid, apm.trnbelifano aprawno, '' as regno, pay.acctgoid AS apacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount, apm.trnbelifaamtnetto aprawgrandtotal,(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode=pay2.cmpcode AND ap.payrefoid=pay2.payapoid AND pay2.cashbankoid<>pay.cashbankoid WHERE ap.payrefoid<>0 AND ap.cmpcode=pay.cmpcode AND ap.reftype=pay.reftype AND ap.refoid=pay.refoid AND ISNULL(pay2.payapres1, '')<>'Lebih Bayar' AND ap.trnaptype LIKE 'PAYAP%'), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND pay.cmpcode=ap2.cmpcode AND pay.reftype=ap2.reftype AND pay.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS appaidamt, pay.payapamt, 1 AS apcurroid,'IDR' AS apcurrcode, pay.payapamtother, pay.payapnote, 0.0 AS apbalance, 0.0 AS subtotalpayment, 0.0 AS totaldiff,0.0 AS payapamtpost, 1 AS apratetoidr, 1 AS apratetousd,pay.reftype FROM QL_trnpayap pay INNER JOIN QL_trnbelimst_fa apm ON pay.cmpcode=apm.cmpcode AND pay.refoid=apm.trnbelifamstoid AND pay.reftype='QL_trnbelimst_fa' INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid INNER JOIN QL_mstcurr cu ON cu.curroid=apm.curroid WHERE pay.cashbankoid=@oid AND ISNULL(pay.payapres1, '')='' " & _
          "UNION ALL " & _
            "SELECT apm.approductmstoid aprawmstoid, apm.approductno aprawno, '' as regno, pay.acctgoid AS apacctgoid,(a.acctgcode + ' - ' + a.acctgdesc) AS apaccount, apm.approductgrandtotal aprawgrandtotal,(ISNULL((SELECT SUM(ap.amtbayar) FROM QL_conap ap INNER JOIN QL_trnpayap pay2 ON ap.cmpcode=pay2.cmpcode AND ap.payrefoid=pay2.payapoid AND pay2.cashbankoid<>pay.cashbankoid WHERE ap.payrefoid<>0 AND ap.cmpcode=pay.cmpcode AND ap.reftype=pay.reftype AND ap.refoid=pay.refoid AND ISNULL(pay2.payapres1, '')<>'Lebih Bayar' AND ap.trnaptype LIKE 'PAYAP%'), 0.0)+ISNULL((SELECT SUM(ap2.amtbayar) FROM QL_conap ap2 WHERE ap2.payrefoid<>0 AND pay.cmpcode=ap2.cmpcode AND pay.reftype=ap2.reftype AND pay.refoid=ap2.refoid AND ap2.trnaptype IN ('DNAP','CNAP', 'RETPI')),0.0)) AS appaidamt, pay.payapamt, 1 AS apcurroid,'IDR' AS apcurrcode, pay.payapamtother, pay.payapnote, 0.0 AS apbalance, 0.0 AS subtotalpayment, 0.0 AS totaldiff,0.0 AS payapamtpost,1 AS apratetoidr,1 AS apratetousd,pay.reftype FROM QL_trnpayap pay INNER JOIN QL_trnapproductmst apm ON pay.cmpcode=apm.cmpcode AND pay.refoid=apm.approductmstoid AND pay.reftype='QL_trnapproductmst' INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid WHERE pay.cashbankoid=@oid AND ISNULL(pay.payapres1, '')='' "
        Dim dt1 As DataTable = cKon.ambiltabel(sSql, "QL_trnpayap1")

        sSql = "SELECT pay.refoid aprawmstoid, pay.reftype,'' AS regno, pay.acctgoid AS apacctgoid, (a.acctgcode + ' - ' + a.acctgdesc) AS apaccount, " & _
            "pay.payapamt, pay.payapamtother, pay.payapnote, ISNULL(pay.payapres1, '') AS payapflag " & _
            "FROM QL_trnpayap pay INNER JOIN QL_mstacctg a ON a.acctgoid=pay.acctgoid  " & _
            "WHERE pay.cashbankoid=" & sOid & " AND ISNULL(pay.payapres1, '')<>''"
        Dim dt2 As DataTable = cKon.ambiltabel(sSql, "QL_trnpayap2")

        Dim dv As DataView = dt2.DefaultView
        CreateTblDtl()
        Dim objTbl As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt1.Rows.Count - 1
            Dim dr As DataRow = objTbl.NewRow
            dr("dtlseq") = C1 + 1
            dr("aprawmstoid") = dt1.Rows(C1)("aprawmstoid")
            dr("aprawno") = dt1.Rows(C1)("aprawno").ToString
            dr("regno") = dt1.Rows(C1)("regno").ToString
            dr("apacctgoid") = dt1.Rows(C1)("apacctgoid")
            dr("apaccount") = dt1.Rows(C1)("apaccount").ToString
            dr("aprawgrandtotal") = ToDouble(dt1.Rows(C1)("aprawgrandtotal").ToString)
            dr("appaidamt") = ToDouble(dt1.Rows(C1)("appaidamt").ToString)
            dr("apbalance") = ToDouble(dt1.Rows(C1)("aprawgrandtotal").ToString) - ToDouble(dt1.Rows(C1)("appaidamt").ToString)
            Dim dDiff As Double = 0
            dv.RowFilter = "aprawmstoid=" & dt1.Rows(C1)("aprawmstoid").ToString
            For C2 As Integer = 0 To dv.Count - 1
                If dv(C2)("payapflag").ToString = "Kurang Bayar" Then
                    dDiff += -ToDouble(dv(C2)("payapamt").ToString)
                Else
                    dDiff += ToDouble(dv(C2)("payapamt").ToString)
                End If
            Next
            dv.RowFilter = ""
            If dDiff < 0 Then
                dr("payapamt") = ToDouble(dt1.Rows(C1)("payapamt").ToString)
            Else
                dr("payapamt") = ToDouble(dt1.Rows(C1)("payapamt").ToString) + dDiff
            End If
            dr("apcurroid") = dt1.Rows(C1)("apcurroid")
            dr("apcurrcode") = dt1.Rows(C1)("apcurrcode").ToString
            dr("payapamtother") = ToDouble(dt1.Rows(C1)("payapamtother").ToString)
            dr("payapnote") = dt1.Rows(C1)("payapnote").ToString
            dr("subtotalpayment") = ToDouble(dt1.Rows(C1)("payapamt").ToString) + Math.Abs(dDiff)
            dr("payapflag") = ""
            dr("totaldiff") = Math.Abs(dDiff)
            If dDiff < 0 Then
                dr("payapamtpost") = ToDouble(dt1.Rows(C1)("payapamt").ToString) + Math.Abs(dDiff)
            Else
                dr("payapamtpost") = ToDouble(dt1.Rows(C1)("payapamt").ToString)
            End If
            dr("apratetoidr") = ToDouble(dt1.Rows(C1)("apratetoidr").ToString)
            dr("apratetousd") = ToDouble(dt1.Rows(C1)("apratetousd").ToString)
            dr("reftype") = dt1.Rows(C1)("reftype").ToString
            objTbl.Rows.Add(dr)

            dv.RowFilter = "aprawmstoid=" & dt1.Rows(C1)("aprawmstoid").ToString & " AND reftype='" & dt1.Rows(C1)("reftype").ToString & "'"
            For C2 As Integer = 0 To dv.Count - 1
                dr = objTbl.NewRow
                dr("dtlseq") = C1 + 1
                dr("aprawmstoid") = dv(C2)("aprawmstoid")
                dr("aprawno") = dt1.Rows(C1)("aprawno").ToString
                dr("regno") = dt1.Rows(C1)("regno").ToString
                dr("apacctgoid") = dv(C2)("apacctgoid")
                dr("apaccount") = dv(C2)("apaccount").ToString
                dr("aprawgrandtotal") = ToDouble(dt1.Rows(C1)("aprawgrandtotal").ToString)
                dr("appaidamt") = ToDouble(dt1.Rows(C1)("appaidamt").ToString)
                dr("apbalance") = ToDouble(dt1.Rows(C1)("aprawgrandtotal").ToString) - ToDouble(dt1.Rows(C1)("appaidamt").ToString)
                dr("payapamt") = Math.Abs(ToDouble(dv(C2)("payapamt").ToString))
                dr("apcurroid") = dt1.Rows(C1)("apcurroid")
                dr("apcurrcode") = dt1.Rows(C1)("apcurrcode").ToString
                dr("payapamtother") = ToDouble(dv(C2)("payapamtother").ToString)
                dr("payapnote") = dv(C2)("payapnote").ToString
                dr("subtotalpayment") = Math.Abs(ToDouble(dv(C2)("payapamt").ToString))
                dr("payapflag") = dv(C2)("payapflag").ToString
                dr("totaldiff") = 0
                If dv(C2)("payapflag").ToString = "Kurang Bayar" Then
                    dr("payapamtpost") = -ToDouble(dv(C2)("payapamt").ToString)
                Else
                    dr("payapamtpost") = ToDouble(dv(C2)("payapamt").ToString)
                End If
                dr("apratetoidr") = ToDouble(dt1.Rows(C1)("apratetoidr").ToString)
                dr("apratetousd") = ToDouble(dt1.Rows(C1)("apratetousd").ToString)
                dr("reftype") = dt1.Rows(C1)("reftype").ToString
                objTbl.Rows.Add(dr)
            Next
            dv.RowFilter = ""
        Next
        objTbl.AcceptChanges()
        Session("TblDtl") = objTbl
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountTotalPayment()
        ReAmountCost()
        Session("lastdpapamt") = ToDouble(payapnett.Text)
    End Sub

    Private Sub ShowReport()
        Dim UserName As String
        sSql = "SELECT profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"
        UserName = GetStrData(sSql)
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptPaymentAPAll.rpt"))
            Dim sWhere As String = "WHERE cashbankgroup='AP'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbType.Checked Then
                    sWhere &= " AND cashbanktype='" & FilterDDLType.SelectedValue & "'"
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("userID", Session("UserID"))
            report.SetParameterValue("userName", UserName)
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "APPaymentAllPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnPayAP.aspx?awal=true")
    End Sub

    Private Sub AutoSetDueDate(ByVal sType As String, ByVal sOid As String)
        Dim sDateFromAP As String = GetStrData("SELECT CAST(AVG(setdate) AS DATETIME) AS avgdate FROM (SELECT CAST(CAST(registerdocrefdate AS DATETIME) AS FLOAT) AS setdate FROM " & sType & " apm INNER JOIN " & sType.Replace("mst", "dtl") & " apd ON apd.cmpcode=apm.cmpcode AND apd." & sType.Replace("ql_trn", "") & "oid=apm." & sType.Replace("ql_trn", "") & "oid INNER JOIN QL_trnregistermst regm ON regm.cmpcode=apd.cmpcode AND regm.registermstoid=apd.registermstoid WHERE apm.cmpcode='" & DDLBusUnit.SelectedValue & "' AND apm." & sType.Replace("ql_trn", "") & "oid=" & sOid & ") AS tbltmp")
        If paytype.Text <> "" And sDateFromAP <> "" Then
            If cashbankduedate.Text = "" Then
                cashbankduedate.Text = Format(DateAdd(DateInterval.Day, ToDouble(paytype.Text), CDate(sDateFromAP)), "MM/dd/yyyy")
            Else
                sDateFromAP = GetStrData("SELECT CAST(AVG(setdate) AS DATETIME) AS avgdate FROM (SELECT CAST(DATEADD(DAY, -" & ToDouble(paytype.Text) & ", CAST('" & cashbankduedate.Text & "' AS DATETIME)) AS FLOAT) AS setdate UNION ALL SELECT CAST(CAST('" & sDateFromAP & "' AS DATETIME) AS FLOAT) AS setdate) AS tbltmp")
                cashbankduedate.Text = Format(DateAdd(DateInterval.Day, ToDouble(paytype.Text), CDate(sDateFromAP)), "MM/dd/yyyy")
            End If
        End If
    End Sub

    Private Sub EnableCurrency(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then sCss = "inpTextDisabled"
        curroid.Enabled = bVal : curroid.CssClass = sCss
        If bVal = True Then
            If curroid.Items.Count = 0 Then
                ' Fill DDL Currency
                sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                FillDDL(curroid, sSql)
            End If
        End If
    End Sub

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                curroid.SelectedValue = sCurrOid
            Else
                If curroid.Enabled = False Then
                    curroid.Items.Clear()
                    showMessage("Please define Currency for selected Payment Account!", 2)
                End If
            End If
        Else
            curroid.SelectedIndex = -1
        End If
        If curroid.Items.Count > 0 Then
            curroid_SelectedIndexChanged(Nothing, Nothing)
        End If
    End Sub

    Private Sub GenerateDPAPNo()
        Dim sNo As String = "DPAP-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(dpapno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trndpap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapno LIKE '%" & sNo & "%'"
        dpapno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
    End Sub

    Private Sub GenerateCBNo2()
        If DDLBusUnit.SelectedValue <> "" Then
            If cashbankdate.Text <> "" Then
                Dim sErr As String = ""
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    If acctgoid.SelectedValue <> "" Then
                        Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                        If GetStrData(sSql) = "" Then
                            cashbankno2.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                        Else
                            cashbankno2.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                        End If
                    End If
                End If
            End If
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnPayAP.aspx")
        End If
        If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - A/P Payment"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPost.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            CheckStatus()
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitAllDDL()
            btnShowCOA.Visible = False
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
                generateNo()
                btnDelete.Visible = False
                TabContainer1.ActiveTabIndex = 0
                curroid_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        Dim dt As New DataTable
        If Not Session("TblDtl") Is Nothing Then
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnPayAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = 0
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, cb.updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process'"
        If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & "'"
            Else
                Exit Sub
            End If
        End If
        If cbType.Checked Then
            sSqlPlus &= " AND cashbanktype='" & FilterDDLType.SelectedValue & "'"
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlPlus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbStatus.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbType.Checked = False
        FilterDDLType.SelectedIndex = -1
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnPayAP.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        btnClearSupp_Click(Nothing, Nothing)
        cashbanktype.SelectedIndex = -1
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        InitDDLAdd()
    End Sub

    Protected Sub btnSearchSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchSupp.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnClearSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSupp.Click
        suppoid.Text = "" : suppname.Text = "" : supptype.Text = "" : paytype.Text = "" : btnClearAP_Click(Nothing, Nothing)
        If cashbanktype.SelectedValue = "BLK" Then
            btnClearDP_Click(Nothing, Nothing)
        End If
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1 : FilterTextListSupp.Text = "" : gvListSupp.SelectedIndex = -1
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListSupp.PageIndexChanging
        gvListSupp.PageIndex = e.NewPageIndex
        BindSupplierData()
        mpeListSupp.Show()
    End Sub

    Protected Sub gvListSupp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListSupp.SelectedIndexChanged
        If suppoid.Text <> gvListSupp.SelectedDataKey.Item("suppoid").ToString Then
            btnClearSupp_Click(Nothing, Nothing)
        End If
        suppoid.Text = gvListSupp.SelectedDataKey.Item("suppoid").ToString
        suppname.Text = gvListSupp.SelectedDataKey.Item("suppname").ToString
        supptype.Text = gvListSupp.SelectedDataKey.Item("supptype").ToString
        paytype.Text = gvListSupp.SelectedDataKey.Item("paytype").ToString
        custoid.Text = gvListSupp.SelectedDataKey.Item("custoid").ToString
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListSupp.Click
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub curroid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles curroid.SelectedIndexChanged
        If cashbanktype.SelectedValue = "BLK" Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        ShowCurrSymbol(curroid.SelectedValue, 0)
        Dim sErr As String = ""
        If cashbankdate.Text <> "" Then
            If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                If curroid.SelectedValue <> "" Then
                    cRate.SetRateValue(CInt(curroid.SelectedValue), cashbankdate.Text)
                    'If cRate.GetRateDailyLastError <> "" Then
                    '    showMessage(cRate.GetRateDailyLastError, 2)
                    '    rateoid.Text = "" : porawratetoidr.Text = "" : porawratetousd.Text = ""
                    '    Exit Sub
                    'End If
                    If cRate.GetRateMonthlyLastError <> "" Then
                        showMessage(cRate.GetRateMonthlyLastError, 2)
                        rate2oid.Text = "" : porawrate2toidr.Text = "" : porawrate2tousd.Text = ""
                        Exit Sub
                    End If
                    'rateoid.Text = cRate.GetRateDailyOid
                    'porawratetoidr.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
                    'porawratetousd.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
                    rate2oid.Text = cRate.GetRateMonthlyOid
                    porawrate2toidr.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
                    porawrate2tousd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
                    If curroid.SelectedItem.Text.ToUpper = "IDR" Then
                        porawrate2toidr.CssClass = "inpTextDisabled" : porawrate2toidr.Enabled = False
                        porawrate2tousd.CssClass = "inpTextDisabled" : porawrate2tousd.Enabled = False
                    Else
                        porawrate2toidr.CssClass = "inpText" : porawrate2toidr.Enabled = True
                        porawrate2tousd.CssClass = "inpText" : porawrate2tousd.Enabled = True
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbanktype.SelectedIndexChanged
        InitDDLCOAPayment()
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            generateNo()
        End If
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If Session("oid") Is Nothing Or Session("oid") = "" Then
            generateNo()
        End If
        SetCOACurrency(acctgoid.SelectedValue)
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), iMask)
        ReAmountCost()
    End Sub

    Protected Sub btnSearchDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDP.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        If cashbanktype.SelectedValue = "BOK" Then
            If custoid.Text = "" Then
                showMessage("Please Set Customer For Supplier '" & suppname.Text & "'..!", 2)
                Exit Sub
            End If
            FilterDDLListSI.SelectedIndex = -1 : FilterTextListSI.Text = "" : gvListSI.SelectedIndex = -1
            BindSIData()
            cProc.SetModalPopUpExtender(btnHideListSI, pnlListSI, mpeListSI, True)
        Else
            FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
            BindDPData()
            cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, True)
        End If
    End Sub

    Protected Sub btnClearDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDP.Click
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : acctgoid.SelectedIndex = -1 : dpapamt.Text = "" : reftype_ar.Text = ""
    End Sub

    Protected Sub btnFindListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDP.Click
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub btnAllListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListDP.Click
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDP.PageIndexChanging
        gvListDP.PageIndex = e.NewPageIndex
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDP.SelectedIndexChanged
        If giroacctgoid.Text <> gvListDP.SelectedDataKey.Item("dpapoid").ToString Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        giroacctgoid.Text = gvListDP.SelectedDataKey.Item("dpapoid").ToString
        cashbankrefno.Text = gvListDP.SelectedDataKey.Item("dpapno").ToString
        acctgoid.SelectedValue = gvListDP.SelectedDataKey.Item("dpapacctgoid").ToString
        dpapamt.Text = ToMaskEdit(ToDouble(gvListDP.SelectedDataKey.Item("dpapamt").ToString), iMask)
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub lkbCloseListDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDP.Click
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub btnSearchAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchAP.Click
        If suppoid.Text = "" Then
            showMessage("Please select Supplier first!", 2)
            Exit Sub
        End If
        If cashbanktakegiro.Text <> "" Then
            Dim sErr As String = ""
            If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
                showMessage("Date Take Giro is invalid. " & sErr, 2)
                Exit Sub
            End If
        End If
        FilterDDLListAP.SelectedIndex = -1 : FilterTextListAP.Text = "" : gvListAP.SelectedIndex = -1
        BindAPData()
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, True)
    End Sub

    Protected Sub btnClearAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearAP.Click
        aprawmstoid.Text = "" : aprawno.Text = "" : reftype.Text = "" : apacctgoid.Text = "" : apaccount.Text = "" : aprawgrandtotal.Text = "" : appaidamt.Text = "" : apbalance.Text = ""
        payapamt.Text = "" : apcurroid.SelectedIndex = -1 : payapamtother.Text = "" : payapamt_TextChanged(Nothing, Nothing) : apratetoidr.Text = "" : apratetousd.Text = ""
        ShowCurrSymbol(apcurroid.SelectedValue, 1)
    End Sub

    Protected Sub btnFindListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListAP.Click
        BindAPData()
        mpeListAP.Show()
    End Sub

    Protected Sub btnAllListAP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListAP.Click
        FilterDDLListAP.SelectedIndex = -1 : FilterTextListAP.Text = "" : gvListAP.SelectedIndex = -1
        BindAPData()
        mpeListAP.Show()
    End Sub

    Protected Sub gvListAP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListAP.PageIndexChanging
        gvListAP.PageIndex = e.NewPageIndex
        BindAPData()
        mpeListAP.Show()
    End Sub

    Protected Sub gvListAP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListAP.SelectedIndexChanged
        If aprawmstoid.Text <> gvListAP.SelectedDataKey.Item("apoid").ToString Then
            btnClearAP_Click(Nothing, Nothing)
        End If
        aprawmstoid.Text = gvListAP.SelectedDataKey.Item("apoid").ToString
        aprawno.Text = gvListAP.SelectedDataKey.Item("apno").ToString
        mrregno.Text = gvListAP.SelectedDataKey.Item("regno").ToString
        reftype.Text = gvListAP.SelectedDataKey.Item("reftype").ToString
        apacctgoid.Text = gvListAP.SelectedDataKey.Item("apacctgoid").ToString
        apaccount.Text = gvListAP.SelectedDataKey.Item("apaccount").ToString
        aprawgrandtotal.Text = ToMaskEdit(ToDouble(gvListAP.SelectedDataKey.Item("apgrandtotal").ToString), iMask)
        appaidamt.Text = ToMaskEdit(ToDouble(gvListAP.SelectedDataKey.Item("appaidamt").ToString), iMask)
        apbalance.Text = ToMaskEdit(ToDouble(gvListAP.SelectedDataKey.Item("apgrandtotal").ToString) - ToDouble(gvListAP.SelectedDataKey.Item("appaidamt").ToString), iMask)
        payapamt.Text = ToMaskEdit(ToDouble(apbalance.Text), iMask)
        subtotalpayment.Text = ToMaskEdit(ToDouble(apbalance.Text), iMask)
        apcurroid.SelectedValue = gvListAP.SelectedDataKey.Item("curroid").ToString
        ShowCurrSymbol(apcurroid.SelectedValue, 1)
        apratetoidr.Text = gvListAP.SelectedDataKey.Item("apratetoidr").ToString
        apratetousd.Text = gvListAP.SelectedDataKey.Item("apratetousd").ToString
        'AutoSetDueDate(reftype.Text.ToLower, aprawmstoid.Text)
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
    End Sub

    Protected Sub lkbCloseListAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListAP.Click
        cProc.SetModalPopUpExtender(btnHideListAP, pnlListAP, mpeListAP, False)
    End Sub

    Protected Sub payapamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles payapamt.TextChanged
        payapamt.Text = ToMaskEdit(ToDouble(payapamt.Text), iMask)
        If ToDouble(apbalance.Text) > 0 Then
            If ToDouble(payapamt.Text) = ToDouble(apbalance.Text) Then
                pnlOverPayment.Visible = False
                Session("TblDtlDiff") = Nothing
                gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
                ClearDetailDiff()
                Session("LastFlagIndex") = Nothing
            Else
                pnlOverPayment.Visible = True
                If ToDouble(payapamt.Text) > ToDouble(apbalance.Text) Then
                    diffflag.SelectedIndex = 0 : diffamount.Text = ToMaskEdit(ToDouble(payapamt.Text) - ToDouble(apbalance.Text), 2)
                Else
                    diffflag.SelectedIndex = 1 : diffamount.Text = ToMaskEdit(ToDouble(apbalance.Text) - ToDouble(payapamt.Text), 2)
                End If
                If Session("LastFlagIndex") Is Nothing Then
                    Session("LastFlagIndex") = diffflag.SelectedIndex
                Else
                    If CInt(Session("LastFlagIndex")) <> diffflag.SelectedIndex Then
                        Session("TblDtlDiff") = Nothing
                        gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
                        ClearDetailDiff()
                    End If
                End If
                InitDDLDiffAcctg()
            End If
        Else
            pnlOverPayment.Visible = False
            Session("TblDtlDiff") = Nothing
            gvDtlDiff.DataSource = Session("TblDtlDiff") : gvDtlDiff.DataBind()
            ClearDetailDiff()
            Session("LastFlagIndex") = Nothing
        End If
        CountTotalDiff()
    End Sub

    Protected Sub btnAddToListDiff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToListDiff.Click
        If IsDiffDetailInputValid() Then
            If Session("TblDtlDiff") Is Nothing Then
                CreateTblDtlDiff()
            End If
            Dim objTable As DataTable = Session("TblDtlDiff")
            Dim dv As DataView = objTable.DefaultView
            If i_u3.Text = "New Detail" Then
                dv.RowFilter = "diffacctgoid=" & diffacctgoid.SelectedValue
            Else
                dv.RowFilter = "diffacctgoid=" & diffacctgoid.SelectedValue & " AND diffseq<>" & diffseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u3.Text = "New Detail" Then
                objRow = objTable.NewRow()
                diffseq.Text = objTable.Rows.Count + 1
                objRow("diffseq") = diffseq.Text
            Else
                objRow = objTable.Rows(diffseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("diffflag") = diffflag.SelectedValue
            objRow("diffacctgoid") = diffacctgoid.SelectedValue
            objRow("diffaccount") = diffacctgoid.SelectedItem.Text
            objRow("diffamount") = ToDouble(diffamount.Text)
            objRow("diffamountother") = ToDouble(diffamountother.Text)
            objRow("diffnote") = diffnote.Text
            If i_u3.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtlDiff") = objTable
            gvDtlDiff.DataSource = Session("TblDtlDiff")
            gvDtlDiff.DataBind()
            ClearDetailDiff()
            CountTotalDiff()
        End If
    End Sub

    Protected Sub btnClearDiff_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDiff.Click
        ClearDetailDiff()
    End Sub

    Protected Sub gvDtlDiff_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtlDiff.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), iMask)
        End If
    End Sub

    Protected Sub gvDtlDiff_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtlDiff.RowDeleting
        Dim objTable As DataTable = Session("TblDtlDiff")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("diffseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtlDiff") = objTable
        gvDtlDiff.DataSource = Session("TblDtlDiff")
        gvDtlDiff.DataBind()
        ClearDetailDiff()
        CountTotalDiff()
    End Sub

    Protected Sub gvDtlDiff_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtlDiff.SelectedIndexChanged
        Try
            diffseq.Text = gvDtlDiff.SelectedDataKey.Item("diffseq").ToString().Trim
            If Session("TblDtlDiff") Is Nothing = False Then
                i_u3.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtlDiff")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "diffseq=" & diffseq.Text
                diffacctgoid.SelectedValue = dv.Item(0).Item("diffacctgoid").ToString
                diffamount.Text = ToMaskEdit(dv.Item(0).Item("diffamount").ToString, iMask)
                diffamountother.Text = ToMaskEdit(dv.Item(0).Item("diffamountother").ToString, iMask)
                diffnote.Text = dv.Item(0).Item("diffnote").ToString
                dv.RowFilter = ""
                gvDtlDiff.Columns(0).Visible = False
                gvDtlDiff.Columns(gvDtlDiff.Columns.Count - 1).Visible = False
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                CreateTblDtl()
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "aprawmstoid=" & aprawmstoid.Text & " AND reftype='" & reftype.Text & "' AND payapflag=''"
            Else
                dv.RowFilter = "aprawmstoid=" & aprawmstoid.Text & " AND reftype='" & reftype.Text & "' AND payapflag='' AND dtlseq<>" & dtlseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim dRate As Double = ToDouble(payapamtother.Text) / ToDouble(payapamt.Text)
            Dim dOther As Double = Math.Round(ToDouble(payapamtother.Text), iRoundDigit, MidpointRounding.AwayFromZero)
            Dim dOtherPlus As Double = 0
            If i_u2.Text = "New Detail" Then
                Dim objRow As DataRow = objTable.NewRow()
                dtlseq.Text = objTable.Rows.Count + 1
                objRow("dtlseq") = dtlseq.Text
                objRow("aprawmstoid") = aprawmstoid.Text
                objRow("aprawno") = aprawno.Text
                objRow("regno") = mrregno.Text
                objRow("apacctgoid") = apacctgoid.Text
                objRow("apaccount") = apaccount.Text
                objRow("aprawgrandtotal") = ToDouble(aprawgrandtotal.Text)
                objRow("appaidamt") = ToDouble(appaidamt.Text)
                objRow("apbalance") = ToDouble(apbalance.Text)
                objRow("payapamt") = ToDouble(payapamt.Text)
                objRow("apcurroid") = apcurroid.SelectedValue
                objRow("apcurrcode") = apcurroid.SelectedItem.Text
                objRow("payapnote") = payapnote.Text
                objRow("subtotalpayment") = ToDouble(subtotalpayment.Text)
                objRow("payapflag") = ""
                objRow("totaldiff") = ToDouble(totaldiff.Text)
                objRow("apratetoidr") = ToDouble(apratetoidr.Text)
                objRow("apratetousd") = ToDouble(apratetousd.Text)
                If ToDouble(payapamt.Text) >= ToDouble(apbalance.Text) Then
                    objRow("payapamtpost") = ToDouble(apbalance.Text)
                    objRow("payapamtother") = Math.Round(ToDouble(apbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    dOtherPlus = Math.Round(ToDouble(apbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                Else
                    objRow("payapamtpost") = ToDouble(subtotalpayment.Text)
                    objRow("payapamtother") = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    dOtherPlus = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                End If
                objRow("reftype") = reftype.Text
                objTable.Rows.Add(objRow)
            Else
                dv.RowFilter = "payapflag='' AND dtlseq=" & dtlseq.Text
                If dv.Count = 1 Then
                    dv(0)("aprawmstoid") = aprawmstoid.Text
                    dv(0)("aprawno") = aprawno.Text
                    dv(0)("regno") = mrregno.Text
                    dv(0)("apacctgoid") = apacctgoid.Text
                    dv(0)("apaccount") = apaccount.Text
                    dv(0)("aprawgrandtotal") = ToDouble(aprawgrandtotal.Text)
                    dv(0)("appaidamt") = ToDouble(appaidamt.Text)
                    dv(0)("apbalance") = ToDouble(apbalance.Text)
                    dv(0)("payapamt") = ToDouble(payapamt.Text)
                    dv(0)("apcurroid") = apcurroid.SelectedValue
                    dv(0)("apcurrcode") = apcurroid.SelectedItem.Text
                    dv(0)("payapnote") = payapnote.Text
                    dv(0)("subtotalpayment") = ToDouble(subtotalpayment.Text)
                    dv(0)("totaldiff") = ToDouble(totaldiff.Text)
                    dv(0)("apratetoidr") = ToDouble(apratetoidr.Text)
                    dv(0)("apratetousd") = ToDouble(apratetousd.Text)
                    If ToDouble(payapamt.Text) >= ToDouble(apbalance.Text) Then
                        dv(0)("payapamtpost") = ToDouble(apbalance.Text)
                        dv(0)("payapamtother") = Math.Round(ToDouble(apbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        dOtherPlus = Math.Round(ToDouble(apbalance.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    Else
                        dv(0)("payapamtpost") = ToDouble(subtotalpayment.Text)
                        dv(0)("payapamtother") = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        dOtherPlus = Math.Round(ToDouble(subtotalpayment.Text) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    End If
                    dv(0)("reftype") = reftype.Text
                End If
                dv.RowFilter = ""
                dv.RowFilter = "payapflag<>'' AND dtlseq=" & dtlseq.Text
                For C1 As Integer = 0 To dv.Count - 1
                    dv.Delete(0)
                Next
                dv.RowFilter = ""
            End If
            objTable.AcceptChanges()
            If Session("TblDtlDiff") IsNot Nothing Then
                Dim dtDiff As DataTable = Session("TblDtlDiff")
                For C1 As Integer = 0 To dtDiff.Rows.Count - 1
                    Dim dr As DataRow = objTable.NewRow
                    dr("dtlseq") = dtlseq.Text
                    dr("aprawmstoid") = aprawmstoid.Text
                    dr("aprawno") = aprawno.Text
                    dr("regno") = mrregno.Text
                    dr("apacctgoid") = dtDiff.Rows(C1)("diffacctgoid").ToString
                    dr("apaccount") = dtDiff.Rows(C1)("diffaccount").ToString
                    dr("aprawgrandtotal") = ToDouble(aprawgrandtotal.Text)
                    dr("appaidamt") = ToDouble(appaidamt.Text)
                    dr("apbalance") = ToDouble(apbalance.Text)
                    dr("payapamt") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    dr("apcurroid") = apcurroid.SelectedValue
                    dr("apcurrcode") = apcurroid.SelectedItem.Text
                    dOtherPlus = Math.Round(dOtherPlus + ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    If C1 = dtDiff.Rows.Count - 1 Then
                        dr("payapamtother") = Math.Round(ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                        If dOtherPlus <> dOther Then
                            dr("payapamtother") = Math.Round(ToDouble(dr("payapamtother").ToString) + (dOther - dOtherPlus), iRoundDigit, MidpointRounding.AwayFromZero)
                        End If
                    Else
                        dr("payapamtother") = Math.Round(ToDouble(dtDiff.Rows(C1)("diffamount").ToString) * dRate, iRoundDigit, MidpointRounding.AwayFromZero)
                    End If
                    dr("payapnote") = dtDiff.Rows(C1)("diffnote").ToString
                    dr("subtotalpayment") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    dr("payapflag") = dtDiff.Rows(C1)("diffflag").ToString
                    dr("totaldiff") = 0
                    dr("apratetoidr") = ToDouble(apratetoidr.Text)
                    dr("apratetousd") = ToDouble(apratetousd.Text)
                    If dtDiff.Rows(C1)("diffflag").ToString = "Lebih Bayar" Then
                        dr("payapamtpost") = ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    Else
                        dr("payapamtpost") = -ToDouble(dtDiff.Rows(C1)("diffamount").ToString)
                    End If
                    dr("reftype") = reftype.Text
                    objTable.Rows.Add(dr)
                Next
            End If
            objTable.AcceptChanges()
            dv = objTable.DefaultView
            dv.Sort = "dtlseq, payapflag"
            objTable.AcceptChanges()
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalPayment()
            ReAmountCost()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), iMask)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), iMask)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), iMask)
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), iMask)
            e.Row.Cells(9).Text = ToMaskEdit(ToDouble(e.Row.Cells(9).Text), iMask)
            If e.Row.Cells(11).Text = "Kurang Bayar" Or e.Row.Cells(11).Text = "Lebih Bayar" Then
                e.Row.Cells(0).Enabled = False
                e.Row.Cells(gvDtl.Columns.Count - 1).Enabled = False
            End If
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            dtlseq.Text = gvDtl.SelectedDataKey.Item("dtlseq").ToString().Trim
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payapflag=''"
                aprawmstoid.Text = dv.Item(0).Item("aprawmstoid").ToString
                aprawno.Text = dv.Item(0).Item("aprawno").ToString
                mrregno.Text = dv.Item(0).Item("regno").ToString
                apacctgoid.Text = dv.Item(0).Item("apacctgoid").ToString
                apaccount.Text = dv.Item(0).Item("apaccount").ToString
                aprawgrandtotal.Text = ToMaskEdit(dv.Item(0).Item("aprawgrandtotal").ToString, iMask)
                appaidamt.Text = ToMaskEdit(dv.Item(0).Item("appaidamt").ToString, iMask)
                apbalance.Text = ToMaskEdit(dv.Item(0).Item("apbalance").ToString, iMask)
                payapamt.Text = ToMaskEdit(dv.Item(0).Item("payapamt").ToString, iMask)
                apcurroid.SelectedValue = dv.Item(0).Item("apcurroid").ToString
                ShowCurrSymbol(apcurroid.SelectedValue, 1)
                payapamtother.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("payapamtother").ToString), iMask)
                payapamt_TextChanged(Nothing, Nothing)
                payapnote.Text = dv.Item(0).Item("payapnote").ToString
                subtotalpayment.Text = ToMaskEdit(dv.Item(0).Item("subtotalpayment").ToString, iMask)
                totaldiff.Text = ToMaskEdit(dv.Item(0).Item("totaldiff").ToString, iMask)
                apratetoidr.Text = dv.Item(0).Item("apratetoidr").ToString
                apratetousd.Text = dv.Item(0).Item("apratetousd").ToString
                reftype.Text = dv.Item(0).Item("reftype").ToString
                dv.RowFilter = ""
                If pnlOverPayment.Visible = True Then
                    If CInt(Session("LastFlagIndex")) = 0 Then
                        dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payapflag='Lebih Bayar'"
                    Else
                        dv.RowFilter = "dtlseq=" & dtlseq.Text & " AND payapflag='Kurang Bayar'"
                    End If
                    If dv.Count > 0 Then
                        CreateTblDtlDiff()
                        Dim dt As DataTable = Session("TblDtlDiff")
                        Dim dOther As Double = ToDouble(payapamtother.Text)
                        For C1 As Integer = 0 To dv.Count - 1
                            Dim dr As DataRow = dt.NewRow
                            dr("diffseq") = C1 + 1
                            dr("diffflag") = dv(C1)("payapflag").ToString
                            dr("diffacctgoid") = dv(C1)("apacctgoid").ToString
                            dr("diffaccount") = dv(C1)("apaccount").ToString
                            dr("diffamount") = Math.Abs(ToDouble(dv(C1)("subtotalpayment").ToString))
                            dr("diffamountother") = ToDouble(dv(C1)("payapamtother").ToString)
                            dOther += ToDouble(dv(C1)("payapamtother").ToString)
                            dr("diffnote") = dv(C1)("payapnote").ToString
                            dt.Rows.Add(dr)
                        Next
                        dt.AcceptChanges()
                        Session("TblDtlDiff") = dt
                        gvDtlDiff.DataSource = Session("TblDtlDiff")
                        gvDtlDiff.DataBind()
                        payapamtother.Text = ToMaskEdit(dOther, iMask)
                    End If
                    dv.RowFilter = ""
                    CountTotalDiff()
                End If
            End If
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        Dim sOid As String = objRow(e.RowIndex)("dtlseq").ToString
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "dtlseq=" & sOid
        For C1 As Int16 = 0 To dv.Count - 1
            dv.Delete(0)
        Next
        dv.RowFilter = ""
        objTable.AcceptChanges()
        Session("TblDtl") = objTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountTotalPayment()
        ReAmountCost()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            Dim cbOper As Integer = 0
            Dim genId As Integer = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND acctgoid=" & acctgoid.SelectedValue & " AND cashbankno='" & cashbankno.Text & "'"
                If CheckDataExists(sSql) Then
                    generateNo()
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            payapoid.Text = GenerateID("QL_TRNPAYAP", CompnyCode)
            conapoid.Text = GenerateID("QL_CONAP", CompnyCode)
            payaroid.Text = GenerateID("QL_TRNPAYAR", CompnyCode)
            conaroid.Text = GenerateID("QL_CONAR", CompnyCode)
            Dim sDueDate As String = ""
            If cashbanktype.SelectedValue = "BKK" Or cashbanktype.SelectedValue = "BLK" Or cashbanktype.SelectedValue = "BOK" Then
                sDueDate = cashbankdate.Text
            Else
                sDueDate = cashbankduedate.Text
            End If
            Dim sDTG As String = "1/1/1900"
            If cashbanktakegiro.Text <> "" Then
                sDTG = cashbanktakegiro.Text
            End If
            Dim glmstoid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim gldtloid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim iDPAPOid As Integer = GenerateID("QL_TRNDPAP", CompnyCode)
            Dim dtSum As DataTable = Nothing
            Dim iDiffIDRAcctgOid, iDiffUSDAcctgOid, iCustGroupOid As Integer
            Dim iAPGiroAcctgOid As Integer = ToInteger(giroacctgoid.Text)
            Dim iAPCurrOid As Integer = GetRateAP()
            Dim sAPCurrCode As String = GetStrData("SELECT currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND curroid=" & iAPCurrOid)
            Dim iCustOid As Integer = 0
            Dim dtCurrentPayap As New DataTable
            Dim sOidDP As Integer = 0
            If Session("oid") = Nothing Or Session("oid") = "" Then
            Else
                ' Select Current Payment detail on DB for later reverse
                sSql = "SELECT refoid,reftype FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND ISNULL(payapres1,'')='' "
                dtCurrentPayap = cKon.ambiltabel(sSql, "QL_trnpayap")
            End If

            If cashbankstatus.Text = "Post" Then
                If cashbanktype.SelectedValue = "BLK" Then
                    cRate.SetRateValue(ToInteger(GetStrData("SELECT rateoid FROM QL_trndpap WHERE dpapoid=" & giroacctgoid.Text)), ToInteger(GetStrData("SELECT rate2oid FROM QL_trndpap WHERE dpapoid=" & giroacctgoid.Text)))
                Else
                    cRate.SetRateValue(CInt(curroid.SelectedValue), cashbankdate.Text)
                End If
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                cRate2.SetRateValue(iAPCurrOid, cashbankdate.Text)
                If cRate2.GetRateDailyLastError <> "" Then
                    showMessage(cRate2.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate2.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate2.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                periodacctg.Text = GetDateToPeriodAcctg(sDueDate)
                dtSum = GetSumDetail()
                Dim sVarErr As String = ""
                If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue) Then
                    sVarErr = "VAR_DIFF_CURR_IDR"
                End If
                If Not IsInterfaceExists("VAR_DIFF_CURR_USD", DDLBusUnit.SelectedValue) Then
                    sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_DIFF_CURR_USD"
                End If
                If cashbanktype.SelectedValue = "BGK" Then
                    If Not IsInterfaceExists("VAR_GIRO", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_GIRO"
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "approved"), 2)
                    Exit Sub
                End If
                iDiffIDRAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue), CompnyCode)
                iDiffUSDAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_USD", DDLBusUnit.SelectedValue), CompnyCode)
                If cashbanktype.SelectedValue = "BGK" Then
                    iAPGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO", DDLBusUnit.SelectedValue), CompnyCode)
                End If
                'Generate lawan Barter
                If cashbanktype.SelectedValue = "BOK" Then
                    iCustOid = custoid.Text
                    Dim oper As String = "BOM-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '" & oper & "%' --AND acctgoid=" & acctgoid.SelectedValue
                    If GetStrData(sSql) = "" Then
                        operNo.Text = GenNumberString(oper, "", 1, DefaultFormatCounter)
                    Else
                        operNo.Text = GenNumberString(oper, "", GetStrData(sSql), DefaultFormatCounter)
                    End If
                    ' Update Credit limit Customer if payment type BOK
                    sSql = "SELECT custgroupoid FROM QL_mstcust WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND custoid=" & iCustOid & ""
                    iCustGroupOid = cKon.ambilscalar(sSql)
                End If
                'Get acctgoid from VAR_DPAP
                sOidDP = GetAcctgOID(GetVarInterface("VAR_DPAP_LEBIH_BAYAR", DDLBusUnit.SelectedValue), CompnyCode)
                GenerateDPAPNo()
                GenerateCBNo2()
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, refsuppoid, cashbankdpp, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, cashbankrate2toidr, cashbankrate2tousd, cashbankreftype) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'AP', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(payapnett.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2tousd.Text) & ", " & suppoid.Text & ", '" & sDueDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sDTG & "', " & iAPGiroAcctgOid & ", " & iCustOid & ", " & ToDouble(dpapamt.Text) & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ", " & ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawrate2tousd.Text) & ", '" & reftype_ar.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbanktype.SelectedValue = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " & ToDouble(payapnett.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(payapnett.Text) & ", cashbankamtidr=" & ToDouble(payapnett.Text) * ToDouble(porawrate2toidr.Text) & ", cashbankamtusd=" & ToDouble(payapnett.Text) * ToDouble(porawrate2tousd.Text) & ", personoid=" & suppoid.Text & ", cashbankduedate='" & sDueDate & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & sDTG & "', giroacctgoid=" & iAPGiroAcctgOid & ", refsuppoid=" & IIf(custoid.Text <> "" Or custoid.Text = 0, CInt(custoid.Text), 0) & ", cashbankdpp=" & ToDouble(dpapamt.Text) & ",addacctgoid1=" & addacctgoid1.SelectedValue & ",addacctgamt1=" & ToDouble(addacctgamt1.Text) & ",addacctgoid2=" & addacctgoid2.SelectedValue & ",addacctgamt2=" & ToDouble(addacctgamt2.Text) & ",addacctgoid3=" & addacctgoid3.SelectedValue & ",addacctgamt3=" & ToDouble(addacctgamt3.Text) & ", cashbankrate2toidr= " & ToDouble(porawrate2toidr.Text) & ", cashbankrate2tousd=" & ToDouble(porawrate2tousd.Text) & ", cashbankreftype='" & reftype_ar.Text & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " & ToDouble(Session("lastdpapamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & Session("lastdpapoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If cashbanktype.SelectedValue = "BLK" Then
                        sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt + " & ToDouble(payapnett.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If

                    ' Reversing each detail A/P
                    For R1 As Integer = 0 To dtCurrentPayap.Rows.Count - 1
                        Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                        Select Case dtCurrentPayap.Rows(R1)("reftype").ToString.ToUpper
                            Case "QL_TRNBELIMST"
                                sTableName = "QL_trnbelimst" : sColStatusName = "trnbelimststatus" : sIDColName = "trnbelimstoid"
                            Case "QL_TRNBELIMST_FA"
                                sTableName = "QL_trnbelimst_fa" : sColStatusName = "trnbelifamststatus" : sIDColName = "trnbelifamstoid"
                            Case "QL_TRNAPPRODUCTMST"
                                sTableName = "QL_trnapproductmst" : sColStatusName = "approductmststatus" : sIDColName = "approductmstoid"
                        End Select
                        sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='" & IIf(sTableName.ToUpper = "QL_TRNBELIMST", "Approved", "Post") & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & _
                            "WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtCurrentPayap.Rows(R1)("refoid").ToString & " "
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next

                    sSql = "DELETE FROM QL_conap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnaptype IN ('PAYAP', 'PAYAPFA') AND payrefoid IN (SELECT payapoid FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim dtTbl As DataTable = Session("TblDtl")
                    Dim dPayAPIDR As Double = 0, dPayAPUSD As Double = 0, dPayAPOther As Double = 0, dPayAPBOK As Double = 0
                    For C1 As Int16 = 0 To dtTbl.Rows.Count - 1
                        Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                        Select Case dtTbl.Rows(C1)("reftype").ToString.ToUpper
                            Case "QL_TRNBELIMST"
                                sTableName = "QL_trnbelimst" : sColStatusName = "trnbelimststatus" : sIDColName = "trnbelimstoid"
                            Case "QL_TRNBELIMST_FA"
                                sTableName = "QL_trnbelimst_fa" : sColStatusName = "trnbelifamststatus" : sIDColName = "trnbelifamstoid"
                            Case "QL_TRNAPPRODUCTMST"
                                sTableName = "QL_trnapproductmst" : sColStatusName = "approductmststatus" : sIDColName = "approductmstoid"
                        End Select
                        ' Insert To QL_trnpayap
                        If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                            If curroid.SelectedItem.Text = "IDR" Then
                                dPayAPIDR = (ToDouble(dtTbl.Rows(C1)("payapamtother").ToString) / ToDouble(dtTbl.Rows(C1)("payapamtpost").ToString)) * (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString))
                                'dPayAPOther = dPayAPIDR
                                If sAPCurrCode = "USD" Then
                                    dPayAPUSD = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayAPUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                End If
                            ElseIf curroid.SelectedItem.Text = "USD" Then
                                If sAPCurrCode = "IDR" Then
                                    dPayAPIDR = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayAPIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                                End If
                                dPayAPUSD = (ToDouble(dtTbl.Rows(C1)("payapamtother").ToString) / ToDouble(dtTbl.Rows(C1)("payapamtpost").ToString)) * (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString))
                                'dPayAPOther = dPayAPIDR
                            Else
                                If sAPCurrCode = "IDR" Then
                                    dPayAPIDR = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                    dPayAPUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                ElseIf sAPCurrCode = "USD" Then
                                    dPayAPIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                                    dPayAPUSD = ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)
                                Else
                                    dPayAPIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                                    dPayAPUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                                End If
                            End If
                        Else
                            dPayAPIDR = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                            dPayAPUSD = (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyUSDValue
                            dPayAPBOK += (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * cRate2.GetRateMonthlyIDRValue
                        End If
                        'insert payap
                        sSql = "INSERT INTO QL_trnpayap (cmpcode, payapoid, cashbankoid, suppoid, reftype, refoid, acctgoid, payaprefno, payapduedate, payapamt, payapamtidr, payapamtusd, payapnote, payapstatus, createuser, createtime, upduser, updtime, payapres1, payapamtother) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(payapoid.Text) + C1 & ", " & cashbankoid.Text & ", " & suppoid.Text & ", '" & dtTbl.Rows(C1)("reftype").ToString & "', " & dtTbl.Rows(C1)("aprawmstoid").ToString & ", " & dtTbl.Rows(C1)("apacctgoid").ToString & ", '" & Tchar(cashbankrefno.Text) & "', '" & sDueDate & "', " & ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) & ", " & dPayAPIDR & ", " & dPayAPUSD & ", '" & Tchar(dtTbl.Rows(C1)("payapnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & dtTbl.Rows(C1)("payapflag").ToString & "', " & ToDouble(dtTbl.Rows(C1)("payapamtother").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert To QL_conap
                        sSql = "INSERT INTO QL_conap (cmpcode, conapoid, reftype, refoid, payrefoid, suppoid, acctgoid, trnapstatus, trnaptype, trnapdate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnapnote, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd, amtbayarother) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(conapoid.Text) + C1 & ", '" & dtTbl.Rows(C1)("reftype").ToString & "', " & dtTbl.Rows(C1)("aprawmstoid").ToString & ", " & CInt(payapoid.Text) + C1 & ", " & suppoid.Text & ", " & dtTbl.Rows(C1)("apacctgoid").ToString & ", '" & cashbankstatus.Text & "', '" & IIf(dtTbl.Rows(C1)("reftype").ToString.ToUpper = "QL_TRNBELIMST_FA", "PAYAPFA", "PAYAP") & "', '1/1/1900', '" & periodacctg.Text & "', " & IIf(cashbanktype.SelectedValue = "BGK", iAPGiroAcctgOid, acctgoid.SelectedValue) & ", '" & cashbankdate.Text & "', '" & Tchar(cashbankrefno.Text) & "', " & IIf(cashbanktype.SelectedValue = "BKK" Or cashbanktype.SelectedValue = "BLK", 0, acctgoid.SelectedValue) & ", '" & sDueDate & "', 0, " & ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString) & ", 'Payment A/P No. " & dtTbl.Rows(C1)("aprawno").ToString & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * ToDouble(dtTbl.Rows(C1)("apratetoidr").ToString) & ", 0, " & (ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) - ToDouble(dtTbl.Rows(C1)("totaldiff").ToString)) * ToDouble(dtTbl.Rows(C1)("apratetousd").ToString) & ", " & ToDouble(dtTbl.Rows(C1)("payapamtother").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        If dtTbl.Rows(C1)("payapflag").ToString = "" Then
                            If ToDouble(dtTbl.Rows(C1)("subtotalpayment").ToString) >= ToDouble(dtTbl.Rows(C1)("apbalance").ToString) Then
                                sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtTbl.Rows(C1)("aprawmstoid").ToString
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtTbl.Rows.Count - 1 + CInt(payapoid.Text)) & " WHERE tablename='QL_TRNPAYAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (dtTbl.Rows.Count - 1 + CInt(conapoid.Text)) & " WHERE tablename='QL_CONAP' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbankstatus.Text = "Post" Then
                        'insert lawan barter cashbank, payar dan conar
                        If cashbanktype.SelectedValue = "BOK" Then
                            If i_u.Text = "Update Data" Then
                                cbOper = CInt(genId)
                            Else
                                cbOper = CInt(cashbankoid.Text) + 1
                            End If
                            sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, cashbankdpp, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, cashbankrate2toidr, cashbankrate2tousd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cbOper & ", '" & periodacctg.Text & "', '" & operNo.Text & "', '" & cashbankdate.Text & "', 'BOM', 'AR', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(payapnett.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2tousd.Text) & ", " & custoid.Text & ", '" & sDueDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sDTG & "', " & iAPGiroAcctgOid & ", " & ToDouble(dpapamt.Text) & "," & addacctgoid1.SelectedValue & "," & ToDouble(addacctgamt1.Text) & "," & addacctgoid2.SelectedValue & "," & ToDouble(addacctgamt2.Text) & "," & addacctgoid3.SelectedValue & "," & ToDouble(addacctgamt3.Text) & ", " & ToDouble(porawrate2toidr.Text) & ", " & ToDouble(porawrate2tousd.Text) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & cbOper & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert To QL_trnpayar
                            sSql = "INSERT INTO QL_trnpayar (cmpcode, payaroid, cashbankoid, custoid, reftype, refoid, acctgoid, payarrefno, payarduedate, payaramt, payaramtidr, payaramtusd, payarnote, payarstatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(payaroid.Text) & ", " & cbOper & ", " & custoid.Text & ", '" & reftype_ar.Text & "', " & giroacctgoid.Text & ", " & acctgoid.SelectedValue & ", '" & Tchar(cashbankrefno.Text) & "', '" & sDueDate & "', " & ToDouble(payapnett.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2toidr.Text) & ", " & ToDouble(payapnett.Text) * ToDouble(porawrate2tousd.Text) & ", '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & payaroid.Text & " WHERE tablename='QL_TRNPAYAR' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            ' Insert To QL_conar
                            sSql = "INSERT INTO QL_conar (cmpcode, conaroid, reftype, refoid, payrefoid, custoid, acctgoid, trnarstatus, trnartype, trnardate, periodacctg, paymentacctgoid, paymentdate, payrefno, paybankoid, payduedate, amttrans, amtbayar, trnarnote, createuser, createtime, upduser, updtime, amttransidr, amtbayaridr, amttransusd, amtbayarusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(conaroid.Text) & ", '" & reftype_ar.Text & "', " & giroacctgoid.Text & ", " & CInt(payaroid.Text) & ", " & custoid.Text & ", " & acctgoid.SelectedValue & ", '" & cashbankstatus.Text & "', 'PAYAR', '1/1/1900', '" & periodacctg.Text & "', " & acctgoid.SelectedValue & ", '" & cashbankdate.Text & "', '" & Tchar(cashbankrefno.Text) & "', " & acctgoid.SelectedValue & ", '" & sDueDate & "', 0, " & ToDouble(payapnett.Text) & ", 'Payment A/R No. " & operNo.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, 0, " & ToDouble(payapnett.Text) * ToDouble(porawrate2toidr.Text) & ", 0, " & ToDouble(payapnett.Text) * ToDouble(porawrate2tousd.Text) & ")"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            sSql = "UPDATE QL_mstoid SET lastoid=" & conaroid.Text & " WHERE tablename='QL_CONAR' AND cmpcode='" & CompnyCode & "'"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()

                            If dPayAPBOK > 0 Then
                                If ToDouble(dPayAPBOK) >= ToDouble(dpapamt.Text) Then
                                    Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = ""
                                    If reftype_ar.Text.ToUpper = "QL_TRNJUALMST" Then
                                        sTableName = "QL_trnjualmst" : sColStatusName = "trnjualstatus" : sIDColName = "trnjualmstoid"
                                    ElseIf reftype_ar.Text.ToUpper = "QL_TRNTRANSITEMMST" Then
                                        sTableName = "QL_trntransitemmst" : sColStatusName = "transitemmststatus" : sIDColName = "transitemmstoid"
                                    End If
                                    sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='Closed', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & ToInteger(giroacctgoid.Text) & " "
                                    xCmd.CommandText = sSql
                                    xCmd.ExecuteNonQuery()
                                End If
                                sSql = "UPDATE QL_mstcustgroup SET custcreditlimitusagerupiah = custcreditlimitusagerupiah - " & ToDouble(dPayAPBOK) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND custgroupoid=" & iCustGroupOid & ""
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If

                        Dim sGLDate As String = cashbankdate.Text
                        If cashbanktype.SelectedValue = "BBK" Then
                            sGLDate = cashbankduedate.Text
                        End If
                        ' Insert GL MST
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sGLDate & "', '" & GetDateToPeriodAcctg(CDate(sGLDate)) & "', 'Payment A/P|No. " & cashbankno.Text & "', 'Post', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & ToDouble(porawrate2toidr.Text) & ", " & cRate.GetRateDailyUSDValue & ", " & ToDouble(porawrate2tousd.Text) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert GL DTL
                        Dim iSeq As Integer = 1
                        Dim dVal As Double = ToDouble(cashbankamt.Text) ' Variable total pembayaran
                        If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                            dVal = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag IN ('(#)', '(+)')").ToString) - ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(-)'").ToString)
                        End If
                        Dim dValPlus As Double = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(+)'").ToString) ' Variable total lebih bayar
                        Dim dValMin As Double = ToDouble(dtSum.Compute("SUM(acctgamt)", "acctgflag='(-)'").ToString) ' Variable total kurang bayar
                        Dim dValPlusOther As Double = ToDouble(dtSum.Compute("SUM(acctgamtother)", "acctgflag='(+)'").ToString) ' Variable total lebih bayar bila dibayar dg mata uang lain
                        Dim dValMinOther As Double = ToDouble(dtSum.Compute("SUM(acctgamtother)", "acctgflag='(-)'").ToString) ' Variable total kurang bayar bila dibayar dg mata uang lain
                        Dim dValIDR As Double = Math.Round(ToDouble(dtSum.Compute("SUM(acctgamtidr)", "acctgflag='(#)'").ToString), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total hutang dlm rupiah
                        Dim dValUSD As Double = Math.Round(ToDouble(dtSum.Compute("SUM(acctgamtusd)", "acctgflag='(#)'").ToString), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total hutang dlm usd
                        Dim dValPayIDR As Double = Math.Round(ToDouble(cashbankamt.Text) * ToDouble(porawrate2toidr.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total pembayaran dlm rupiah
                        Dim dValPayUSD As Double = Math.Round(ToDouble(cashbankamt.Text) * ToDouble(porawrate2tousd.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total pembayaran dlm usd
                        Dim dValPlusPayIDR As Double = Math.Round(dValPlus * cRate2.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total lebih bayar dlm rupiah
                        Dim dValPlusPayUSD As Double = Math.Round(dValPlus * cRate2.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total lebih bayar dlm usd
                        Dim dValMinPayIDR As Double = Math.Round(dValMin * cRate2.GetRateMonthlyIDRValue, iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total kurang bayar dlm rupiah
                        Dim dValMinPayUSD As Double = Math.Round(dValMin * cRate2.GetRateMonthlyUSDValue, iRoundDigit, MidpointRounding.AwayFromZero) ' Variable total kurang bayar dlm usd

                        Dim dAddCost1IDR As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * ToDouble(porawrate2toidr.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 1 dlm rupiah
                        Dim dAddCost2IDR As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * ToDouble(porawrate2toidr.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 2 dlm rupiah
                        Dim dAddCost3IDR As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * ToDouble(porawrate2toidr.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 3 dlm rupiah
                        Dim dAddCost1USD As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * ToDouble(porawrate2tousd.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 1 dlm usd
                        Dim dAddCost2USD As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * ToDouble(porawrate2tousd.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 2 dlm usd
                        Dim dAddCost3USD As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * ToDouble(porawrate2tousd.Text), iRoundDigit, MidpointRounding.AwayFromZero) ' Variable cost 3 dlm usd
                        Dim dAddCost1 As Decimal = dAddCost1IDR / cRate2.GetRateMonthlyIDRValue
                        Dim dAddCost2 As Decimal = dAddCost2IDR / cRate2.GetRateMonthlyIDRValue
                        Dim dAddCost3 As Decimal = dAddCost3IDR / cRate2.GetRateMonthlyIDRValue

                        If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                            If curroid.SelectedItem.Text = "IDR" Then
                                dValPayIDR = ToDouble(cashbankamt.Text)
                                dValPlusPayIDR = dValPlusOther
                                dValMinPayIDR = dValMinOther
                                If sAPCurrCode = "USD" Then
                                    If cashbanktype.SelectedValue <> "BLK" Then
                                        dValPayUSD = dVal
                                    End If
                                    dValPlusPayUSD = dValPlus
                                    dValMinPayUSD = dValMin
                                End If
                            ElseIf curroid.SelectedItem.Text = "USD" Then
                                If sAPCurrCode = "IDR" Then
                                    If cashbanktype.SelectedValue <> "BLK" Then
                                        dValPayIDR = dVal
                                    End If
                                    dValPlusPayIDR = dValPlus
                                    dValMinPayIDR = dValMin
                                End If
                                dValPayUSD = ToDouble(cashbankamt.Text)
                                dValPlusPayUSD = dValPlusOther
                                dValMinPayUSD = dValMinOther
                            Else
                                If sAPCurrCode = "IDR" Then
                                    dValPayIDR = dVal
                                    dValPayUSD = dVal * cRate2.GetRateMonthlyUSDValue
                                    dValPlusPayIDR = dValPlus
                                    dValMinPayIDR = dValMin
                                ElseIf sAPCurrCode = "USD" Then
                                    dValPayIDR = dVal * cRate2.GetRateMonthlyIDRValue
                                    dValPayUSD = dVal
                                    dValPlusPayUSD = dValPlus
                                    dValMinPayUSD = dValMin
                                Else
                                    dValPayIDR = dVal * cRate2.GetRateMonthlyIDRValue
                                    dValPayUSD = dVal * cRate2.GetRateMonthlyUSDValue
                                End If
                            End If
                        End If

                        Dim dvSum As DataView = dtSum.DefaultView
                        ' === DEBET
                        ' Hutang Usaha
                        dvSum.RowFilter = "acctgflag='(#)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'D', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(dvSum(C1)("acctgamtidr").ToString) & ", " & ToDouble(dvSum(C1)("acctgamtusd").ToString) & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""

                        ' Additional Cost 1+
                        If ToDouble(addacctgamt1.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid1.SelectedValue & ", 'D', " & dAddCost1 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost1IDR & ", " & dAddCost1USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 1 - Payment A/P|No. " & cashbankno.Text & "','K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 2+
                        If ToDouble(addacctgamt2.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid2.SelectedValue & ", 'D', " & dAddCost2 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost2IDR & ", " & dAddCost2USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 2 - Payment A/P|No. " & cashbankno.Text & "','K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 3+
                        If ToDouble(addacctgamt3.Text) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid3.SelectedValue & ", 'D', " & dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost3IDR & ", " & dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "','Additional Cost 3 - Payment A/P|No. " & cashbankno.Text & "','K')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If

                        ' Kelebihan Bayar
                        dvSum.RowFilter = "acctgflag='(+)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                            Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                            If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                                If curroid.SelectedItem.Text = "IDR" Then
                                    dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    If sAPCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                ElseIf curroid.SelectedItem.Text = "USD" Then
                                    If sAPCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                    dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                Else
                                    If sAPCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    ElseIf sAPCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                End If
                            End If
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'D', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""
                        ' === CREDIT
                        If cashbanktype.SelectedValue = "BGK" Then
                            ' Hutang Giro
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & iAPGiroAcctgOid & ", 'C', " & dVal + dAddCost1 + dAddCost2 + dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValPayIDR + dAddCost1IDR + dAddCost2IDR + dAddCost3IDR & ", " & dValPayUSD + dAddCost1USD + dAddCost2USD + dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Else
                            ' Kas/Bank
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & acctgoid.SelectedValue & ", 'C', " & dVal + dAddCost1 + dAddCost2 + dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dValPayIDR + dAddCost1IDR + dAddCost2IDR + dAddCost3IDR & ", " & dValPayUSD + dAddCost1USD + dAddCost2USD + dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        End If

                        ' Additional Cost 1-
                        If ToDouble(addacctgamt1.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid1.SelectedValue & ", 'C', " & -dAddCost1 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -dAddCost1IDR & ", " & -dAddCost1USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 1 - Payment A/P|No. " & cashbankno.Text & "', 'M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 2-
                        If ToDouble(addacctgamt2.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid2.SelectedValue & ", 'C', " & -dAddCost2 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -dAddCost2IDR & ", " & -dAddCost2USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 2 - Payment A/P|No. " & cashbankno.Text & "', 'M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If
                        ' Additional Cost 3-
                        If ToDouble(addacctgamt3.Text) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & addacctgoid3.SelectedValue & ", 'C', " & -dAddCost3 & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -dAddCost3IDR & ", " & -dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 3 - Payment A/P|No. " & cashbankno.Text & "', 'M')"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                            gldtloid += 1 : iSeq += 1
                        End If

                        ' Kekurangan Bayar
                        dvSum.RowFilter = "acctgflag='(-)'"
                        For C1 As Integer = 0 To dvSum.Count - 1
                            Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                            Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                            If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                                If curroid.SelectedItem.Text = "IDR" Then
                                    dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    If sAPCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                ElseIf curroid.SelectedItem.Text = "USD" Then
                                    If sAPCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                    dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                Else
                                    If sAPCurrCode = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    ElseIf sAPCurrCode = "USD" Then
                                        dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                    End If
                                End If
                            End If
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'C', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & cashbankno.Text & "', 'Payment A/P|" & suppname.Text & "', 'Post', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            gldtloid += 1
                            iSeq += 1
                        Next
                        dvSum.RowFilter = ""
                        Dim iCBMstoid2 As Integer = genId
                        Dim IsDP As Boolean = False
                        If cashbanktype.SelectedValue <> "BLK" Then
                            ' Kelebihan Bayar (DP)
                            dvSum.RowFilter = "acctgflag='(+)' AND acctgoid=" & sOidDP
                            For C1 As Integer = 0 To dvSum.Count - 1
                                IsDP = True
                                Dim dIDR As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyIDRValue
                                Dim dUSD As Double = ToDouble(dvSum(C1)("acctgamt").ToString) * cRate2.GetRateMonthlyUSDValue
                                If CInt(curroid.SelectedValue) <> iAPCurrOid Then
                                    If curroid.SelectedItem.Text = "IDR" Then
                                        dIDR = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                        If sAPCurrCode = "USD" Then
                                            dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                    ElseIf curroid.SelectedItem.Text = "USD" Then
                                        If sAPCurrCode = "IDR" Then
                                            dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                        dUSD = ToDouble(dvSum(C1)("acctgamtother").ToString)
                                    Else
                                        If sAPCurrCode = "IDR" Then
                                            dIDR = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        ElseIf sAPCurrCode = "USD" Then
                                            dUSD = ToDouble(dvSum(C1)("acctgamt").ToString)
                                        End If
                                    End If
                                End If

                                'Insert ke DPAP jika lebih bayar akun DPAP
                                sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCBMstoid2 & ", '" & periodacctg.Text & "', '" & cashbankno2.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'DPAP', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", " & dIDR & ", " & dUSD & ", " & suppoid.Text & ", '" & IIf(cashbanktype.SelectedValue <> "BKK", cashbankduedate.Text, "1/1/1900") & "', '" & Tchar(cashbankrefno.Text) & "', 'DPAP Lebih Bayar From " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(cashbanktype.SelectedValue = "BGK", cashbanktakegiro.Text, "1/1/1900") & "', " & iAPGiroAcctgOid & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                sSql = "INSERT INTO QL_trndpap (cmpcode, dpapoid, periodacctg, dpapno, dpapdate, suppoid, acctgoid, cashbankoid, dpappaytype, dpappayacctgoid, dpappayrefno, dpapduedate, curroid, rateoid, rate2oid, dpapamt, dpapaccumamt, dpapnote, dpapstatus, createuser, createtime, upduser, updtime, dpaptakegiro, giroacctgoid, dpapamtidr, dpapamtusd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iDPAPOid & ", '" & periodacctg.Text & "', '" & dpapno.Text & "', '" & cashbankdate.Text & "', " & suppoid.Text & ", " & dvSum(C1)("acctgoid").ToString & ", " & iCBMstoid2 & ", '" & cashbanktype.SelectedValue & "', " & acctgoid.SelectedValue & ", '" & Tchar(cashbankrefno.Text) & "', '" & IIf(cashbanktype.SelectedValue <> "BKK", cashbankduedate.Text, "1/1/1900") & "', " & curroid.SelectedValue & ", " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", 0, 'DPAP Lebih Bayar Dari " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & IIf(cashbanktype.SelectedValue = "BGK", cashbanktakegiro.Text, "1/1/1900") & "', " & iAPGiroAcctgOid & ", " & dIDR & ", " & dUSD & ")"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                Dim sDate2 As String = cashbankdate.Text
                                If cashbanktype.SelectedValue = "BBK" Then
                                    sDate2 = cashbankduedate.Text
                                End If
                                Dim sPeriod2 As String = GetDateToPeriodAcctg(CDate(sDate2))
                                glmstoid += 1
                                ' Insert Into GL Mst
                                'sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid & ", '" & sDate2 & "', '" & sPeriod2 & "', 'DP A/P|No=" & dpapno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'iSeq = 1
                                '' Insert Into GL Dtl
                                '' Uang Muka
                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & dvSum(C1)("acctgoid").ToString & ", 'D', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & dpapno.Text & "', 'DPAP Lebih Bayar From " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trndpap " & iDPAPOid & "')"
                                'xCmd.CommandText = sSql
                                'xCmd.ExecuteNonQuery()
                                'gldtloid += 1
                                'iSeq += 1

                                '' Cash/Bank/Giro
                                'sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & gldtloid & ", " & iSeq & ", " & glmstoid & ", " & IIf(cashbanktype.SelectedValue = "BGK", iAPGiroAcctgOid, acctgoid.SelectedValue) & ", 'C', " & ToDouble(dvSum(C1)("acctgamt").ToString) & ", '" & dpapno.Text & "', 'DPAP Lebih Bayar From " & Tchar(cashbankno.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dIDR & ", " & dUSD & ", 'QL_trndpap " & iDPAPOid & "')"
                                'xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                                'gldtloid += 1 : iSeq += 1
                                'iCBMstoid2 += 1 : iDPAPOid += 1
                            Next
                            If IsDP Then
                                sSql = "UPDATE QL_mstoid SET lastoid=" & iCBMstoid2 & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()

                                sSql = "UPDATE QL_mstoid SET lastoid=" & iDPAPOid & " WHERE tablename='QL_TRNDPAP' and cmpcode='" & CompnyCode & "'"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                            End If
                        End If
                        dvSum.RowFilter = ""

                        sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "UPDATE QL_mstoid SET lastoid=" & gldtloid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    objTrans.Commit()
                    conn.Close()
                End If
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.ToString, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.ToString, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.ToString, 1)
                conn.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnPayAP.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnPayAP.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select A/P Payment data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If

        Dim dtCurrentPayap As New DataTable
        If Session("oid") = Nothing Or Session("oid") = "" Then
        Else
            ' Select Current Payment detail on DB for later reverse
            sSql = "SELECT refoid,reftype FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & " AND ISNULL(payapres1,'')='' "
            dtCurrentPayap = cKon.ambiltabel(sSql, "QL_trnpayap")
        End If
        Dim sPayType As String = "PAYAP"
        Dim objTrans As SqlClient.SqlTransaction
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            ' Reversing each detail A/P
            For R1 As Integer = 0 To dtCurrentPayap.Rows.Count - 1
                Dim sTableName As String = "" : Dim sColStatusName As String = "" : Dim sIDColName As String = "" : Dim sValueStatusName As String = ""
                Select Case dtCurrentPayap.Rows(R1)("reftype").ToString.ToUpper
                    Case "QL_TRNBELIMST"
                        sTableName = "QL_trnbelimst" : sColStatusName = "trnbelimststatus" : sIDColName = "trnbelimstoid" : sValueStatusName = "Approved"
                    Case "QL_TRNBELIMST_FA"
                        sTableName = "QL_trnbelimst_fa" : sColStatusName = "trnbelifamststatus" : sIDColName = "trnbelifamstoid" : sValueStatusName = "Post" : sPayType = "PAYAPFA"
                    Case "QL_TRNAPPRODUCTMST"
                        sTableName = "QL_trnapproductmst" : sColStatusName = "approductmststatus" : sIDColName = "approductmstoid" : sValueStatusName = "Post"
                End Select
                sSql = "UPDATE " & sTableName & " SET " & sColStatusName & "='" & sValueStatusName & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP " & _
                    "WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & sIDColName & "=" & dtCurrentPayap.Rows(R1)("refoid").ToString & " "
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            Next

            sSql = "DELETE FROM QL_conap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND trnaptype='" & sPayType & "' AND payrefoid IN (SELECT payapoid FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ")"
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnpayap WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLK" Then
                sSql = "UPDATE QL_trndpap SET dpapaccumamt = dpapaccumamt - " & ToDouble(Session("lastdpapamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dpapoid=" & Session("lastdpapoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            conn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            conn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnPayAP.aspx?awal=true")
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPost.Click
        periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
        If isPeriodClosed(DDLBusUnit.SelectedValue, periodacctg.Text) Then
            showMessage("Cannot continue this transaction anymore because the period : " & MonthName(CInt(periodacctg.Text.Substring(4, 2))) & " " & periodacctg.Text.Substring(0, 4) & " have been closed.", 3)
            Exit Sub
        End If
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), iMask)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), iMask)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub lkbCloseListSI_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSI, pnlListSI, mpeListSI, False)
    End Sub

    Protected Sub gvListSI_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        giroacctgoid.Text = gvListSI.SelectedDataKey.Item("trnjualmstoid").ToString
        cashbankrefno.Text = gvListSI.SelectedDataKey.Item("trnjualno").ToString
        reftype_ar.Text = gvListSI.SelectedDataKey.Item("reftype").ToString
        dpapamt.Text = ToMaskEdit(ToDouble(gvListSI.SelectedDataKey.Item("trnjualamtnetto").ToString), iMask)
        cProc.SetModalPopUpExtender(btnHideListSI, pnlListSI, mpeListSI, False)
    End Sub

    Protected Sub gvListSI_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvListSI.PageIndex = e.NewPageIndex
        gvListSI.DataSource = Session("TblConar")
        gvListSI.DataBind()
        mpeListSI.Show()
    End Sub

    Protected Sub btnFindListSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        BindSIData()
        mpeListSI.Show()
    End Sub

    Protected Sub btnAllListSI_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSI.SelectedIndex = -1 : FilterTextListSI.Text = "" : gvListSI.SelectedIndex = -1
        BindSIData()
        mpeListSI.Show()
    End Sub

    Protected Sub diffamount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Text = ToMaskEdit(ToDouble(diffamount.Text), 2)
    End Sub

    Protected Sub gvListSI_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), iMask)
        End If
    End Sub
#End Region

    Protected Sub gvListAP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), iMask)
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), iMask)
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), iMask)
        End If
    End Sub

    Protected Sub gvListDP_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), iMask)
        End If
    End Sub
End Class
