<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnCreditNote.aspx.vb" Inherits="Accounting_trnCreditNote" Title="Credit Note" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
  <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="Label5" runat="server" Font-Bold="False" Text=".: Credit Note" CssClass="Title"></asp:Label></th>
                    </tr>
                </table>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Width="100%" >
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><asp:Panel id="pnlDataMst" runat="server" __designer:wfdid="w377" DefaultButton="btnFind"><TABLE><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w378"><asp:ListItem Value="a.cnno">CN No.</asp:ListItem>
<asp:ListItem Value="a.suppcustname">Supplier/Customer</asp:ListItem>
<asp:ListItem Value="a.aparno">A/P - A/R No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="235px" __designer:wfdid="w379"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Periode" __designer:wfdid="w380"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="Date1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w381"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w382"></asp:ImageButton>&nbsp; - <asp:TextBox id="Date2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w383"></asp:TextBox>&nbsp; <asp:ImageButton id="sTgl2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w384"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w385"></asp:Label> <ajaxToolkit:CalendarExtender id="ce2" runat="server" TargetControlID="Date2" PopupButtonID="sTgl2" Format="MM/dd/yyyy" Enabled="True" __designer:wfdid="w386"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="Date2" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w387" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" TargetControlID="Date1" PopupButtonID="sTgl1" Format="MM/dd/yyyy" Enabled="True" __designer:wfdid="w388"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="Date1" Mask="99/99/9999" MaskType="Date" __designer:wfdid="w389" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" __designer:wfdid="w390"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem Value="QL_trnbelimst">A/P</asp:ListItem>
<asp:ListItem Value="QL_trnjualmst">A/R</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterStatus" runat="server" CssClass="inpText" __designer:wfdid="w391"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w392"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w393"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD class="Label" align=left><asp:LinkButton id="lkbCNInProcess" runat="server" Visible="False" __designer:wfdid="w394"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvMst" runat="server" Width="100%" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" __designer:wfdid="w395" AllowSorting="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="cnoid" DataNavigateUrlFormatString="trnCreditNote.aspx?oid={0}" DataTextField="cnoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cnno" HeaderText="CN No" SortExpression="cnno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cndate" HeaderText="Date" SortExpression="cndate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="Type" SortExpression="tipe">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppcustname" HeaderText="Customer/Supplier" SortExpression="suppcustname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aparno" HeaderText="A/P - A/R No" SortExpression="aparno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cnamt" HeaderText="Amount" SortExpression="cnamt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Bussiness Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cnstatus" HeaderText="Status" SortExpression="cnstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w396"></asp:Label></TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/List.gif" height="16" />
                            <strong><span style="font-size: 9pt">List of Credit Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 border=0><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="WIDTH: 1%; HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="WIDTH: 1%; HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=left></TD><TD class="Label" align=left><asp:DropDownList id="bus_unit" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w455" AutoPostBack="True"></asp:DropDownList> <asp:Label id="apardate" runat="server" Visible="False" __designer:wfdid="w472"></asp:Label> <asp:Label id="aparreftype" runat="server" Visible="False" __designer:wfdid="w471"></asp:Label></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w456" Format="MM/dd/yyyy" PopupButtonID="imbDate" TargetControlID="cndate"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 1%" class="Label" align=left></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w457" TargetControlID="cndate" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblcnno" runat="server" __designer:wfdid="w458">Draft No</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:Label id="cnoid" runat="server" __designer:wfdid="w459"></asp:Label> <asp:TextBox id="cnno" runat="server" CssClass="inpTextDisabled" Width="150px" Visible="False" __designer:wfdid="w460" Enabled="False" size="20"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left>&nbsp;&nbsp; </TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="reftype" runat="server" CssClass="inpText" Width="55px" __designer:wfdid="w465" AutoPostBack="True"><asp:ListItem Value="ap">A/P</asp:ListItem>
<asp:ListItem Value="ar">A/R</asp:ListItem>
</asp:DropDownList> <asp:Label id="supp_cust_oid" runat="server" Visible="False" __designer:wfdid="w466"></asp:Label></TD><TD class="Label" align=left>Date</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="cndate" runat="server" CssClass="inpText" Width="81px" __designer:wfdid="w461"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w462"></asp:ImageButton> <asp:Label id="Label1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w463"></asp:Label> <asp:Label id="i_u" runat="server" CssClass="Important" Text="New" Visible="False" __designer:wfdid="w464"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparno" runat="server" Text="A/P No" __designer:wfdid="w474"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparno" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w475" size="20"></asp:TextBox> <asp:ImageButton id="btnFindAPAR" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w476"></asp:ImageButton> <asp:ImageButton id="btnClearAPAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w477"></asp:ImageButton> <asp:Label id="refoid" runat="server" Visible="False" __designer:wfdid="w478"></asp:Label></TD><TD class="Label" align=left><asp:Label id="lblsupp_cust" runat="server" Text="Supplier" __designer:wfdid="w467"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="supp_cust_name" runat="server" CssClass="inpTextDisabled" Width="231px" __designer:wfdid="w468" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSC" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w469"></asp:ImageButton> <asp:ImageButton id="btnClearSC" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w470"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView id="gvAPAR" runat="server" __designer:wfdid="w483" BackColor="White" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" DataKeyNames="reftype,refoid">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="transno" HeaderText="A/P No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transdate" HeaderText="A/P Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="currcode" HeaderText="Currency">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="A/P Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbayar" HeaderText="Paid Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbalance" HeaderText="A/P Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">No Account Payable data !!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparamt" runat="server" Text="A/P Amount" __designer:wfdid="w484"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparamt" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w485" Enabled="False" size="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblaparcurr" runat="server" Text="A/P Currency" __designer:wfdid="w479"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w480" Enabled="False"></asp:DropDownList> <asp:Label id="aparrateoid" runat="server" Visible="False" __designer:wfdid="w481"></asp:Label> <asp:Label id="aparrate2oid" runat="server" Visible="False" __designer:wfdid="w482"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparbalance" runat="server" Text="A/P Balance" __designer:wfdid="w488"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparbalance" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w489" Enabled="False" size="20"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblaparpaidamt" runat="server" Text="A/P Paid Amt" __designer:wfdid="w486"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparpaidamt" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w487" Enabled="False" size="20"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparafter" runat="server" Text="A/P Balance After CN" __designer:wfdid="w493"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparbalanceafter" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w494" Enabled="False" size="20"></asp:TextBox></TD><TD class="Label" align=left>CN Amount</TD><TD class="Label" align=left></TD><TD class="Label" align=left><asp:TextBox id="cnamt" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w490" AutoPostBack="True" MaxLength="15"></asp:TextBox> <asp:Label id="lblmaxamt" runat="server" CssClass="Important" Visible="False" __designer:wfdid="w491">Max =</asp:Label> <asp:Label id="maxamount" runat="server" CssClass="Important" Visible="False" __designer:wfdid="w492">0</asp:Label></TD></TR><TR><TD class="Label" align=left>COA Debet</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="coadebet" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w495" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left>COA Credit</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="coacredit" runat="server" CssClass="inpTextDisabled" Width="300px" __designer:wfdid="w496" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="cnnote" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w497" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="cnstatus" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w498" Enabled="False" size="20"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="cntaxtype" runat="server" CssClass="inpText" Visible="False" __designer:wfdid="w499"><asp:ListItem Value="NONTAX">NONTAX</asp:ListItem>
<asp:ListItem>INCLUDE</asp:ListItem>
<asp:ListItem>EXCLUDE</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="cntaxamt" runat="server" CssClass="inpTextDisabled" Width="150px" Visible="False" __designer:wfdid="w500" Enabled="False" size="20"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE cellSpacing=0 border=0><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left><asp:Label id="create" runat="server" __designer:wfdid="w501"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w502"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w503"></asp:Label></TD></TR><TR><TD style="HEIGHT: 28px" align=left><TABLE cellSpacing=0 border=0><TBODY><TR><TD><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w504"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w505"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w506"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w507"></asp:ImageButton> <asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" Visible="False" __designer:wfdid="w508"></asp:ImageButton></TD><TD><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w509" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w510"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/Form.gif" height="16" /> <strong><span style="font-size: 9pt">
                                Form Credit Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
</td>
        </tr>
    </table>
    <asp:UpdatePanel id="upListSupp" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="name">Name</asp:ListItem>
<asp:ListItem Value="code">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSuppCust" runat="server" ForeColor="#333333" Width="98%" DataKeyNames="oid,name,code" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" AllowSorting="True" PageSize="5" OnSelectedIndexChanged="gvSuppCust_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="oid" HeaderText="Oid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" PopupControlID="pnlListSupp" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblListSupp" Drag="True"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel>
<asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
<contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="700px" Visible="False" __designer:wfdid="w524"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Bold="True" __designer:wfdid="w525"></asp:Label></TD></TR><TR><TD colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle" Width="24px" Height="24px" __designer:wfdid="w526"></asp:Image></TD><TD style="WIDTH: 660px; TEXT-ALIGN: left"><asp:Label id="lblMessage" runat="server" CssClass="Important" __designer:wfdid="w527"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server" Visible="False" __designer:wfdid="w528"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnOKPopUp" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w529"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" __designer:wfdid="w530"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w531"></asp:Button> 
</contenttemplate>
</asp:UpdatePanel>
</asp:Content>
