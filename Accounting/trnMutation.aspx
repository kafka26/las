<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnMutation.aspx.vb" Inherits="Accounting_CashBankMutation" title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2">
                <asp:Label ID="Label1" runat="server" CssClass="Title" Font-Bold="False" Text=".: Cash/Bank Overbooking"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of Cash/Bank Overbooking :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" Width="100%" DefaultButton="btnSearch" __designer:wfdid="w30"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=5></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label19" runat="server" Text="Filter" __designer:wfdid="w31"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w32"><asp:ListItem Value="t.cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="t.cashbankno_to">Cash/Bank No. To</asp:ListItem>
<asp:ListItem Value="t.acctgdesc">Account From</asp:ListItem>
<asp:ListItem Value="t.acctgdesc_to">Account To</asp:ListItem>
<asp:ListItem Value="t.cashbanknote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="200px" __designer:wfdid="w33"></asp:TextBox></TD><TD align=left></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text=" Period" __designer:wfdid="w34"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w35"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w36"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to" __designer:wfdid="w37"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w38"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w39"></asp:ImageButton> <asp:Label id="lbldate1" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w40"></asp:Label></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text=" Status" __designer:wfdid="w41"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="120px" __designer:wfdid="w42"><asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="Print Option" __designer:wfdid="w43"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLPrintOption" runat="server" CssClass="inpText" Width="120px" __designer:wfdid="w44"><asp:ListItem>Print Bukti Keluar</asp:ListItem>
<asp:ListItem>Print Bukti Masuk</asp:ListItem>
</asp:DropDownList></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" __designer:wfdid="w45" Format="MM/dd/yyyy" PopupButtonID="imbDate1" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" __designer:wfdid="w46" TargetControlID="FilterPeriod1" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" __designer:wfdid="w47" Format="MM/dd/yyyy" PopupButtonID="imbDate2" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" __designer:wfdid="w48" TargetControlID="FilterPeriod2" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> </TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w49"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w50"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle" __designer:wfdid="w51"></asp:ImageButton></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lkbMutationInProcess" runat="server" __designer:wfdid="w52"></asp:LinkButton></TD><TD align=left colSpan=1></TD></TR><TR><TD style="HEIGHT: 5pt" class="Label" align=left colSpan=4></TD><TD align=left colSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" __designer:wfdid="w53" AllowSorting="True" AllowPaging="True" DataKeyNames="cashbankoid" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="cashbankoid" DataNavigateUrlFormatString="~\Accounting\trnMutation.aspx?oid={0}" DataTextField="cashbankno" HeaderText="Cash/Bank No." SortExpression="cashbankno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cashbankdate" HeaderText="Overbooking Date" SortExpression="cashbankdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktype" HeaderText="Type" SortExpression="cashbanktype">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankno_to" HeaderText="Cash/Bank No. To">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account From" SortExpression="acctgdesc">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc_to" HeaderText="Account To">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status" SortExpression="cashbankstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note" SortExpression="cashbanknote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:TemplateField><ItemTemplate>
<asp:CheckBox id="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>'></asp:CheckBox> <asp:Label id="lblOidGVMst" runat="server" Text='<%# eval("cashbankoid") %>' Visible="False"></asp:Label>
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" BackColor="Sienna" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w54"></asp:Label></TD><TD align=left colSpan=1></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form Cash/Bank Overbooking :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label17" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Cash/Bank Overbooking Header" Font-Underline="False" __designer:wfdid="w713"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" Text="New Data" CssClass="Important" __designer:wfdid="w714"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" __designer:wfdid="w715" Visible="False"></asp:Label> <asp:Label id="cashbankoid" runat="server" __designer:wfdid="w716" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="giroacctgoid" runat="server" __designer:wfdid="w717" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Business Unit" __designer:wfdid="w718" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w719" Visible="False" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblInfoNo" runat="server" Text="Cash/Bank No." Width="96px" __designer:wfdid="w720"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w721" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Overbooking Date" __designer:wfdid="w722"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankdate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w723" AutoPostBack="True" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w724"></asp:ImageButton> <asp:Label id="Label22" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w725"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Account Type" Width="80px" __designer:wfdid="w726"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="cashbanktype" runat="server" CssClass="inpText" Width="105px" __designer:wfdid="w727" AutoPostBack="True"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">BANK</asp:ListItem>
                <asp:ListItem Value="BGK">GIRO</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label3" runat="server" Text="Overbooking From" __designer:wfdid="w728"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" __designer:wfdid="w729" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label15" runat="server" Text="Currency" Width="56px" __designer:wfdid="w730"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Width="105px" __designer:wfdid="w731" Enabled="False">
            </asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label25" runat="server" Text="Total Amount" __designer:wfdid="w732"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankamt" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w733" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblRefNo" runat="server" Text="Ref. No." Width="48px" __designer:wfdid="w734"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankrefno" runat="server" CssClass="inpText" Width="160px" __designer:wfdid="w735" MaxLength="30"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date" __designer:wfdid="w736"></asp:Label> <asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w737" Visible="False"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDueDate" runat="server" Text=":" __designer:wfdid="w738"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="cashbankduedate" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w739" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w740"></asp:ImageButton> <asp:Label id="lblInfoDueDate" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w741"></asp:Label></TD></TR><TR><TD id="TD3" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label12" runat="server" Text="PIC" Width="32px" __designer:wfdid="w742"></asp:Label></TD><TD id="TD2" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TD1" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="personoid" runat="server" CssClass="inpText" Width="205px" __designer:wfdid="w743"><asp:ListItem>0</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label16" runat="server" Text="Rate Manual to IDR" Width="120px" __designer:wfdid="w744"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankresamt" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w745"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label23" runat="server" Text="Additional Cost On" Width="112px" __designer:wfdid="w746"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:RadioButtonList id="rdbAdd" runat="server" __designer:wfdid="w747" AutoPostBack="True" RepeatDirection="Horizontal"><asp:ListItem Selected="True">Header</asp:ListItem>
<asp:ListItem>Detail</asp:ListItem>
</asp:RadioButtonList></TD><TD class="Label" align=left><asp:Label id="lblDTG" runat="server" Text="Date Take Giro" Width="96px" __designer:wfdid="w748"></asp:Label> <asp:Label id="lblWarnDTG" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w749"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDTG" runat="server" Text=":" __designer:wfdid="w750"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="cashbanktakegiro" runat="server" CssClass="inpText" Width="80px" __designer:wfdid="w751" ToolTip="MM/DD/YYYY"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w752"></asp:ImageButton> <asp:Label id="lblInfoDTG" runat="server" Text="(MM/dd/yyyy)" CssClass="Important" __designer:wfdid="w753"></asp:Label></TD></TR><TR><TD id="TDAcc11" class="Label" align=left runat="server"><asp:Label id="Label24" runat="server" Text="Add Cost 1" __designer:wfdid="w754"></asp:Label></TD><TD id="TDAcc12" class="Label" align=center runat="server">:</TD><TD id="TDAcc13" class="Label" align=left runat="server"><asp:DropDownList id="addacctgoid1" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w755" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmt11" class="Label" align=left runat="server"><asp:Label id="Label40" runat="server" Text="Cost Amt 1" Width="72px" __designer:wfdid="w756"></asp:Label></TD><TD id="TDAmt12" class="Label" align=center runat="server">:</TD><TD id="TDAmt13" class="Label" align=left runat="server"><asp:TextBox id="addacctgamt1" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w757" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TDAcc21" class="Label" align=left runat="server"><asp:Label id="Label41" runat="server" Text="Add Cost 2" __designer:wfdid="w758"></asp:Label></TD><TD id="TDAcc22" class="Label" align=center runat="server">:</TD><TD id="TDAcc23" class="Label" align=left runat="server"><asp:DropDownList id="addacctgoid2" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w759" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmt21" class="Label" align=left runat="server"><asp:Label id="Label42" runat="server" Text="Cost Amt 2" Width="72px" __designer:wfdid="w760"></asp:Label></TD><TD id="TDAmt22" class="Label" align=center runat="server">:</TD><TD id="TDAmt23" class="Label" align=left runat="server"><asp:TextBox id="addacctgamt2" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w761" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TDAcc31" class="Label" align=left runat="server"><asp:Label id="Label43" runat="server" Text="Add Cost 3" __designer:wfdid="w762"></asp:Label></TD><TD id="TDAcc32" class="Label" align=center runat="server" Visible="true">:</TD><TD id="TDAcc33" class="Label" align=left runat="server"><asp:DropDownList id="addacctgoid3" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w763" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmt31" class="Label" align=left runat="server"><asp:Label id="Label44" runat="server" Text="Cost Amt 3" Width="72px" __designer:wfdid="w764"></asp:Label></TD><TD id="TDAmt32" class="Label" align=center runat="server">:</TD><TD id="TDAmt33" class="Label" align=left runat="server"><asp:TextBox id="addacctgamt3" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w765" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TDAcc41" class="Label" align=left runat="server"><asp:Label id="Label45" runat="server" Text="Total Amount + Cost" Width="136px" __designer:wfdid="w766"></asp:Label></TD><TD id="TDAcc42" class="Label" align=center runat="server">:</TD><TD id="TDAcc43" class="Label" align=left runat="server"><asp:TextBox id="cbnett" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w767" Enabled="False"></asp:TextBox></TD><TD id="TDAmt41" class="Label" align=left runat="server"></TD><TD id="TDAmt42" class="Label" align=center runat="server"></TD><TD id="TDAmt43" class="Label" align=left runat="server"></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note" __designer:wfdid="w768"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanknote" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w769" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status" __designer:wfdid="w770"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankstatus" runat="server" CssClass="inpTextDisabled" Width="80px" __designer:wfdid="w771" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" __designer:wfdid="w772" TargetControlID="cashbankduedate" PopupButtonID="imbDueDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" __designer:wfdid="w773" TargetControlID="cashbankduedate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbRateManual" runat="server" __designer:wfdid="w774" TargetControlID="cashbankresamt" ValidChars="1234567890,.">
                </ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w775" TargetControlID="cashbankdate" PopupButtonID="imbDate" Format="MM/dd/yyyy">
                    </ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDate" runat="server" __designer:wfdid="w776" TargetControlID="cashbankdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
                    </ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt1" runat="server" __designer:wfdid="w777" TargetControlID="addacctgamt1" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt2" runat="server" __designer:wfdid="w778" TargetControlID="addacctgamt2" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt3" runat="server" __designer:wfdid="w779" TargetControlID="addacctgamt3" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:CalendarExtender id="ceDTG" runat="server" __designer:wfdid="w780" TargetControlID="cashbanktakegiro" PopupButtonID="imbDTG" Format="MM/dd/yyyy">
    </ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" __designer:wfdid="w781" TargetControlID="cashbanktakegiro" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999">
    </ajaxToolkit:MaskedEditExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6 rowSpan=1></TD></TR><TR><TD class="Label" align=left colSpan=6 rowSpan=1><asp:Label id="Label18" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Cash/Bank Overbooking Detail" Font-Underline="False" __designer:wfdid="w782"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" __designer:wfdid="w783" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="cashbankglseq" runat="server" __designer:wfdid="w784" Visible="False">1</asp:Label> <asp:Label id="cashbankgloid" runat="server" __designer:wfdid="w785" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="acctgoid2" runat="server" __designer:wfdid="w786" Visible="False"></asp:Label> <asp:Label id="acctgcode" runat="server" __designer:wfdid="w787" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="Overbooking To" __designer:wfdid="w788"></asp:Label> <asp:Label id="Label21" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w789"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="acctgdesc" runat="server" CssClass="inpTextDisabled" Width="160px" __designer:wfdid="w790" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchCOA" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w791"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearCOA" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w792"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Amount" __designer:wfdid="w793"></asp:Label> <asp:Label id="Label20" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w794"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankglamt" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w795" MaxLength="15"></asp:TextBox></TD></TR><TR><TD id="TDAccDtl11" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label26" runat="server" Text="Add Cost 1" __designer:wfdid="w796"></asp:Label></TD><TD id="TDAccDtl12" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAccDtl13" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoiddtl1" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w797" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmtDtl11" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label29" runat="server" Text="Cost Amt 1" __designer:wfdid="w798"></asp:Label></TD><TD id="TDAmtDtl12" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAmtDtl13" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamtdtl1" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w799" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TDAccDtl21" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label27" runat="server" Text="Add Cost 2" __designer:wfdid="w800"></asp:Label></TD><TD id="TDAccDtl22" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAccDtl23" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoiddtl2" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w801" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmtDtl21" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label30" runat="server" Text="Cost Amt 2" __designer:wfdid="w802"></asp:Label></TD><TD id="TDAmtDtl22" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAmtDtl23" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamtdtl2" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w803" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD id="TDAccDtl31" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label28" runat="server" Text="Add Cost 3" __designer:wfdid="w804"></asp:Label></TD><TD id="TDAccDtl32" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAccDtl33" class="Label" align=left runat="server" Visible="false"><asp:DropDownList id="addacctgoiddtl3" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w805" AutoPostBack="True"></asp:DropDownList></TD><TD id="TDAmtDtl31" class="Label" align=left runat="server" Visible="false"><asp:Label id="Label31" runat="server" Text="Cost Amt 3" __designer:wfdid="w806"></asp:Label></TD><TD id="TDAmtDtl32" class="Label" align=center runat="server" Visible="false">:</TD><TD id="TDAmtDtl33" class="Label" align=left runat="server" Visible="false"><asp:TextBox id="addacctgamtdtl3" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w807" Enabled="False" AutoPostBack="True" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note" __designer:wfdid="w808"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankglnote" runat="server" CssClass="inpText" Width="225px" __designer:wfdid="w809" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" __designer:wfdid="w810" TargetControlID="cashbankglamt" ValidChars="1234567890,.">
                </ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamtdtl1" runat="server" __designer:wfdid="w811" TargetControlID="addacctgamtdtl1" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamtdtl2" runat="server" __designer:wfdid="w812" TargetControlID="addacctgamtdtl2" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamtdtl3" runat="server" __designer:wfdid="w813" TargetControlID="addacctgamtdtl3" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle" __designer:wfdid="w814"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle" __designer:wfdid="w815"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlGVDetail" runat="server" CssClass="inpText" Width="100%" __designer:wfdid="w816" ScrollBars="Vertical" Height="175px"><asp:GridView id="gvDtl" runat="server" Width="100%" __designer:wfdid="w817" DataKeyNames="cashbankglseq" PageSize="5" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankglnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="addacctgamtdtl1" HeaderText="Cost 1" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="addacctgamtdtl2" HeaderText="Cost 2" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="addacctgamtdtl3" HeaderText="Cost 3" Visible="False">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="25px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red" Width="25px"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label7" runat="server" ForeColor="Red" Text="No detail data !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True" __designer:wfdid="w818"></asp:Label> On <asp:Label id="createtime" runat="server" Font-Bold="True" __designer:wfdid="w819"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w820"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w821"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w822" AlternateText="Save"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w823" AlternateText="Cancel"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w824" AlternateText="Delete"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnposting" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w825"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w826" AlternateText="Show COA"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress2" runat="server" __designer:wfdid="w827" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div1" class="progressBackgroundFilter"></DIV><DIV id="Div2" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w828"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV>
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upCOAPosting" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlCOAPosting" runat="server" CssClass="modalBox" Width="600px" __designer:wfdid="w830" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblCOAPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting Rate :" __designer:wfdid="w831"></asp:Label> <asp:DropDownList id="DDLRateType" runat="server" Font-Size="Medium" Font-Bold="True" CssClass="inpText" Width="125px" __designer:wfdid="w832" AutoPostBack="True">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvCOAPosting" runat="server" ForeColor="#333333" Width="99%" __designer:wfdid="w833" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseCOAPosting" runat="server" __designer:wfdid="w834">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideCOAPosting" runat="server" __designer:wfdid="w835" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeCOAPosting" runat="server" __designer:wfdid="w836" TargetControlID="btnHideCOAPosting" PopupDragHandleControlID="lblCOAPosting" PopupControlID="pnlCOAPosting" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListCOA" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListCOA" runat="server" CssClass="modalBox" Width="550px" __designer:wfdid="w838" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCOA" runat="server" Font-Size="Medium" Font-Bold="True" __designer:wfdid="w839" Font-Underline="False">List Of Overbooking Account</asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCOA" runat="server" CssClass="Label" Width="100%" DefaultButton="btnFindListCOA" __designer:wfdid="w840">Filter : <asp:DropDownList id="FilterDDLListCOA" runat="server" CssClass="inpText" Width="100px" __designer:wfdid="w841"><asp:ListItem Value="a.acctgcode">Account No.</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">Account Desc.</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListCOA" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w842"></asp:TextBox> <asp:ImageButton id="btnFindListCOA" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w843"></asp:ImageButton> <asp:ImageButton id="btnAllListCOA" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w844"></asp:ImageButton></asp:Panel></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCOA" runat="server" ForeColor="#333333" Width="99%" __designer:wfdid="w845" AllowPaging="True" DataKeyNames="acctgoid,acctgcode,acctgdesc" PageSize="8" CellPadding="4" AutoGenerateColumns="False" GridLines="None">
<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="acctgcode" HeaderText="Account No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvpopup" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label14" runat="server" Text="No data found !!" CssClass="Important"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
</asp:GridView> </TD></TR><TR><TD vAlign=top align=center colSpan=3><asp:LinkButton id="lkbCloseListCOA" runat="server" __designer:wfdid="w846">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <asp:Button id="btnHideListCOA" runat="server" __designer:wfdid="w847" Visible="False"></asp:Button> <ajaxToolkit:ModalPopupExtender id="mpeListCOA" runat="server" __designer:wfdid="w848" TargetControlID="btnHideListCOA" PopupDragHandleControlID="lblListCOA" PopupControlID="pnlListCOA" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
        <tr>
            <td align="left">
                &nbsp;</td>
        </tr>
    </table>
                            <asp:UpdatePanel id="upPopUpMsg" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" __designer:wfdid="w294" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True" __designer:wfdid="w295"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" __designer:wfdid="w296" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red" __designer:wfdid="w297"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w298"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" __designer:wfdid="w299" DropShadow="True" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" __designer:wfdid="w300" Visible="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel>
</asp:Content>

