<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFixAssetBalance.aspx.vb" Inherits="Accounting_trnFixAssetBalance" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset Balance"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0" __designer:wfdid="w15"><asp:View id="View3" runat="server" __designer:wfdid="w16"><asp:Panel id="Panel1" runat="server" __designer:wfdid="w17" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 66px" align=left><asp:Label id="Label11" runat="server" Text="Periode :" __designer:wfdid="w2"></asp:Label></TD><TD align=left><asp:TextBox id="txtPeriode1" runat="server" Width="75px" __designer:wfdid="w53" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w54"></asp:ImageButton> <asp:Label id="lblTo" runat="server" Text="to" __designer:wfdid="w55"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" __designer:wfdid="w56" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w57"></asp:ImageButton>&nbsp;</TD></TR><TR><TD style="WIDTH: 66px" align=left><asp:Label id="Label19" runat="server" Text="Filter :" __designer:wfdid="w18"></asp:Label></TD><TD align=left><asp:DropDownList id="DDLfilter" runat="server" Width="100px" __designer:wfdid="w19" CssClass="inpText" AutoPostBack="True">
            <asp:ListItem Value="gendesc">Group</asp:ListItem>
            <asp:ListItem Value="fixcode">Kode</asp:ListItem>
            <asp:ListItem Value="fixflag">Status</asp:ListItem>
            <asp:ListItem Value="f.fixdesc">Description</asp:ListItem>
        </asp:DropDownList> &nbsp; <asp:TextBox id="txtFilter" runat="server" Width="150px" __designer:wfdid="w20" CssClass="inpText"></asp:TextBox> &nbsp;<asp:ImageButton id="BtnSearch" onclick="BtnSearch_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" __designer:wfdid="w21"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnViewAll" onclick="BtnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w22"></asp:ImageButton> </TD></TR><TR><TD align=left colSpan=2><asp:GridView id="GVFixedAsset" runat="server" Width="100%" __designer:wfdid="w68" OnPageIndexChanging="GVFixedAsset_PageIndexChanging" GridLines="None" DataKeyNames="cmpcode,fixoid" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="8" OnRowDataBound="GVFixedAsset_RowDataBound">
                <PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>
                <RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>
                <EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="fixoid" DataNavigateUrlFormatString="~/Accounting/trnFixAssetBalance.aspx?oid={0}" DataTextField="fixcode" HeaderText="Fixed Code">
                        <HeaderStyle CssClass="gvhdr"></HeaderStyle>
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="gendesc" HeaderText="Group">
                        <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="fixdate" HeaderText="Fixed Date" SortExpression="fixdate">
                        <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="fixdesc" HeaderText="Description">
                        <HeaderStyle CssClass="gvhdr"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="fixfirstvalue" HeaderText="First Value">
                        <HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="fixpresentvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
                        <HeaderStyle CssClass="gvhdr"></HeaderStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="fixflag" HeaderText="Status">
                        <HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>
                <PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
                <EmptyDataTemplate>
                    <asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label>
                </EmptyDataTemplate>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>
                <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
            </asp:GridView> <asp:Label id="lblViewInfo" runat="server" Text="Click button Find or View All to view data" __designer:wfdid="w24" CssClass="Important"></asp:Label><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w62" TargetControlID="txtPeriode1" PopupButtonID="btnPeriode1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w63" TargetControlID="txtPeriode2" PopupButtonID="btnPeriode2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w64" TargetControlID="txtPeriode1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" CultureName="en-US"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w65" TargetControlID="txtPeriode2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999" CultureName="en-US"></ajaxToolkit:MaskedEditExtender></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; </asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                                List of Fixed Asset Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 100%" vAlign=top colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" ActiveViewIndex="0" __designer:wfdid="w86"><asp:View id="View1" runat="server" __designer:wfdid="w87"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD id="TD4" align=left><asp:Label id="Label27" runat="server" Text="Asset Code" __designer:wfdid="w24"></asp:Label></TD><TD style="WIDTH: 206px" id="TD6" align=left><asp:TextBox id="FixCode" runat="server" Width="150px" __designer:wfdid="w16" MaxLength="20" CssClass="inpText"></asp:TextBox>&nbsp;<asp:Label id="Label1" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w17"></asp:Label></TD><TD id="TD3" align=left><asp:Label id="lblNo" runat="server" Text="Draft No." __designer:wfdid="w19" Visible="False"></asp:Label></TD><TD style="WIDTH: 196px" id="TD2" align=left><asp:Label id="fixmstoid" runat="server" __designer:wfdid="w20" Visible="False"></asp:Label></TD><TD id="TD5" align=left><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Bold="True" Text="Informasi" __designer:wfdid="w21" Visible="False"></asp:Label> <asp:Label id="Label30" runat="server" ForeColor="#585858" Font-Size="Small" Font-Bold="False" Text="|" __designer:wfdid="w22" Visible="False"></asp:Label></TD><TD id="TD7" align=left><asp:LinkButton id="lbkDetil" runat="server" __designer:wfdid="w23" CssClass="submenu" Visible="False">More Informasi </asp:LinkButton></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Text="Asset Date" __designer:wfdid="w32"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:TextBox id="fixDate" runat="server" Width="75px" __designer:wfdid="w18" CssClass="inpTextDisabled" Enabled="False" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w19"></asp:ImageButton> <asp:Label id="Label14" runat="server" Text="(MM/dd/yyyy)" __designer:wfdid="w20" CssClass="Important"></asp:Label></TD><TD align=left><asp:Label id="Label6" runat="server" Text="Description Asset" Width="106px" __designer:wfdid="w27"></asp:Label></TD><TD style="WIDTH: 196px" align=left><asp:TextBox id="fixdesc" runat="server" Width="150px" __designer:wfdid="w28" MaxLength="200" CssClass="inpText"></asp:TextBox> <asp:Label id="Label4" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w2"></asp:Label></TD><TD align=left><asp:Label id="Label28" runat="server" Text="Type asset" __designer:wfdid="w30"></asp:Label></TD><TD align=left><asp:DropDownList id="fixgroup" runat="server" Width="154px" __designer:wfdid="w31" CssClass="inpText" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label15" runat="server" Text="Currency" __designer:wfdid="w32"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:DropDownList id="CurroidDDL" runat="server" __designer:wfdid="w6" CssClass="inpTextDisabled" Enabled="False" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged"></asp:DropDownList></TD><TD align=left><asp:Label id="Label7" runat="server" Text="Harga Perolehan" Width="95px" __designer:wfdid="w36"></asp:Label> </TD><TD style="WIDTH: 196px" align=left><asp:TextBox id="fixfirstvalue" runat="server" __designer:wfdid="w37" MaxLength="16" CssClass="inpText" AutoPostBack="True">0</asp:TextBox> <asp:Label id="Label2x" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w38"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Nilai Buku" __designer:wfdid="w39"></asp:Label>&nbsp; </TD><TD align=left><asp:TextBox id="fixPresentValue" runat="server" Width="145px" __designer:wfdid="w40" MaxLength="16" CssClass="inpText" AutoPostBack="True">0</asp:TextBox><asp:Label id="Label40" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w41"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" ForeColor="DarkBlue" Text="Accum. Dep. Value" __designer:wfdid="w42"></asp:Label></TD><TD style="WIDTH: 206px" align=left><asp:TextBox id="accumDV" runat="server" Width="150px" __designer:wfdid="w43" CssClass="inpTextDisabled" Enabled="False" OnTextChanged="accumDV_TextChanged" ReadOnly="True">0</asp:TextBox></TD><TD align=left><asp:Label id="Label8" runat="server" Text="Depreciation" __designer:wfdid="w44"></asp:Label> </TD><TD style="WIDTH: 196px" align=left><asp:TextBox id="fixdepmonth" runat="server" Width="50px" __designer:wfdid="w45" MaxLength="2" CssClass="inpText" AutoPostBack="True">0</asp:TextBox> <asp:Label id="Label9" runat="server" ForeColor="Red" Font-Size="X-Small" Text="(month)" __designer:wfdid="w46"></asp:Label> <asp:Label id="Label33" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w47"></asp:Label></TD><TD align=left><asp:Label id="Label10" runat="server" Text="Dep. Value" Width="76px" __designer:wfdid="w48"></asp:Label></TD><TD align=left><asp:TextBox id="fixdepval" runat="server" Width="150px" __designer:wfdid="w49" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True">0</asp:TextBox> </TD></TR><TR><TD id="TD9" class="Label" align=left runat="server" Visible="true"><asp:Label id="Label42" runat="server" ForeColor="DarkBlue" Text="Aset Value Terakhir" Width="128px" __designer:wfdid="w50"></asp:Label></TD><TD style="WIDTH: 206px" id="TD8" align=left runat="server" Visible="true"><asp:TextBox id="fixLastAsset" runat="server" Width="150px" __designer:wfdid="w8" CssClass="inpText" AutoPostBack="True" OnTextChanged="fixLastAsset_TextChanged">0</asp:TextBox> <asp:Label id="Label16" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w9"></asp:Label></TD><TD id="Td1" align=left colSpan=4 runat="server"><asp:Label id="Label13" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w2"></asp:Label> <asp:Label id="Label12" runat="server" ForeColor="Red" Font-Bold="True" Text="Set Depreciation (month)  -1, If have no Depreciation !!" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label17" runat="server" Text="Akun Asset" __designer:wfdid="w54"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLassets" runat="server" Width="350px" __designer:wfdid="w55" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="lblPOST" runat="server" Text="lblPOST" __designer:wfdid="w56" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="CutofDate" runat="server" Width="150px" __designer:wfdid="w57" MaxLength="20" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label23" runat="server" Text="Accum. Dep" __designer:wfdid="w58"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLaccum" runat="server" Width="350px" __designer:wfdid="w59" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="payacctgoid" runat="server" Text="payacctgoid" __designer:wfdid="w60" Visible="False"></asp:Label></TD><TD align=left><asp:TextBox id="AccumVal" runat="server" Width="150px" __designer:wfdid="w61" MaxLength="20" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label22" runat="server" Text="Accum. Dep. Expense" __designer:wfdid="w62"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLadExpense" runat="server" Width="350px" __designer:wfdid="w63" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left></TD><TD align=left><asp:TextBox id="fixlocation" runat="server" Width="150px" __designer:wfdid="w64" MaxLength="20" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w65" Format="MM/dd/yyyy" PopupButtonID="btnDate" TargetControlID="fixDate"></ajaxToolkit:CalendarExtender></TD><TD align=left colSpan=3><asp:ImageButton id="GerenatedBtn" onclick="GerenatedBtn_Click" runat="server" ImageUrl="~/Images/gendetail.png" __designer:wfdid="w3"></asp:ImageButton> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w4" TargetControlID="fixLastAsset" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w5" TargetControlID="fixfirstvalue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender3" runat="server" __designer:wfdid="w6" TargetControlID="fixPresentValue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender4" runat="server" __designer:wfdid="w7" TargetControlID="fixdepmonth" ValidChars="-1234567890,."></ajaxToolkit:FilteredTextBoxExtender></TD><TD align=left></TD><TD align=left><asp:DropDownList id="DDLoutlet" runat="server" __designer:wfdid="w18" CssClass="inpTextDisabled" Visible="False" Enabled="False" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>&nbsp; <asp:TextBox id="fixperson" runat="server" Width="150px" __designer:wfdid="w67" MaxLength="20" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" __designer:wfdid="w783" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" DataKeyNames="fixperiod" CellPadding="4" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" PageSize="8" OnRowDataBound="GVFixedAssetdtl_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField>
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" DataFormatString="{0:#,##0.00}" HeaderText="Depreciation">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepaccum" DataFormatString="{0:#,##0.00}" HeaderText="Accumulation">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True" __designer:wfdid="w26">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True" __designer:wfdid="w27">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w28"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w29"></asp:Label></TD><TD align=left></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w66" TargetControlID="fixDate" ErrorTooltipEnabled="True" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError" CultureName="en-US" Mask="99/99/9999" MaskType="Date" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="ButtonSave" onclick="ButtonSave_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" __designer:wfdid="w4"></asp:ImageButton> <asp:ImageButton id="ButtonCancel" onclick="ButtonCancel_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" __designer:wfdid="w5"></asp:ImageButton>&nbsp;<asp:ImageButton id="BtnDelete" onclick="BtnDelete_Click" runat="server" ImageUrl="~/Images/btndelete.bmp" __designer:wfdid="w6"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w7"></asp:ImageButton></TD><TD align=left></TD><TD align=left><asp:TextBox id="fixother" runat="server" Width="150px" __designer:wfdid="w69" MaxLength="20" CssClass="inpText" Visible="False"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w77"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w78"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=left></TD><TD align=left></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR><TR></TR><TR></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image3" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt"> Form Fixed Asset Balance :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" Visible="False" CssClass="modalMsgBox"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>