<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trninitdpar.aspx.vb" Inherits="Accounting_trninitdpar" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text=".: DP AR Balance" CssClass="Title"></asp:Label>
            </th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: List of DP AR Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE width=750><TBODY><TR><TD id="TD6" align=left Visible="false">Periode&nbsp; </TD><TD id="TD1" align=left Visible="false">:</TD><TD id="TD3" align=left colSpan=4 Visible="false"><asp:TextBox id="dateAwal" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnImg1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp; -&nbsp; <asp:TextBox id="dateAkhir" runat="server" CssClass="inpText" Width="75px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnImg2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton>&nbsp;<ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" TargetControlID="dateAwal" PopupButtonID="btnImg1" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" TargetControlID="dateAkhir" PopupButtonID="btnImg2" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeAwal" runat="server" TargetControlID="dateAwal" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="meeAkhir" runat="server" TargetControlID="dateAkhir" MaskType="Date" Mask="99/99/9999" UserDateFormat="MonthDayYear"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left>Filter&nbsp;</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText"><asp:ListItem Value="dparno">DP AR Balance No.</asp:ListItem>
<asp:ListItem Value="custname">Customer</asp:ListItem>
<asp:ListItem Value="dparpaytype">Tipe Pembayaran</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=4><asp:DropDownList id="statuse" runat="server" CssClass="inpText" Width="119px"><asp:ListItem Value="ALL">ALL</asp:ListItem>
<asp:ListItem Value="IN PROCESS">IN PROCESS</asp:ListItem>
<asp:ListItem>POST</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp; </TD></TR><TR><TD align=left colSpan=6><asp:ImageButton id="btnFind" onclick="btnFind_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" AlternateText="Find"></asp:ImageButton> <asp:ImageButton id="btnViewAll" onclick="btnViewAll_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" AlternateText="View All"></asp:ImageButton> <asp:ImageButton id="btnExcel" onclick="btnExcel_Click" runat="server" ImageUrl="~/Images/toexcel.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD id="TD5" align=left colSpan=6 runat="server" Visible="false"><asp:Button id="btnCheckAll" runat="server" Font-Bold="True" CssClass="green" Text="SELECT ALL"></asp:Button> <asp:Button id="btnUncheckAll" runat="server" Font-Bold="True" CssClass="red" Text="SELECT NONE"></asp:Button> <asp:Button id="btnPosting" runat="server" Font-Bold="True" CssClass="orange" Text="POST SELECTED" Visible="False"></asp:Button> <asp:Button id="btnSearch" runat="server" Font-Bold="True" CssClass="orange" Text="FIND" Width="75px" Visible="False"></asp:Button> <asp:Button id="btnAll" runat="server" Font-Bold="True" CssClass="gray" Text="VIEW ALL" Width="75px" Visible="False"></asp:Button></TD></TR></TBODY></TABLE><asp:GridView id="gvMst" runat="server" BorderColor="#DEDFDE" Font-Size="X-Small" Width="100%" DataKeyNames="dparoid,cmpcode" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowSorting="True" EmptyDataText="No data in database.">
<PagerSettings PageButtonCount="15"></PagerSettings>

<RowStyle CssClass="gvrow"></RowStyle>
<Columns>
<asp:CommandField SelectText="Show" ShowSelectButton="True" Visible="False"></asp:CommandField>
<asp:TemplateField HeaderText="Posting" Visible="False"><ItemTemplate>
<asp:CheckBox id="cbPosting" runat="server" ToolTip="<%# GetIDCB() %>"></asp:CheckBox> 
</ItemTemplate>

<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="5px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:TemplateField>
<asp:HyperLinkField DataNavigateUrlFields="dparoid,cmpcode" DataNavigateUrlFormatString="trninitdpar.aspx?oid={0}&amp;cmpcode={1}" DataTextField="dparno" HeaderText="DP AR No.">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle CssClass="trninitdpap.aspx?oid={0}&amp;cmpcode={1}"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="outlet" HeaderText="Outlet" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle Width="80px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpardate" HeaderText="Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custname" HeaderText="Customer">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Wrap="True"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dparpaytype" HeaderText="Tipe Pembayaran">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="75px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dparamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White" Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dparstatus" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White" Width="75px"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="50px"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle CssClass="gvfooter" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvfooter" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" ForeColor="Red" Font-Size="X-Small" Text="Data not found !!"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnExcel"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 527px"><TBODY><TR><TD id="TD2" title="HEIGHT: 0px" align=left runat="server" Visible="false"><asp:Label id="trndparoid" runat="server" Visible="False"></asp:Label></TD><TD title="HEIGHT: 0px" align=left runat="server" Visible="false"></TD><TD id="TD4" title="HEIGHT: 0px" align=left colSpan=3 runat="server" Visible="false"><asp:DropDownList id="outlet" runat="server" CssClass="inpTextDisabled" Visible="False" Enabled="False"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 135px; HEIGHT: 23px" align=left>Date</TD><TD style="HEIGHT: 23px" align=left>:</TD><TD style="HEIGHT: 23px" align=left colSpan=3><asp:TextBox id="trndpardate" runat="server" CssClass="inpText" Width="75px"></asp:TextBox> <asp:ImageButton id="imbDPARDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:Label id="Label1" runat="server" CssClass="Important" Text="dd/MM/yyyy"></asp:Label></TD></TR><TR><TD style="WIDTH: 135px" align=left>DP No<asp:Label id="Label3" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparno" runat="server" CssClass="inpText" Width="125px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 135px" align=left>Customer<asp:Label id="Label7" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="custname" runat="server" CssClass="inpTextDisabled" Width="233px" Enabled="False" MaxLength="50"></asp:TextBox> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:ImageButton id="imbSearchCust" onclick="imbSearchCust_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Height="16px"></asp:ImageButton> <asp:ImageButton id="imbClearCust" onclick="imbClearCust_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 135px; HEIGHT: 4px" align=left><asp:Label id="Label9" runat="server" Text="Account DP" ToolTip="VAR_DPAR"></asp:Label></TD><TD style="HEIGHT: 4px" align=left>:</TD><TD style="HEIGHT: 4px" align=left colSpan=3><asp:DropDownList id="trndparacctgoid" runat="server" CssClass="inpText"></asp:DropDownList></TD></TR><TR><TD style="WIDTH: 135px; HEIGHT: 14px" align=left>Tipe Pembayaran</TD><TD style="HEIGHT: 14px" align=left>:</TD><TD style="HEIGHT: 14px" align=left colSpan=3><asp:DropDownList id="payreftype" runat="server" CssClass="inpText" Width="132px" AutoPostBack="True"><asp:ListItem Value="BKM">CASH</asp:ListItem>
<asp:ListItem Value="BBM">NONCASH</asp:ListItem>
<asp:ListItem Value="BGM">GIRO</asp:ListItem>
</asp:DropDownList></TD></TR><TR id="tr1" runat="server"><TD style="WIDTH: 135px; HEIGHT: 16px" align=left>Jatuh Tempo&nbsp;<asp:Label id="Label4" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 16px" align=left>:</TD><TD style="HEIGHT: 16px" align=left colSpan=3><asp:TextBox id="payduedate" runat="server" CssClass="inpText" Width="75px"></asp:TextBox> <asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lblDate" runat="server" CssClass="Important" Text="dd/MM/yyyy"></asp:Label></TD></TR><TR id="tr2" runat="server"><TD style="WIDTH: 135px; HEIGHT: 16px" align=left>Ref No.&nbsp;<asp:Label id="Label2" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 16px" align=left>:</TD><TD style="HEIGHT: 16px" align=left colSpan=3><asp:TextBox id="payrefno" runat="server" CssClass="inpText" Width="125px" MaxLength="20"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 135px; HEIGHT: 23px" align=left>Amount<asp:Label id="Label8" runat="server" CssClass="Important" Text="*"></asp:Label></TD><TD style="HEIGHT: 23px" align=left>:</TD><TD style="HEIGHT: 23px" align=left><asp:TextBox id="trndparamt" runat="server" CssClass="inpText" Width="125px" MaxLength="12" AutoPostBack="True" OnTextChanged="trndparamt_TextChanged"></asp:TextBox></TD><TD style="HEIGHT: 23px" align=left></TD><TD style="HEIGHT: 23px" align=left>&nbsp;</TD></TR><TR><TD style="WIDTH: 135px" align=left>Note</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparnote" runat="server" CssClass="inpText" Width="375px" Height="33px" MaxLength="200" TextMode="MultiLine"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 135px" align=left>Status</TD><TD align=left>:</TD><TD align=left colSpan=3><asp:TextBox id="trndparstatus" runat="server" CssClass="inpTextDisabled" Width="100px" ReadOnly="True"></asp:TextBox></TD></TR><TR><TD style="FONT-SIZE: x-small; WIDTH: 135px" align=left></TD><TD style="FONT-SIZE: x-small" align=left></TD><TD align=left></TD><TD align=left></TD><TD align=left></TD></TR><TR><TD align=left colSpan=5><asp:Label id="create" runat="server" Font-Bold="False"></asp:Label>&nbsp;<asp:Label id="update" runat="server" Font-Bold="False"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="ftbAmount" runat="server" TargetControlID="trndparamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:MaskedEditExtender id="meepayduedate" runat="server" TargetControlID="payduedate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="cepayduedate" runat="server" TargetControlID="payduedate" Format="MM/dd/yyyy" PopupButtonID="imbDueDate"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="meeDPARDate" runat="server" TargetControlID="trndpardate" Mask="99/99/9999" MaskType="Date" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:CalendarExtender id="ceDPARDate" runat="server" TargetControlID="trndpardate" Format="MM/dd/yyyy" PopupButtonID="imbDPARDate"></ajaxToolkit:CalendarExtender><BR /><TABLE><TBODY><TR><TD align=left colSpan=4><asp:Button id="btnSave" runat="server" Font-Bold="True" CssClass="green" Text="SAVE" Width="75px" Visible="False"></asp:Button><asp:Button id="btnCancel" runat="server" Font-Bold="True" CssClass="gray" Text="CANCEL" Width="75px" Visible="False"></asp:Button><asp:Button id="btnPosting2" runat="server" Font-Bold="True" CssClass="orange" Text="POSTING" Width="75px" Visible="False"></asp:Button><asp:Button id="btnDelete" runat="server" Font-Bold="True" CssClass="red" Text="DELETE" Width="75px" Visible="False"></asp:Button><BR /><asp:ImageButton id="btnSave1" onclick="ImageButton3_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel1" onclick="btnCancel1_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete1" onclick="btnDelete1_Click" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnposting1" onclick="btnposting1_Click" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD><TD style="FONT-WEIGHT: bold" align=left><asp:UpdateProgress id="UpdateProgress1" runat="server"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/corner.gif" />
                            <strong><span style="font-size: 9pt">:: Form DP AR Balance</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
                            <asp:UpdatePanel id="upListCust" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListCust" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListCust" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Customer"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListCust" runat="server" Width="100%" DefaultButton="btnFindListCust">Filter : <asp:DropDownList id="FilterDDLListCust" runat="server" CssClass="inpText" Width="100px">
                                        <asp:ListItem Value="custcode">Code</asp:ListItem>
                                        <asp:ListItem Value="custname">Name</asp:ListItem>
                                        <asp:ListItem Value="custaddr">Address</asp:ListItem>
                                    </asp:DropDownList> <asp:TextBox id="FilterTextListCust" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListCust" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListCust" onclick="btnAllListCust_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListCust" runat="server" ForeColor="#333333" Width="98%" OnSelectedIndexChanged="gvListCust_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="gvListCust_PageIndexChanging" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" DataKeyNames="custoid,custname">
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="custcode" HeaderText="Code">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custname" HeaderText="Name">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="custaddr" HeaderText="Address">
                                            <HeaderStyle CssClass="gvpopup" HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle CssClass="gvpopup" Font-Bold="True" ForeColor="White" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                </asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListCust" onclick="lkbCloseListCust_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListCust" runat="server" TargetControlID="btnHideListCust" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListCust" PopupDragHandleControlID="lblListCust">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListCust" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upPopUpMsg" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
                            </asp:UpdatePanel>
</asp:Content>