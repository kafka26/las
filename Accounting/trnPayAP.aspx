<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnPayAP.aspx.vb" Inherits="Accounting_trnPayAP" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
    <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center" colspan="2" style="height: 3px">
                <asp:Label ID="Label1" runat="server" ForeColor="#804000" Text=".: A/P Payment" CssClass="Title"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />
                            <strong><span style="font-size: 9pt">List of A/P Payment :.</span></strong>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:Panel id="Panel1" runat="server" DefaultButton="btnSearch"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=left colSpan=4></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label15" runat="server" Text="Filter"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="cashbankno">Cash/Bank No.</asp:ListItem>
<asp:ListItem Value="suppname">Supplier</asp:ListItem>
<asp:ListItem Value="cashbanknote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="150px"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:TextBox id="FilterPeriod1" runat="server" CssClass="inpText" Width="60px"></asp:TextBox> <asp:ImageButton id="imbDate1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="Label5" runat="server" Text="to"></asp:Label> <asp:TextBox id="FilterPeriod2" runat="server" CssClass="inpText" Width="60px"></asp:TextBox> <asp:ImageButton id="imbDate2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:Label id="lbldate1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbType" runat="server" Text="Payment Type"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLType" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">TRANSFER</asp:ListItem>
<asp:ListItem Value="BGK">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Value="BLK">DOWN PAYMENT</asp:ListItem>
    <asp:ListItem Value="BOK">BARTER</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="cbStatus" runat="server" Text="Status"></asp:CheckBox></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left colSpan=2><asp:DropDownList id="FilterDDLStatus" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="In Process">In Process</asp:ListItem>
<asp:ListItem Value="Post">Post</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left colSpan=2><ajaxToolkit:CalendarExtender id="CalendarExtender1" runat="server" PopupButtonID="imbDate1" Format="MM/dd/yyyy" TargetControlID="FilterPeriod1"></ajaxToolkit:CalendarExtender> <ajaxToolkit:CalendarExtender id="CalendarExtender2" runat="server" PopupButtonID="imbDate2" Format="MM/dd/yyyy" TargetControlID="FilterPeriod2"></ajaxToolkit:CalendarExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender3" runat="server" TargetControlID="FilterPeriod1" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender> <ajaxToolkit:MaskedEditExtender id="MaskedEditExtender4" runat="server" TargetControlID="FilterPeriod2" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:ImageButton id="btnSearch" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnPrint" runat="server" ImageUrl="~/Images/print.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:LinkButton id="lbInProcess" runat="server"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left colSpan=4></TD></TR><TR><TD class="Label" align=left colSpan=4><asp:GridView id="gvTRN" runat="server" ForeColor="#333333" Width="100%" AutoGenerateColumns="False" DataKeyNames="cashbankoid" CellPadding="4" GridLines="None" AllowPaging="True" PageSize="8" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="cashbankoid" DataNavigateUrlFormatString="~\Accounting\trnPayAP.aspx?oid={0}" DataTextField="cashbankno" HeaderText="Cash/Bank No." SortExpression="cashbankno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="cashbankdate" HeaderText="Payment Date" SortExpression="cashbankdate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supplier" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanktype" HeaderText="Payment Type" SortExpression="cashbanktype">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbankstatus" HeaderText="Status" SortExpression="cashbankstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cashbanknote" HeaderText="Note" SortExpression="cashbanknote">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Business Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="cbGVMst" runat="server" Checked='<%# eval("checkvalue") %>' />
            <asp:Label ID="lblOidGVMst" runat="server" Text='<%# eval("cashbankoid") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        <HeaderStyle CssClass="gvhdr" HorizontalAlign="Center" />
        <ItemStyle HorizontalAlign="Center" />
    </asp:TemplateField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" ForeColor="White" CssClass="gvhdr" Font-Bold="True"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="Data can't be found !"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data"></asp:Label></TD></TR><TR><TD class="Label" align=center colSpan=4><asp:UpdateProgress id="UpdateProgress21" runat="server" AssociatedUpdatePanelID="UpdatePanel2"><ProgressTemplate>
<SPAN></SPAN><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image21" runat="server" ImageUrl="~/Images/loading_animate.gif"></asp:Image><BR />Please Wait .....</SPAN><BR />
</ProgressTemplate>
</asp:UpdateProgress></TD></TR></TBODY></TABLE></asp:Panel> 
</contenttemplate>
                                <triggers>
<asp:PostBackTrigger ControlID="btnPrint"></asp:PostBackTrigger>
<asp:PostBackTrigger ControlID="gvTRN"></asp:PostBackTrigger>
</triggers>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
<asp:UpdatePanel id="UpdatePanel1" runat="server"><ContentTemplate>
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="A/P Payment Header" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u" runat="server" CssClass="Important" Text="New Data"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="periodacctg" runat="server" Visible="False"></asp:Label> <asp:Label id="suppoid" runat="server" Visible="False"></asp:Label> <asp:Label id="custoid" runat="server" Visible="False"></asp:Label> <asp:Label id="payaroid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="rate2oid" runat="server" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="cashbankoid" runat="server" Visible="False"></asp:Label> <asp:Label id="giroacctgoid" runat="server" Visible="False"></asp:Label> <asp:Label id="paytype" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label10" runat="server" Text="Business Unit" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:DropDownList id="DDLBusUnit" runat="server" CssClass="inpText" Visible="False" AutoPostBack="True"></asp:DropDownList> <asp:Label id="operNo" runat="server" Visible="False"></asp:Label> <asp:Label id="conaroid" runat="server" Visible="False"></asp:Label> <asp:Label id="reftype_ar" runat="server" Visible="False" __designer:wfdid="w8"></asp:Label></TD><TD class="Label" align=left><asp:Label id="dpapno" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="cashbankno2" runat="server" Visible="False"></asp:Label> <asp:Label id="cashbankoid2" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblTrnNo" runat="server" Text="Cash/Bank No."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankno" runat="server" CssClass="inpTextDisabled" Width="125px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label6" runat="server" Text="Payment Date"></asp:Label> <asp:Label id="Label19" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankdate" runat="server" CssClass="inpText" Width="80px"></asp:TextBox>&nbsp;<asp:ImageButton id="imnPaymentDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px"></asp:ImageButton>&nbsp;<asp:Label id="Label48" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label4" runat="server" Text="Supplier"></asp:Label> <asp:Label id="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="suppname" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSupp" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearSupp" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label28" runat="server" Text="Supplier Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="supptype" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label18" runat="server" Text="Payment Type"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="cashbanktype" runat="server" CssClass="inpText" Width="105px" AutoPostBack="True"><asp:ListItem Value="BKK">CASH</asp:ListItem>
<asp:ListItem Value="BBK">TRANSFER</asp:ListItem>
<asp:ListItem Value="BGK">GIRO/CHEQUE</asp:ListItem>
<asp:ListItem Value="BLK">DOWN PAYMENT</asp:ListItem>
<asp:ListItem Value="BOK">BARTER</asp:ListItem>
</asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label2" runat="server" Text="Payment Account"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="acctgoid" runat="server" CssClass="inpText" Width="95%" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label27" runat="server" Text="Currency"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" Width="105px" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label17" runat="server" Text="Total Payment"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label46" runat="server" Text="Monthly Rate to IDR"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="porawrate2toidr" runat="server" CssClass="inpText" Width="100px">0</asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label47" runat="server" Text="Monthly Rate To USD"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="porawrate2tousd" runat="server" CssClass="inpText" Width="100px">0</asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label21" runat="server" Text="Add Cost 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="addacctgoid1" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label40" runat="server" Text="Cost Amt 1"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="addacctgamt1" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label41" runat="server" Text="Add Cost 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="addacctgoid2" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label42" runat="server" Text="Cost Amt 2"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="addacctgamt2" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label43" runat="server" Text="Add Cost 3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="addacctgoid3" runat="server" CssClass="inpText" Width="205px" AutoPostBack="True"></asp:DropDownList></TD><TD class="Label" align=left><asp:Label id="Label44" runat="server" Text="Cost Amt 3"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="addacctgamt3" runat="server" CssClass="inpTextDisabled" Width="100px" AutoPostBack="True" Enabled="False" MaxLength="18"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label45" runat="server" Text="Total Pay + Cost"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payapnett" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblRefNo" runat="server" Text="Ref. No."></asp:Label> <asp:Label id="lblWarnRefNo" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankrefno" runat="server" CssClass="inpText" Width="160px" MaxLength="30"></asp:TextBox>&nbsp;<asp:ImageButton id="btnSearchDP" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearDP" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="lblDueDate" runat="server" Text="Due Date"></asp:Label> <asp:Label id="lblWarnDueDate" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center><asp:Label id="lblSeptDueDate" runat="server" Text=":"></asp:Label></TD><TD class="Label" align=left><asp:TextBox id="dpapamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox><asp:TextBox id="cashbankduedate" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox>&nbsp;<asp:ImageButton id="imbDueDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px"></asp:ImageButton> <asp:Label id="lblInfoDueDate" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label36" runat="server" Text="Date Take Giro"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanktakegiro" runat="server" CssClass="inpText" Width="80px" ToolTip="MM/dd/yyyy"></asp:TextBox> <asp:ImageButton id="imbDTG" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" Width="16px"></asp:ImageButton> <asp:Label id="lblInfoDTG" runat="server" CssClass="Important" Text="(MM/dd/yyyy)"></asp:Label></TD><TD class="Label" align=left><asp:Label id="Label9" runat="server" Text="Status"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbankstatus" runat="server" CssClass="inpTextDisabled" Width="80px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label8" runat="server" Text="Header Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="cashbanknote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:CalendarExtender id="ceDueDate" runat="server" PopupButtonID="imbDueDate" Format="MM/dd/yyyy" TargetControlID="cashbankduedate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDueDate" runat="server" TargetControlID="cashbankduedate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ceDTG" runat="server" PopupButtonID="imbDTG" Format="MM/dd/yyyy" TargetControlID="cashbanktakegiro"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="meeDTG" runat="server" TargetControlID="cashbanktakegiro" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt1" runat="server" TargetControlID="addacctgamt1" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt2" runat="server" TargetControlID="addacctgamt2" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbamt3" runat="server" TargetControlID="addacctgamt3" ValidChars="1234567890,.-"></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FteRate2toidr" runat="server" TargetControlID="porawrate2toidr" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="FteRate2tousd" runat="server" TargetControlID="porawrate2tousd" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:CalendarExtender id="CePaymentDate" runat="server" PopupButtonID="imnPaymentDate" Format="MM/dd/yyyy" TargetControlID="cashbankdate"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="MeePaymentDate" runat="server" TargetControlID="cashbankdate" UserDateFormat="MonthDayYear" MaskType="Date" Mask="99/99/9999"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label25" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="A/P Payment Detail" Font-Underline="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="i_u2" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="dtlseq" runat="server" Visible="False">1</asp:Label> <asp:Label id="payapoid" runat="server"></asp:Label> <asp:Label id="aprawmstoid" runat="server" Visible="False"></asp:Label> <asp:Label id="reftype" runat="server" Visible="False"></asp:Label> <asp:Label id="mrregno" runat="server" Visible="False"></asp:Label></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="apacctgoid" runat="server" Visible="False"></asp:Label> <asp:Label id="conapoid" runat="server"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label11" runat="server" Text="A/P No."></asp:Label> <asp:Label id="Label23" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawno" runat="server" CssClass="inpTextDisabled" Width="160px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchAP" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearAP" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle"></asp:ImageButton></TD><TD class="Label" align=left><asp:Label id="Label22" runat="server" Text="A/P Account"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="apaccount" runat="server" CssClass="inpTextDisabled" Width="200px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label29" runat="server" Text="A/P Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="aprawgrandtotal" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label30" runat="server" Text="Paid Amount"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="appaidamt" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label31" runat="server" Text="A/P Balance"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="apbalance" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label39" runat="server" Text="A/P Currency"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="apcurroid" runat="server" CssClass="inpTextDisabled" Width="105px" Enabled="False">
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label32" runat="server" Text="A/P Payment Amt."></asp:Label> <asp:Label id="Label14" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="lblAPPayment" runat="server" ForeColor="Black" Font-Bold="True"></asp:Label> <asp:TextBox id="payapamt" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="16"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label37" runat="server" Text="Payment Amt."></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="lblPayment" runat="server" ForeColor="Black" Font-Bold="True"></asp:Label> <asp:TextBox id="payapamtother" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False" MaxLength="12"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label35" runat="server" Text="Total Difference"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="totaldiff" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label33" runat="server" Text="Sub Total Payment"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="subtotalpayment" runat="server" CssClass="inpTextDisabled" Width="100px" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label13" runat="server" Text="Detail Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="payapnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:Label id="apratetoidr" runat="server" Visible="False"></asp:Label> <asp:Label id="apratetousd" runat="server" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbPayAmt" runat="server" TargetControlID="payapamt" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbPayAmtOther" runat="server" TargetControlID="payapamtother" ValidChars="1234567890,.">
    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Panel id="pnlOverPayment" runat="server" Width="100%" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:Label id="Label12" runat="server" ForeColor="Black" Font-Size="8pt" Font-Bold="True" Text="Difference Between Payment Amount and A/P Balance" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="i_u3" runat="server" Text="New Detail" Visible="False"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD style="WIDTH: 35%" class="Label" align=left><asp:Label id="diffseq" runat="server" Visible="False">1</asp:Label></TD><TD style="WIDTH: 14%" class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label26" runat="server" Text="Flag"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD style="WIDTH: 35%" class="Label" align=left><asp:DropDownList id="diffflag" runat="server" CssClass="inpTextDisabled" Width="125px" Enabled="False"><asp:ListItem Value="Lebih Bayar">Kelebihan Bayar</asp:ListItem>
<asp:ListItem Value="Kurang Bayar">Kekurangan Bayar</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 14%" class="Label" align=left><asp:Label id="Label16" runat="server" Text="Diff. Account"></asp:Label></TD><TD style="WIDTH: 1%" class="Label" align=center>:</TD><TD class="Label" align=left><asp:DropDownList id="diffacctgoid" runat="server" CssClass="inpText" Width="205px"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label20" runat="server" Text="A/P Diff. Amount"></asp:Label>&nbsp;<asp:Label id="Label34" runat="server" ForeColor="Red" Text="*"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:Label id="lblDiffPayment" runat="server" ForeColor="Black" Font-Bold="True"></asp:Label> <asp:TextBox id="diffamount" runat="server" CssClass="inpText" Width="100px" AutoPostBack="True" MaxLength="12" OnTextChanged="diffamount_TextChanged"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="Label38" runat="server" Text="Diff. Amount" Visible="False"></asp:Label></TD><TD class="Label" align=center></TD><TD class="Label" align=left><asp:TextBox id="diffamountother" runat="server" CssClass="inpText" Width="100px" Visible="False" MaxLength="12"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="Label24" runat="server" Text="Diff. Note"></asp:Label></TD><TD class="Label" align=center>:</TD><TD class="Label" align=left><asp:TextBox id="diffnote" runat="server" CssClass="inpText" Width="200px" MaxLength="100"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=center></TD><TD class="Label" align=left></TD></TR><TR><TD class="Label" align=left colSpan=6><ajaxToolkit:FilteredTextBoxExtender id="ftbDiff" runat="server" TargetControlID="diffamount" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender><ajaxToolkit:FilteredTextBoxExtender id="ftbDiffOther" runat="server" TargetControlID="diffamountother" ValidChars="1234567890,.">
    </ajaxToolkit:FilteredTextBoxExtender> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToListDiff" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClearDiff" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtlDiff" runat="server" Width="99%" GridLines="None" CellPadding="4" DataKeyNames="diffseq" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="diffaccount" HeaderText="Diff. Account">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="diffamount" HeaderText="A/P Diff. Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="diffnote" HeaderText="Diff. Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR></TBODY></TABLE></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:ImageButton id="btnAddToList" runat="server" ImageUrl="~/Images/addtolist.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnClear" runat="server" ImageUrl="~/Images/clear.png" ImageAlign="AbsMiddle"></asp:ImageButton> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=left colSpan=6></TD></TR><TR><TD class="Label" align=left colSpan=6><asp:GridView id="gvDtl" runat="server" Width="99%" GridLines="None" CellPadding="4" DataKeyNames="dtlseq" AutoGenerateColumns="False">
<RowStyle BackColor="#EFF3FB"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="aprawno" HeaderText="A/P No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="regno" HeaderText="MR No." Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apcurrcode" HeaderText="A/P Curr">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apaccount" HeaderText="A/P Account">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aprawgrandtotal" HeaderText="A/P Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="appaidamt" HeaderText="Paid Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apbalance" HeaderText="A/P Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payapamtpost" HeaderText="A/P Payment Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payapamtother" HeaderText="Payment Amt.">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payapnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="payapflag" HeaderText="Flag">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:CommandField DeleteText="X" ShowDeleteButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Font-Bold="True" Font-Size="Small" ForeColor="Red"></ItemStyle>
</asp:CommandField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left colSpan=6></TD></TR></TBODY></TABLE><TABLE><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left>Created By <asp:Label id="createuser" runat="server" Font-Bold="True"></asp:Label>&nbsp;On <asp:Label id="createtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True"></asp:Label> On <asp:Label id="updtime" runat="server" Font-Bold="True"></asp:Label></TD></TR><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" AlternateText="Save"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" AlternateText="Cancel"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" AlternateText="Delete"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" AlternateText="Show COA"></asp:ImageButton></TD></TR></TBODY></TABLE><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                </div>
                <div id="processMessage" class="processMessage">
                    <span style="font-weight: bold; font-size: 10pt; color: purple">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" /><br />
                        Please Wait .....</span><br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress> </DIV>
</ContentTemplate>
</asp:UpdatePanel> 
</ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt">Form A/P Payment :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel ID="upListSupp" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="suppcode">Code</asp:ListItem>
<asp:ListItem Value="suppname">Name</asp:ListItem>
<asp:ListItem Value="suppaddr">Address</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSupp" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSupp" runat="server" Width="99%" AutoGenerateColumns="False" DataKeyNames="suppoid,suppname,supptype,paytype,custoid" CellPadding="4" GridLines="None" AllowPaging="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="suppcode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppaddr" HeaderText="Address">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="custoid" HeaderText="custoid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlListSupp" PopupDragHandleControlID="lblListSupp">
            </ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="upListAP" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListAP" runat="server" CssClass="modalBox" Width="700px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListAP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of A/P"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListAP" runat="server" Width="100%" DefaultButton="btnFindListAP">Filter : <asp:DropDownList id="FilterDDLListAP" runat="server" CssClass="inpText"><asp:ListItem Value="apno">A/P No.</asp:ListItem>
<asp:ListItem Value="regno">MR No.</asp:ListItem>
<asp:ListItem Value="apnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListAP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListAP" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListAP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListAP" runat="server" Width="99%" AutoGenerateColumns="False" DataKeyNames="apoid,apno,apacctgoid,apaccount,apgrandtotal,appaidamt,curroid,apratetoidr,apratetousd,reftype,regno" CellPadding="4" GridLines="None" AllowPaging="True" PageSize="5" OnRowDataBound="gvListAP_RowDataBound">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="apno" HeaderText="A/P No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="regno" HeaderText="MR No." Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apdate" HeaderText="A/P Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apdatetakegiro" HeaderText="Date Take Giro">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="apgrandtotal" HeaderText="A/P Amount">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="appaidamt" HeaderText="A/P Payment">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="ap_balance" HeaderText="A/P Balance">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="apnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListAP" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListAP" runat="server" TargetControlID="btnHideListAP" PopupDragHandleControlID="lblListAP" PopupControlID="pnlListAP" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListAP" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
                <asp:UpdatePanel ID="upListDP" runat="server">
        <contenttemplate>
<asp:Panel id="pnlListDP" runat="server" CssClass="modalBox" Width="750px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListDP" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Down Payment A/P"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListDP" runat="server" Width="100%" DefaultButton="btnFindListDP">Filter : <asp:DropDownList id="FilterDDLListDP" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="dpapno">DP No.</asp:ListItem>
<asp:ListItem Value="a.acctgdesc">DP Account</asp:ListItem>
<asp:ListItem Value="dpapnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListDP" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListDP" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListDP" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListDP" runat="server" Width="99%" AutoGenerateColumns="False" DataKeyNames="dpapoid,dpapno,dpapacctgoid,dpapamt" CellPadding="4" GridLines="None" AllowPaging="True" OnRowDataBound="gvListDP_RowDataBound" PageSize="5">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="dpapno" HeaderText="DP No.">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapdate" HeaderText="DP Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="acctgdesc" HeaderText="DP Account">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapamt" HeaderText="Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dpapnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListDP" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListDP" runat="server" TargetControlID="btnHideListDP" PopupDragHandleControlID="lblListDP" PopupControlID="pnlListDP" Drag="True" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListDP" runat="server" Visible="False"></asp:Button> 
</ContentTemplate>
    </asp:UpdatePanel><asp:UpdatePanel ID="UPlistSI" runat="server"><contenttemplate>
<asp:Panel id="pnlListSI" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSI" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Sales Invoice"></asp:Label></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSI" runat="server" Width="100%" DefaultButton="btnFindListSI">Filter : <asp:DropDownList id="FilterDDLListSI" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="trnjualno">SI No.</asp:ListItem>
<asp:ListItem Value="trnjualnote">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSI" runat="server" CssClass="inpText" Width="150px"></asp:TextBox> <asp:ImageButton id="btnFindListSI" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" OnClick="btnFindListSI_Click"></asp:ImageButton> <asp:ImageButton id="btnAllListSI" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" OnClick="btnAllListSI_Click"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvListSI" runat="server" Width="99%" GridLines="None" CellPadding="4" DataKeyNames="trnjualmstoid,trnjualno,trnjualdate,trnjualnote,trnjualamtnetto,reftype" AutoGenerateColumns="False" AllowPaging="True" OnRowDataBound="gvListSI_RowDataBound" OnSelectedIndexChanged="gvListSI_SelectedIndexChanged" OnPageIndexChanging="gvListSI_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="trnjualno" HeaderText="SI No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualdate" HeaderText="SI Date">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualamtnetto" HeaderText="Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="trnjualnote" HeaderText="Note">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSI" onclick="lkbCloseListSI_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpeListSI" runat="server" TargetControlID="btnHideListSI" PopupDragHandleControlID="lblListSupp" PopupControlID="pnlListSI" Drag="True" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender><asp:Button id="btnHideListSI" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel id="upPopUpMsg" runat="server">
        <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TBODY><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlPopUpMsg" PopupDragHandleControlID="lblCaption" DropShadow="True"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button>
</contenttemplate>
    </asp:UpdatePanel></td>
        </tr>
        <tr>
            <td align="left">
    <asp:UpdatePanel ID="upCOAPosting" runat="server">
        <ContentTemplate>
<asp:Panel id="pnlCOAPosting" runat="server" CssClass="modalBox" Width="600px" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblCOAPosting" runat="server" Font-Size="Medium" Font-Bold="True" Text="COA - Posting Rate :"></asp:Label> <asp:DropDownList id="DDLRateType" runat="server" Font-Size="Medium" Font-Bold="True" CssClass="inpText" Width="125px" AutoPostBack="True">
                                <asp:ListItem Value="Default">Rate Default</asp:ListItem>
                                <asp:ListItem Value="IDR">Rate To IDR</asp:ListItem>
                                <asp:ListItem Value="USD">Rate To USD</asp:ListItem>
                            </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="gvCOAPosting" runat="server" ForeColor="#333333" Width="99%" GridLines="None" CellPadding="4" AutoGenerateColumns="False">
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="acctgcode" HeaderText="Account No.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdesc" HeaderText="Account Desc.">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Left" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgdebet" HeaderText="Debet">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="acctgcredit" HeaderText="Credit">
                                        <HeaderStyle CssClass="gvpopup" Font-Size="X-Small" HorizontalAlign="Right" Width="15%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="No Data Found"></asp:Label>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseCOAPosting" runat="server">[ CLOSE ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel><asp:Button id="btnHideCOAPosting" runat="server" Visible="False"></asp:Button><ajaxToolkit:ModalPopupExtender id="mpeCOAPosting" runat="server" TargetControlID="btnHideCOAPosting" BackgroundCssClass="modalBackground" Drag="True" PopupControlID="pnlCOAPosting" PopupDragHandleControlID="lblCOAPosting">
            </ajaxToolkit:ModalPopupExtender> 
</ContentTemplate>
    </asp:UpdatePanel></td>
        </tr>
    </table>
</asp:Content>