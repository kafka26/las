<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnFixAsset.aspx.vb" Inherits="Accounting_trnFixAsset" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="middleContent" Runat="Server">
<table id="tbRight" bgcolor="white" border="1" cellpadding="5" cellspacing="0" class="tabelhias"
        width="100%">
        <tr>
            <th align="left" class="header" valign="center">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="21px" Text=".: Fixed Asset"></asp:Label></th>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 100%">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="100%">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel2" runat="server">
                                <contenttemplate>
<asp:MultiView id="MultiView2" runat="server" ActiveViewIndex="0" __designer:wfdid="w287"><asp:View id="View3" runat="server" __designer:wfdid="w288"><asp:Panel id="Panel1" runat="server" __designer:wfdid="w289" DefaultButton="btnSearch"><asp:Label id="lblPosInformation" runat="server" ForeColor="Black" Font-Bold="True" Text="Information" __designer:wfdid="w290"></asp:Label>&nbsp;<asp:Label id="Label36" runat="server" ForeColor="Black" Font-Size="Small" Font-Bold="True" Text="|" __designer:wfdid="w291"></asp:Label> <asp:LinkButton id="lbkPostMoreInfo" onclick="lbkPostMoreInfo_Click" runat="server" __designer:wfdid="w292" CssClass="submenu">Monthly Posting</asp:LinkButton>&nbsp;<BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbPeriode" runat="server" Text="Periode" Width="80px" __designer:wfdid="w293"></asp:CheckBox></TD><TD align=left colSpan=4><asp:TextBox id="txtPeriode1" runat="server" Width="75px" __designer:wfdid="w294" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w295"></asp:ImageButton> <asp:Label id="Label160" runat="server" Text="to" __designer:wfdid="w296"></asp:Label> <asp:TextBox id="txtPeriode2" runat="server" Width="75px" __designer:wfdid="w297" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnPeriode2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w298"></asp:ImageButton> <asp:CheckBox id="cbDesc" runat="server" Text="Asset Desc" __designer:wfdid="w299" Visible="False"></asp:CheckBox></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:Label id="Label4" runat="server" Text="Filter" Width="40px" __designer:wfdid="w300"></asp:Label></TD><TD align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" __designer:wfdid="w301" CssClass="inpText"><asp:ListItem>ALL</asp:ListItem>
<asp:ListItem Value="f.fixcode">Code</asp:ListItem>
<asp:ListItem Value="g.gendesc">Group</asp:ListItem>
<asp:ListItem Value="f.fixdesc">Description</asp:ListItem>
<asp:ListItem Value="f.fixother">Note</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="txtFilter" runat="server" Width="200px" __designer:wfdid="w302" CssClass="inpText"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 115px" align=left><asp:CheckBox id="cbBlmPosting" runat="server" Text="Not Posted Yet" __designer:wfdid="w303"></asp:CheckBox></TD><TD align=left colSpan=4><asp:ImageButton id="btnSearch" onclick="btnSearch_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" __designer:wfdid="w304"></asp:ImageButton> <asp:ImageButton id="btnList" onclick="btnList_Click" runat="server" ImageUrl="~/Images/viewall.png" __designer:wfdid="w305"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 115px" align=left><ajaxToolkit:CalendarExtender id="ce2" runat="server" __designer:wfdid="w306" TargetControlID="txtPeriode1" Format="MM/dd/yyyy" PopupButtonID="btnPeriode1"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:CalendarExtender id="ce3" runat="server" __designer:wfdid="w307" TargetControlID="txtPeriode2" Format="MM/dd/yyyy" PopupButtonID="btnPeriode2"></ajaxToolkit:CalendarExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee10" runat="server" __designer:wfdid="w308" TargetControlID="txtPeriode1" UserDateFormat="MonthDayYear" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD><TD align=left><ajaxToolkit:MaskedEditExtender id="mee20" runat="server" __designer:wfdid="w309" TargetControlID="txtPeriode2" UserDateFormat="MonthDayYear" CultureName="en-US" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=5><asp:GridView id="GVFixedAsset" runat="server" Width="100%" __designer:wfdid="w310" OnSelectedIndexChanged="GVFixedAsset_SelectedIndexChanged" OnRowDataBound="GVFixedAsset_RowDataBound" PageSize="8" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="cmpcode,fixoid" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVFixedAsset_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixoid" HeaderText="fixoid" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="FixCode" HeaderText="FixCode">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="gendesc" HeaderText="Group">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdate" HeaderText="Fixed Date" SortExpression="fixdate">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixfirstvalue" HeaderText="First Value">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixpresentvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixother" HeaderText="Note">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView><asp:Label id="lblViewInfo" runat="server" Text="Click button Find or View All to view data" __designer:wfdid="w269" CssClass="Important"></asp:Label></TD></TR></TBODY></TABLE></asp:Panel></asp:View>&nbsp; <asp:View id="View4" runat="server" __designer:wfdid="w270"><asp:UpdatePanel id="UpdatePanelPostingDtl" runat="server" __designer:wfdid="w271"><ContentTemplate>
<asp:LinkButton id="lbkPostInfo" onclick="lbkPostInfo_Click" runat="server" __designer:wfdid="w272" CssClass="submenu">Information</asp:LinkButton> <asp:Label id="Label37" runat="server" ForeColor="Black" Font-Size="Small" Text="|" __designer:wfdid="w273"></asp:Label> <asp:Label id="Label26" runat="server" ForeColor="Black" Font-Bold="True" Text="Monthly Posting" __designer:wfdid="w274"></asp:Label><BR /><TABLE style="WIDTH: 100%"><TBODY><TR><TD align=left colSpan=6><asp:Label id="Label24" runat="server" Font-Bold="False" Text="Bulan" __designer:wfdid="w275"></asp:Label>&nbsp;<asp:DropDownList id="ddlMonth" runat="server" __designer:wfdid="w276" CssClass="inpText"><asp:ListItem Text="January" Value="01"></asp:ListItem>
<asp:ListItem Text="February" Value="02"></asp:ListItem>
<asp:ListItem Text="March" Value="03"></asp:ListItem>
<asp:ListItem Text="April" Value="04"></asp:ListItem>
<asp:ListItem Text="May" Value="05"></asp:ListItem>
<asp:ListItem Text="June" Value="06"></asp:ListItem>
<asp:ListItem Text="July" Value="07"></asp:ListItem>
<asp:ListItem Text="August" Value="08"></asp:ListItem>
<asp:ListItem Text="September" Value="09"></asp:ListItem>
<asp:ListItem Text="October" Value="10"></asp:ListItem>
<asp:ListItem Text="November" Value="11"></asp:ListItem>
<asp:ListItem Text="December" Value="12"></asp:ListItem>
</asp:DropDownList>&nbsp;<asp:Label id="Label25" runat="server" Font-Bold="False" Text="Tahun" __designer:wfdid="w277"></asp:Label>&nbsp;<asp:DropDownList id="ddlYear" runat="server" __designer:wfdid="w278" CssClass="inpText"></asp:DropDownList>&nbsp;<asp:ImageButton id="btnViewDtl" onclick="btnViewDtl_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" __designer:wfdid="w279"></asp:ImageButton> <asp:ImageButton id="btnCancelDtl" onclick="btnCancelDtl_Click" runat="server" ImageUrl="~/Images/btncancel.bmp" __designer:wfdid="w280"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPostingDtl" onclick="btnPostingDtl_Click" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w281"></asp:ImageButton> <asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w282"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w283"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVDtlMonth" runat="server" Width="100%" __designer:wfdid="w284" OnRowDataBound="GVDtlMonth_RowDataBound" PageSize="8" AllowSorting="True" CellPadding="4" AutoGenerateColumns="False" DataKeyNames="cmpcode,fixdtloid,fixoid,fixperiodvalue,fixcode,fixperiod,fixpostdate" AllowPaging="True" GridLines="None" OnPageIndexChanging="GVDtlMonth_PageIndexChanging">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:BoundField DataField="fixdtloid" HeaderText="ID" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="cmpcode" HeaderText="Branch" Visible="False">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdesc" HeaderText="Description">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" HeaderText="Dep Value">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixdtloid" HeaderText="fixdtloid" Visible="False"></asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel></asp:View></asp:MultiView> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <strong><span style="font-size: 9pt">
                                <asp:Image ID="Image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/List.gif" />List of Fixed Asset :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 723px; HEIGHT: 100%" vAlign=top colSpan=3 rowSpan=3><asp:MultiView id="MultiView1" runat="server" __designer:wfdid="w124" ActiveViewIndex="0"><asp:View id="View1" runat="server" __designer:wfdid="w125"><DIV style="HEIGHT: 100%"><TABLE style="WIDTH: 100%"><TBODY><TR><TD id="TD8" align=left runat="server" Visible="false"><asp:Label id="Outlet" runat="server" Text="Outlet" __designer:wfdid="w126" Visible="False"></asp:Label></TD><TD style="WIDTH: 318px" id="TD7" align=left runat="server" Visible="false"><asp:DropDownList id="DDLoutlet" runat="server" Width="170px" __designer:wfdid="w127" CssClass="inpTextDisabled" Visible="False" OnSelectedIndexChanged="DDLoutlet_SelectedIndexChanged" Enabled="False" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 96px" id="TD6" align=left runat="server" Visible="false"></TD><TD style="WIDTH: 449px" id="TD1" align=left runat="server" Visible="false">&nbsp;&nbsp;</TD><TD id="TD2" align=left runat="server" Visible="false">&nbsp;</TD><TD style="WIDTH: 332px" id="TD3" align=left runat="server" Visible="false">&nbsp;</TD></TR><TR><TD align=left><asp:Label id="Label27" runat="server" Text="Asset Code" __designer:wfdid="w128"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="FixCode" runat="server" Width="150px" __designer:wfdid="w129" CssClass="inpText" MaxLength="20"></asp:TextBox> <asp:Label id="Label18" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w130"></asp:Label>&nbsp;<asp:Label id="FAPmstoid" runat="server" Text="0" __designer:wfdid="w131" Visible="False"></asp:Label></TD><TD style="WIDTH: 96px" align=left><asp:Label id="lblFaMst" runat="server" Text="Akun FA Purchase" Width="104px" __designer:wfdid="w132"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="FAPMst" runat="server" Width="150px" __designer:wfdid="w133" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="BtnCari1" onclick="BtnCari1_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w134" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnHapus1" onclick="btnHapus1_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w135" Height="16px"></asp:ImageButton></TD><TD id="TD5" align=left runat="server" Visible="true"><asp:Label id="lblFaDtl" runat="server" Text="FA Purchase Detail" Width="112px" __designer:wfdid="w136"></asp:Label></TD><TD style="WIDTH: 332px" id="TD4" align=left runat="server" Visible="true"><asp:TextBox id="FAPDtl" runat="server" Width="150px" __designer:wfdid="w137" CssClass="inpTextDisabled" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="btnCari2" onclick="btnCari2_Click" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w138" Height="16px"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnHapus2" onclick="btnHapus2_Click" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" Width="16px" __designer:wfdid="w139" Height="16px"></asp:ImageButton></TD></TR><TR><TD align=left><asp:Label id="Label5" runat="server" Text="Asset Date" __designer:wfdid="w140"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="fixDate" runat="server" Width="75px" __designer:wfdid="w141" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w142"></asp:ImageButton> <asp:Label id="Label14" runat="server" Text="(MM/dd/yyyy)" __designer:wfdid="w143" CssClass="Important"></asp:Label></TD><TD style="WIDTH: 96px" align=left><asp:Label id="Label6" runat="server" Text="Description Asset" Width="104px" __designer:wfdid="w144"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixdesc" runat="server" Width="150px" __designer:wfdid="w145" CssClass="inpText" MaxLength="200"></asp:TextBox> <asp:Label id="Label15" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w146"></asp:Label></TD><TD align=left><asp:Label id="Label28" runat="server" Text="Type asset" __designer:wfdid="w147"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:DropDownList id="fixgroup" runat="server" Width="170px" __designer:wfdid="w148" CssClass="inpText" OnSelectedIndexChanged="fixgroup_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD align=left><asp:Label id="Label13" runat="server" Text="Currency" __designer:wfdid="w149"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:DropDownList id="CurrDDL" runat="server" __designer:wfdid="w150" CssClass="inpText" OnSelectedIndexChanged="CurrDDL_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></TD><TD style="WIDTH: 96px" align=left><asp:Label id="Label19" runat="server" Text="Daily Rate to IDR" Width="104px" __designer:wfdid="w151"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="RateToIDR" runat="server" Width="100px" __designer:wfdid="w152" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD><TD align=left><asp:Label id="Label41" runat="server" Text="Daily Rate To USD" __designer:wfdid="w153"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="RateToUSD" runat="server" Width="150px" __designer:wfdid="w154" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label240" runat="server" ForeColor="DarkBlue" Text="Accum.  Value" __designer:wfdid="w155"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="accumDV" runat="server" Width="150px" __designer:wfdid="w156" CssClass="inpTextDisabled" Enabled="False" OnTextChanged="accumDV_TextChanged" ReadOnly="True">0</asp:TextBox></TD><TD style="WIDTH: 96px" align=left><asp:Label id="Label20" runat="server" Text="Monthly Rate to IDR" Width="104px" __designer:wfdid="w157"></asp:Label></TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="Rate2ToIDR" runat="server" Width="100px" __designer:wfdid="w158" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD><TD align=left><asp:Label id="Label21" runat="server" Text="Monthly Rate To USD" Width="104px" __designer:wfdid="w159"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="Rate2ToUsd" runat="server" Width="150px" __designer:wfdid="w160" CssClass="inpTextDisabled" Enabled="False">0</asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label1" runat="server" Text="Note." __designer:wfdid="w161"></asp:Label></TD><TD style="WIDTH: 318px" align=left><asp:TextBox id="FAnote" runat="server" Width="180px" __designer:wfdid="w162" CssClass="inpText" MaxLength="50"></asp:TextBox> <asp:Label id="Label16" runat="server" ForeColor="Red" Text="*" __designer:wfdid="w163"></asp:Label></TD><TD style="WIDTH: 96px" align=left><asp:Label id="Label7" runat="server" Text="Harga Perolehan" __designer:wfdid="w164"></asp:Label> </TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixfirstvalue" runat="server" Width="150px" __designer:wfdid="w165" CssClass="inpText" AutoPostBack="True" MaxLength="16"></asp:TextBox> <asp:Label id="Label2x" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w166"></asp:Label></TD><TD align=left><asp:Label id="Label29" runat="server" Text="Nilai Buku" __designer:wfdid="w167"></asp:Label>&nbsp; </TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixPresentValue" runat="server" Width="150px" __designer:wfdid="w168" CssClass="inpTextDisabled" Enabled="False" AutoPostBack="True" MaxLength="16"></asp:TextBox> <asp:Label id="Label40" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w169"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="fixmstoid" runat="server" __designer:wfdid="w170" Visible="False"></asp:Label> <asp:Label id="lblCekPage" runat="server" __designer:wfdid="w171" Visible="False"></asp:Label> <asp:Label id="Rate2Oid" runat="server" __designer:wfdid="w172" Visible="False"></asp:Label><asp:Label id="RateOid" runat="server" __designer:wfdid="w173" Visible="False"></asp:Label></TD><TD style="WIDTH: 318px" align=left>&nbsp;<asp:Label id="FAPcmpcode" runat="server" __designer:wfdid="w174" Visible="False"></asp:Label> <asp:Label id="AssetType" runat="server" __designer:wfdid="w175" Visible="False"></asp:Label> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender1" runat="server" __designer:wfdid="w176" TargetControlID="fixfirstvalue" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> <ajaxToolkit:FilteredTextBoxExtender id="FilteredTextBoxExtender2" runat="server" __designer:wfdid="w177" TargetControlID="fixLastAsset" ValidChars="1234567890,."></ajaxToolkit:FilteredTextBoxExtender> </TD><TD style="WIDTH: 96px" align=left><asp:Label id="Label8" runat="server" Text="Depreciation" __designer:wfdid="w178"></asp:Label> </TD><TD style="WIDTH: 449px" align=left><asp:TextBox id="fixdepmonth" runat="server" Width="50px" __designer:wfdid="w179" CssClass="inpText" AutoPostBack="True" MaxLength="2">-1</asp:TextBox> <asp:Label id="Label9" runat="server" ForeColor="Red" Font-Size="X-Small" Text="(month)" __designer:wfdid="w180"></asp:Label> <asp:Label id="Label33" runat="server" ForeColor="Red" Font-Size="X-Small" Text="*" __designer:wfdid="w181"></asp:Label></TD><TD align=left><asp:Label id="Label10" runat="server" Text="Dep. Value" __designer:wfdid="w182"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixdepval" runat="server" Width="150px" __designer:wfdid="w183" CssClass="inpTextDisabled" Enabled="False" ReadOnly="True">0</asp:TextBox> </TD></TR><TR><TD class="Label" align=left></TD><TD style="WIDTH: 318px" align=left></TD><TD align=left colSpan=2>&nbsp;<asp:Label id="Label12" runat="server" ForeColor="Red" Font-Bold="True" Text="* Set Depreciation (month)  -1, If have no Depreciation !!" Width="280px" __designer:wfdid="w184"></asp:Label></TD><TD align=left><asp:Label id="Label42" runat="server" ForeColor="DarkBlue" Text="Aset Value Terakhir" Width="64px" __designer:wfdid="w185"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixLastAsset" runat="server" Width="150px" __designer:wfdid="w186" CssClass="inpText" AutoPostBack="True" OnTextChanged="fixLastAsset_TextChanged">0</asp:TextBox></TD></TR><TR><TD style="HEIGHT: 23px" align=left><asp:Label id="Label17" runat="server" Text="Akun Asset" __designer:wfdid="w187"></asp:Label></TD><TD style="HEIGHT: 23px" align=left colSpan=3><asp:DropDownList id="DDLassets" runat="server" Width="320px" __designer:wfdid="w188" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD style="HEIGHT: 23px" align=left><asp:Label id="lblPOST" runat="server" Text="lblPOST" __designer:wfdid="w189" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px; HEIGHT: 23px" align=left><asp:Label id="CutofDate" runat="server" __designer:wfdid="w190" Visible="False"></asp:Label></TD></TR><TR><TD align=left><asp:Label id="Label23" runat="server" Text="Accum. Dep" __designer:wfdid="w191"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLaccum" runat="server" Width="320px" __designer:wfdid="w192" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="payacctgoid" runat="server" Text="payacctgoid" __designer:wfdid="w193" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="AccumVal" runat="server" Width="150px" __designer:wfdid="w194" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left><asp:Label id="Label22" runat="server" Text="Accum.  Expense" __designer:wfdid="w195"></asp:Label></TD><TD align=left colSpan=3><asp:DropDownList id="DDLadExpense" runat="server" Width="320px" __designer:wfdid="w196" CssClass="inpTextDisabled" Enabled="False"></asp:DropDownList></TD><TD align=left><asp:Label id="lblNo" runat="server" Text="Draft No." __designer:wfdid="w197" Visible="False"></asp:Label> <asp:Label id="Label30" runat="server" ForeColor="#585858" Font-Size="Small" Font-Bold="False" Text="|" __designer:wfdid="w198" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixlocation" runat="server" Width="150px" __designer:wfdid="w199" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left><ajaxToolkit:CalendarExtender id="ce1" runat="server" __designer:wfdid="w200" PopupButtonID="btnDate" Format="MM/dd/yyyy" TargetControlID="fixDate"></ajaxToolkit:CalendarExtender></TD><TD align=left colSpan=3><asp:ImageButton id="btnGenerate" onclick="btnGenerate_Click1" runat="server" ImageUrl="~/Images/gendetail.png" __designer:wfdid="w201"></asp:ImageButton></TD><TD align=left><asp:Label id="Label3" runat="server" ForeColor="Black" Font-Bold="True" Text="Informasi" __designer:wfdid="w202" Visible="False"></asp:Label></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixperson" runat="server" Width="150px" __designer:wfdid="w203" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=6><asp:GridView id="GVFixedAssetdtl" runat="server" Width="100%" __designer:wfdid="w204" OnPageIndexChanging="GVFixedAssetdtl_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="fixperiod" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="8" OnRowDataBound="GVFixedAssetdtl_RowDataBound" OnSelectedIndexChanged="GVFixedAssetdtl_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last"></PagerSettings>

<RowStyle BackColor="#EFF3FB" Font-Size="X-Small"></RowStyle>

<EmptyDataRowStyle BackColor="#FFFFC0"></EmptyDataRowStyle>
<Columns>
<asp:CommandField>
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:CommandField>
<asp:BoundField DataField="fixperiod" HeaderText="Periode">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepvalue" DataFormatString="{0:#,##0.00}" HeaderText="Depreciation">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperiodvalue" DataFormatString="{0:#,##0.00}" HeaderText="Book Value">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="fixperioddepaccum" DataFormatString="{0:#,##0.00}" HeaderText="Accumulation">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" Font-Bold="True"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="fixflag" HeaderText="Status">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvhdr" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="lblEmpty" runat="server" CssClass="Important" Text="No Data Found !!"></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

<HeaderStyle BackColor="#507CD1" Font-Bold="True" Font-Size="X-Small" ForeColor="White"></HeaderStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 16px" align=left colSpan=4>Created By&nbsp;<asp:Label id="lblUser" runat="server" Font-Bold="True" __designer:wfdid="w205">User</asp:Label>&nbsp; On <asp:Label id="lblTime" runat="server" Font-Bold="True" __designer:wfdid="w206">Time</asp:Label>,&nbsp; Last Update On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w207"></asp:Label>&nbsp; By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w208"></asp:Label></TD><TD style="HEIGHT: 16px" align=left><asp:LinkButton id="lbkDetil" runat="server" __designer:wfdid="w209" CssClass="submenu" Visible="False">More Informasi </asp:LinkButton></TD><TD style="WIDTH: 332px; HEIGHT: 16px" align=left><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" __designer:wfdid="w210" TargetControlID="fixDate" MaskType="Date" Mask="99/99/9999" CultureName="en-US" UserDateFormat="MonthDayYear" OnInvalidCssClass="MaskedEditError" OnFocusCssClass="MaskedEditFocus" MessageValidatorTip="true" ErrorTooltipEnabled="True"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD align=left colSpan=4><asp:ImageButton id="btnSave" onclick="btnSave_Click" runat="server" ImageUrl="~/Images/btnsave.bmp" __designer:wfdid="w211"></asp:ImageButton> <asp:ImageButton id="btnCancel" onclick="btnCancel_Click1" runat="server" ImageUrl="~/Images/btncancel.bmp" __designer:wfdid="w212"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnDelete" onclick="btnDelete_Click1" runat="server" ImageUrl="~/Images/btndelete.bmp" __designer:wfdid="w213"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnPosting" onclick="btnPosting_Click1" runat="server" ImageUrl="~/Images/posting.png" __designer:wfdid="w214"></asp:ImageButton></TD><TD align=left></TD><TD style="WIDTH: 332px" align=left><asp:TextBox id="fixother" runat="server" Width="150px" __designer:wfdid="w215" CssClass="inpText" Visible="False" MaxLength="20"></asp:TextBox></TD></TR><TR><TD align=left colSpan=4><asp:UpdateProgress id="UpdateProgress5" runat="server" __designer:wfdid="w216"><ProgressTemplate>
<asp:Image id="Image1" runat="server" ImageUrl="~/Images/loadingbar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w217"></asp:Image>&nbsp;Please Wait ... 
</ProgressTemplate>
</asp:UpdateProgress></TD><TD align=left></TD><TD style="WIDTH: 332px" align=left></TD></TR></TBODY></TABLE></DIV></asp:View> </asp:MultiView> </TD></TR><TR></TR><TR></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="upListSupp" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" Width="650px" __designer:wfdid="w219" CssClass="modalBox" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Fixed Asset Purchase" __designer:wfdid="w220"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" __designer:wfdid="w221" DefaultButton="btnFindListSupp"><asp:Label id="Label11" runat="server" Text="Filter : " __designer:wfdid="w222"></asp:Label>&nbsp;<asp:DropDownList id="FilterFAPDDL" runat="server" __designer:wfdid="w223" CssClass="inpText"><asp:ListItem Value="BeliNo">No FA Purchase</asp:ListItem>
<asp:ListItem>suppname</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterFAP" runat="server" Width="150px" __designer:wfdid="w224" CssClass="inpText"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w225"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w226"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="GvFAPmst" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w227" OnPageIndexChanging="GvFAPmst_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="cmpcode,Mstoid,BeliNo,suppname,PayType " AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="5" OnSelectedIndexChanged="GvFAPmst_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cmpcode" HeaderText="Cmpcode" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="Mstoid" HeaderText="FAOid" Visible="False">
<HeaderStyle CssClass="gvhdr"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="BeliNo" HeaderText="FA Purchase No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppname" HeaderText="Supp Name" SortExpression="suppname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="PayType" HeaderText="Type" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup" Width="40%"></HeaderStyle>

<ItemStyle HorizontalAlign="Left" Width="40%"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server" __designer:wfdid="w228">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" __designer:wfdid="w229" TargetControlID="btnHideListSupp" PopupDragHandleControlID="lblListSupp" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" __designer:wfdid="w230" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel id="UpdatePanel3" runat="server">
                                <contenttemplate>
<asp:Panel id="pnlListReg" runat="server" Width="800px" __designer:wfdid="w232" CssClass="modalBox" Visible="False"><TABLE style="WIDTH: 100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListReg" runat="server" Font-Size="Medium" Font-Bold="True" Text="List Of Fixed Asset Purchase Detail" __designer:wfdid="w233" Font-Underline="False"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:Panel id="pnlFindListReg" runat="server" Width="100%" __designer:wfdid="w234" DefaultButton="btnFindListReg">Filter :&nbsp;<asp:DropDownList id="FilterDDLListReg" runat="server" Width="100px" __designer:wfdid="w235" CssClass="inpText"><asp:ListItem Value="ItemCode">Item Code</asp:ListItem>
<asp:ListItem Value="ItemDesc">Description</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListReg" runat="server" Width="150px" __designer:wfdid="w236" CssClass="inpText"></asp:TextBox> <asp:ImageButton id="btnFindListReg" onclick="btnFindListReg_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w237"></asp:ImageButton> <asp:ImageButton id="btnAllListReg" onclick="btnAllListReg_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w238"></asp:ImageButton></asp:Panel> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" align=center colSpan=3><asp:GridView id="GvFAPdtl" runat="server" ForeColor="#333333" Width="98%" __designer:wfdid="w239" OnPageIndexChanging="GvFAPdtl_PageIndexChanging" GridLines="None" AllowPaging="True" DataKeyNames="cmpcode,DtlOid,MstOid,BeliNo,ItemCode,ItemDesc,DtlAmtNetto" AutoGenerateColumns="False" CellPadding="4" AllowSorting="True" PageSize="5" OnSelectedIndexChanged="GvFAPdtl_SelectedIndexChanged">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="cmpcode" HeaderText="CmpCode" Visible="False"></asp:BoundField>
<asp:BoundField DataField="DtlOid" HeaderText="FAdtlOid" SortExpression="pono" Visible="False">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemCode" HeaderText="Code">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="ItemDesc" HeaderText="Description">
<HeaderStyle CssClass="gvpopup"></HeaderStyle>
</asp:BoundField>
<asp:BoundField DataField="DtlAmtNetto" HeaderText="Sub Total">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
                                    <asp:Label ID="Label114" runat="server" CssClass="Important" Text="No data found !!"></asp:Label>
                                
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView> </TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListReg" onclick="lkbCloseListReg_Click" runat="server" __designer:wfdid="w240">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListReg" runat="server" __designer:wfdid="w241" TargetControlID="btnHideListReg" PopupDragHandleControlID="lblListReg" Drag="True" BackgroundCssClass="modalBackground" PopupControlID="pnlListReg">
            </ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListReg" runat="server" ForeColor="Transparent" __designer:wfdid="w242" Visible="False"></asp:Button> 
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <asp:Image ID="Image2" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/Form.gif" />
                            <strong><span style="font-size: 9pt"> Form Fixed Asset :.</span></strong>&nbsp;
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
             
                <asp:UpdatePanel id="upPopUpMsg" runat="server">
                    <contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Visible="False"><TABLE><TR><TD style="BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Size="Small" Font-Bold="True"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px" colSpan=2></TD></TR><TR><TD><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" Width="24px" Height="24px"></asp:Image></TD><TD style="TEXT-ALIGN: left" class="Label"><asp:Label id="lblPopUpMsg" runat="server" ForeColor="Red"></asp:Label></TD></TR><TR><TD style="HEIGHT: 10px; TEXT-ALIGN: center" colSpan=2></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2>&nbsp;<asp:ImageButton id="imbOKPopUpMsg" onclick="imbOKPopUpMsg_Click" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle"></asp:ImageButton></TD></TR></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblCaption"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False"></asp:Button> 
</contenttemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>