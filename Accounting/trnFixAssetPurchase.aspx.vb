
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnFixAssetPurchase
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompnyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Private ws As DataTable
    Dim ckon As New Koneksi
    Dim cRate As New ClassRate
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
    Dim PPN As Integer = 10
#End Region

#Region "Function"

    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("tbldtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtloid", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifamstoid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnbelifadtlseq", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("itemfacode", Type.GetType("System.String"))
        dtlTable.Columns.Add("itemfadesc", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtlqty", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlprice", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtldisctype", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtldiscvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtldiscamt", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlamtnetto", Type.GetType("System.String"))
        dtlTable.Columns.Add("trnbelifadtldep", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("trnbelifadtllastvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("trnbelifadtlnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub


    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(fixDate.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Fixed Asset Date is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(fixDate.Text) <= CDate(CutofDate.Text) Then
            showMessage("Tanggal fix asset purchase Tidak boleh <= Tanggal CutofDate (" & CutofDate.Text & ") !!", 2)
            Return False
        End If
        'If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
        '    showMessage("Period 2 must be more than Period 1 !", 2)
        '    Return False
        'End If
        Return True
    End Function
#End Region

#Region "Prosedure"

    Public Sub binddata()
        Try
            Dim tgle As Date = CDate(txtPeriode1.Text)
            tgle = CDate(txtPeriode1.Text)
        Catch ex As Exception
            showMessage("Invalid format date !!", 2)
            Exit Sub
        End Try
        If CDate(txtPeriode1.Text) > CDate(txtPeriode2.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If

        sSql = "SELECT m.cmpcode,m.trnbelifamstoid,m.periodacctg,m.trnbelifano,m.trnbelifadate,m.trnbelifaref,m.suppoid,m.trnbelifatype,m.trnbelifapaytype,m.trnbelifaamt,m.trnbelifadisctype,m.trnbelifadiscvalue,m.trnbelifadiscamt,m.trnbelifamsttaxtype,m.trnbelifamsttaxpct,m.trnbelifamsttaxamt,m.trnbelifaamtnetto,m.trnbelifamstnote,m.trnbelifamststatus,s.suppoid,s.suppname,g.gencode,g.gendesc FROM QL_trnbelimst_fa m inner  JOIN QL_mstsupp s ON s.suppoid=m.suppoid AND s.cmpcode=m.cmpcode INNER JOIN QL_mstgen g ON g.genoid=m.trnbelifapaytype WHERE m.cmpcode='" & Session("CompnyCode") & "'" & IIf(DDLstatus.SelectedValue = "ALL", "", " AND m.trnbelifamststatus = '" & DDLstatus.SelectedValue & "'") & "AND " & DDLfilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'"

        If cbPeriode.Checked Then
            sSql += "AND m.trnbelifadate BETWEEN '" & CDate(txtPeriode1.Text) & "' AND '" & CDate(txtPeriode2.Text) & "' "
        End If
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnbelimst_fa")
        Session("tbldata") = objTable
        GVFixedAsset.DataSource = objTable
        GVFixedAsset.DataBind()
    End Sub

    Public Sub BindDataSupp()
        sSql = "select suppoid,suppcode,supptype,suppname,suppaddr,apacctgoid AS coa_hutang, acctgcode AS coa FROM QL_mstsupp s inner join ql_mstacctg a ON a.acctgoid=s.apacctgoid WHERE s.cmpcode = '" & Session("CompnyCode") & "' AND " & FilterDDLListSupp.SelectedValue & " LIKE '%" & Tchar(FilterTextListSupp.Text) & "%' ORDER BY suppname "

        Dim objTbl As DataTable = ckon.ambiltabel(sSql, "QL_mstsupp")
        Session("TblSupp") = objTbl
        gvSupplier.DataSource = objTbl
        gvSupplier.DataBind()

        'sSql = "SELECT sp.cmpcode, sp.suppoid, sp.suppcode, sp.suppname, sp.suppaddr,sp.activeflag  FROM ql_mstsupp sp WHERE cmpcode='" & Session("CompnyCode") & "' AND (sp.suppcode like '%" & Tchar(Supp.Text) & "%' OR sp.suppname like '%" & Tchar(Supp.Text) & "%')"

        'Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_mstsupp")
        'Session("TableSupp") = objTable
        'GVsupp.DataSource = objTable
        'GVsupp.DataBind()
    End Sub

    Public Sub BindDataItem()
        sSql = "select i.itemoid,i.itemcode,i.itemShortDescription FROM QL_mstitem i WHERE i.cmpcode='" & Session("CompnyCode") & "' AND " & FilterDDLListMat.SelectedValue & " LIKE '%" & Tchar(FilterTextListMat.Text) & "%' AND i.itemType ='NON'"

        'FillGV(GVmstitem, sSql, "ql_mstitem")
        Dim objTable As DataTable = ckon.ambiltabel(sSql, "ql_mstitem")
        Session("TableItem") = objTable
        GVmstitem.DataSource = objTable
        GVmstitem.DataBind()
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, True)
    End Sub

    Protected Sub initAllDDL()
        sSql = "select genoid,gendesc FROM QL_mstgen where gengroup = 'ASSETTYPE' AND cmpcode='" & Session("CompnyCode") & "'"
        FillDDL(fixgroup, sSql)

        sSql = "SELECT genoid, gendesc FROM ql_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='PAYMENT TYPE' ORDER BY gendesc"
        FillDDL(DDLpayterm, sSql)

        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & Session("CompnyCode") & "' AND activeflag='ACTIVE'"
        FillDDL(CurrDDL, sSql)
    End Sub

    Private Sub fillTextBox(ByVal sCmpcode As String, ByVal vjurnaloid As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT m.cmpcode,m.trnbelifamstoid,m.periodacctg,m.trnbelifano,m.trnbelifadate,m.trnbelifaref,m.suppoid,m.trnbelifatype,m.trnbelifapaytype,m.trnbelifaamt,m.trnbelifadisctype,m.trnbelifadiscvalue,m.trnbelifadiscamt,m.trnbelifamsttaxtype,m.trnbelifamsttaxpct,m.trnbelifamsttaxamt,m.trnbelifaamtnetto,m.trnbelifamstnote,m.trnbelifamststatus,m.createuser,m.createtime,m.upduser,m.updtime, s.suppoid,s.suppname,g.gencode,g.gendesc,m.curroid ,m.rateoid ,m.beliratetoidr ,m.beliratetousd ,m.rate2oid,m.belirate2toidr,m.belirate2tousd, isnull((select SUM(d.trnbelifadtldiscamt) trnbelifadtldiscamt FROM QL_trnbelidtl_fa d where d.trnbelifamstoid = " & vjurnaloid & " AND cmpcode = '" & sCmpcode & "'),0) AS trnbelifadtldiscamt FROM QL_trnbelimst_fa m INNER JOIN QL_mstgen g ON g.genoid=m.trnbelifapaytype INNER JOIN QL_mstsupp s ON s.suppoid=m.suppoid WHERE m.trnbelifamstoid = '" & vjurnaloid & "' AND m.cmpcode = '" & sCmpcode & "'"

        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If objRow.Length > 0 Then

            'DDLoutlet.SelectedValue = Trim(objRow(0)("cmpcode").ToString)
            famstoid.Text = Trim(objRow(0)("trnbelifamstoid").ToString)
            fixDate.Text = Format(objRow(0)("trnbelifadate"), "MM/dd/yyyy")
            fixgroup.SelectedValue = Trim(objRow(0)("trnbelifatype").ToString)
            FixAssetNo.Text = Trim(objRow(0)("trnbelifano").ToString)
            RefNo.Text = Trim(objRow(0)("trnbelifaref").ToString)
            lblSuppOid.Text = Trim(objRow(0)("suppoid").ToString)
            Supp.Text = Trim(objRow(0)("suppname").ToString)
            TotalDetail.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamt").ToString), 2)
            TotalDiscDetail.Text = ToMaskEdit(Trim(objRow(0)("trnbelifadtldiscamt").ToString), 2)
            DDLtype1.SelectedValue = Trim(objRow(0)("trnbelifadisctype").ToString)
            FilterType1.Text = Trim(objRow(0)("trnbelifadiscvalue").ToString)
            Tax.Text = ToMaskEdit(Trim(objRow(0)("trnbelifamsttaxamt").ToString), 2)
            DDLtax.SelectedValue = Trim(objRow(0)("trnbelifamsttaxtype").ToString)
            DiscHdr.Text = ToMaskEdit(Trim(objRow(0)("trnbelifadiscamt").ToString), 2)
            Netto.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamtnetto").ToString), 2)
            Status.Text = Trim(objRow(0)("trnbelifamststatus").ToString)
            Note1.Text = Trim(objRow(0)("trnbelifamstnote").ToString)
            CurrDDL.SelectedValue = Trim(objRow(0)("curroid").ToString)
            Rateoid.Text = Trim(objRow(0)("rateoid").ToString)
            Rate2oid.Text = Trim(objRow(0)("rate2oid").ToString)
            RateToIDR.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("beliratetoidr").ToString)), GetRoundValue(Trim(objRow(0)("beliratetoidr").ToString)))
            RateToUSD.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("beliratetousd").ToString)), GetRoundValue(Trim(objRow(0)("beliratetousd").ToString)))
            Rate2ToIDR.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("belirate2toidr").ToString)), GetRoundValue(Trim(objRow(0)("belirate2toidr").ToString)))
            Rate2ToUsd.Text = ToMaskEdit(ToDouble(Trim(objRow(0)("belirate2tousd").ToString)), GetRoundValue(Trim(objRow(0)("belirate2tousd").ToString)))

            If DDLtax.SelectedValue = "INCLUSIVE" Then
                DPP.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamtnetto").ToString) - Trim(objRow(0)("trnbelifamsttaxamt").ToString), 2)
            Else
                DPP.Text = ToMaskEdit(Trim(objRow(0)("trnbelifaamt").ToString) - Trim(objRow(0)("trnbelifadiscamt").ToString), 2)
            End If

            lblUser.Text = Trim(objRow(0)("createuser").ToString)
            lblTime.Text = Trim(objRow(0)("createtime").ToString)
            upduser.Text = Trim(objRow(0)("upduser").ToString)
            updtime.Text = Trim(objRow(0)("updtime").ToString)

            'sSql = "SELECT genoid FROM ql_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='PAYMENT TYPE' ORDER BY gendesc"
            'FillDDL(DDLpayterm, sSql)
            DDLpayterm.SelectedValue = Trim(objRow(0)("trnbelifapaytype").ToString)

            sSql = "select genother5 FROM QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "' and cmpcode='" & Session("CompnyCode") & "'"
            fixdepmonth.Text = GetStrData(sSql)

            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            CutofDate.Text = Format(CUTOFFDATE, "MM/dd/yyyy")
            If Status.Text = "Post" Then
                btnPosting.Visible = False
                btnDelete.Visible = False
                btnSave.Visible = False
            Else
                btnPosting.Visible = True
                btnDelete.Visible = True
                btnSave.Visible = True
            End If
            If Trim(objRow(0)("trnbelifamststatus").ToString) <> "Post" Then
                DDLoutlet.Enabled = False
                DDLoutlet.CssClass = "inpTextDisabled"
                btnSave.Visible = True
                btnDelete.Visible = True
                btnPosting.Visible = True
            Else
                DDLoutlet.Enabled = False
                DDLoutlet.CssClass = "inpTextDisabled"
                btnSave.Visible = False
                btnDelete.Visible = False
                btnPosting.Visible = False
            End If

            'data detail
            sqlSelect = "select cmpcode,trnbelifadtloid, trnbelifamstoid, trnbelifadtlseq, itemfacode, itemfadesc, trnbelifadtlqty, trnbelifadtlprice, trnbelifadtldisctype, trnbelifadtldiscvalue, trnbelifadtldiscamt, trnbelifadtlamtnetto, trnbelifadtldep, trnbelifadtllastvalue, trnbelifadtlnote, crtuser, crttime, upduser, updtime FROM QL_trnbelidtl_fa where trnbelifamstoid =" & vjurnaloid & " AND cmpcode='" & sCmpcode & "'"

            Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            Dim objDsDtl As New DataSet
            Dim objTableDtl As DataTable
            Dim objRowDtl() As DataRow

            mySqlDAdtl.Fill(objDsDtl)
            objTableDtl = objDsDtl.Tables(0)
            objRowDtl = objTableDtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Session("tbldtl") = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataSource = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataBind()
        End If
        mySqlConn.Close()
        TotalDtl()
        ClearDtl()
    End Sub

    Private Sub generatemstoid()
        famstoid.Text = GenerateID("QL_trnbelimst_fa", Session("CompnyCode"))
    End Sub

    Private Sub generetedtloid()
        fadtloid.Text = GenerateID("QL_trnbelidtl_fa", Session("CompnyCode"))
    End Sub

    Private Sub GenerateFixNo()
        ' Cashbank No
        If lblPOST.Text = "Post" Then
            Dim iCurID As Integer = 0
            Session("vNofa") = "PIFA-" & Format(CDate(fixDate.Text), "yyMM") & "-"
            sSql = "SELECT MAX(replace(trnbelifano,'" & Session("vNofa") & "','')) trnbelifano FROM QL_trnbelimst_fa WHERE trnbelifano LIKE '" & Session("vNofa") & "%' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDBNull(ckon.ambilscalar(sSql)) Then
                iCurID = ckon.ambilscalar(sSql) + 1
            Else
                iCurID = 1
            End If
            Session("sNo") = GenNumberString(Session("vNofa"), "", iCurID, DefCounter)
        Else
            Session("sNo") = famstoid.Text
        End If
    End Sub

    Protected Sub cbPeriode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtPeriode1.Enabled = cbPeriode.Checked : txtPeriode2.Enabled = cbPeriode.Checked
        btnPeriode1.Visible = cbPeriode.Checked : btnPeriode2.Visible = cbPeriode.Checked
        If cbPeriode.Checked Then
            txtPeriode1.CssClass = "inpText" : txtPeriode2.CssClass = "inpText"
        Else
            txtPeriode1.CssClass = "inpTextDisabled" : txtPeriode2.CssClass = "inpTextDisabled"
        End If
    End Sub

    Private Sub ReAmountDep()
        If ToMaskEdit(ToDecimal(Price.Text), 2) <> "" And ToMaskEdit(ToDecimal(Qty.Text), 0) <> "" Then
            subTotal.Text = ToMaskEdit(ToDecimal(Price.Text), 2) * ToMaskEdit(ToDecimal(Qty.Text), 0)

            If ToMaskEdit(ToDecimal(FilterType2.Text), 2) <> "" Then
                If DDLtype2.SelectedValue = "AMT" Then
                    FilterType2.MaxLength = 10
                    subTotal.Text = ToMaskEdit((ToDecimal(Price.Text) * ToDecimal(Qty.Text)) - ToDecimal(FilterType2.Text), 2)
                    discDtl.Text = ToMaskEdit(ToDecimal(FilterType2.Text), 2)

                ElseIf DDLtype2.SelectedValue = "PCT" Then
                    FilterType2.MaxLength = 3
                    Dim disc As Decimal = ToMaskEdit(ToDecimal(subTotal.Text) * (ToDecimal(FilterType2.Text) / 100), 2)
                    subTotal.Text -= ToMaskEdit(ToDecimal(disc), 2)
                    discDtl.Text = ToMaskEdit(ToDecimal(disc), 2)
                End If
            End If

            'If Session("oid") <> Nothing And Session("oid") <> "" Then
            'Else
            '    Session("tbldtl") = Nothing : GVFixedAssetdtl.DataSource = Nothing : GVFixedAssetdtl.DataBind()
            'End If
        End If
        subTotal.Text = ToMaskEdit(ToDecimal(subTotal.Text), 2)
    End Sub

    Private Sub ReAmountHdr()
        If (ToMaskEdit(ToDecimal(TotalDetail.Text), 2) <> "" And ToMaskEdit(ToDecimal(TotalDiscDetail.Text), 2) <> "") Or (ToMaskEdit(ToDecimal(TotalDetail.Text), 2) <> "0.00" And ToMaskEdit(ToDecimal(TotalDiscDetail.Text), 2) <> "0.00") Then
            If ToMaskEdit(ToDecimal(FilterType1.Text), 2) <> "" Then
                If DDLtype1.SelectedValue = "AMT" Then
                    FilterType1.MaxLength = 10
                    DiscHdr.Text = ToMaskEdit(ToDecimal(FilterType1.Text), 2)

                ElseIf DDLtype1.SelectedValue = "PCT" Then
                    FilterType1.MaxLength = 3
                    DiscHdr.Text = ToMaskEdit(ToDecimal(TotalDetail.Text) * (ToDecimal(FilterType1.Text) / 100), 2)
                    DiscHdr.Text = ToMaskEdit(ToDecimal(DiscHdr.Text), 2)
                End If
            End If

            If DDLtax.SelectedValue = "NON TAX" Then
                DPP.Text = ToMaskEdit(ToDecimal(TotalDetail.Text) - ToDecimal(DiscHdr.Text), 2)
                Tax.Text = 0
                Netto.Text = ToMaskEdit(ToDecimal(DPP.Text), 2)
            ElseIf DDLtax.SelectedValue = "INCLUSIVE" Then
                Dim totHdr As Decimal = ToMaskEdit(ToDecimal(TotalDetail.Text) - ToDecimal(DiscHdr.Text), 2)
                DPP.Text = ToMaskEdit(ToDecimal(totHdr) / ((100 + PPN) / 100), 2)
                Tax.Text = ToMaskEdit((PPN / (100 + PPN)) * ToDecimal(totHdr), 2)
                Netto.Text = ToMaskEdit(ToDecimal(totHdr), 2)
            ElseIf DDLtax.SelectedValue = "EXCLUSIVE" Then
                DPP.Text = ToMaskEdit(ToDecimal(TotalDetail.Text) - ToDecimal(DiscHdr.Text), 2)
                Tax.Text = ToMaskEdit(ToDecimal(DPP.Text) * (PPN / 100), 2)
                Netto.Text = ToMaskEdit(ToDecimal(DPP.Text) + ToDecimal(Tax.Text), 2)
            End If
            TotalDetail.Text = ToMaskEdit(ToDecimal(TotalDetail.Text), 2)
            TotalDiscDetail.Text = ToMaskEdit(ToDecimal(TotalDiscDetail.Text), 2)
            DPP.Text = ToMaskEdit(ToDecimal(DPP.Text), 2)
            Tax.Text = ToMaskEdit(ToDecimal(Tax.Text), 2)
            Netto.Text = ToMaskEdit(ToDecimal(Netto.Text), 2)
        End If
    End Sub

    Private Sub TotalDtl()
        Dim iCountrow As Integer = GVFixedAssetdtl.Rows.Count
        Dim iGtotal As Double = 0
        Dim iGdisc As Double = 0
        Dim dt_temp As New DataTable
        dt_temp = Session("dtlTable")
        For i As Integer = 0 To iCountrow - 1
            iGtotal += ToDecimal(GVFixedAssetdtl.Rows(i).Cells(7).Text)
            iGdisc += ToDecimal(GVFixedAssetdtl.Rows(i).Cells(6).Text)
            'dt_temp.Rows(i).BeginEdit()
            'dt_temp.Rows(i).Item(0) = i + 1
            'dt_temp.Rows(i).EndEdit()
        Next
        'Session("dtlTable") = dt_temp
        'GVFixedAssetdtl.DataSource = dt_temp
        'GVFixedAssetdtl.DataBind()
        TotalDetail.Text = ToMaskEdit(ToDecimal(iGtotal), 2)
        TotalDiscDetail.Text = ToMaskEdit(ToDecimal(iGdisc), 2)
        DPP.Text = ToMaskEdit(ToDecimal(TotalDetail.Text), 2)
        Netto.Text = ToMaskEdit(ToDecimal(TotalDetail.Text), 2)
        ReAmountHdr()
    End Sub

    Protected Sub ClearDtl()
        If Session("tbldtl") Is Nothing = False Then
            Dim objTable As DataTable = Session("tbldtl")
            If objTable.Rows.Count > 0 Then
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        Code.Text = ""
        Desc.Text = ""
        Qty.Text = ""
        Price.Text = ""
        FilterType2.Text = ""
        subTotal.Text = ""
        FinalValue.Text = ""
        discDtl.Text = ""
        Note2.Text = ""
        AddToList.Text = "Add To List"
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        fixgroup.Enabled = bVal : fixgroup.CssClass = sCss
        BtnCariSupp.Visible = bVal : btnHapusSupp.Visible = bVal
        DDLtax.Enabled = bVal : DDLtax.CssClass = sCss
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim CompnyCode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = CompnyCode
            Session("sCmpcode") = CompnyCode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnFixAssetPurchase.aspx")
        End If
        If checkPagePermission("~\Accounting\trnFixAssetPurchase.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If

        Page.Title = CompnyName & " - Fix Asset Purchase "
        Session("oid") = Request.QueryString("oid")

        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        'btnPostingDtl.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST all this data?');")

        If Not IsPostBack Then
            'binddata()
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            txtPeriode1.Text = Format(GetServerTime, "MM/01/yyyy")
            txtPeriode2.Text = Format(GetServerTime, "MM/dd/yyyy")
            lblViewInfo.Visible = True
            fixgroup_SelectedIndexChanged(Nothing, Nothing)
            initAllDDL()
            AddToList.Text = "Add To List"
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(Session("sCmpcode"), Session("oid"))
                generetedtloid()
                lblPOST.Text = "In Process"
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 1
            Else
                generatemstoid()
                generetedtloid()
                GenerateFixNo()
                CutofDate.Text = Format(CDate(CUTOFFDATE), "MM/dd/yyyy")
                btnDelete.Visible = False
                btnPosting.Visible = True
                fixdepmonth.Text = -1
                fixdepmonth.CssClass = "inpTextDisabled"
                fixdepmonth.Enabled = False
                fixgroup_SelectedIndexChanged(Nothing, Nothing)
                fixDate.Text = Format(GetServerTime, "MM/dd/yyyy")
                FixAssetNo.Text = Session("sNo")
                upduser.Text = "-"
                updtime.Text = "-"
                CurrDDL_SelectedIndexChanged(Nothing, Nothing)
                lblUser.Text = Session("UserID")
                lblTime.Text = GetServerTime()
                Status.Text = "In Process"
                lblPOST.Text = "In Process"
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As DataTable
            dt = Session("tbldtl")
            GVFixedAssetdtl.DataSource = dt
            GVFixedAssetdtl.DataBind()
        End If
		
        If Status.Text = "Post" Or Status.Text = "Closed" Then
            btnshowCOA.Visible = False
            btnPosting.Visible = False
            btnDelete.Visible = False
            btnSave.Visible = False
        Else
            btnshowCOA.Visible = False
            btnPosting.Visible = True
            btnDelete.Visible = True
            btnSave.Visible = True
        End If
    End Sub

    Protected Sub GVFixedAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("trnFixAssetPurchase.aspx?cmpcode=" & GVFixedAsset.SelectedDataKey("cmpcode").ToString & "&oid=" & GVFixedAsset.SelectedDataKey("trnbelifamstoid").ToString & "")
    End Sub

    Protected Sub GVFixedAsset_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAsset.PageIndex = e.NewPageIndex
        binddata()
    End Sub

    Protected Sub GVFixedAsset_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = Format(CDate(e.Row.Cells(3).Text), "MM/dd/yyyy")
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
            e.Row.Cells(6).Text = ToMaskEdit(e.Row.Cells(6).Text, 2)
            e.Row.Cells(7).Text = ToMaskEdit(e.Row.Cells(7).Text, 2)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblItemOid.Text = GVFixedAssetdtl.SelectedDataKey.Item(1).ToString
        Code.Text = GVFixedAssetdtl.SelectedDataKey.Item(2).ToString
        Desc.Text = GVFixedAssetdtl.SelectedDataKey.Item(3).ToString
        Qty.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(4).ToString)
        Price.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(5).ToString)
        discDtl.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(6).ToString)
        subTotal.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(7).ToString)
        fixdepmonth.Text = GVFixedAssetdtl.SelectedDataKey.Item(8).ToString
        FinalValue.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(9).ToString)
        Note2.Text = GVFixedAssetdtl.SelectedDataKey.Item(10).ToString
        FilterType2.Text = NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(11).ToString)
        DDLtype2.SelectedValue = GVFixedAssetdtl.SelectedDataKey.Item(12).ToString
        'TotalDetail.Text -= NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(7).ToString)
        'TotalDiscDetail.Text -= NewMaskEdit(GVFixedAssetdtl.SelectedDataKey.Item(6).ToString)
        AddToList.Text = "EDIT"
    End Sub

    Protected Sub GVFixedAssetdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAssetdtl.PageIndex = e.NewPageIndex
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
    End Sub

    Protected Sub GVDtlMonth_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
        End If
    End Sub

    Protected Sub fixgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        sSql = "SELECT genother5 FROM QL_mstgen WHERE gengroup = 'ASSETTYPE' AND genoid = " & fixgroup.SelectedValue & ""
        fixdepmonth.Text = GetStrData(sSql)
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkPostMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 1
    End Sub

    Protected Sub lbkPostInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 0
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("tbldata") = Nothing
        GVFixedAsset.PageIndex = 0
        Session("SearchFixAsset") = sql_temp
        lblViewInfo.Visible = False
        binddata()
    End Sub

    Protected Sub btnList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'cbPeriode.Checked = False
        'cbPeriode_CheckedChanged(Nothing, Nothing)
        txtFilter.Text = ""
        DDLstatus.SelectedValue = "ALL"
        lblViewInfo.Visible = False
        binddata()
        txtPeriode1.Text = Format(GetServerTime, "MM/01/yyyy")
        txtPeriode2.Text = Format(GetServerTime, "MM/dd/yyyy")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        ' Validasi
        Dim CompnyCode As String = Session("CompnyCode")
        Dim sMsg As String = ""
        If Not IsValidDate(fixDate.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End If
        If CDate(fixDate.Text) <= CDate(CutofDate.Text) Then
            showMessage("- Tanggal fix asset purchase Tidak boleh Kurang dari CutoffDate (" & CutofDate.Text & ") !!<BR>", 2)
            Exit Sub
        End If
        If Supp.Text.Trim = "" Then
            sMsg &= "- Supplier Kosong !!<BR>"
        End If
        If RefNo.Text.Trim = "" Then
            sMsg &= "- Ref No Kosong !!<BR>"
        End If
        If DDLtype1.SelectedValue = "PCT" Then
            If FilterType1.Text > 100 Then
                sMsg &= "- Discount PCT Header tidak boleh > 100 !!<BR>"
            End If
        ElseIf DDLtype1.SelectedValue = "AMT" Then
            If ToDecimal(FilterType1.Text) > ToDecimal(TotalDetail.Text) Then
                sMsg &= "- Discount AMT Header tidak boleh > dari Total Detail !!<BR>"
            End If
        End If
        If TotalDetail.Text.Length > 16 Then
            sMsg &= "- maximum digit total detail tidak boleh > 16 digit !!<BR>"
        End If
        If DPP.Text.Length > 16 Then
            sMsg &= "- maximum digit DPP tidak boleh > 16 digit !!<BR>"
        End If
        If Netto.Text.Length > 16 Then
            sMsg &= "- maximum digit Netto tidak boleh > 16 digit !!<BR>"
        End If
        If ToDecimal(Netto.Text) <= 0 Then
            sMsg &= "- Netto tidak boleh <= 0 !!<BR>"
        End If
        If Note1.Text.Length > 100 Then
            sMsg &= "- Karakter Note Tidak Boleh > 100 !!<BR>"
        End If
        If Note2.Text.Length > 100 Then
            sMsg &= "- Karakter Detail Note Tidak Boleh > 100 !!<BR>"
        End If
        sSql = "SELECT COUNT(-1) FROM ql_trnbelimst_fa WHERE cmpcode='" & CompnyCode & "' AND trnbelifaref='" & Tchar(RefNo.Text) & "'"
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            sSql &= " AND trnbelifamstoid<>" & Session("oid")
        End If
        If GetStrData(sSql) > 0 Then
            sMsg &= "- Kode ini sudah digunakan oleh fixed aset yang lain!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "In Process"
            Exit Sub
        End If

        If lblPOST.Text = "Post" Then
            GenerateFixNo()
        End If
        ' Generate oid
        If Session("oid") = Nothing Or Session("oid") = "" Then
            famstoid.Text = GenerateID("QL_trnbelimst_fa", CompnyCode)
        End If
        Dim DtlOid As String = GenerateID("QL_trnbelidtl_fa", CompnyCode)
        Dim MstGLOid As Integer = GenerateID("QL_trnglmst", CompnyCode)
        Dim DtlGLOid As Integer = GenerateID("QL_trngldtl", CompnyCode)
        Dim MstConakOid As Integer = GenerateID("QL_conap", CompnyCode)
        Dim acctgPPN As Integer = GetAcctgOID(GetVarInterface("VAR_PPN_IN", CompnyCode), CompnyCode)
        Dim iSeq As Integer = 1

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try

            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT INTO QL_trnbelimst_fa (cmpcode, trnbelifamstoid, periodacctg, trnbelifano, trnbelifadate, trnbelifatype, trnbelifaref, suppoid, pomstoid, trnbelifapaytype, curroid, Rateoid, beliratetoidr, beliratetousd, Rate2oid, belirate2toidr, belirate2tousd, trnbelifaamt, trnbelifaamtidr, trnbelifaamtusd, trnbelifadisctype, trnbelifadiscvalue, trnbelifadiscamt, trnbelifadiscamtidr, trnbelifadiscamtusd, trnbelifamsttaxtype, trnbelifamsttaxpct, trnbelifamsttaxamt, trnbelifamsttaxamtidr, trnbelifamsttaxamtusd, trnbelifaamtnetto, trnbelifaamtnettoidr, trnbelifaamtnettousd, trnbelifamstnote, trnbelifamststatus, createuser, createtime, upduser, updtime) VALUES ('" & CompnyCode & "'," & famstoid.Text & ",'" & GetDateToPeriodAcctg(CDate(fixDate.Text)) & "','" & Tchar(FixAssetNo.Text.Trim) & "','" & CDate(fixDate.Text) & "'," & fixgroup.SelectedValue & ",'" & Tchar(RefNo.Text.Trim) & "'," & lblSuppOid.Text & ",0,'" & DDLpayterm.SelectedValue & "','" & CurrDDL.SelectedValue & "','" & Rateoid.Text & "','" & ToDecimal(RateToIDR.Text) & "','" & ToDecimal(RateToUSD.Text) & "','" & Rate2oid.Text & "','" & ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(Rate2ToUsd.Text) & "','" & ToDecimal(TotalDetail.Text) & "','" & ToDecimal(TotalDetail.Text) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(TotalDetail.Text) * ToDecimal(Rate2ToUsd.Text) & "','" & DDLtype1.SelectedValue & "','" & ToDecimal(FilterType1.Text) & "','" & ToDecimal(DiscHdr.Text) & "','" & ToDecimal(DiscHdr.Text) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(DiscHdr.Text) * ToDecimal(Rate2ToUsd.Text) & "','" & DDLtax.SelectedValue & "','10','" & ToDecimal(Tax.Text) & "','" & ToDecimal(Tax.Text) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(Tax.Text) * ToDecimal(Rate2ToUsd.Text) & "','" & ToDecimal(Netto.Text) & "','" & ToDecimal(Netto.Text) * ToDecimal(Rate2ToIDR.Text) & "','" & ToDecimal(Netto.Text) * ToDecimal(Rate2ToUsd.Text) & "','" & Tchar(Note1.Text) & "','" & lblPOST.Text & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'Update mst lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & famstoid.Text & " WHERE tablename='QL_trnbelimst_fa' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

            Else
                sSql = "UPDATE QL_trnbelimst_fa SET periodacctg='" & GetDateToPeriodAcctg(CDate(fixDate.Text)) & "' , trnbelifadate='" & CDate(fixDate.Text) & "', trnbelifaref='" & Tchar(RefNo.Text.Trim) & "', suppoid=" & lblSuppOid.Text & ", trnbelifatype='" & fixgroup.SelectedValue & "', trnbelifapaytype='" & DDLpayterm.SelectedValue & "',curroid ='" & CurrDDL.SelectedValue & "', Rateoid ='" & Rateoid.Text & "', beliratetoidr='" & ToDecimal(RateToIDR.Text) & "', beliratetousd = '" & ToDecimal(RateToUSD.Text) & "', Rate2oid ='" & Rate2oid.Text & "', belirate2toidr = '" & ToDecimal(Rate2ToIDR.Text) & "', belirate2tousd = '" & ToDecimal(Rate2ToUsd.Text) & "', trnbelifaamt=" & ToDecimal(TotalDetail.Text) & ",trnbelifaamtidr ='" & ToDecimal(TotalDetail.Text) * ToDecimal(Rate2ToIDR.Text) & "', trnbelifaamtusd = '" & ToDecimal(TotalDetail.Text) * ToDecimal(Rate2ToUsd.Text) & "', trnbelifadisctype='" & DDLtype1.SelectedValue & "', trnbelifadiscvalue=" & ToDecimal(FilterType1.Text) & ", trnbelifadiscamt=" & ToDecimal(DiscHdr.Text) & ", trnbelifadiscamtidr = '" & ToDecimal(DiscHdr.Text) * ToDecimal(Rate2ToIDR.Text) & "', trnbelifadiscamtusd ='" & ToDecimal(DiscHdr.Text) * ToDecimal(Rate2ToUsd.Text) & "',trnbelifamsttaxtype='" & DDLtax.SelectedValue & "', trnbelifamsttaxamt=" & ToDecimal(Tax.Text) & ",trnbelifamsttaxamtidr ='" & ToDecimal(Tax.Text) * ToDecimal(Rate2ToIDR.Text) & "', trnbelifamsttaxamtusd ='" & ToDecimal(Tax.Text) * ToDecimal(Rate2ToUsd.Text) & "', trnbelifaamtnetto=" & ToDecimal(Netto.Text) & ",  trnbelifaamtnettoidr ='" & ToDecimal(Netto.Text) * ToDecimal(Rate2ToIDR.Text) & "', trnbelifaamtnettousd ='" & ToDecimal(Netto.Text) * ToDecimal(Rate2ToUsd.Text) & "', trnbelifamstnote ='" & Tchar(Note1.Text) & "', trnbelifamststatus='" & lblPOST.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE trnbelifamstoid = " & famstoid.Text & " AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "Delete FROM QL_trnbelidtl_fa where cmpcode='" & CompnyCode & "' AND trnbelifamstoid='" & famstoid.Text & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If Not Session("tbldtl") Is Nothing Then
                Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For i = 0 To objRow.Length - 1
                    ' Insert new dtl
                    sSql = "INSERT INTO QL_trnbelidtl_fa (cmpcode,trnbelifadtloid,trnbelifamstoid,trnbelifadtlseq,itemfacode,itemfadesc,trnbelifadtlqty,trnbelifadtlprice,trnbelifadtldisctype,trnbelifadtldiscvalue,trnbelifadtldiscamt,trnbelifadtlamtnetto,trnbelifadtldep,trnbelifadtllastvalue,trnbelifadtlusage,trnbelifadtlnote,crtuser,crttime,upduser,updtime) VALUES ('" & CompnyCode & "'," & DtlOid & "," & famstoid.Text & "," & (i + 1) & ",'" & Tchar(objRow(i)("itemfacode").ToString.Trim) & "','" & Tchar(objRow(i)("itemfadesc").ToString.Trim) & "','" & objRow(i)("trnbelifadtlqty").ToString.Trim & "','" & objRow(i)("trnbelifadtlprice").ToString.Trim & "','" & objRow(i)("trnbelifadtldisctype").ToString.Trim & "','" & objRow(i)("trnbelifadtldiscvalue").ToString.Trim & "','" & objRow(i)("trnbelifadtldiscamt").ToString.Trim & "','" & objRow(i)("trnbelifadtlamtnetto").ToString.Trim & "','" & objRow(i)("trnbelifadtldep").ToString.Trim & "','" & objRow(i)("trnbelifadtllastvalue").ToString.Trim & "','0','" & Tchar(objRow(i)("trnbelifadtlnote").ToString.Trim) & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"

                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    DtlOid += 1
                Next
                ' Update dtl lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & (DtlOid + objRow.Length - 1) & " WHERE tablename='QL_trnbelidtl_fa' AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If lblPOST.Text = "Post" Then
                'Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                sSql = "UPDATE QL_trnbelimst_fa SET trnbelifano='" & Session("sNo") & "' WHERE trnbelifamstoid = " & famstoid.Text & " AND cmpcode='" & CompnyCode & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "select suppname FROM QL_mstsupp where suppoid = '" & lblSuppOid.Text & "'"
                Dim SpOid As String = GetStrData(sSql)
                sSql = "select apacctgoid FROM QL_mstsupp where suppoid = '" & lblSuppOid.Text & "'"
                Dim spAcctgOid As Integer = GetStrData(sSql)

                sSql = "select genother1 FROM QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
                Dim acctgOid As Integer = GetStrData(sSql)
                Dim payType As Integer = 0
                'Session("DueDate") = 0
                If DDLpayterm.SelectedItem.Text = "CASH" Then
                    payType = 0
                    Session("DueDate") = Format(GetServerTime(), "MM/dd/yyyy")
                Else
                    sSql = "select genoid,gendesc FROM QL_mstgen where gengroup = 'PAYMENT TYPE' and genoid = '" & DDLpayterm.SelectedValue & "'"
                    payType = GetStrData(sSql)
                    For C1 As Int16 = 0 To CInt(payType) - 1
                        Session("DueDate") = CDate(fixDate.Text).AddDays(Integer.Parse(C1))
                    Next
                End If

                '//////INSERT INTO TABLE CONAP
                sSql = "INSERT INTO QL_conap (cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,curroid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnapnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES ('" & CompnyCode & "'," & MstConakOid & ",'ql_trnbelimst_fa'," & famstoid.Text & ",0,'" & lblSuppOid.Text & "','" & spAcctgOid & "','" & CurrDDL.SelectedValue & "','Post','APFA','" & CDate(fixDate.Text) & "','" & GetDateToPeriodAcctg(CDate(fixDate.Text)) & "',0,'1/1/1900','" & Session("sNo") & "',0,'" & Session("DueDate") & "','" & ToDecimal(Netto.Text) & "', 0,'FA Purchase|No. " & Session("sNo") & "- Supplier=" & SpOid & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDecimal(Netto.Text) & "', 0,'" & ToDecimal(Netto.Text) & "', 0 , 0)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                '//////INSERT INTO TRN GL MST
                sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type) VALUES ( '" & CompnyCode & "'," & MstGLOid & ",'" & CDate(fixDate.Text) & "','" & GetDateToPeriodAcctg(CDate(fixDate.Text)) & "','FA Purchase|No. " & Session("sNo") & "-Supplier=" & SpOid & "','Post',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'ASSET')"

                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                'insert data GL DTL D
                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glflag,upduser,updtime,glamtidr,glamtusd) VALUES ( '" & CompnyCode & "'," & DtlGLOid & "," & iSeq & "," & MstGLOid & "," & acctgOid & ",'D','" & ToDecimal(DPP.Text) & "','" & Session("sNo") & "','FA Purchase|No. " & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & famstoid.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(DPP.Text) & "','" & ToDouble(DPP.Text) & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                DtlGLOid += 1 : iSeq += 1
                If ToDouble(Tax.Text) > 0 Then
                    sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glflag,upduser,updtime,glamtidr,glamtusd) VALUES ( '" & CompnyCode & "'," & DtlGLOid & "," & iSeq & "," & MstGLOid & "," & acctgPPN & ",'D','" & ToDecimal(Tax.Text) & "','" & Session("sNo") & "','FIXED ASSET-PURCHASE-" & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & famstoid.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDouble(Tax.Text) & "','" & ToDouble(Tax.Text) & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    DtlGLOid += 1 : iSeq += 1
                End If
                'insert data GL DTL C
                sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glother1,glflag,upduser,updtime,glamtidr,glamtusd) VALUES ( '" & CompnyCode & "'," & DtlGLOid & "," & iSeq & "," & MstGLOid & "," & spAcctgOid & ",'C','" & ToDecimal(Netto.Text) & "','" & Session("sNo") & "','FA Purchase|No. " & Session("sNo") & "- Supplier=" & SpOid & "','QL_trnbelimst_fa " & famstoid.Text & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & ToDecimal(Netto.Text) & "','" & ToDecimal(Netto.Text) & "')"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
                DtlGLOid += 1 : iSeq += 1

                sSql = "UPDATE QL_mstoid SET lastoid=" & MstConakOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_conap'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & MstGLOid & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trnglmst'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "UPDATE QL_mstoid SET lastoid=" & DtlGLOid - 1 & " WHERE cmpcode='" & CompnyCode & "' AND tablename='QL_trngldtl'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            lblPOST.Text = "In Process"
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("tbldtl") = Nothing
        Session("oid") = Nothing
        If lblPOST.Text = "Post" Then
            Session("SavedInfo") &= "Data telah diposting !!!<BR>Fix Asset Code = " & Session("sNo")
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAssetPurchase.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim strSQL As String
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            'delete dtl
            strSQL = "Delete FROM QL_trnbelidtl_fa where trnbelifamstoid=" & famstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            ' delete mst
            strSQL = "DELETE FROM QL_trnbelimst_fa where trnbelifamstoid=" & famstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        If lblPOST.Text <> "" Then
            Session("SavedInfo") &= "Data telah dihapus !!!<BR>Fix Asset Code = " & FixAssetNo.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAssetPurchase.aspx?awal=true")
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'btnSave.Enabled = True
        'Session("oid") = Nothing
        'Session("tbldtl") = Nothing
        Response.Redirect("trnFixAssetPurchase.aspx?awal=true")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPOST.Text = "Post"
        btnSave_Click(sender, e)
    End Sub

    Protected Sub btnFindListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListSupp.Click
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAllListSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListSupp.Click
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub lkbCloseListSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub BtnCariSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListSupp.SelectedIndex = -1
        FilterTextListSupp.Text = ""
        gvSupplier.SelectedIndex = -1
        BindDataSupp()
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnHapusSupp_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblSuppOid.Text = ""
        Supp.Text = ""
    End Sub

    Protected Sub gvSupplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Supp.Text = gvSupplier.SelectedDataKey(1).ToString().Trim & " - " & gvSupplier.SelectedDataKey(2).ToString().Trim
        lblSuppOid.Text = gvSupplier.SelectedDataKey(0).ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, False)
    End Sub

    Protected Sub gvSupplier_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSupplier.PageIndexChanging
        gvSupplier.PageIndex = e.NewPageIndex
        BindDataSupp()
        mpeListSupp.Show()
    End Sub

    Protected Sub btnAddtoList_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim sMsg As String = ""
        If Code.Text.Trim = "" Then
            sMsg &= "Code Detail harus diisi !!<BR>"
        End If
        If Qty.Text = "" Then
            sMsg &= "QTY Harus Diisi!!"
        End If
        If ToDecimal(Price.Text) <= 0 Then
            sMsg &= "Price Harus Diisi!!"
        End If
        If subTotal.Text.Length > 18 Then
            sMsg &= "maximum digit sub total tidak boleh > 18 digit !!<BR>"
        End If
        If ToDecimal(FinalValue.Text) > ToDecimal(Price.Text) Then
            sMsg &= "Final Value tidak boleh > dari Price !!<BR>"
        End If
        If DDLtype2.SelectedValue = "PCT" Then
            If FilterType2.Text > 100 Then
                sMsg &= "Discon PCT Detail tidak boleh > 100 !!<BR>"
            End If
        ElseIf DDLtype2.SelectedValue = "AMT" Then
            If ToDecimal(FilterType2.Text) > ToDecimal(subTotal.Text) Then
                sMsg &= "Discon AMT Detail tidak boleh > dari Sub Total !!<BR>"
            End If
        End If
        'If Note1.Text.Length > 100 Then
        '    sMsg &= "Karakter Note Tidak Boleh > 100 !!<BR>"
        'End If
        If Note2.Text.Length > 100 Then
            sMsg &= "Karakter Detail Note Tidak Boleh > 100 !!<BR>"
        End If

        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If

        If AddToList.Text = "Add To List" Then
            If IsNothing(Session("tbldtl")) Then
                Dim Seq As Integer = 0
                Dim dtlTableNew As DataTable = SetTableDetail()
                Dim xrow As DataRow
                xrow = dtlTableNew.NewRow
                xrow(3) = Seq + 1
                xrow(4) = Code.Text
                xrow(5) = Desc.Text
                xrow(6) = ToDecimal(Qty.Text)
                xrow(7) = ToDecimal(Price.Text)
                xrow(8) = DDLtype2.SelectedValue
                xrow(9) = ToDecimal(FilterType2.Text)
                xrow(10) = ToDecimal(discDtl.Text)
                xrow(11) = ToDecimal(subTotal.Text)
                xrow(12) = ToDecimal(fixdepmonth.Text)
                xrow(13) = ToDecimal(FinalValue.Text)
                xrow(14) = Note2.Text
                dtlTableNew.Rows.Add(xrow)
                Session("tbldtl") = dtlTableNew
                GVFixedAssetdtl.DataSource = Session("tbldtl")
                GVFixedAssetdtl.DataBind()
                ClearDtl()
            Else
                Dim Seq As Integer = GVFixedAssetdtl.Rows.Count
                Dim dtlTable As DataTable
                dtlTable = Session("tbldtl")
                Dim dv As DataView = dtlTable.DefaultView
                dv.RowFilter = "trnbelifadtloid = " & fadtloid.Text & ""

                ' cek apakah data telah ada
                'If dv.Count > 0 Then
                '    dv.RowFilter = ""
                '    QLMsgBox1.ShowMessage("COA ini telah digunakan !!", 2, "")
                '    ClearDtlAP()
                '    Exit Sub

                'Else
                '    ' kalo data blom ada

                dv.RowFilter = ""
                'dv.Sort = "seq desc"
                'If dv.Count > 0 Then
                '    Seq = dv.Item(0).Item(0)
                'Else
                '    Seq = 0
                'End If

                Dim xrow As DataRow
                xrow = dtlTable.NewRow
                xrow(3) = Seq + 1
                xrow(4) = Code.Text
                xrow(5) = Desc.Text
                xrow(6) = ToDecimal(Qty.Text)
                xrow(7) = ToDecimal(Price.Text)
                xrow(8) = DDLtype2.SelectedValue
                xrow(9) = ToDecimal(FilterType2.Text)
                xrow(10) = ToDecimal(discDtl.Text)
                xrow(11) = ToDecimal(subTotal.Text)
                xrow(12) = ToDecimal(fixdepmonth.Text)
                xrow(13) = ToDecimal(FinalValue.Text)
                xrow(14) = Note2.Text
                dtlTable.Rows.Add(xrow)
                Session("tbldtl") = dtlTable
                Dim dv2 As DataView = dtlTable.DefaultView
                'dv2.Sort = "seq desc"
                GVFixedAssetdtl.DataSource = dtlTable
                GVFixedAssetdtl.DataBind()
                ClearDtl()
                'End If
            End If
        Else
            ' untuk transaksi edit
            Dim xindex = GVFixedAssetdtl.SelectedIndex
            Dim dt_temp As New DataTable
            dt_temp = Session("tbldtl")
            dt_temp.Rows(xindex).BeginEdit()
            dt_temp.Rows(xindex).Item(4) = Code.Text
            dt_temp.Rows(xindex).Item(5) = Desc.Text
            dt_temp.Rows(xindex).Item(6) = ToDecimal(Qty.Text)
            dt_temp.Rows(xindex).Item(7) = ToDecimal(Price.Text)
            dt_temp.Rows(xindex).Item(8) = DDLtype2.SelectedValue
            dt_temp.Rows(xindex).Item(9) = ToDecimal(FilterType2.Text)
            dt_temp.Rows(xindex).Item(10) = ToDecimal(discDtl.Text)
            dt_temp.Rows(xindex).Item(11) = ToDecimal(subTotal.Text)
            dt_temp.Rows(xindex).Item(12) = ToDecimal(fixdepmonth.Text)
            dt_temp.Rows(xindex).Item(13) = ToDecimal(FinalValue.Text)
            dt_temp.Rows(xindex).Item(14) = Note2.Text
            dt_temp.Rows(xindex).EndEdit()
            Session("tbldtl") = dt_temp
            GVFixedAssetdtl.DataSource = dt_temp
            GVFixedAssetdtl.DataBind()
            AddToList.Text = "Add To List"
            ClearDtl()
        End If
        TotalDtl()
        GVFixedAssetdtl.SelectedIndex = -1
    End Sub

    Protected Sub Price_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Price.Text = ToMaskEdit(ToDecimal(Price.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub Qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Qty.Text = ToMaskEdit(ToDecimal(Qty.Text), 0)
        ReAmountDep()
    End Sub

    Protected Sub DDLtype2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType2.Text = "0"
        ReAmountDep()
    End Sub

    Protected Sub FilterType2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        discDtl.Text = ToMaskEdit(ToDecimal(FilterType2.Text), 2)
        subTotal.Text -= ToMaskEdit(ToDecimal(discDtl.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub btnSearchCode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        FilterDDLListMat.SelectedIndex = -1
        FilterTextListMat.Text = ""
        Session("TblMat") = Nothing
        Session("TblMatView") = Nothing
        GVmstitem.DataSource = Nothing
        GVmstitem.DataBind()
        BindDataItem()
        'cProc.SetModalPopUpExtender(btnHideListSupp, pnlListSupp, mpeListSupp, True)
    End Sub

    Protected Sub btnHapusCode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        'GVmstitem.Visible = False
        Code.Text = ""
        Desc.Text = ""
    End Sub

    Protected Sub GVmstitem_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Code.Text = GVmstitem.SelectedDataKey(1).ToString().Trim
        Desc.Text = GVmstitem.SelectedDataKey(2).ToString().Trim
        lblCode.Text = GVmstitem.SelectedDataKey(1).ToString().Trim
        lblItemOid.Text = GVmstitem.SelectedDataKey(0).ToString().Trim
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub GVmstitem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVmstitem.PageIndexChanging
        GVmstitem.PageIndex = e.NewPageIndex
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub lkbCloseListMat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListMat.Click
        cProc.SetModalPopUpExtender(btnHideListMat, pnlListMat, mpeListMat, False)
    End Sub

    Protected Sub btnFindListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListMat.Click
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub btnAllListMat_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListMat.Click
        BindDataItem()
        mpeListMat.Show()
    End Sub

    Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ClearDtl()
        TotalDtl()
    End Sub

    Protected Sub FinalValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FinalValue.Text = ToMaskEdit(ToDecimal(FinalValue.Text), 2)
    End Sub

    Protected Sub GVFixedAssetdtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        If Status.Text = "In Process" Then
            Dim test As String = e.RowIndex
            Dim objTable As DataTable
            objTable = Session("tbldtl")
            'Dim iCountrow As Integer = objTable.Rows.Count
            'Dim idx As Integer = iCountrow - 1 - e.RowIndex
            objTable.Rows.RemoveAt(test)
            Session("tbldtl") = objTable
            GVFixedAssetdtl.DataSource = objTable
            GVFixedAssetdtl.DataBind()
        Else
            showMessage("Data terposting tidak dapat di hapus!!", 2)
        End If
        TotalDtl()
        ClearDtl()
    End Sub

    Protected Sub TotalDetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TotalDetail.Text = ToMaskEdit(ToDecimal(TotalDetail.Text), 2)
        ReAmountHdr()
    End Sub

    Protected Sub TotalDiscDetail_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        TotalDiscDetail.Text = ToMaskEdit(ToDecimal(TotalDiscDetail.Text), 2)
    End Sub

    Protected Sub DDLtax_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ReAmountHdr()
    End Sub

    Protected Sub DDLtype1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType1.Text = "0"
        ReAmountHdr()
    End Sub

    Protected Sub FilterType1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        FilterType1.Text = ToMaskEdit(ToDecimal(FilterType1.Text), 2)
        ReAmountHdr()
    End Sub

    Protected Sub Netto_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Netto.Text = ToMaskEdit(ToDecimal(Netto.Text), 2)
    End Sub

    Protected Sub DDLoutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        generatemstoid()
        GenerateFixNo()
        FixAssetNo.Text = Session("sNo")
    End Sub

    Protected Sub btnshowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ShowCOAPosting(FixAssetNo.Text, CompnyCode, gvakun) 'cashbankoid.Text)
        pnlPosting2.Visible = True
        btnHidePosting2.Visible = True
        mpePosting2.Show()
    End Sub

    Protected Sub lkbCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPosting2.Visible = False : btnHidePosting2.Visible = False : mpePosting2.Hide()
    End Sub

    Protected Sub CurrDDL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sErr As String = ""
        If fixDate.Text <> "" Then
            If IsValidDate(fixDate.Text, "MM/dd/yyyy", sErr) Then
                If CurrDDL.SelectedValue <> "" Then
                    cRate.SetRateValue(CInt(CurrDDL.SelectedValue), fixDate.Text)
                    If cRate.GetRateDailyLastError <> "" Then
                        showMessage(cRate.GetRateDailyLastError, 2)
                        Rateoid.Text = ""
                        RateToIDR.Text = ""
                        RateToUSD.Text = ""
                        Exit Sub
                    End If
                    If cRate.GetRateMonthlyLastError <> "" Then
                        showMessage(cRate.GetRateMonthlyLastError, 2)
                        Rate2oid.Text = ""
                        Rate2ToIDR.Text = ""
                        Rate2ToUsd.Text = ""
                        Exit Sub
                    End If
                    Rateoid.Text = cRate.GetRateDailyOid
                    RateToIDR.Text = ToMaskEdit(cRate.GetRateDailyIDRValue, GetRoundValue(cRate.GetRateDailyIDRValue.ToString))
                    RateToUSD.Text = ToMaskEdit(cRate.GetRateDailyUSDValue, GetRoundValue(cRate.GetRateDailyUSDValue.ToString))
                    Rate2oid.Text = cRate.GetRateMonthlyOid
                    Rate2ToIDR.Text = ToMaskEdit(cRate.GetRateMonthlyIDRValue, GetRoundValue(cRate.GetRateMonthlyIDRValue.ToString))
                    Rate2ToUsd.Text = ToMaskEdit(cRate.GetRateMonthlyUSDValue, GetRoundValue(cRate.GetRateMonthlyUSDValue.ToString))
                End If
            End If
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnFixAssetPurchase.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub fixDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim fDate As Date = CDate(fixDate.Text)
            fDate = CDate(fixDate.Text)
        Catch ex As Exception
            showMessage("- Format tanggal aset salah !!<BR>", 2)
            Exit Sub
        End Try
    End Sub

#End Region

End Class
