<%@ Page Language="VB" MasterPageFile="~/MasterUser.master" AutoEventWireup="false" CodeFile="trnDebetNote.aspx.vb" Inherits="Accounting_trnDebetNote" Title="Debet Note" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="middleContent">
  <table id="tbRight" bgcolor="white" border="1" cellpadding="3" cellspacing="0" class="tabelhias"
                    width="100%">
                    <tr>
                        <th align="left" class="header" valign="center">
                            <asp:Label ID="Label5" runat="server" Font-Bold="False" Text=".: Debet Note" CssClass="Title"></asp:Label></th>
                    </tr>
                </table>
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left">
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="100%" >
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
<DIV style="WIDTH: 100%; TEXT-ALIGN: left"><asp:Panel id="pnlDataMst" runat="server" DefaultButton="btnFind" __designer:wfdid="w179"><TABLE><TBODY><TR><TD class="Label" align=left>Filter</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterDDL" runat="server" CssClass="inpText" __designer:wfdid="w180"><asp:ListItem Value="a.dnno">DN No.</asp:ListItem>
<asp:ListItem Value="a.suppcustname">Supplier/Customer</asp:ListItem>
<asp:ListItem Value="a.aparno">A/P - A/R No</asp:ListItem>
</asp:DropDownList>&nbsp;<asp:TextBox id="FilterText" runat="server" CssClass="inpText" Width="235px" __designer:wfdid="w181"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:CheckBox id="chkPeriod" runat="server" Text="Periode" __designer:wfdid="w182"></asp:CheckBox></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:TextBox id="Date1" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w183"></asp:TextBox>&nbsp;<asp:ImageButton id="sTgl1" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w184"></asp:ImageButton>&nbsp; - <asp:TextBox id="Date2" runat="server" CssClass="inpText" Width="60px" __designer:wfdid="w185"></asp:TextBox>&nbsp; <asp:ImageButton id="sTgl2" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w186"></asp:ImageButton> <asp:Label id="Label2" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w187"></asp:Label> <ajaxToolkit:CalendarExtender id="ce2" runat="server" Format="MM/dd/yyyy" PopupButtonID="sTgl2" TargetControlID="Date2" Enabled="True" __designer:wfdid="w188"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee2" runat="server" TargetControlID="Date2" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w189" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender><ajaxToolkit:CalendarExtender id="ce1" runat="server" Format="MM/dd/yyyy" PopupButtonID="sTgl1" TargetControlID="Date1" Enabled="True" __designer:wfdid="w190"></ajaxToolkit:CalendarExtender><ajaxToolkit:MaskedEditExtender id="mee1" runat="server" TargetControlID="Date1" MaskType="Date" Mask="99/99/9999" __designer:wfdid="w191" CultureName="id-ID"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterType" runat="server" CssClass="inpText" __designer:wfdid="w192"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem Value="QL_trnbelimst">A/P</asp:ListItem>
<asp:ListItem Value="QL_trnjualmst">A/R</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left colSpan=4><asp:DropDownList id="FilterStatus" runat="server" CssClass="inpText" __designer:wfdid="w193"><asp:ListItem Value="%">ALL</asp:ListItem>
<asp:ListItem>In Process</asp:ListItem>
<asp:ListItem>Post</asp:ListItem>
</asp:DropDownList> <asp:ImageButton id="btnFind" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w194"></asp:ImageButton> <asp:ImageButton id="btnViewAll" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle" __designer:wfdid="w195"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel> <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD class="Label" align=left><asp:LinkButton id="lkbDNInProcess" runat="server" Visible="False" __designer:wfdid="w196"></asp:LinkButton></TD></TR><TR><TD class="Label" align=left><asp:GridView id="gvMst" runat="server" Width="100%" AllowPaging="True" CellPadding="4" AutoGenerateColumns="False" GridLines="None" AllowSorting="True" __designer:wfdid="w197">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6"></RowStyle>

<EmptyDataRowStyle CssClass="gvcontainer"></EmptyDataRowStyle>
<Columns>
<asp:HyperLinkField DataNavigateUrlFields="dnoid" DataNavigateUrlFormatString="trnDebetNote.aspx?oid={0}" DataTextField="dnoid" HeaderText="Draft No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="dnno" HeaderText="DN No" SortExpression="dnno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dndate" HeaderText="Date" SortExpression="dndate">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="tipe" HeaderText="Type" SortExpression="tipe">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="suppcustname" HeaderText="Customer/Supplier" SortExpression="suppcustname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="aparno" HeaderText="A/P - A/R No" SortExpression="aparno">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dnamt" HeaderText="Amount" SortExpression="dnamt">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="divname" HeaderText="Bussiness Unit" SortExpression="divname">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="dnstatus" HeaderText="Status" SortExpression="dnstatus">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" ForeColor="White"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCC99"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label5" runat="server" CssClass="Important" Text="No data found."></asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblViewInfo" runat="server" ForeColor="Black" Text="Click button Find or View All to view data" __designer:wfdid="w198"></asp:Label></TD></TR></TBODY></TABLE></DIV>
</ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/List.gif" height="16" />
                            <strong><span style="font-size: 9pt">List of Debet Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <ContentTemplate>
                            <asp:UpdatePanel id="UpdatePanel1" runat="server">
                                <contenttemplate>
<TABLE style="WIDTH: 100%" cellSpacing=0 border=0><TBODY><TR><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="WIDTH: 1%; HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="WIDTH: 1%; HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD></TR><TR><TD class="Label" align=left></TD><TD style="WIDTH: 1%" class="Label" align=left></TD><TD class="Label" align=left><asp:DropDownList id="bus_unit" runat="server" CssClass="inpText" __designer:wfdid="w257" AutoPostBack="True" Visible="False"></asp:DropDownList> <asp:Label id="apardate" runat="server" __designer:wfdid="w274" Visible="False"></asp:Label> <asp:Label id="aparreftype" runat="server" __designer:wfdid="w273" Visible="False"></asp:Label></TD><TD class="Label" align=left><ajaxToolkit:CalendarExtender id="ceDate" runat="server" __designer:wfdid="w258" TargetControlID="dndate" PopupButtonID="imbDate" Format="MM/dd/yyyy"></ajaxToolkit:CalendarExtender></TD><TD style="WIDTH: 1%" class="Label" align=left></TD><TD class="Label" align=left><ajaxToolkit:MaskedEditExtender id="meedate" runat="server" __designer:wfdid="w259" TargetControlID="dndate" Mask="99/99/9999" MaskType="Date"></ajaxToolkit:MaskedEditExtender></TD></TR><TR><TD class="Label" align=left><asp:Label id="lbldnno" runat="server" __designer:wfdid="w260">Draft No</asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:Label id="dnoid" runat="server" __designer:wfdid="w261"></asp:Label> <asp:TextBox id="dnno" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w262" Visible="False" size="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left>&nbsp;&nbsp; </TD></TR><TR><TD class="Label" align=left>Type</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="reftype" runat="server" CssClass="inpText" Width="55px" __designer:wfdid="w267" AutoPostBack="True"><asp:ListItem Value="ap">A/P</asp:ListItem>
<asp:ListItem Value="ar">A/R</asp:ListItem>
</asp:DropDownList> <asp:Label id="supp_cust_oid" runat="server" __designer:wfdid="w268" Visible="False"></asp:Label></TD><TD class="Label" align=left>Date</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="dndate" runat="server" CssClass="inpText" Width="81px" __designer:wfdid="w263"></asp:TextBox> <asp:ImageButton id="imbDate" runat="server" ImageUrl="~/Images/oCalendar.gif" ImageAlign="AbsMiddle" __designer:wfdid="w264"></asp:ImageButton> <asp:Label id="Label1" runat="server" CssClass="Important" Text="(MM/dd/yyyy)" __designer:wfdid="w265"></asp:Label> <asp:Label id="i_u" runat="server" CssClass="Important" Text="New" __designer:wfdid="w266" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparno" runat="server" Text="A/P No" __designer:wfdid="w276"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparno" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w277" size="20"></asp:TextBox> <asp:ImageButton id="btnFindAPAR" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w278"></asp:ImageButton> <asp:ImageButton id="btnClearAPAR" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w279"></asp:ImageButton> <asp:Label id="refoid" runat="server" __designer:wfdid="w280" Visible="False"></asp:Label></TD><TD class="Label" align=left><asp:Label id="lblsupp_cust" runat="server" Text="Supplier" __designer:wfdid="w269"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="supp_cust_name" runat="server" CssClass="inpTextDisabled" Width="231px" __designer:wfdid="w270" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnSearchSC" runat="server" ImageUrl="~/Images/search.gif" ImageAlign="AbsMiddle" __designer:wfdid="w3"></asp:ImageButton>&nbsp;<asp:ImageButton id="btnClearSC" runat="server" ImageUrl="~/Images/erase.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w4"></asp:ImageButton>&nbsp;<asp:Label id="aparrateoid" runat="server" __designer:wfdid="w283" Visible="False"></asp:Label> <asp:Label id="aparrate2oid" runat="server" __designer:wfdid="w284" Visible="False"></asp:Label></TD></TR><TR><TD class="Label" align=left></TD><TD class="Label" align=left></TD><TD class="Label" align=left colSpan=4><asp:GridView id="gvAPAR" runat="server" __designer:wfdid="w285" BackColor="White" DataKeyNames="reftype,refoid" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True">
<PagerSettings Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6"></RowStyle>

<EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="transno" HeaderText="A/P No">
<HeaderStyle HorizontalAlign="Left" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="transdate" HeaderText="A/P Date">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="currcode" HeaderText="Currency">
<HeaderStyle HorizontalAlign="Center" CssClass="gvhdr" Width="50px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="50px"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amttrans" HeaderText="A/P Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbayar" HeaderText="Paid Amount">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="amtbalance" HeaderText="A/P Balance">
<HeaderStyle HorizontalAlign="Right" CssClass="gvhdr"></HeaderStyle>

<ItemStyle HorizontalAlign="Right"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<PagerStyle HorizontalAlign="Left" CssClass="gvhdr" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="nodata" runat="server" Font-Size="X-Small" ForeColor="Red">No Account Payable data !!</asp:Label> 
</EmptyDataTemplate>

<SelectedRowStyle BackColor="Yellow" Font-Bold="True"></SelectedRowStyle>

<HeaderStyle BackColor="#124E81" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparamt" runat="server" Text="A/P Amount" __designer:wfdid="w286"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparamt" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w287" size="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblaparcurr" runat="server" Text="A/P Currency" __designer:wfdid="w281"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="curroid" runat="server" CssClass="inpTextDisabled" __designer:wfdid="w282" Enabled="False"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparbalance" runat="server" Text="A/P Balance" __designer:wfdid="w290"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparbalance" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w291" size="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left><asp:Label id="lblaparpaidamt" runat="server" Text="A/P Paid Amt" __designer:wfdid="w288"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparpaidamt" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w289" size="20" Enabled="False"></asp:TextBox></TD></TR><TR><TD class="Label" align=left><asp:Label id="lblaparafter" runat="server" Text="A/P Balance After DN" __designer:wfdid="w295"></asp:Label></TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="aparbalanceafter" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w296" size="20" Enabled="False"></asp:TextBox></TD><TD class="Label" align=left>DN Amount</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="dnamt" runat="server" CssClass="inpText" Width="150px" __designer:wfdid="w292" AutoPostBack="True" MaxLength="15"></asp:TextBox><asp:Label id="lblmaxamt" runat="server" CssClass="Important" __designer:wfdid="w1">Max =</asp:Label> <asp:Label id="maxamount" runat="server" CssClass="Important" __designer:wfdid="w2">0</asp:Label></TD></TR><TR><TD class="Label" align=left>COA Debet</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="coadebet" runat="server" CssClass="inpTextDisabled" Width="300px" __designer:wfdid="w297" AutoPostBack="True" Enabled="False"></asp:DropDownList></TD><TD class="Label" align=left>COA Credit</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:DropDownList id="coacredit" runat="server" CssClass="inpText" Width="300px" __designer:wfdid="w298" AutoPostBack="True"></asp:DropDownList></TD></TR><TR><TD class="Label" align=left>Note</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="dnnote" runat="server" CssClass="inpText" Width="250px" __designer:wfdid="w299" MaxLength="50"></asp:TextBox></TD><TD class="Label" align=left>Status</TD><TD class="Label" align=left>:</TD><TD class="Label" align=left><asp:TextBox id="dnstatus" runat="server" CssClass="inpTextDisabled" Width="100px" __designer:wfdid="w300" size="20" Enabled="False"></asp:TextBox></TD></TR><TR><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:DropDownList id="dntaxtype" runat="server" CssClass="inpText" __designer:wfdid="w301" Visible="False"><asp:ListItem Value="NONTAX">NONTAX</asp:ListItem>
<asp:ListItem>INCLUDE</asp:ListItem>
<asp:ListItem>EXCLUDE</asp:ListItem>
</asp:DropDownList></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left></TD><TD style="HEIGHT: 10px" class="Label" align=left><asp:TextBox id="dntaxamt" runat="server" CssClass="inpTextDisabled" Width="150px" __designer:wfdid="w302" Visible="False" size="20" Enabled="False"></asp:TextBox></TD></TR></TBODY></TABLE><TABLE cellSpacing=0 border=0><TBODY><TR><TD style="COLOR: #585858" class="Label" align=left><asp:Label id="create" runat="server" __designer:wfdid="w303"></asp:Label></TD></TR><TR><TD style="COLOR: #585858" class="Label" align=left>Last Update By <asp:Label id="upduser" runat="server" Font-Bold="True" __designer:wfdid="w304"></asp:Label>&nbsp;On <asp:Label id="updtime" runat="server" Font-Bold="True" __designer:wfdid="w305"></asp:Label></TD></TR><TR><TD style="HEIGHT: 28px" align=left><TABLE style="WIDTH: 100%" cellSpacing=0 border=0><TBODY><TR><TD align=left><asp:ImageButton id="btnSave" runat="server" ImageUrl="~/Images/btnsave.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w306"></asp:ImageButton> <asp:ImageButton id="btnCancel" runat="server" ImageUrl="~/Images/btncancel.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w307"></asp:ImageButton> <asp:ImageButton id="btnPost" runat="server" ImageUrl="~/Images/posting.png" ImageAlign="AbsMiddle" __designer:wfdid="w308"></asp:ImageButton> <asp:ImageButton id="btnDelete" runat="server" ImageUrl="~/Images/btndelete.bmp" ImageAlign="AbsMiddle" __designer:wfdid="w309" Visible="False"></asp:ImageButton> <asp:ImageButton id="btnShowCOA" runat="server" ImageUrl="~/Images/showCOA.png" ImageAlign="AbsMiddle" __designer:wfdid="w310" Visible="False"></asp:ImageButton></TD><TD align=left><DIV style="WIDTH: 100%; TEXT-ALIGN: center"><asp:UpdateProgress id="UpdateProgress1" runat="server" __designer:wfdid="w311" AssociatedUpdatePanelID="UpdatePanel1"><ProgressTemplate>
<DIV id="Div3" class="progressBackgroundFilter"></DIV><DIV id="Div4" class="processMessage"><SPAN style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: purple"><asp:Image id="Image1" runat="server" ImageUrl="~/Images/loading_animate.gif" __designer:wfdid="w312"></asp:Image><BR />Please Wait .....</SPAN><BR /></DIV>
</ProgressTemplate>
</asp:UpdateProgress> </DIV></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
</contenttemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                        <HeaderTemplate>
                            <img align="absMiddle" alt="" src="../Images/Form.gif" height="16" /> <strong><span style="font-size: 9pt">
                                Form Debet Note :.</span></strong>
                        </HeaderTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
</td>
        </tr>
    </table>
	<asp:UpdatePanel id="upListSupp" runat="server">
		<contenttemplate>
<asp:Panel id="pnlListSupp" runat="server" CssClass="modalBox" Width="650px" Visible="False"><TABLE width="100%"><TBODY><TR><TD class="Label" align=center colSpan=3><asp:Label id="lblListSupp" runat="server" Font-Size="Medium" Font-Bold="True" Text="List of Supplier"></asp:Label></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:Panel id="pnlFilterListSupp" runat="server" Width="100%" DefaultButton="btnFindListSupp">Filter : <asp:DropDownList id="FilterDDLListSupp" runat="server" CssClass="inpText" Width="100px"><asp:ListItem Value="name">Name</asp:ListItem>
<asp:ListItem Value="code">Code</asp:ListItem>
</asp:DropDownList> <asp:TextBox id="FilterTextListSupp" runat="server" CssClass="inpText" Width="150px"></asp:TextBox>&nbsp;<asp:ImageButton id="btnFindListSupp" onclick="btnFindListSupp_Click" runat="server" ImageUrl="~/Images/btnfind.bmp" ImageAlign="AbsMiddle"></asp:ImageButton> <asp:ImageButton id="btnAllListSupp" onclick="btnAllListSupp_Click" runat="server" ImageUrl="~/Images/viewall.png" ImageAlign="AbsMiddle"></asp:ImageButton></asp:Panel></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD class="Label" vAlign=top align=center colSpan=3><asp:GridView id="gvSuppCust" runat="server" ForeColor="#333333" Width="98%" DataKeyNames="oid,name,code" GridLines="None" AutoGenerateColumns="False" CellPadding="4" AllowPaging="True" OnSelectedIndexChanged="gvSuppCust_SelectedIndexChanged" PageSize="5" AllowSorting="True">
<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"></PagerSettings>

<RowStyle BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>
<Columns>
<asp:CommandField ShowSelectButton="True">
<HeaderStyle HorizontalAlign="Center" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
</asp:CommandField>
<asp:BoundField DataField="oid" HeaderText="Oid" Visible="False"></asp:BoundField>
<asp:BoundField DataField="code" HeaderText="Code" SortExpression="code">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name" HeaderText="Name" SortExpression="name">
<HeaderStyle HorizontalAlign="Left" CssClass="gvpopup"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
</asp:BoundField>
</Columns>

<FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>

<PagerStyle HorizontalAlign="Right" CssClass="gvpopup" Font-Bold="True" ForeColor="White"></PagerStyle>
<EmptyDataTemplate>
<asp:Label id="Label3" runat="server" ForeColor="Red" Font-Bold="True" Text="No Found Data" __designer:wfdid="w136"></asp:Label>
</EmptyDataTemplate>

<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"></SelectedRowStyle>

<HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"></HeaderStyle>

<AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
</asp:GridView></TD></TR><TR><TD style="HEIGHT: 5px" class="Label" vAlign=top align=center colSpan=3></TD></TR><TR><TD align=center colSpan=3><asp:LinkButton id="lkbCloseListSupp" onclick="lkbCloseListSupp_Click" runat="server">[ Cancel & Close ]</asp:LinkButton></TD></TR></TBODY></TABLE></asp:Panel> <ajaxToolkit:ModalPopupExtender id="mpeListSupp" runat="server" TargetControlID="btnHideListSupp" Drag="True" PopupDragHandleControlID="lblListSupp" BackgroundCssClass="modalBackground" PopupControlID="pnlListSupp"></ajaxToolkit:ModalPopupExtender> <asp:Button id="btnHideListSupp" runat="server" Visible="False"></asp:Button> 
</contenttemplate>
	</asp:UpdatePanel>
<asp:UpdatePanel id="UpdatePanelValidasi" runat="server">
<contenttemplate>
<asp:Panel id="pnlPopUpMsg" runat="server" CssClass="modalMsgBox" Width="700px" Visible="False" __designer:wfdid="w2"><TABLE width="100%"><TBODY><TR><TD style="BACKGROUND-IMAGE: none; BACKGROUND-COLOR: #cc0000; TEXT-ALIGN: left" colSpan=2><asp:Label id="lblCaption" runat="server" ForeColor="White" Font-Bold="True" __designer:wfdid="w3"></asp:Label></TD></TR><TR><TD colSpan=2></TD></TR><TR><TD style="VERTICAL-ALIGN: top; WIDTH: 40px; TEXT-ALIGN: center"><asp:Image id="imIcon" runat="server" ImageUrl="~/Images/error.jpg" ImageAlign="AbsMiddle" Width="24px" Height="24px" __designer:wfdid="w4"></asp:Image></TD><TD style="WIDTH: 660px; TEXT-ALIGN: left"><asp:Label id="lblMessage" runat="server" CssClass="Important" __designer:wfdid="w5"></asp:Label></TD></TR><TR><TD align=center colSpan=2><asp:Label id="lblState" runat="server" Visible="False" __designer:wfdid="w6"></asp:Label></TD></TR><TR><TD style="TEXT-ALIGN: center" colSpan=2><asp:ImageButton id="btnOKPopUp" runat="server" ImageUrl="~/Images/ok.png" ImageAlign="AbsMiddle" __designer:wfdid="w7"></asp:ImageButton></TD></TR></TBODY></TABLE></asp:Panel><ajaxToolkit:ModalPopupExtender id="mpePopUpMsg" runat="server" TargetControlID="bePopUpMsg" DropShadow="True" PopupControlID="pnlPopUpMsg" BackgroundCssClass="modalBackground" PopupDragHandleControlID="lblCaption" __designer:wfdid="w8"></ajaxToolkit:ModalPopupExtender><asp:Button id="bePopUpMsg" runat="server" CausesValidation="False" Visible="False" __designer:wfdid="w9"></asp:Button>
</contenttemplate>
</asp:UpdatePanel>
</asp:Content>
