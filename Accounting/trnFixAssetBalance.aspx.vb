
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Data.OleDb
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnFixAssetBalance
    Inherits System.Web.UI.Page

#Region "Variabel"
    Public DefCounter As String = ConfigurationSettings.AppSettings("DefaultFormatCounter")
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompnyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Public ConnStr As String = ConfigurationSettings.AppSettings("QL_CONN")
    Public objConn As New SqlConnection(ConnStr)
    Dim xCmd As New SqlCommand("", objConn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cProc As New ClassProcedure
    Private connExcel As New OleDbConnection
    Private adapt As New OleDbDataAdapter
    Dim dsData As New DataSet
    Dim ckon As New Koneksi
    Dim cRate As New ClassRate
    Private ws As DataTable
    Public sql_temp As String
    Private Report As New ReportDocument
    Public folderReport As String = "~/report/"
    'Public PrinterLX As String = ConfigurationSettings.AppSettings("PrintLX")
    Dim tempPayFlag As String = ""
#End Region

#Region "Function"
    Private Function SetTableDetail() As DataTable
        Dim dtlTable As DataTable = New DataTable("TblDtl")
        dtlTable.Columns.Add("cmpcode", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtloid", Type.GetType("System.Int32"))
        dtlTable.Columns.Add("fixoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixdtlseq", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiod", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixperiodvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepvalue", Type.GetType("System.Double"))
        dtlTable.Columns.Add("fixperioddepaccum", Type.GetType("System.Double"))
        dtlTable.Columns.Add("depcostacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("accdepacctgoid", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixnote", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixflag", Type.GetType("System.String"))
        dtlTable.Columns.Add("fixpostdate", Type.GetType("System.DateTime"))
        dtlTable.Columns.Add("upduser", Type.GetType("System.String"))
        dtlTable.Columns.Add("updtime", Type.GetType("System.DateTime"))
        Return dtlTable
    End Function

    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption
        lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub
#End Region

#Region "Prosedure"

    Private Sub ReAmountDep()
        If fixPresentValue.Text <> "" And fixdepmonth.Text <> "" Then
            If ToDecimal(fixPresentValue.Text <= 0) Then
                showMessage("Nilai buku harus lebih besar dari 0 !!", 2)
                fixdepval.Text = ""
                Exit Sub
            End If

            If ToDecimal(fixPresentValue.Text) > 0 Then
                accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
            End If

            If ToDecimal(fixdepmonth.Text) = -1 Or ToDecimal(fixdepmonth.Text) = 0 Then
                fixdepval.Text = "0.00"
                fixPresentValue.Text = "0.00"
                accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
            Else
                fixdepval.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
                AccumVal.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
            End If

            If Session("oid") <> Nothing And Session("oid") <> "" Then
                Session("tbldtl") = Nothing
                GVFixedAssetdtl.DataSource = Nothing
                GVFixedAssetdtl.DataBind()
            Else
                Session("tbldtl") = Nothing
                GVFixedAssetdtl.DataSource = Nothing
                GVFixedAssetdtl.DataBind()
            End If
        End If
    End Sub

    Public Sub binddata(ByVal Sfilter As String)
        If CDate(txtPeriode1.Text) > CDate(txtPeriode2.Text) Then
            showMessage("Second/End Period must be greater than First Period !!", 2)
            Exit Sub
        End If
        sSql = "SELECT f.cmpcode, f.fixoid, f.fixcode, f.fixdesc, f.fixgroup, f.fixdate, f.fixfirstvalue, f.fixdepmonth, f.fixdepval, f.fixflag,c.curroid,c.currcode+' - '+c.currdesc AS currency, f.upduser, f.updtime, f.fixpresentvalue, g.genoid, g.gendesc from QL_trnfixmst f " & _
        " inner join ql_mstacctg a on a.acctgoid=f.acctgoid " & _
        " inner join QL_mstgen g on g.genoid = f.fixgroup  " & _
        " inner join QL_mstcurr c on c.curroid=f.curroid" & _
        " WHERE f.cmpcode='" & Session("CompnyCode") & "' AND f.fixflag not in('DELETE') AND f.fixoid < 0 " & Sfilter & ""

        Dim objTable As DataTable = ckon.ambiltabel(sSql, "QL_trnfixmst")
        Session("tbldata") = objTable
        GVFixedAsset.DataSource = objTable
        GVFixedAsset.DataBind()
    End Sub

    Protected Sub initAllDDL()
        sSql = "select genoid,gendesc from QL_mstgen where gengroup = 'ASSETTYPE'"
        FillDDL(fixgroup, sSql)
        fixDate.Text = Format(GetServerTime(), "MM/dd/yyyy")
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & Session("CompnyCode") & "' AND activeflag='ACTIVE'"
        FillDDL(CurroidDDL, sSql)
        cRate.SetRateValue(CInt(CurroidDDL.SelectedValue), fixDate.Text)
    End Sub

    Private Sub InitAllVar()
        'FillDDLAcctg(cashbankacctgoid, New String() {"VAR_CASH"}, "")
        FillDDLAcctg(DDLassets, "VAR_ASSET", Session("CompnyCode"))
        FillDDLAcctg(DDLaccum, "VAR_ASSET_ACCUM", Session("CompnyCode"))
        FillDDLAcctg(DDLadExpense, "VAR_ACCUM_DEP_EXPENSE", Session("CompnyCode"))
    End Sub

    Private Sub fillTextBox(ByVal vjurnaloid As String)
        Dim mySqlConn As New SqlConnection(ConnStr)
        Dim sqlSelect As String = "SELECT f.cmpcode, fixoid, fixdate, acctgoid, payacctgoid, fixdesc, fixlocation, fixperson, fixother, LEFT(convert(varchar,fixfirstvalue), LEN(convert(varchar,fixfirstvalue))-2) fixfirstvalue,fixdepmonth, LEFT(convert(varchar,fixdepval), LEN(convert(varchar,fixdepval))-2) fixdepval, fixflag,c.curroid ,f.createuser,f.createtime, f.upduser,f.updtime, fixcode, fixpresentvalue, fixgroup, accumdepacctgoid,accumdepexpacctgoid, fixlastasset FROM QL_trnfixmst f " & _
        " INNER JOIN QL_mstcurr c ON c.curroid=f.curroid WHERE fixoid =" & vjurnaloid & "  AND f.cmpcode = '" & Session("CompnyCode") & "'"
        Dim mySqlDA As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
        Dim objDs As New DataSet
        Dim objTable As DataTable
        Dim objRow() As DataRow

        mySqlDA.Fill(objDs)
        objTable = objDs.Tables(0)
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

        If objRow.Length > 0 Then
            sSql = "select genother4 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
            fixdepmonth.Text = GetStrData(sSql)
            If fixdepmonth.Text = "-1" Then
                fixLastAsset.Visible = False
                Label42.Visible = False
                Label16.Visible = False
            Else
                fixLastAsset.Visible = True
                Label42.Visible = True
                Label16.Visible = True
            End If

            'DDLoutlet.SelectedValue = Trim(objRow(0)("cmpcode").ToString)
            fixmstoid.Text = Trim(objRow(0)("fixoid").ToString)
            fixDate.Text = Format(objRow(0)("fixdate"), "MM/dd/yyyy")
            DDLassets.SelectedValue = Trim(objRow(0)("acctgoid").ToString)
            DDLaccum.SelectedValue = Trim(objRow(0)("accumdepacctgoid").ToString)
            CurroidDDL.SelectedValue = Trim(objRow(0)("curroid").ToString)
            DDLadExpense.SelectedValue = Trim(objRow(0)("accumdepexpacctgoid").ToString)
            fixdesc.Text = Trim(objRow(0)("fixdesc").ToString)
            fixlocation.Text = Trim(objRow(0)("fixlocation").ToString)
            fixperson.Text = Trim(objRow(0)("fixperson").ToString)
            fixother.Text = Trim(objRow(0)("fixother").ToString)
            fixfirstvalue.Text = ToMaskEdit(Trim(objRow(0)("fixfirstvalue").ToString), 2)
            fixdepmonth.Text = CInt(Trim(objRow(0)("fixdepmonth").ToString))
            fixdepval.Text = ToMaskEdit(Trim(objRow(0)("fixdepval").ToString), 2)
            fixLastAsset.Text = ToMaskEdit(Trim(objRow(0)("fixLastAsset").ToString), 2)
            AccumVal.Text = ToMaskEdit(Trim(objRow(0)("fixdepval").ToString), 2)
            lblUser.Text = Trim(objRow(0)("createuser").ToString)
            lblTime.Text = Trim(objRow(0)("createtime").ToString)
            upduser.Text = Trim(objRow(0)("upduser").ToString)
            updtime.Text = Trim(objRow(0)("updtime").ToString)
            FixCode.Text = Trim(objRow(0)("fixcode").ToString)
            fixgroup.SelectedValue = Trim(objRow(0)("fixgroup").ToString)
            fixPresentValue.Text = ToMaskEdit(Trim(objRow(0)("fixpresentvalue").ToString), 2)
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)

            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            CutofDate.Text = Format(CUTOFFDATE, "MM/dd/yyyy")

            If Trim(objRow(0)("fixflag").ToString) <> "IN PROCESS" Then
                ButtonSave.Visible = False
                BtnDelete.Visible = False
                btnPosting.Visible = False
                GerenatedBtn.Visible = False
                fixfirstvalue.Enabled = False
                fixPresentValue.Enabled = False
                fixdepmonth.Enabled = False
                fixLastAsset.Enabled = False
            Else
                ButtonSave.Visible = True
                BtnDelete.Visible = True
                btnPosting.Visible = True
            End If

            'data detail
            sqlSelect = "select cmpcode, fixdtloid, fixoid, fixdtlseq, fixperiod, fixperiodvalue, fixperioddepvalue, fixperioddepaccum, depcostacctgoid, accdepacctgoid, fixnote, fixpostdate, fixflag, crtuser, crttime, upduser, updtime from QL_trnfixdtl where fixoid=" & vjurnaloid & " AND cmpcode='" & Session("CompnyCode") & "'"

            Dim mySqlDAdtl As New SqlClient.SqlDataAdapter(sqlSelect, ConnStr)
            Dim objDsDtl As New DataSet
            Dim objTableDtl As DataTable
            Dim objRowDtl() As DataRow

            mySqlDAdtl.Fill(objDsDtl)
            objTableDtl = objDsDtl.Tables(0)
            objRowDtl = objTableDtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
            Session("tbldtl") = objDsDtl.Tables(0)

            GVFixedAssetdtl.DataSource = objDsDtl.Tables(0)
            GVFixedAssetdtl.DataBind()
            GVFixedAssetdtl.Visible = True
        End If
        mySqlConn.Close()
        'DDLoutlet.Enabled = False
        'DDLoutlet.CssClass = "inpTextDisabled"
    End Sub

    Private Sub GenerateMstOid()
        fixmstoid.Text = GetStrData("SELECT isnull(MIN(fixoid)-1,-1) FROM ql_trnfixmst WHERE cmpcode='" & Session("CompnyCode") & "' AND fixoid < 0")
    End Sub

    Private Sub GenerateDtlOid()
        Session("dtloid") = GenerateID("QL_trnfixdtl", DDLoutlet.SelectedValue)
    End Sub

#End Region

#Region "Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim CompnyCode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = CompnyCode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnFixAssetBalance.aspx")
        End If
        If checkPagePermission("~\Accounting\trnFixAssetBalance.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Fix Asset Balance "
        Session("oid") = Request.QueryString("oid")

        BtnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnPosting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        'btnPostingDtl.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST all this data?');")

        If Not IsPostBack Then
            'binddata()
            lblViewInfo.Visible = True
            fixgroup_SelectedIndexChanged(Nothing, Nothing)
            initAllDDL()
            InitAllVar()
            Dim CUTOFFDATE As Date
            sSql = "SELECT genother1 FROM QL_mstgen WHERE gengroup ='CUTOFDATE' AND cmpcode='" & Session("CompnyCode") & "'"
            If Not IsDate(CDate(GetStrData(sSql))) Then
                showMessage("Invalid Cut Off Date setup on General group 'CUTOFDATE' !", 2)
                Exit Sub
            Else
                CUTOFFDATE = CDate(GetStrData(sSql))
            End If
            CutofDate.Text = Format(CUTOFFDATE, "MM/dd/yyyy")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                fillTextBox(Session("oid"))
                lblPOST.Text = "IN PROCESS"
                upduser.Text = Session("UserID")
                updtime.Text = GetServerTime()
                TabContainer1.ActiveTabIndex = 1
                DDLassets.Enabled = False
                DDLassets.CssClass = "inpTextDisabled"
                DDLaccum.Enabled = False
                DDLaccum.CssClass = "inpTextDisabled"
                DDLadExpense.Enabled = False
                DDLadExpense.CssClass = "inpTextDisabled"
                fixgroup.Enabled = False
                fixgroup.CssClass = "inpTextDisabled"
            Else
                GenerateMstOid()
                BtnDelete.Visible = False
                btnPosting.Visible = False
                fixLastAsset.Visible = False
                Label42.Visible = False
                Label16.Visible = False
                fixDate.Text = Format(CUTOFFDATE.AddDays(-1), "MM/dd/yyyy")
                fixdepmonth.Text = "-1"
                fixgroup_SelectedIndexChanged(Nothing, Nothing)
                fixdepval.Text = "0.00"
                accumDV.Text = "0.00"
                upduser.Text = "-"
                updtime.Text = "-"
                txtPeriode1.Text = Format(Now, "MM/01/yyyy")
                txtPeriode2.Text = Format(GetServerTime, "MM/dd/yyyy")
                lblUser.Text = Session("UserID")
                lblTime.Text = GetServerTime()
                lblPOST.Text = "IN PROCESS"
                TabContainer1.ActiveTabIndex = 0
            End If
            Dim dt As DataTable
            dt = Session("tbldtl")
            GVFixedAssetdtl.DataSource = dt
            GVFixedAssetdtl.DataBind()
        End If
    End Sub

    'Protected Sub GVFixedAsset_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Response.Redirect("trnFixAssetBalance.aspx?cmpcode=" & GVFixedAsset.SelectedDataKey("cmpcode").ToString & "& oid=" & GVFixedAsset.SelectedDataKey("fixoid").ToString & "")
    'End Sub

    Protected Sub GVFixedAsset_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAsset.PageIndex = e.NewPageIndex
        binddata("")
    End Sub

    Protected Sub GVFixedAsset_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVFixedAsset.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = Format(CDate(e.Row.Cells(2).Text), "MM/dd/yyyy")
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(5).Text = ToMaskEdit(e.Row.Cells(5).Text, 2)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVFixedAssetdtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Text = ToMaskEdit(e.Row.Cells(4).Text, 2)
            e.Row.Cells(3).Text = ToMaskEdit(e.Row.Cells(3).Text, 2)
            e.Row.Cells(2).Text = ToMaskEdit(e.Row.Cells(2).Text, 2)
        End If
    End Sub

    Protected Sub GVFixedAssetdtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objTable As DataTable : objTable = Session("tbldtl")
        Dim dv As DataView = objTable.DefaultView
        dv.RowFilter = "fixperiod='" & GVFixedAssetdtl.SelectedDataKey("fixperiod").ToString & "'"
        dv.RowFilter = "fixflag = 'IN PROCESS'"
        If dv.Count > 0 Then
            AccumVal.Text = ToMaskEdit(ToDecimal(dv(0)("fixperioddepaccum").ToString), 2)
        Else
            showMessage(" Tidak dapat mengeluarakan data detail !!", 2)
        End If
        dv.RowFilter = ""
    End Sub

    Protected Sub GVFixedAssetdtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        GVFixedAssetdtl.PageIndex = e.NewPageIndex
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
    End Sub

    Protected Sub fixgroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'FillDDLAcctg(DDLassets, New String() {"VAR_ASSET"}, "")
        FillDDLAcctg(DDLassets, "VAR_ASSET", Session("CompnyCode"))
        sSql = "select genother1 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLassets.SelectedValue = GetStrData(sSql)

        'FillDDLAcctg(DDLaccum, New String() {"VAR_ASSET_ACCUM"}, "")
        FillDDLAcctg(DDLaccum, "VAR_ASSET_ACCUM", Session("CompnyCode"))
        sSql = "select genother2 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLaccum.SelectedValue = GetStrData(sSql)

        'FillDDLAcctg(DDLadExpense, New String() {"VAR_ACCUM_DEP_EXPENSE"}, "")
        FillDDLAcctg(DDLadExpense, "VAR_ACCUM_DEP_EXPENSE", Session("CompnyCode"))
        sSql = "select genother3 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        DDLadExpense.SelectedValue = GetStrData(sSql)

        sSql = "select genother5 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        fixdepmonth.Text = GetStrData(sSql)
        If ToDecimal(fixdepmonth.Text) = -1 Then
            fixLastAsset.Visible = False
            Label42.Visible = False
            Label16.Visible = False
            'fixPresentValue.Text = "0.00"
            fixdepval.Text = "0.00"
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
        Else
            fixLastAsset.Visible = True
            Label42.Visible = True
            Label16.Visible = True
            'fixPresentValue.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text), 2)
            If fixdepmonth.Text = "0" Then
                fixdepval.Text = "0.00"
            Else
                fixdepval.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
            End If
            AccumVal.Text = ToMaskEdit((ToDecimal(fixPresentValue.Text)) / Val(ToDecimal(fixdepmonth.Text)), 2)
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
        End If
        GVFixedAssetdtl.Visible = False
    End Sub

    Protected Sub fixdepmonth_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixdepmonth.TextChanged
        ReAmountDep()
    End Sub

    Protected Sub fixPresentValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixPresentValue.TextChanged
        fixPresentValue.Text = ToMaskEdit(ToDecimal(fixPresentValue.Text), 2)
        accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub fixLastAsset_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixLastAsset.TextChanged
        fixLastAsset.Text = ToMaskEdit(ToDecimal(fixLastAsset.Text), 2)
        If ToDecimal(fixLastAsset.Text) > ToDecimal(fixPresentValue.Text) Then
            showMessage("Asset terakhir tidak dapat lebih dari asset saat ini ! ", 2)
            Exit Sub
        End If
        ReAmountDep()
        If Session("oid") <> Nothing And Session("oid") <> "" Then
        Else
            Session("tbldtl") = Nothing
            GVFixedAssetdtl.DataSource = Nothing
            GVFixedAssetdtl.DataBind()
        End If
    End Sub

    Protected Sub fixfirstvalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles fixfirstvalue.TextChanged
        fixfirstvalue.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text), 2)
        If ToDecimal(fixdepmonth.Text) = -1 Then
            fixPresentValue.Text = "0.00"
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
        Else
            'fixPresentValue.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text), 2)
            accumDV.Text = ToMaskEdit(ToDecimal(fixfirstvalue.Text) - ToDecimal(fixPresentValue.Text), 2)
        End If
        ReAmountDep()
    End Sub

    Protected Sub accumDV_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        accumDV.Text = ToMaskEdit(ToDecimal(accumDV.Text), 2)
        ReAmountDep()
    End Sub

    Protected Sub DDLoutlet_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        GenerateMstOid()
        GenerateDtlOid()
    End Sub

    Protected Sub lbkHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView1.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkPostMoreInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 1
    End Sub

    Protected Sub lbkPostInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        MultiView2.ActiveViewIndex = 0
    End Sub

    Protected Sub lbkDetil_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbkDetil.Click
        MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnSearch.Click
        Session("tbldata") = Nothing
        GVFixedAsset.PageIndex = 0
        Session("SearchFixAsset") = sql_temp
        lblViewInfo.Visible = False
        If Not IsValidDate(txtPeriode1.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal period 1 salah !!<BR>", 2)
            Exit Sub
        End If
        If Not IsValidDate(txtPeriode2.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal period 2 salah !!<BR>", 2)
            Exit Sub
        End If
        Dim sFilter As String = " AND f.fixdate BETWEEN '" & CDate(txtPeriode1.Text) & "' AND '" & CDate(txtPeriode2.Text) & "' AND " & DDLfilter.SelectedValue & " like '%" & Tchar(txtFilter.Text) & "%'"
        binddata(sFilter)
    End Sub

    Protected Sub BtnViewAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnViewAll.Click
        txtFilter.Text = ""
        lblViewInfo.Visible = False
        If Not IsValidDate(txtPeriode1.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal period 1 salah !!<BR>", 2)
            Exit Sub
        End If
        If Not IsValidDate(txtPeriode2.Text, "MM/dd/yyyy", "") Then
            showMessage("- Format tanggal period 2 salah !!<BR>", 2)
            Exit Sub
        End If
        binddata("")
    End Sub

    Protected Sub GerenatedBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim dtlTable As DataTable = SetTableDetail()
        Session("tbldtl") = dtlTable

        ' Validation
        sSql = "select genother4 from QL_mstgen where gengroup = 'ASSETTYPE' and genoid = '" & fixgroup.SelectedValue & "'"
        If GetStrData(sSql) = "NO" Then
            showMessage("Type Asset ini tidak membutuhkan Generate !!", 2)
            Exit Sub
        End If
        If ToDecimal(fixdepmonth.Text) = -1 Then
            showMessage("Depreciation -1 tidak membutuhkan Generate !!", 2)
            Exit Sub
        End If
        If IsDate(CDate(fixDate.Text)) = False Or IsDate(CDate(fixDate.Text)) = False Then
            showMessage("Tanggal asset salah !!", 2)
            Exit Sub
        End If

        Dim sMsg As String = ""
        If ToDecimal(fixPresentValue.Text) <= 0 And ToDecimal(fixdepmonth.Text) <> -1 Then
            sMsg &= "Nilai buku harus lebih besar dari 0!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) > ToDecimal(fixfirstvalue.Text) Then
            sMsg &= "Nilai buku tidak boleh lebih besar Harga Perolehan!!<BR>"
        End If
        If fixdepmonth.Text = "" Then
            sMsg &= "Depreciation harus lebih besar dari 0 !!<BR>"
        ElseIf CDbl(fixdepmonth.Text) <> -1 And CDbl(fixdepmonth.Text) <= 0 Then
            sMsg &= "Depreciation harus lebih besar dari 0 !!<BR>"
        End If

        If ToDecimal(fixLastAsset.Text) > ToDecimal(fixdepval.Text) Then
            sMsg &= "Aset Value terakhir tidak boleh >= Depreciation Value !!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            Exit Sub
        End If

        ' Generate
        Dim FAdtlID As String = "1"
        'Generate FA detail ID
        Dim objTableFAdtl As DataTable
        Dim objRowDAdt() As DataRow
        objTableFAdtl = Session("tbldtl")
        objRowDAdt = objTableFAdtl.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        FAdtlID = objRowDAdt.Length + 1
        Dim objTable As DataTable
        Dim objRow As DataRow
        objTable = Session("tbldtl")

        Dim iYear As Integer = CDate(CutofDate.Text).Year
        Dim iMonth As Integer = CDate(CutofDate.Text).Month
        'If CDate(toDate(fixDate.Text)).Day <= 15 Then
        'iMonth = CDate(toDate(fixDate.Text)).Month
        'Else
        '    iMonth = CDate(toDate(fixDate.Text)).Month + 1
        'End If
        Dim sMonth As String = ""
        Dim iValue As Decimal = ToDecimal(fixPresentValue.Text)
        Dim Accum As Decimal = ToDecimal(AccumVal.Text)
        Dim Sisa As Decimal = ToDecimal(fixdepval.Text)
        Dim timeServer = GetServerTime()

        For C1 As Int16 = 0 To CInt(fixdepmonth.Text) - 1
            If CStr(iMonth).Length = 1 Then
                sMonth = "0" & CStr(iMonth)
            Else
                sMonth = CStr(iMonth)
            End If

            iValue = iValue - Math.Round(ToDecimal(Sisa), 2, MidpointRounding.ToEven)
            If Math.Round(ToDecimal(iValue), 2, MidpointRounding.ToEven) < 1 Then
                Accum += Math.Round(ToDecimal(iValue), 2, MidpointRounding.ToEven)
                iValue = ToDecimal(fixLastAsset.Text)
                Sisa = Math.Round(ToDecimal(Sisa), 2, MidpointRounding.ToEven) - Math.Round(ToDecimal(fixLastAsset.Text), 2, MidpointRounding.ToEven)
            End If
            If C1 = 0 Then
                Accum += ToDecimal(accumDV.Text)
            End If

            If Session("tbldtl") IsNot Nothing Then
                objRow = objTable.NewRow()
                objRow("cmpcode") = DDLoutlet.SelectedValue
                objRow("fixdtloid") = FAdtlID + C1
                objRow("fixoid") = 1
                objRow("fixdtlseq") = 1
                objRow("fixperiod") = CStr(iYear) & "" & sMonth
                objRow("fixperiodvalue") = iValue
                objRow("fixperioddepvalue") = Sisa
                objRow("fixperioddepaccum") = Accum
                objRow("depcostacctgoid") = DDLaccum.SelectedValue
                objRow("accdepacctgoid") = DDLadExpense.SelectedValue
                objRow("fixnote") = ""
                objRow("fixflag") = "IN PROCESS"
                objRow("fixpostdate") = timeServer
                objRow("updtime") = timeServer
                objRow("upduser") = Session("UserID")
                objTable.Rows.Add(objRow)
            End If

            Accum += ToDecimal(Sisa)
            iMonth += 1
            If iMonth = 13 Then
                iMonth = 1
                iYear += 1
            End If
        Next

        Session("tbldtl") = objTable
        GVFixedAssetdtl.DataSource = Session("tbldtl")
        GVFixedAssetdtl.DataBind()
        GVFixedAssetdtl.Visible = True

        'ButtonSave.Visible = True
        'MultiView1.ActiveViewIndex = 1
    End Sub

    Protected Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Dim C As Integer = GVFixedAssetdtl.Rows.Count
        If C = 0 And fixdepmonth.Text > 0 Then
            showMessage("Silahkan Click Auto Generate, Untuk Mengisi Data Detail !!!", 2)
            Exit Sub
        End If

        If CDate(fixDate.Text) >= CDate(CutofDate.Text) Then
            showMessage("Tanggal asset Tidak boleh >= CutoffDate (" & CutofDate.Text & ") !!", 2)
            Exit Sub
        End If

        Dim strSQL As String = ""
        ' Validasi
        Dim sMsg As String = ""
        If FixCode.Text.Trim = "" Then
            sMsg &= "- Silahkan isi Kode aset !!<BR>"
        End If
        If fixdesc.Text.Trim = "" Then
            sMsg &= "- Silahkan isi deskripsi aset !!<BR>"
        End If
        If fixfirstvalue.Text = "" Or ToDecimal(fixfirstvalue.Text) <= 0 Then
            sMsg &= "- Nilai awal harus lebih besar dari 0!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) > ToDecimal(fixfirstvalue.Text) Then
            sMsg &= "- Nilai buku tidak boleh lebih besar Harga Perolehan!!<BR>"
        End If
        If ToDecimal(fixPresentValue.Text) <= 0 And ToDecimal(fixdepmonth.Text) <> -1 Then
            sMsg &= "- Nilai buku harus lebih besar dari 0!!<BR>"
        End If
        If fixdepmonth.Text = "" Or fixdepmonth.Text = 0 Then
            sMsg &= "- Depreciation harus lebih besar dari 0 bulan!!<BR>"
        End If
        If fixDate.Text = "" Then
            fixDate.Text = Format(GetServerTime.Date(), "MM/dd/yyyy")
        Else
            Try
                Dim dtB As Date = CDate(fixDate.Text)
            Catch ex As Exception
                sMsg &= "- Format tanggal aset salah !!<BR>"
            End Try
        End If

        sSql = "SELECT COUNT(-1) FROM ql_trnfixmst WHERE cmpcode='" & Session("CompnyCode") & "' AND fixcode='" & Tchar(FixCode.Text) & "' AND fixoid < 0"
        If Session("oid") <> Nothing Or Session("oid") <> "" Then
            sSql &= " AND fixoid<>" & Session("oid")
        End If
        Dim hitungCode As Integer = Integer.Parse(GetStrData(sSql))
        If hitungCode > 0 Then
            sMsg &= "- Kode ini sudah digunakan oleh fixed aset yang lain!!<BR>"
        End If
        If sMsg <> "" Then
            showMessage(sMsg, 2)
            lblPOST.Text = "IN PROCESS"
            Exit Sub
        End If

        ' Generate oid
        Dim MstOid As String = GetStrData("SELECT isnull(MIN(fixoid)-1,-1) FROM ql_trnfixmst WHERE cmpcode='" & Session("CompnyCode") & "' AND fixoid < 0")
        Dim DtlOid As String = GenerateID("QL_trnfixdtl", Session("CompnyCode"))

        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "INSERT into QL_trnfixmst (cmpcode, fixoid, fixcode, fixdesc, fixgroup, fixdate, fixfirstvalue, fixdepmonth, fixdepval, fixlocation, fixpresentvalue, fixdepakum, fixperson, fixother, fixlastasset, acctgoid, payacctgoid, accumdepacctgoid, accumdepexpacctgoid, fapurchasemstoid, fapurchasedtloid,fixflag,curroid,rateoid,rate2oid,createuser,createtime,upduser,updtime) " & _
                " VALUES " & _
                "('" & Session("CompnyCode") & "', " & MstOid & ", '" & Tchar(FixCode.Text) & "', '" & Tchar(fixdesc.Text) & "', '" & fixgroup.SelectedValue & "','" & CDate(fixDate.Text) & "'," & ToDecimal(fixfirstvalue.Text) & "," & ToDecimal(fixdepmonth.Text) & "," & ToDecimal(fixdepval.Text) & ",'" & Tchar(fixlocation.Text) & "'," & ToDecimal(fixPresentValue.Text) & "," & ToDecimal(accumDV.Text) & ",'" & Tchar(fixperson.Text) & "','" & Tchar(fixother.Text) & "'," & ToDecimal(fixLastAsset.Text) & "," & DDLassets.SelectedValue & ",0," & DDLaccum.SelectedValue & "," & DDLadExpense.SelectedValue & ",0,0,'" & lblPOST.Text & "','" & CurroidDDL.SelectedValue & "','" & cRate.GetRateDailyOid & "','" & cRate.GetRateMonthlyOid & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                ' Update mst lastoid
                'sSql = "UPDATE QL_mstoid SET lastoid=" & MstOid & " WHERE tablename='QL_trnfixmst' AND cmpcode='" & DDLoutlet.SelectedValue & "'"
                'xCmd.CommandText = sSql
                'xCmd.ExecuteNonQuery()

            Else
                ' Update
                sSql = "UPDATE QL_trnfixmst SET fixcode='" & Tchar(FixCode.Text) & "', fixdesc='" & Tchar(fixdesc.Text.Trim) & "', fixfirstvalue=" & ToDecimal(fixfirstvalue.Text) & ", fixdepmonth=" & ToDecimal(fixdepmonth.Text) & ", fixdepval=" & ToDecimal(fixdepval.Text) & ", fixpresentvalue=" & ToDecimal(fixPresentValue.Text) & ", fixdepakum=" & ToDecimal(accumDV.Text) & ", fixlastasset=" & ToDecimal(fixLastAsset.Text) & ", acctgoid=" & DDLassets.SelectedValue & ", fixdate='" & CDate(fixDate.Text) & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, fixgroup='" & fixgroup.SelectedValue & "', fixflag='" & lblPOST.Text & "', accumdepacctgoid=" & DDLaccum.SelectedValue & ", accumdepexpacctgoid=" & DDLadExpense.SelectedValue & " WHERE fixoid = " & fixmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()

                sSql = "Delete from QL_trnfixdtl where cmpcode='" & Session("CompnyCode") & "' AND fixoid='" & fixmstoid.Text & "'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            If Not Session("tbldtl") Is Nothing Then
                Dim i As Integer
                Dim objTable As DataTable
                Dim objRow() As DataRow
                ' Insert
                objTable = Session("tbldtl")
                objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)

                For i = 0 To objRow.Length - 1
                    ' Insert new dtl
                    strSQL = "INSERT into QL_trnfixdtl (cmpcode, fixdtloid, fixoid, fixdtlseq, fixperiod, fixperiodvalue, fixperioddepvalue, fixperioddepaccum, depcostacctgoid, accdepacctgoid, fixnote, fixpostdate, fixflag, crtuser, crttime, upduser, updtime) " & _
                    " VALUES " & _
                    " ('" & Session("CompnyCode") & "'," & DtlOid & "," & fixmstoid.Text & "," & (i + 1) & ",'" & objRow(i)("fixperiod").ToString.Trim & "'," & objRow(i)("fixperiodvalue").ToString.Trim & "," & objRow(i)("fixperioddepvalue").ToString.Trim & ", " & objRow(i)("fixperioddepaccum").ToString.Trim & "," & objRow(i)("depcostacctgoid").ToString.Trim & "," & objRow(i)("accdepacctgoid").ToString.Trim & ",'',CURRENT_TIMESTAMP,'" & objRow(i)("fixflag").ToString.Trim & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP)"
                    xCmd.CommandText = strSQL
                    xCmd.ExecuteNonQuery()
                    DtlOid += 1
                Next
                ' Update dtl lastoid
                sSql = "UPDATE QL_mstoid SET lastoid=" & (DtlOid + objRow.Length - 1) & " WHERE cmpcode='" & Session("CompnyCode") & "' AND tablename='QL_trnfixdtl'"
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If

            objTrans.Commit()
            objConn.Close()
            Session("tbldtl") = Nothing
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            lblPOST.Text = "IN PROCESS"
            showMessage(ex.ToString, 1)
            Exit Sub
        End Try
        Session("dtlTable") = Nothing
        Session("VIDCashBank") = Nothing
        Session("VIDCost") = Nothing
        Session("oid") = Nothing

        If lblPOST.Text = "POST" Then
            Session("SavedInfo") &= "Data telah diposting !!! Fix Asset Code = " & FixCode.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAssetBalance.aspx?awal=true")
        End If
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim strSQL As String
        Dim objTrans As SqlClient.SqlTransaction
        If objConn.State = ConnectionState.Closed Then
            objConn.Open()
        End If
        objTrans = objConn.BeginTransaction()
        xCmd.Connection = objConn
        xCmd.Transaction = objTrans
        Try
            'delete dtl
            strSQL = "DELETE FROM QL_trnfixdtl where fixoid=" & fixmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            ' delete mst
            strSQL = "DELETE FROM QL_trnfixmst where fixoid=" & fixmstoid.Text & " AND cmpcode='" & Session("CompnyCode") & "'"
            xCmd.CommandText = strSQL
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            Session("tbldtl") = Nothing
            objConn.Close()
        Catch ex As Exception
            objTrans.Rollback()
            objConn.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Session("tbldtl") = Nothing
        Session("oid") = Nothing
        If lblPOST.Text <> "" Then
            Session("SavedInfo") &= " - Data telah dihapus !!! Fix Asset Code = " & FixCode.Text
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            showMessage(Session("SavedInfo"), 3)
        Else
            Response.Redirect("~\Accounting\trnFixAssetBalance.aspx?awal=true")
        End If
    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        ButtonSave.Enabled = True
        Session("oid") = Nothing
        Session("tbldtl") = Nothing
        Response.Redirect("trnFixAssetBalance.aspx")
    End Sub

    Protected Sub btnPosting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        lblPOST.Text = "POST"
        ButtonSave_Click(sender, e)
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\accounting\trnFixAssetBalance.aspx?awal=true")
            End If
        End If
    End Sub
#End Region
End Class
