Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_CashBankMutation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid2.Text = "" Then
            sError &= "- Please select MUTATION FROM ACCOUNT field!<BR>"
        End If
        If cashbankglamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(cashbankglamt.Text) <= 0 Then
                sError &= "- AMOUNT must be more than 0!<BR>"
            End If
        End If
        If addacctgoiddtl1.SelectedValue <> 0 Then
            If ToDouble(addacctgamtdtl1.Text) <= 0 Then
                sError &= "- Additional Cost Amount 1 must be greater than 0!<BR>"
            End If
        End If
        If addacctgoiddtl2.SelectedValue <> 0 Then
            If ToDouble(addacctgamtdtl2.Text) <= 0 Then
                sError &= "- Additional Cost Amount 2 must be greater than 0!<BR>"
            End If
            If addacctgoiddtl1.SelectedValue <> 0 Then
                If addacctgoiddtl1.SelectedValue = addacctgoiddtl2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoiddtl3.SelectedValue <> 0 Then
            If ToDouble(addacctgamtdtl3.Text) <= 0 Then
                sError &= "- Additional Cost Amount 3 must be greater than 0!<BR>"
            End If
            If addacctgoiddtl1.SelectedValue <> 0 Then
                If addacctgoiddtl1.SelectedValue = addacctgoiddtl3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoiddtl2.SelectedValue <> 0 Then
                If addacctgoiddtl2.SelectedValue = addacctgoiddtl3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sError &= "- Please fill MUTATION DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- MUTATION DATE is invalid. " & sErr & "<BR>"
            Else
                If CDate(cashbankdate.Text) > Today Then
                    sError &= "- MUTATION DATE must less or equal then TODAY!<BR>"
                End If
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select MUTATION TO ACCOUNT field!<BR>"
        End If
        If cashbanktype.SelectedValue <> "BKK" Then
            If cashbankduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than MUTATION DATE!<BR>"
                    End If
                End If
            End If
            If cashbanktype.SelectedValue = "BGK" Then
                If cashbanktakegiro.Text = "" Then
                    sError &= "- Please fill DATE TAKE GIRO field!<BR>"
                Else
                    If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
                        sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
                    Else
                        If CDate(cashbankdate.Text) > CDate(cashbanktakegiro.Text) Then
                            sError &= "- DATE TAKE GIRO must be more or equal than MUTATION DATE!<BR>"
                        End If
                    End If
                End If
            End If
        End If
        If personoid.SelectedValue = "" Then
            sError &= "- Please select PIC field!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If
        If addacctgoid1.SelectedValue <> 0 Then
            If ToDouble(addacctgamt1.Text) <= 0 Then
                sError &= "- Additional Cost Amount 1 must be greater than 0!<BR>"
            End If
        End If
        If addacctgoid2.SelectedValue <> 0 Then
            If ToDouble(addacctgamt2.Text) <= 0 Then
                sError &= "- Additional Cost Amount 2 must be greater than 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid2.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 2 must be in different account!<BR>"
                End If
            End If
        End If
        If addacctgoid3.SelectedValue <> 0 Then
            If ToDouble(addacctgamt3.Text) <= 0 Then
                sError &= "- Additional Cost Amount 3 must be greater than 0!<BR>"
            End If
            If addacctgoid1.SelectedValue <> 0 Then
                If addacctgoid1.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 1 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
            If addacctgoid2.SelectedValue <> 0 Then
                If addacctgoid2.SelectedValue = addacctgoid3.SelectedValue Then
                    sError &= "- Additional Cost 2 and Additional Cost 3 must be in different account!<BR>"
                End If
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetCashBankType(ByVal iAcctgoid As Integer, ByVal sType As String) As String
        GetCashBankType = ""
		Dim sCode As String = GetStrData("SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH' AND interfaceres1='" & CompnyCode & "'")
        If sCode = "" Then
			sCode = GetStrData("SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_CASH' AND interfaceres1='" & CompnyCode & "'")
        End If
        If sCode <> "" Then
            sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND acctgoid=" & iAcctgoid & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C2 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                If C2 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND acctgoid NOT IN (SELECT DISTINCT acctggrp3 FROM QL_mstacctg WHERE acctggrp3 IS NOT NULL AND cmpcode='" & CompnyCode & "')"
            If CheckDataExists(sSql) Then
                If sType = "CB" Then
                    GetCashBankType = "BKM"
                ElseIf sType = "COST" Then
                    GetCashBankType = "BKK"
                End If
            End If
        End If
		sCode = GetStrData("SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK' AND interfaceres1='" & CompnyCode & "'")
        If sCode = "" Then
			sCode = GetStrData("SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_BANK' AND interfaceres1='" & CompnyCode & "'")
        End If
        If sCode <> "" Then
            sSql = "SELECT COUNT(*) FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' AND acctgoid=" & iAcctgoid & " AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C2 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "acctgcode LIKE '" & LTrim(sSplitCode(C2)) & "%'"
                If C2 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND acctgoid NOT IN (SELECT DISTINCT acctggrp3 FROM QL_mstacctg WHERE acctggrp3 IS NOT NULL AND cmpcode='" & CompnyCode & "')"
            If CheckDataExists(sSql) Then
                If sType = "CB" Then
                    GetCashBankType = "BBM"
                ElseIf sType = "COST" Then
                    GetCashBankType = "BBK"
                End If
            End If
        End If
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckMutationStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='MUTATION'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnMutation.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbMutationInProcess.Visible = True
            lkbMutationInProcess.Text = "You have " & GetStrData(sSql) & " In Process Cash/Bank Overbooking data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLPerson()
        End If
        ' Fill DDL Currency
        'sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        'FillDDL(curroid, sSql)
        InitDDLAdd()
    End Sub

    Private Sub CurDDL(ByVal Curr As String)
        ' Fill DDL Currency
        sSql = "SELECT DISTINCT a.curroid, currcode FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='PRKP' AND c.activeflag='ACTIVE' " & Curr & " "
        FillDDL(curroid, sSql)
    End Sub

    Private Sub InitDDLAdd()
        ' COA ADDITIONAL 1,2,3
        FillDDLAcctg(addacctgoid1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid1.Items.Count < 1 Then FillDDL(addacctgoid1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        FillDDLAcctg(addacctgoid2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid2.Items.Count < 1 Then FillDDL(addacctgoid2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        FillDDLAcctg(addacctgoid3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoid3.Items.Count < 1 Then FillDDL(addacctgoid3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
        FillDDLAcctg(addacctgoiddtl1, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoiddtl1.Items.Count < 1 Then FillDDL(addacctgoiddtl1, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoiddtl1_SelectedIndexChanged(addacctgoiddtl1, Nothing)
        FillDDLAcctg(addacctgoiddtl2, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoiddtl2.Items.Count < 1 Then FillDDL(addacctgoiddtl2, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoiddtl2_SelectedIndexChanged(addacctgoiddtl2, Nothing)
        FillDDLAcctg(addacctgoiddtl3, "VAR_ADD_COST", DDLBusUnit.SelectedValue, "0", "NONE")
        If addacctgoiddtl3.Items.Count < 1 Then FillDDL(addacctgoiddtl3, "SELECT 0 AS value,'NONE' AS text ")
        addacctgoiddtl3_SelectedIndexChanged(addacctgoiddtl3, Nothing)
    End Sub

    Private Sub InitDDLPerson()
        ' Fill DDL PIC
        'sSql = "SELECT personoid, personname FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        'FillDDL(personoid, sSql)
        'Try
        '    personoid.SelectedValue = GetStrData("SELECT personoid FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'")
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "select * from (SELECT cb.cashbankoid, cb.cashbankno, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, (CASE cashbanktype WHEN 'BKK' THEN 'CASH' WHEN 'BBK' THEN 'BANK' ELSE 'GIRO' END) AS cashbanktype, (a.acctgcode + ' - ' + a.acctgdesc) AS acctgdesc, cb.cashbankstatus, cb.cashbanknote, 'False' AS checkvalue, isnull((select top 1 h.cashbankno from QL_trncashbankgl d inner join QL_trncashbankmst h on h.cashbankoid=d.cashbankoid where d.cashbankglres1=cb.cashbankoid), '') cashbankno_to, cb.cashbankdate filter_date, (select top 1 dd.acctgdesc from QL_trncashbankgl d inner join QL_mstacctg dd on dd.acctgoid=d.acctgoid where d.cashbankoid=cb.cashbankoid) acctgdesc_to FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid WHERE cb.cashbankgroup='MUTATION' AND cb.cmpcode='" & Session("CompnyCode") & "') t where 1=1"
        sSql &= sSqlPlus & " ORDER BY filter_date DESC, t.cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub EnableAddInfo(ByVal bVal As Boolean)
        lblDueDate.Visible = bVal
        lblWarnDueDate.Visible = bVal
        lblSeptDueDate.Visible = bVal
        cashbankduedate.Visible = bVal
        imbDueDate.Visible = bVal
        lblInfoDueDate.Visible = bVal
    End Sub

    Private Sub EnableAddInfo2(ByVal bVal As Boolean)
        lblDTG.Visible = bVal
        lblDTG.Visible = bVal
        lblSeptDTG.Visible = bVal
        cashbanktakegiro.Visible = bVal
        imbDTG.Visible = bVal
        lblInfoDTG.Visible = bVal
    End Sub

    Private Sub GenerateMutationNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & acctgoid.SelectedValue
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindListCOA()
        If cashbanktype.SelectedValue = "BGK" Then
            Dim sVar() As String = {"VAR_BANK"}
            FillGVAcctg(gvListCOA, sVar, DDLBusUnit.SelectedValue, "AND " & FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%' AND a.acctgoid<>" & acctgoid.SelectedValue & "")
        Else
            Dim sVar() As String = {"VAR_CASH", "VAR_BANK"}
            FillGVAcctg(gvListCOA, sVar, DDLBusUnit.SelectedValue, "AND " & FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%' AND a.acctgoid<>" & acctgoid.SelectedValue & "")
        End If
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        cashbankglseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                cashbankglseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        acctgoid2.Text = ""
        acctgcode.Text = ""
        acctgdesc.Text = ""
        cashbankglamt.Text = ""
        addacctgoiddtl1.SelectedIndex = 0 : addacctgamtdtl1.Text = ""
        addacctgoiddtl1_SelectedIndexChanged(addacctgoiddtl1, Nothing)
        addacctgoiddtl2.SelectedIndex = 0 : addacctgamtdtl2.Text = ""
        addacctgoiddtl2_SelectedIndexChanged(addacctgoiddtl2, Nothing)
        addacctgoiddtl3.SelectedIndex = 0 : addacctgamtdtl3.Text = ""
        addacctgoiddtl3_SelectedIndexChanged(addacctgoiddtl3, Nothing)
        cashbankglnote.Text = ""
        gvDtl.Columns(0).Visible = True : gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = True
        gvDtl.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        cashbanktype.CssClass = sCss : cashbanktype.Enabled = bVal
        acctgoid.CssClass = sCss : acctgoid.Enabled = bVal
        DDLBusUnit.CssClass = sCss : DDLBusUnit.Enabled = bVal
        rdbAdd.Enabled = bVal
    End Sub

    Private Sub CountTotalAmt()
        Dim dVal As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dVal += ToDouble(dt.Rows(C1)("cashbankglamt").ToString)
            Next
        End If
        cashbankamt.Text = ToMaskEdit(dVal, 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, acctgoid, curroid, cashbankamt, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime,cashbankresamt, cb.addacctgoid1, cb.addacctgamt1, cb.addacctgoid2, cb.addacctgamt2, cb.addacctgoid3, cb.addacctgamt3," & _
                "(SELECT SUM(cbgl.addacctgamt1) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode=cb.cmpcode AND cbgl.cashbankoid=cb.cashbankoid) sumcost1," & _
                "(SELECT SUM(cbgl.addacctgamt2) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode=cb.cmpcode AND cbgl.cashbankoid=cb.cashbankoid) sumcost2," & _
                "(SELECT SUM(cbgl.addacctgamt3) FROM QL_trncashbankgl cbgl WHERE cbgl.cmpcode=cb.cmpcode AND cbgl.cashbankoid=cb.cashbankoid) sumcost3, cashbanktakegiro, giroacctgoid FROM QL_trncashbankmst cb WHERE cb.cashbankoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                cashbanktakegiro.Text = Format(xreader("cashbanktakegiro"), "MM/dd/yyyy")
                cashbankrefno.Text = Trim(xreader("cashbankrefno").ToString)
                giroacctgoid.Text = Trim(xreader("giroacctgoid").ToString)
                cashbanktype.SelectedValue = Trim(xreader("cashbanktype").ToString)
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                If ToDouble(Trim(xreader("sumcost1").ToString)) > 0 Or ToDouble(Trim(xreader("sumcost2").ToString)) > 0 Or ToDouble(Trim(xreader("sumcost3").ToString)) > 0 Then
                    rdbAdd.SelectedIndex = 1
                End If
                rdbAdd_SelectedIndexChanged(rdbAdd, Nothing)
                addacctgoid1.SelectedValue = xreader("addacctgoid1").ToString
                addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
                addacctgamt1.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt1").ToString)), 4)
                addacctgoid2.SelectedValue = xreader("addacctgoid2").ToString
                addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
                addacctgamt2.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt2").ToString)), 4)
                addacctgoid3.SelectedValue = xreader("addacctgoid3").ToString
                addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
                addacctgamt3.Text = ToMaskEdit(ToDouble(Trim(xreader("addacctgamt3").ToString)), 4)
                personoid.SelectedValue = Trim(xreader("personoid").ToString)
                cashbanknote.Text = Trim(xreader("cashbanknote").ToString)
                cashbankstatus.Text = Trim(xreader("cashbankstatus").ToString)
                If ToDouble(Trim(xreader("cashbankresamt").ToString)) > 0 Then
                    cashbankresamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankresamt").ToString)), 4)
                End If
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        cashbanktype.CssClass = "inpTextDisabled" : cashbanktype.Enabled = False
        acctgoid.CssClass = "inpTextDisabled" : acctgoid.Enabled = False
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            btnShowCOA.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, '' AS cashbankno, '' AS cashbanktype, cbgl.addacctgoid1 addacctgoiddtl1, cbgl.addacctgamt1 addacctgamtdtl1, cbgl.addacctgoid2 addacctgoiddtl2, cbgl.addacctgamt2 addacctgamtdtl2, cbgl.addacctgoid3 addacctgoiddtl3, cbgl.addacctgamt3 addacctgamtdtl3, '' AS cashbanknocost, '' AS cashbanktypecost FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" & sOid
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankgl")
        For C1 As Integer = 0 To dtTable.Rows.Count - 1
            dtTable.Rows(C1)("cashbankglseq") = C1 + 1
        Next
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
        CountTotalAmt()
        ReAmountCost()
    End Sub

    Private Sub InsertCashBankNo()
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                Dim sType As String = "BGM"
                If cashbanktype.SelectedValue <> "BGK" Then
                    sType = GetCashBankType(dt.Rows(C1)("acctgoid"), "CB")
                End If
				Dim sNo As String = sType & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%' --AND acctgoid=" & dt.Rows(C1)("acctgoid")
                If GetStrData(sSql) = "" Then
                    sNo = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                Else
                    sNo = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                End If
                dt.Rows(C1)("cashbanktype") = sType
                dt.Rows(C1)("cashbankno") = sNo

                Dim sTypeCost As String = "BGK"
                If cashbanktype.SelectedValue <> "BGK" Then
                    sTypeCost = GetCashBankType(dt.Rows(C1)("acctgoid"), "COST")
                End If
				Dim sNoCost As String = sTypeCost & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNoCost & "%' --AND acctgoid=" & dt.Rows(C1)("acctgoid")
                If GetStrData(sSql) = "" Then
                    sNoCost = GenNumberString(sNoCost, "", 1, DefaultFormatCounter)
                Else
                    sNoCost = GenNumberString(sNoCost, "", GetStrData(sSql), DefaultFormatCounter)
                End If
                dt.Rows(C1)("cashbanktypecost") = sTypeCost
                dt.Rows(C1)("cashbanknocost") = sNoCost
            Next
            dt.AcceptChanges()
            Session("TblDtl") = dt
        End If
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    If FilterDDLPrintOption.SelectedIndex = 0 Then
                        sOid &= dv(C1)("cashbankoid").ToString & ","
                    Else
                        sOid &= "'" & dv(C1)("cashbankoid").ToString & "',"
                    End If
                Next
                dv.RowFilter = ""
            End If
            Dim sWhere As String = ""
            If FilterDDLPrintOption.SelectedIndex = 0 Then
                report.Load(Server.MapPath(folderReport & "rptMutation.rpt"))
                sWhere = "WHERE cashbankgroup='MUTATION'"
            Else
                report.Load(Server.MapPath(folderReport & "rptMutation2.rpt"))
                sWhere = "WHERE cashbankgroup='MUTATIONTO'"
            End If
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnMutation.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                If FilterDDLPrintOption.SelectedIndex = 0 Then
                    sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
                Else
                    sWhere &= " AND cb.cashbankoid IN (SELECT glx.cashbankoid FROM QL_trncashbankgl glx WHERE glx.cashbankglres1 IN (" & sOid & "))"
                End If
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "OverbookingPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnMutation.aspx?awal=true")
    End Sub

    Private Sub SetControlCost(ByVal bStateHeader As Boolean) ' True = Cost on Header
        TDAcc11.Visible = bStateHeader : TDAcc12.Visible = bStateHeader : TDAcc13.Visible = bStateHeader
        TDAcc21.Visible = bStateHeader : TDAcc22.Visible = bStateHeader : TDAcc23.Visible = bStateHeader
        TDAcc31.Visible = bStateHeader : TDAcc32.Visible = bStateHeader : TDAcc33.Visible = bStateHeader
        TDAcc41.Visible = bStateHeader : TDAcc42.Visible = bStateHeader : TDAcc43.Visible = bStateHeader
        TDAmt11.Visible = bStateHeader : TDAmt12.Visible = bStateHeader : TDAmt13.Visible = bStateHeader
        TDAmt21.Visible = bStateHeader : TDAmt22.Visible = bStateHeader : TDAmt23.Visible = bStateHeader
        TDAmt31.Visible = bStateHeader : TDAmt32.Visible = bStateHeader : TDAmt33.Visible = bStateHeader
        TDAmt41.Visible = bStateHeader : TDAmt42.Visible = bStateHeader : TDAmt43.Visible = bStateHeader

        addacctgoid1.SelectedIndex = 0 : addacctgoid2.SelectedIndex = 0 : addacctgoid3.SelectedIndex = 0
        addacctgoid1_SelectedIndexChanged(addacctgoid1, Nothing)
        addacctgoid2_SelectedIndexChanged(addacctgoid2, Nothing)
        addacctgoid3_SelectedIndexChanged(addacctgoid3, Nothing)
        addacctgamt1.Text = "" : addacctgamt2.Text = "" : addacctgamt3.Text = ""
        ReAmountCost()

        TDAccDtl11.Visible = Not bStateHeader : TDAccDtl12.Visible = Not bStateHeader : TDAccDtl13.Visible = Not bStateHeader
        TDAccDtl21.Visible = Not bStateHeader : TDAccDtl22.Visible = Not bStateHeader : TDAccDtl23.Visible = Not bStateHeader
        TDAccDtl31.Visible = Not bStateHeader : TDAccDtl32.Visible = Not bStateHeader : TDAccDtl33.Visible = Not bStateHeader
        TDAmtDtl11.Visible = Not bStateHeader : TDAmtDtl12.Visible = Not bStateHeader : TDAmtDtl13.Visible = Not bStateHeader
        TDAmtDtl21.Visible = Not bStateHeader : TDAmtDtl22.Visible = Not bStateHeader : TDAmtDtl23.Visible = Not bStateHeader
        TDAmtDtl31.Visible = Not bStateHeader : TDAmtDtl32.Visible = Not bStateHeader : TDAmtDtl33.Visible = Not bStateHeader

        gvDtl.Columns(5).Visible = Not bStateHeader
        gvDtl.Columns(6).Visible = Not bStateHeader
        gvDtl.Columns(7).Visible = Not bStateHeader

        addacctgoiddtl1.SelectedIndex = 0 : addacctgoiddtl2.SelectedIndex = 0 : addacctgoiddtl3.SelectedIndex = 0
        addacctgoiddtl1_SelectedIndexChanged(addacctgoiddtl1, Nothing)
        addacctgoiddtl2_SelectedIndexChanged(addacctgoiddtl2, Nothing)
        addacctgoiddtl3_SelectedIndexChanged(addacctgoiddtl3, Nothing)
        addacctgamtdtl1.Text = "" : addacctgamtdtl2.Text = "" : addacctgamtdtl3.Text = ""

        ' Reset Detail 
    End Sub

    Private Sub ReAmountCost()
        cbnett.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(addacctgamt1.Text) + ToDouble(addacctgamt2.Text) + ToDouble(addacctgamt3.Text), 4)
    End Sub

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                curroid.SelectedValue = sCurrOid
            Else
                curroid.Items.Clear()
                showMessage("Please define Currency for selected Payment Account!", 2)
            End If
        Else
            curroid.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnMutation.aspx")
        End If
        If checkPagePermission("~\Accounting\trnMutation.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Cash/Bank Overbooking"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckMutationStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            CurDDL("")
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnMutation.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbMutationInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbMutationInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, t.filter_date, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' "
        If checkPagePermission("~\Accounting\trnMutation.aspx", Session("SpecialAccess")) = False Then
            'sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND t.filter_date>='" & FilterPeriod1.Text & " 00:00:00' AND t.filter_date<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND t.cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnMutation.aspx", Session("SpecialAccess")) = False Then
            'sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnMutation.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLPerson()
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateMutationNo()
        End If
        btnClearCOA_Click(Nothing, Nothing)
        InitDDLAdd()
    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankdate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateMutationNo()
        End If
        cashbankduedate.Text = cashbankdate.Text
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbanktype.SelectedIndexChanged
        If cashbanktype.SelectedValue = "BKK" Then
            cashbankduedate.Text = cashbankdate.Text : cashbanktakegiro.Text = "" : giroacctgoid.Text = "0"
            EnableAddInfo(False)
            EnableAddInfo2(False)
            FillDDLAcctg(acctgoid, "VAR_CASH", DDLBusUnit.SelectedValue)
        ElseIf cashbanktype.SelectedValue = "BBK" Then
            cashbanktakegiro.Text = "" : giroacctgoid.Text = "0"
            EnableAddInfo(True)
            EnableAddInfo2(False)
            FillDDLAcctg(acctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        Else
            giroacctgoid.Text = GetAcctgOID(GetVarInterface("VAR_GIRO", DDLBusUnit.SelectedValue), CompnyCode)
            EnableAddInfo(True)
            EnableAddInfo2(True)
            FillDDLAcctg(acctgoid, "VAR_BANK", DDLBusUnit.SelectedValue)
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' AND a.acctgoid='" & acctgoid.SelectedValue & "'"
        Dim sCurrOid As Integer = Integer.Parse(GetStrData(sSql))
        If sCurrOid <> 0 Then
            CurDDL("AND a.acctgoid='" & acctgoid.SelectedValue & "'")
        Else
            CurDDL("")
            'showMessage("Please define Currency for selected Payment Account!", 2)
        End If
        SetCOACurrency(acctgoid.SelectedValue)
        'SetCOACurrency(acctgoid.SelectedValue)
        If i_u.Text = "New Data" Then
            GenerateMutationNo()
        End If
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateMutationNo()
        End If
        sSql = "SELECT COUNT(*) FROM QL_mstcurr c INNER JOIN QL_mstacctg a ON a.curroid=c.curroid WHERE a.cmpcode='" & DDLBusUnit.SelectedValue & "' AND c.activeflag='ACTIVE' AND a.acctgoid='" & acctgoid.SelectedValue & "'"
        Dim sCurrOid As Integer = Integer.Parse(GetStrData(sSql))
        If sCurrOid <> 0 Then
            CurDDL("AND a.acctgoid='" & acctgoid.SelectedValue & "'")
        Else
            CurDDL("")
            'showMessage("Please define Currency for selected Payment Account!", 2)
        End If
        SetCOACurrency(acctgoid.SelectedValue)
    End Sub

    Protected Sub rdbAdd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbAdd.SelectedIndexChanged
        SetControlCost(rdbAdd.SelectedValue.ToUpper = "HEADER")
    End Sub

    Protected Sub addacctgoid1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid1.SelectedIndexChanged
        addacctgamt1.Text = ""
        addacctgamt1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt1)
    End Sub

    Protected Sub addacctgoid2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid2.SelectedIndexChanged
        addacctgamt2.Text = ""
        addacctgamt2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt2)
    End Sub

    Protected Sub addacctgoid3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoid3.SelectedIndexChanged
        addacctgamt3.Text = ""
        addacctgamt3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamt3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        ReAmountCost()
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamt3)
    End Sub

    Protected Sub addacctgoiddtl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoiddtl1.SelectedIndexChanged
        addacctgamtdtl1.Text = ""
        addacctgamtdtl1.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamtdtl1.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamtdtl1)
    End Sub

    Protected Sub addacctgoiddtl2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoiddtl2.SelectedIndexChanged
        addacctgamtdtl2.Text = ""
        addacctgamtdtl2.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamtdtl2.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamtdtl2)
    End Sub

    Protected Sub addacctgoiddtl3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addacctgoiddtl3.SelectedIndexChanged
        addacctgamtdtl3.Text = ""
        addacctgamtdtl3.Enabled = IIf(sender.SelectedValue = 0, False, True)
        addacctgamtdtl3.CssClass = IIf(sender.SelectedValue = 0, "inpTextDisabled", "inpText")
        If sender.SelectedValue <> 0 Then cProc.SetFocusToControl(Me.Page, addacctgamtdtl3)
    End Sub

    Protected Sub addacctgamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    addacctgamt1.TextChanged, addacctgamt2.TextChanged, addacctgamt3.TextChanged, addacctgamtdtl1.TextChanged, addacctgamtdtl2.TextChanged, addacctgamtdtl3.TextChanged
        sender.Text = ToMaskEdit(ToDouble(sender.Text), 4)
        ReAmountCost()
    End Sub

    Protected Sub btnSearchCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCOA.Click
        If acctgoid.SelectedValue = "" Then
            showMessage("Please select Overbooking To Account first!", 2)
            Exit Sub
        End If
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
    End Sub

    Protected Sub btnClearCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCOA.Click
        acctgoid2.Text = ""
        acctgdesc.Text = ""
        acctgcode.Text = ""
    End Sub

    Protected Sub btnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCOA.Click
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub btnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCOA.Click
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCOA.PageIndexChanging
        gvListCOA.PageIndex = e.NewPageIndex
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCOA.SelectedIndexChanged
        If acctgoid2.Text <> gvListCOA.SelectedDataKey.Item("acctgoid").ToString Then
            btnClearCOA_Click(Nothing, Nothing)
        End If
        acctgoid2.Text = gvListCOA.SelectedDataKey.Item("acctgoid").ToString
        acctgcode.Text = gvListCOA.SelectedDataKey.Item("acctgcode").ToString
        acctgdesc.Text = gvListCOA.SelectedDataKey.Item("acctgdesc").ToString
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub lkbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCOA.Click
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = New DataTable("TabelMutation")
                dtlTable.Columns.Add("cashbankglseq", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
                dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbankglamt", Type.GetType("System.Decimal"))
                dtlTable.Columns.Add("cashbankglnote", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbankno", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbanktype", Type.GetType("System.String"))
                dtlTable.Columns.Add("addacctgoiddtl1", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("addacctgamtdtl1", Type.GetType("System.Decimal"))
                dtlTable.Columns.Add("addacctgoiddtl2", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("addacctgamtdtl2", Type.GetType("System.Decimal"))
                dtlTable.Columns.Add("addacctgoiddtl3", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("addacctgamtdtl3", Type.GetType("System.Decimal"))
                dtlTable.Columns.Add("cashbanknocost", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbanktypecost", Type.GetType("System.String"))
                Session("TblDtl") = dtlTable
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "'"
            Else
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' AND cashbankglseq<>" & cashbankglseq.Text
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                cashbankglseq.Text = objTable.Rows.Count + 1
                objRow("cashbankglseq") = cashbankglseq.Text
            Else
                objRow = objTable.Rows(cashbankglseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("acctgoid") = acctgoid2.Text
            objRow("acctgcode") = acctgcode.Text
            objRow("acctgdesc") = acctgdesc.Text
            objRow("cashbankglamt") = ToDouble(cashbankglamt.Text)
            objRow("addacctgoiddtl1") = addacctgoiddtl1.SelectedValue
            objRow("addacctgamtdtl1") = ToDouble(addacctgamtdtl1.Text)
            objRow("addacctgoiddtl2") = addacctgoiddtl2.SelectedValue
            objRow("addacctgamtdtl2") = ToDouble(addacctgamtdtl2.Text)
            objRow("addacctgoiddtl3") = addacctgoiddtl3.SelectedValue
            objRow("addacctgamtdtl3") = ToDouble(addacctgamtdtl3.Text)
            objRow("cashbankglnote") = cashbankglnote.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalAmt()
            ReAmountCost()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
            If gvDtl.Columns(5).Visible Then e.Row.Cells(5).Text = ToMaskEdit(ToDouble(e.Row.Cells(5).Text), 4)
            If gvDtl.Columns(6).Visible Then e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
            If gvDtl.Columns(7).Visible Then e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            cashbankglseq.Text = gvDtl.SelectedDataKey.Item("cashbankglseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "cashbankglseq=" & cashbankglseq.Text
                acctgoid2.Text = dv.Item(0).Item("acctgoid").ToString
                acctgcode.Text = dv.Item(0).Item("acctgcode").ToString
                acctgdesc.Text = dv.Item(0).Item("acctgdesc").ToString
                cashbankglamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("cashbankglamt").ToString), 4)

                addacctgoiddtl1.SelectedValue = dv.Item(0).Item("addacctgoiddtl1").ToString
                addacctgoiddtl1_SelectedIndexChanged(addacctgoiddtl1, Nothing)
                addacctgamtdtl1.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("addacctgamtdtl1").ToString), 4)
                addacctgoiddtl2.SelectedValue = dv.Item(0).Item("addacctgoiddtl2").ToString
                addacctgoiddtl2_SelectedIndexChanged(addacctgoiddtl2, Nothing)
                addacctgamtdtl2.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("addacctgamtdtl2").ToString), 4)
                addacctgoiddtl3.SelectedValue = dv.Item(0).Item("addacctgoiddtl3").ToString
                addacctgoiddtl3_SelectedIndexChanged(addacctgoiddtl3, Nothing)
                addacctgamtdtl3.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("addacctgamtdtl3").ToString), 4)

                gvDtl.Columns(0).Visible = False : gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
                cashbankglnote.Text = dv.Item(0).Item("cashbankglnote").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("cashbankglseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        CountTotalAmt()
        ReAmountCost()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            Dim iCashBankOid As Integer = 0
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                Dim sNo As String = cashbankno.Text
                GenerateMutationNo()
                If sNo <> cashbankno.Text Then
                    isRegenOid = True
                End If
                iCashBankOid = ToInteger(cashbankoid.Text) + 1
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
                iCashBankOid = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
            End If
            cashbankgloid.Text = GenerateID("QL_TRNCASHBANKGL", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim cRate As New ClassRate()
            periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
            Dim iLastBBM As Integer = 0, iLastBKM As Integer = 0
            Dim iSelisihAcctgOid As Integer = 0, iAyatSilangAcctgOid As Integer = 0
            Dim dUsedRateIDR As Double = 0
            Dim dUsedRateIDRforCostDtl As Double = 0
            Dim iGiroAcctgIn As Integer = 0
            Dim sDate As String = cashbankdate.Text
            Dim sDate_to As String = cashbankduedate.Text
            If cashbanktype.SelectedValue = "BBK" Then
                'sDate = cashbankduedate.Text
            End If
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            Dim sPeriod_to As String = GetDateToPeriodAcctg(CDate(sDate_to))

            If cashbankstatus.Text = "Post" Then
				If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sDate) Then
					showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : cashbankstatus.Text = "In Process" : Exit Sub
				End If
                cRate.SetRateValue(CInt(curroid.SelectedValue), IIf(cashbanktype.SelectedValue = "BBK", cashbankduedate.Text, cashbankdate.Text))
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                If Not IsInterfaceExists("VAR_AYAT_SILANG", DDLBusUnit.SelectedValue) Then
                    showMessage(GetInterfaceWarning("VAR_AYAT_SILANG", "posting"), 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                Else
                    iAyatSilangAcctgOid = GetAcctgOID(GetVarInterface("VAR_AYAT_SILANG", DDLBusUnit.SelectedValue), CompnyCode)
                End If
                If ToDouble(cashbankresamt.Text) > 0 Then
                    If Not IsInterfaceExists("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue) Then
                        showMessage(GetInterfaceWarning("VAR_DIFF_CURR_IDR", "posting"), 2)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    Else
                        iSelisihAcctgOid = GetAcctgOID(GetVarInterface("VAR_DIFF_CURR_IDR", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If cashbanktype.SelectedValue = "BGK" Then
                    If Not IsInterfaceExists("VAR_GIRO_IN", DDLBusUnit.SelectedValue) Then
                        showMessage(GetInterfaceWarning("VAR_GIRO_IN", "posting"), 2)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    Else
                        iGiroAcctgIn = GetAcctgOID(GetVarInterface("VAR_GIRO_IN", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                InsertCashBankNo()
			End If

            If ToDouble(cashbankresamt.Text) > 0 Then
                dUsedRateIDR = ToDouble(cashbankresamt.Text)
                dUsedRateIDRforCostDtl = ToDouble(cashbankresamt.Text)
            Else
                dUsedRateIDR = cRate.GetRateMonthlyIDRValue
                dUsedRateIDRforCostDtl = cRate.GetRateMonthlyIDRValue
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbankresamt, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3, giroacctgoid, cashbanktakegiro) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'MUTATION', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(cbnett.Text) & ", " & ToDouble(cbnett.Text) * dUsedRateIDR & ", " & ToDouble(cbnett.Text) * cRate.GetRateMonthlyUSDValue & ", " & personoid.SelectedValue & ", '" & IIf(cashbanktype.SelectedValue <> "BKK", cashbankdate.Text, "1/1/1900") & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankresamt.Text) & ", " & addacctgoid1.SelectedValue & ", " & ToDouble(addacctgamt1.Text) & ", " & addacctgoid2.SelectedValue & ", " & ToDouble(addacctgamt2.Text) & ", " & addacctgoid3.SelectedValue & ", " & ToDouble(addacctgamt3.Text) & ", " & giroacctgoid.Text & ", '" & IIf(cashbanktype.SelectedValue = "BGK", cashbanktakegiro.Text, "1/1/1900") & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(cbnett.Text) & ", cashbankamtidr=" & ToDouble(cbnett.Text) * dUsedRateIDR & ", cashbankamtusd=" & ToDouble(cbnett.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & personoid.SelectedValue & ", cashbankduedate='" & IIf(cashbanktype.SelectedValue <> "BKK", cashbankdate.Text, "1/1/1900") & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbankresamt=" & ToDouble(cashbankresamt.Text) & ", addacctgoid1=" & addacctgoid1.SelectedValue & ", addacctgamt1=" & ToDouble(addacctgamt1.Text) & ", addacctgoid2=" & addacctgoid2.SelectedValue & ", addacctgamt2=" & ToDouble(addacctgamt2.Text) & ", addacctgoid3=" & addacctgoid3.SelectedValue & ", addacctgamt3=" & ToDouble(addacctgamt3.Text) & ", giroacctgoid=" & giroacctgoid.Text & ", cashbanktakegiro='" & IIf(cashbanktype.SelectedValue = "BGK", cashbanktakegiro.Text, "1/1/1900") & "' WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trncashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglduedate, cashbankglrefno, cashbankglnote, cashbankglstatus, createuser, createtime, upduser, updtime, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(cashbankgloid.Text)) & ", " & cashbankoid.Text & ", " & objTable.Rows(c1)("acctgoid").ToString & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", '" & cashbankdate.Text & "', '', '" & Tchar(objTable.Rows(c1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & objTable.Rows(c1)("addacctgoiddtl1").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) & ", " & objTable.Rows(c1)("addacctgoiddtl2").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) & ", " & objTable.Rows(c1)("addacctgoiddtl3").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    cashbankgloid.Text = (objTable.Rows.Count + ToInteger(cashbankgloid.Text))
                    sSql = "UPDATE QL_mstoid SET lastoid=" & ToInteger(cashbankgloid.Text) - 1 & " WHERE tablename='QL_TRNCASHBANKGL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If cashbankstatus.Text = "Post" Then
                    Dim dAddCost1 As Decimal = ToDouble(addacctgamt1.Text)
                    Dim dAddCost2 As Decimal = ToDouble(addacctgamt2.Text)
                    Dim dAddCost3 As Decimal = ToDouble(addacctgamt3.Text)
                    Dim dAddCost1IDR As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyIDRValue, 4, MidpointRounding.AwayFromZero)
                    Dim dAddCost2IDR As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyIDRValue, 4, MidpointRounding.AwayFromZero)
                    Dim dAddCost3IDR As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyIDRValue, 4, MidpointRounding.AwayFromZero)
                    Dim dAddCost1USD As Decimal = Math.Round(ToDouble(addacctgamt1.Text) * cRate.GetRateMonthlyUSDValue, 4, MidpointRounding.AwayFromZero)
                    Dim dAddCost2USD As Decimal = Math.Round(ToDouble(addacctgamt2.Text) * cRate.GetRateMonthlyUSDValue, 4, MidpointRounding.AwayFromZero)
                    Dim dAddCost3USD As Decimal = Math.Round(ToDouble(addacctgamt3.Text) * cRate.GetRateMonthlyUSDValue, 4, MidpointRounding.AwayFromZero)
                    ' JURNAL BANK KELUAR
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cash/Bank Overbooking|No=" & cashbankno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' DEBET
                    ' Ayat Silang
                    Dim iSeq As Integer = 1
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAyatSilangAcctgOid & ", 'D', " & ToDouble(cashbankamt.Text) & ", '" & cashbankno.Text & "', 'Ayat Silang - Cash/Bank Overbooking|No=" & cashbankno.Text & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iSeq += 1
                    ' Additional Cost 1+
                    If ToDouble(addacctgamt1.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid1.SelectedValue & ", 'D', " & dAddCost1 & ", '" & cashbankno.Text & "', 'Additional Cost 1 - C/B Overbooking|No. " & cashbankno.Text & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost1IDR & ", " & dAddCost1USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 1 - C/B Overbooking|No. " & cashbankno.Text & "', 'K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 2+
                    If ToDouble(addacctgamt2.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid2.SelectedValue & ", 'D', " & dAddCost2 & ", '" & cashbankno.Text & "', 'Additional Cost 2 - C/B Overbooking|No. " & cashbankno.Text & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost2IDR & ", " & dAddCost2USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 2 - C/B Overbooking|No. " & cashbankno.Text & "', 'K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' Additional Cost 3+
                    If ToDouble(addacctgamt3.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & addacctgoid3.SelectedValue & ", 'D', " & dAddCost3 & ", '" & cashbankno.Text & "', 'Additional Cost 3 - C/B Overbooking|No. " & cashbankno.Text & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & dAddCost3IDR & ", " & dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Additional Cost 3 - C/B Overbooking|No. " & cashbankno.Text & "', 'K')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iSeq += 1
                    End If
                    ' CREDIT
                    ' Kas/Bank Keluar
                    sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & IIf(cashbanktype.SelectedValue = "BGK", giroacctgoid.Text, acctgoid.SelectedValue) & ", 'C', " & ToDouble(cbnett.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyIDRValue) + dAddCost1IDR + dAddCost2IDR + dAddCost3IDR & ", " & (ToDouble(cashbankamt.Text) * cRate.GetRateMonthlyUSDValue) + dAddCost1USD + dAddCost2USD + dAddCost3USD & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    iGlDtlOid += 1 : iGlMstOid += 1

                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, giroacctgoid, cashbanktakegiro, addacctgoid1, addacctgamt1, addacctgoid2, addacctgamt2, addacctgoid3, addacctgamt3) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iCashBankOid & ", '" & sPeriod_to & "', '" & objTable.Rows(c1)("cashbankno").ToString & "', '" & sDate_to & "', '" & objTable.Rows(c1)("cashbanktype").ToString & "', 'MUTATIONTO', " & objTable.Rows(c1)("acctgoid") & ", " & curroid.SelectedValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", " & personoid.SelectedValue & ", '" & sDate_to & "', '', '" & Tchar(objTable.Rows(c1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & iGiroAcctgIn & ", '" & IIf(cashbanktype.SelectedValue = "BGK", cashbanktakegiro.Text, "1/1/1900") & "', " & objTable.Rows(c1)("addacctgoiddtl1").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) & ", " & objTable.Rows(c1)("addacctgoiddtl2").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) & ", " & objTable.Rows(c1)("addacctgoiddtl3").ToString & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        sSql = "INSERT INTO QL_trncashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglduedate, cashbankglrefno, cashbankglnote, cashbankglstatus, createuser, createtime, upduser, updtime, cashbankglres1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & CInt(cashbankgloid.Text) & ", " & iCashBankOid & ", " & acctgoid.SelectedValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * dUsedRateIDR & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", '" & sDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' JURNAL BANK MASUK
                        iSeq = 1
                        ' Insert Into GL Mst
                        sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate_to & "', '" & sPeriod_to & "', 'Cash/Bank Overbooking|No=" & objTable.Rows(c1)("cashbankno").ToString & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        ' Insert Into GL Dtl
                        ' DEBET
                        ' Kas/Bank Masuk
                        Dim dTotCostPerLine As Decimal = 0
                        If cashbanktype.SelectedValue <> "BGK" Then
                            dTotCostPerLine = ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) + ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) + ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString)
                        End If
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & IIf(cashbanktype.SelectedValue = "BGK", iGiroAcctgIn, objTable.Rows(c1)("acctgoid").ToString) & ", 'D', " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) - dTotCostPerLine & ", '" & objTable.Rows(c1)("cashbankno").ToString & "', '" & Tchar(objTable.Rows(c1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) - dTotCostPerLine) * dUsedRateIDR & ", " & (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) - dTotCostPerLine) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & iCashBankOid & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        Dim dValIDR As Double = (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) - dTotCostPerLine) * dUsedRateIDR
                        iGlDtlOid += 1 : iSeq += 1
                        If dTotCostPerLine > 0 Then
                            ' Additional Cost 1+
                            If ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(c1)("addacctgoiddtl1").ToString & ", 'D', " & ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) & ", '" & objTable.Rows(c1)("cashbankno").ToString & "', 'Cash/Bank Overbooking Additional Cost 1 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) * dUsedRateIDRforCostDtl & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & iCashBankOid & "', 'Cash/Bank Overbooking Additional Cost 1 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', 'K')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGlDtlOid += 1 : iSeq += 1
                                dValIDR += ToDouble(objTable.Rows(c1)("addacctgamtdtl1").ToString) * dUsedRateIDRforCostDtl
                            End If
                            ' Additional Cost 2+
                            If ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(c1)("addacctgoiddtl2").ToString & ", 'D', " & ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) & ", '" & objTable.Rows(c1)("cashbankno").ToString & "', 'Cash/Bank Overbooking Additional Cost 2 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) * dUsedRateIDRforCostDtl & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & iCashBankOid & "', 'Cash/Bank Overbooking Additional Cost 2 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', 'K')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGlDtlOid += 1 : iSeq += 1
                                dValIDR += ToDouble(objTable.Rows(c1)("addacctgamtdtl2").ToString) * dUsedRateIDRforCostDtl
                            End If
                            ' Additional Cost 3+
                            If ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) > 0 Then
                                sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(c1)("addacctgoiddtl3").ToString & ", 'D', " & ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) & ", '" & objTable.Rows(c1)("cashbankno").ToString & "', 'Cash/Bank Overbooking Additional Cost 3 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) * dUsedRateIDRforCostDtl & ", " & ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & iCashBankOid & "', 'Cash/Bank Overbooking Additional Cost 3 on Detail|No=" & objTable.Rows(c1)("cashbankno").ToString & "', 'K')"
                                xCmd.CommandText = sSql
                                xCmd.ExecuteNonQuery()
                                iGlDtlOid += 1 : iSeq += 1
                                dValIDR += ToDouble(objTable.Rows(c1)("addacctgamtdtl3").ToString) * dUsedRateIDRforCostDtl
                            End If
                        End If
                        ' Selisih IDR
                        Dim sSelisihDBCR As String = "", sCBType As String = ""
                        If dValIDR > ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue Then
                            sSelisihDBCR = "C" : sCBType = ""
                        ElseIf dValIDR < (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) - dTotCostPerLine) * cRate.GetRateMonthlyIDRValue Then
                            sSelisihDBCR = "D" : sCBType = ""
                        End If
                        If sSelisihDBCR <> "" Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iSelisihAcctgOid & ", '" & sSelisihDBCR & "', 0, '" & objTable.Rows(c1)("cashbankno").ToString & "', 'Selisih IDR C/B Overbooking No. " & objTable.Rows(c1)("cashbankno").ToString & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & Math.Abs(dValIDR - (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue)) & ", 0, 'QL_trncashbankmst " & iCashBankOid & "', 'Selisih IDR C/B Overbooking No. " & objTable.Rows(c1)("cashbankno").ToString & "', '" & sCBType & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1 : iSeq += 1
                        End If
                        ' Ayat Silang
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iAyatSilangAcctgOid & ", 'C', " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", '" & objTable.Rows(c1)("cashbankno").ToString & "', 'Ayat Silang - Cash/Bank Overbooking|No=" & objTable.Rows(c1)("cashbankno").ToString & " dg " & cashbankno.Text & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue) & ", " & (ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue) & ", 'QL_trncashbankmst " & iCashBankOid & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1 : iGlMstOid += 1
                        iCashBankOid += 1 : cashbankgloid.Text = ToInteger(cashbankgloid.Text) + 1
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & ToInteger(cashbankgloid.Text) - 1 & " WHERE tablename='QL_TRNCASHBANKGL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iCashBankOid - 1 & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid - 1 & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnMutation.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnMutation.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select Cash/Bank Overbooking data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnMutation.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        If FilterDDLPrintOption.SelectedIndex > 0 Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            dv.RowFilter = "checkvalue='True'"
            If dv.Count <= 0 Then
                showMessage("Please select data to be printed!", 2)
                dv.RowFilter = ""
                Exit Sub
            End If
        End If
        ShowReport()
    End Sub
#End Region

End Class