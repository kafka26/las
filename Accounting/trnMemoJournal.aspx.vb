Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_MemorialJournal
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select COA field!<BR>"
        End If
        If glamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(glamt.Text) <= 0 Then
                sError &= "- AMOUNT must be more than 0!<BR>"
            End If
        End If
        If groupoid.SelectedValue = "" Then
            sError &= "- Please select DIVISION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        If gldate.Text = "" Then
            sError &= "- Please fill MEMO DATE field!<BR>"
        Else
            If Not IsValidDate(gldate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- MEMO DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If ToDouble(tbTotalDebit.Text) > 0 And ToDouble(tbTotalCredit.Text) > 0 Then
            If ToDouble(tbTotalDebit.Text) <> ToDouble(tbTotalCredit.Text) Then
                sError &= "- TOTAL DEBIT must be equal with TOTAL CREDIT!<BR>"
            End If
        Else
            sError &= "- Both of TOTAL DEBIT dan TOTAL CREDIT must be more than 0!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            glflag.Text = "In Process"
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trnglmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND glflag='In Process' AND ISNULL(glother1, '')<>''"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lbInProcess.Visible = True
            lbInProcess.Text = "You have " & GetStrData(sSql) & " In Process Memorial Journal data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If FillDDL(DDLBusUnit, sSql) Then
            InitDDLDivision()
        End If
        ' Fill DDL Account Group
		sSql = "SELECT DISTINCT acctggrp1, acctggrp1 AS acctggrp FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE' ORDER BY acctggrp1"
        If FillDDL(acctggroup, sSql) Then
            InitDDLCOA()
        End If
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
    End Sub

    Private Sub InitDDLDivision()
        ' Fill DDL Division
        sSql = "SELECT groupoid, groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        FillDDLWithAdditionalText(groupoid, sSql, "None", "0")
    End Sub

    Private Sub InitDDLCOA()
        acctgoid.Items.Clear()
        sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_MEMO_JOURNAL' AND interfaceres1='" & DDLBusUnit.SelectedValue & "'"
        Dim sCode As String = GetStrData(sSql)
        If sCode = "" Then
            sSql = "SELECT TOP 1 interfacevalue FROM QL_mstinterface WHERE cmpcode='" & CompnyCode & "' AND interfacevar='VAR_MEMO_JOURNAL' AND interfaceres1='All'"
            sCode = GetStrData(sSql)
        End If
        If sCode <> "" Then
            sSql = "SELECT DISTINCT a.acctgoid, ('[' + a.acctgcode + '] ' + a.acctgdesc) AS acctgdesc FROM QL_mstacctg a WHERE a.cmpcode='" & CompnyCode & "' AND a.activeflag='ACTIVE' AND ("
            Dim sSplitCode() As String = sCode.Split(",")
            For C1 As Integer = 0 To sSplitCode.Length - 1
                sSql &= "a.acctgcode LIKE '" & LTrim(sSplitCode(C1)) & "%'"
                If C1 < sSplitCode.Length - 1 Then
                    sSql &= " OR "
                End If
            Next
            sSql &= ") AND a.acctgoid NOT IN (SELECT DISTINCT ac.acctggrp3 FROM QL_mstacctg ac WHERE ac.acctggrp3 IS NOT NULL AND ac.cmpcode=a.cmpcode) AND a.acctggrp1='" & acctggroup.SelectedValue & "' ORDER BY acctgdesc"
            FillDDL(acctgoid, sSql)
        End If
    End Sub

    Private Sub GenerateNo()
        If DDLBusUnit.SelectedValue <> "" Then
            If gldate.Text <> "" Then
                Dim sErr As String = ""
                If IsValidDate(gldate.Text, "MM/dd/yyyy", sErr) Then
                    Dim sNo As String = "MJ-" & Format(CDate(gldate.Text), "yyMM") & "-"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(glother1, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trnglmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND glother1 LIKE '%" & sNo & "%'"
                    If GetStrData(sSql) = "" Then
                        glother1.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        glother1.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT 'False' AS Checkvalue, glmstoid, glother1, CONVERT(VARCHAR(10), gldate, 101) AS gldate, glflag, glother3 FROM QL_trnglmst glm WHERE (ISNULL(glother1, '')<>'' AND type='MJ')"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND glm.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, glm.gldate) DESC, glmstoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trnglmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        glseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                Dim dv As DataView = dt.DefaultView
                dv.Sort = "gldbcr DESC, glseq"
                dt = dv.ToTable
                For C1 As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(C1)("glseq") = C1 + 1
                Next
                dt.AcceptChanges()
                glseq.Text = dt.Rows.Count + 1
                Session("TblDtl") = dt
                gvDtl.DataSource = Session("TblDtl")
                gvDtl.DataBind()
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        acctggroup.SelectedIndex = -1
        acctggroup_SelectedIndexChanged(Nothing, Nothing)
        gldbcr.SelectedIndex = -1
        glamt.Text = ""
        groupoid.SelectedIndex = -1
        glnote.Text = ""
        gvDtl.SelectedIndex = -1
        CountTotalAmt()
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.Enabled = bVal : DDLBusUnit.CssClass = sCss
    End Sub

    Private Sub CountTotalAmt()
        Dim dDb As Double = 0
        Dim dCr As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            For C1 As Integer = 0 To dt.Rows.Count - 1
                dDb += ToDouble(dt.Rows(C1)("glamtdb").ToString)
                dCr += ToDouble(dt.Rows(C1)("glamtcr").ToString)
            Next
        End If
        tbTotalDebit.Text = ToMaskEdit(dDb, 4)
        tbTotalCredit.Text = ToMaskEdit(dCr, 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String)
        Try
            sSql = "SELECT cmpcode, glmstoid, periodacctg, glother1, gldate, glother3, glflag, createuser, createtime, upduser, updtime, ISNULL(glother2, '1') AS curroid FROM QL_trnglmst WHERE glmstoid=" & sOid
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = xreader("cmpcode").ToString
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                glmstoid.Text = xreader("glmstoid").ToString
                periodacctg.Text = xreader("periodacctg").ToString
                glother1.Text = xreader("glother1").ToString
                gldate.Text = Format(xreader("gldate"), "MM/dd/yyyy")
                glother3.Text = xreader("glother3").ToString
                glflag.Text = xreader("glflag").ToString
                createuser.Text = xreader("createuser").ToString
                createtime.Text = xreader("createtime").ToString
                upduser.Text = xreader("upduser").ToString
                updtime.Text = xreader("updtime").ToString
                curroid.SelectedValue = xreader("curroid").ToString
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        gldate.Enabled = False : gldate.CssClass = "inpTextDisabled" : imbDate.Visible = False
        If glflag.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            btnShowCOA.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT glseq, acctggrp1 AS acctggroup, gld.acctgoid, acctgcode, acctgdesc, gldbcr, (CASE gldbcr WHEN 'D' THEN glamt ELSE 0.0 END) AS glamtdb, (CASE gldbcr WHEN 'D' THEN 0.0 ELSE glamt END) AS glamtcr, glnote, ISNULL(groupoid, 0) AS groupoid, ISNULL((SELECT groupcode + ' - ' + groupdesc FROM QL_mstdeptgroup dg WHERE dg.cmpcode=gld.cmpcode AND dg.groupoid=gld.groupoid), 'None') AS groupdesc FROM QL_trngldtl gld INNER JOIN QL_mstacctg a ON a.acctgoid=gld.acctgoid WHERE glmstoid=" & sOid & " ORDER BY glseq"
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trngldtl")
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "glmstoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub PrintReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("glmstoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptMemoJournal.rpt"))
            Dim sWhere As String = " WHERE"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere &= " glm.cmpcode='" & Session("CompnyCode") & "'"
            Else
                sWhere &= " glm.cmpcode LIKE '%'"
            End If
            If sOid = "" Then
                sWhere = " AND glm." & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND glm.gldate>='" & FilterPeriod1.Text & " 00:00:00' AND glm.gldate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND glm.glflag='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND glm.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND glm.glmstoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            report.SetParameterValue("PrintUserID", Session("UserID"))
            report.SetParameterValue("PrintUserName", GetStrData("SELECT TOP 1 profname FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'"))
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "MemorialJournalPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            report.Close()
            report.Dispose()
            showMessage(ex.Message, 1)
        End Try
        Response.Redirect("~\Accounting\trnMemoJournal.aspx?awal=true")
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnMemoJournal.aspx")
        End If
        If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Memorial Journal"
        Session("oid") = Request.QueryString("oid")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"))
                TabContainer1.ActiveTabIndex = 1
            Else
                glmstoid.Text = GenerateID("QL_TRNGLMST", CompnyCode)
                gldate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                GenerateNo()
                glflag.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnMemoJournal.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lbInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, glm.updtime, GETDATE()) > " & nDays & " AND glflag='In Process'"
        If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND glm.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND gldate>='" & FilterPeriod1.Text & " 00:00:00' AND gldate<='" & CDate(FilterPeriod2.Text) & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND glflag='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND glm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnMemoJournal.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND glm.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateNo()
        End If
        InitDDLDivision()
    End Sub

    Protected Sub gldate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gldate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateNo()
        End If
    End Sub

    Protected Sub acctggroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctggroup.SelectedIndexChanged
        InitDDLCOA()
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = New DataTable("TblDetailMemo")
                dtlTable.Columns.Add("glseq", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctggroup", Type.GetType("System.String"))
                dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
                dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
                dtlTable.Columns.Add("gldbcr", Type.GetType("System.String"))
                dtlTable.Columns.Add("glamtdb", Type.GetType("System.Double"))
                dtlTable.Columns.Add("glamtcr", Type.GetType("System.Double"))
                dtlTable.Columns.Add("glnote", Type.GetType("System.String"))
                dtlTable.Columns.Add("groupoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("groupdesc", Type.GetType("System.String"))
                Session("TblDtl") = dtlTable
            End If
            Dim objTable As DataTable = Session("TblDtl")
            'Dim dv As DataView = objTable.DefaultView
            'If i_u2.Text = "New Detail" Then
            '    dv.RowFilter = "acctgoid=" & acctgoid.SelectedValue
            'Else
            '    dv.RowFilter = "acctgoid=" & acctgoid.SelectedValue & " AND glseq<>" & glseq.Text
            'End If
            'If dv.Count > 0 Then
            '    dv.RowFilter = ""
            '    showMessage("This Data has been added before, please check!", 2)
            '    Exit Sub
            'End If
            'dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                glseq.Text = objTable.Rows.Count + 1
                objRow("glseq") = glseq.Text
            Else
                objRow = objTable.Rows(glseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("acctggroup") = acctggroup.SelectedValue
            objRow("acctgoid") = acctgoid.SelectedValue
            Dim sData() As String = acctgoid.SelectedItem.Text.Split("]")
            objRow("acctgcode") = sData(0).Replace("[", "").Trim
            objRow("acctgdesc") = sData(1).Trim
            objRow("gldbcr") = gldbcr.SelectedValue
            If gldbcr.SelectedValue = "D" Then
                objRow("glamtdb") = ToDouble(glamt.Text)
                objRow("glamtcr") = 0
            Else
                objRow("glamtdb") = 0
                objRow("glamtcr") = ToDouble(glamt.Text)
            End If
            objRow("glnote") = glnote.Text
            objRow("groupoid") = groupoid.SelectedValue
            objRow("groupdesc") = groupoid.SelectedItem.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 2)
            e.Row.Cells(4).Text = ToMaskEdit(ToDouble(e.Row.Cells(4).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            glseq.Text = gvDtl.SelectedDataKey.Item("glseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "glseq=" & glseq.Text
                acctggroup.SelectedValue = dv(0).Item("acctggroup").ToString
                acctggroup_SelectedIndexChanged(Nothing, Nothing)
                acctgoid.SelectedValue = dv(0).Item("acctgoid").ToString
                gldbcr.SelectedValue = dv(0).Item("gldbcr").ToString
                If gldbcr.SelectedValue = "D" Then
                    glamt.Text = ToMaskEdit(ToDouble(dv(0).Item("glamtdb").ToString), 4)
                Else
                    glamt.Text = ToMaskEdit(ToDouble(dv(0).Item("glamtcr").ToString), 4)
                End If
                glnote.Text = dv(0).Item("glnote").ToString
                groupoid.SelectedValue = dv(0).Item("groupoid").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("glseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trnglmst WHERE glmstoid=" & glmstoid.Text
                If CheckDataExists(sSql) Then
                    glmstoid.Text = GenerateID("QL_TRNGLMST", CompnyCode)
                End If
                Dim sNo As String = glother1.Text
                GenerateNo()
                If sNo <> glother1.Text Then
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trnglmst", "glmstoid", glmstoid.Text, "glflag", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    glflag.Text = "In Process"
                    Exit Sub
                End If
            End If
            gldtloid.Text = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim sDatePost As String = "01/01/1900"
            Dim cRate As New ClassRate()
            periodacctg.Text = GetDateToPeriodAcctg(CDate(gldate.Text))
            If glflag.Text = "Post" Then
                If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, gldate.Text) Then
                    showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(gldate.Text))).ToUpper & " " & Year(CDate(gldate.Text)).ToString & " anymore because the period has been closed. Please select another period!", 3) : glflag.Text = "In Process" : Exit Sub
                End If
                cRate.SetRateValue(ToInteger(curroid.SelectedValue), Format(GetServerTime(), "MM/dd/yyyy"))
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                sDatePost = Format(GetServerTime(), "MM/dd/yyyy")
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, glother1, glother2, glother3, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & glmstoid.Text & ", '" & gldate.Text & "', '" & periodacctg.Text & "', 'Memo|" & glother1.Text & "', '" & glflag.Text & "', '" & sDatePost & "', '" & createuser.Text & "', '" & createtime.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP,'MJ', '" & glother1.Text & "', '" & curroid.SelectedValue & "', '" & Tchar(glother3.Text) & "', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & glmstoid.Text & " WHERE tablename='QL_TRNGLMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                Else
                    sSql = "UPDATE QL_trnglmst SET gldate='" & gldate.Text & "', periodacctg='" & periodacctg.Text & "', glnote='Memo|" & glother1.Text & "', glflag='" & glflag.Text & "', postdate='" & sDatePost & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, glother1='" & glother1.Text & "', glother2='" & curroid.SelectedValue & "', glother3='" & Tchar(glother3.Text) & "', rateoid=" & cRate.GetRateDailyOid & ", rate2oid=" & cRate.GetRateMonthlyOid & ", glrateidr=" & cRate.GetRateDailyIDRValue & ", glrate2idr=" & cRate.GetRateMonthlyIDRValue & ", glrateusd=" & cRate.GetRateDailyUSDValue & ", glrate2usd=" & cRate.GetRateMonthlyUSDValue & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND glmstoid=" & glmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "DELETE FROM QL_trngldtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND glmstoid=" & glmstoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, noref, glnote, glflag, upduser, updtime, glamt, glamtidr, glamtusd, glother1, groupoid) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(gldtloid.Text)) & ", " & c1 + 1 & ", " & glmstoid.Text & ", " & objTable.Rows(c1)("acctgoid").ToString & ", '" & objTable.Rows(c1)("gldbcr").ToString & "', '" & glother1.Text & "', '" & Tchar(objTable.Rows(c1)("glnote").ToString) & "', '" & glflag.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP"
                        If objTable.Rows(c1)("gldbcr").ToString = "D" Then
                            sSql &= ", " & ToDouble(objTable.Rows(c1)("glamtdb").ToString) & ", " & ToDouble(objTable.Rows(c1)("glamtdb").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("glamtdb").ToString) * cRate.GetRateMonthlyUSDValue
                        Else
                            sSql &= ", " & ToDouble(objTable.Rows(c1)("glamtcr").ToString) & ", " & ToDouble(objTable.Rows(c1)("glamtcr").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("glamtcr").ToString) * cRate.GetRateMonthlyUSDValue
                        End If
                        sSql &= ", 'QL_trnglmst " & glmstoid.Text & "', " & objTable.Rows(c1)("groupoid").ToString & ")"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(gldtloid.Text)) & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        glflag.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    glflag.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                glflag.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Memo No. have been regenerated because being used by another data. Your new Memo No. is " & glother1.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnMemoJournal.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnMemoJournal.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If glmstoid.Text = "" Then
            showMessage("Please select Memorial Journal data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trnglmst", "glmstoid", glmstoid.Text, "glflag", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                glflag.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trngldtl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND glmstoid=" & glmstoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trnglmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND glmstoid=" & glmstoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnMemoJournal.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        glflag.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(glother1.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trnglmst " & glmstoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(glother1.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trnglmst " & glmstoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        UpdateCheckedValue()
        PrintReport()
    End Sub
#End Region

End Class