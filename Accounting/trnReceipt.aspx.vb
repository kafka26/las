Imports System.Data
Imports System.Data.SqlClient
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Partial Class Accounting_trnReceiptDP
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompanyCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function IsValidPeriod() As Boolean
        Dim sErr As String = ""
        If Not IsValidDate(FilterPeriod1.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 1 is invalid. " & sErr & "", 2)
            Return False
        End If
        If Not IsValidDate(FilterPeriod2.Text, "MM/dd/yyyy", sErr) Then
            showMessage("Period 2 is invalid. " & sErr & "", 2)
            Return False
        End If
        If CDate(FilterPeriod1.Text) > CDate(FilterPeriod2.Text) Then
            showMessage("Period 2 must be more than Period 1 !", 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If acctgoid2.Text = "" Then
            sError &= "- Please select RECEIPT ACCOUNT field!<BR>"
        End If
        If cashbankglamt.Text = "" Then
            sError &= "- Please fill AMOUNT field!<BR>"
        Else
            If ToDouble(cashbankglamt.Text) = 0 Then
                sError &= "- AMOUNT must be not equal to 0!<BR>"
            Else
                Dim sErrReply As String = ""
                If Not isLengthAccepted("cashbankglamt", "QL_trncashbankgl", ToDouble(cashbankglamt.Text), sErrReply) Then
                    sError &= "- AMOUNT must be less than MAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
                End If
            End If
        End If
        If groupoid.SelectedValue = "" Then
            sError &= "- Please select DIVISION field!<BR>"
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            Return False
        End If
        Return True
    End Function

    Private Function IsInputValid() As Boolean
        Dim sError As String = ""
        Dim sErr As String = ""
        If DDLBusUnit.SelectedValue = "" Then
            sError &= "- Please select BUSINESS UNIT field!<BR>"
        End If
        If cashbankdate.Text = "" Then
            sError &= "- Please fill RECEIPT DATE field!<BR>"
        Else
            If Not IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                sError &= "- RECEIPT DATE is invalid. " & sErr & "<BR>"
            End If
        End If
        If curroid.SelectedValue = "" Then
            sError &= "- Please select CURRENCY field!<BR>"
        End If
        Dim sErrReply As String = ""
        If Not isLengthAccepted("cashbankdpp", "QL_trncashbankmst", ToDouble(cashbankamt.Text), sErrReply) Then
            sError &= "- TOTAL AMOUNT must be less than MAX TOTAL AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If acctgoid.SelectedValue = "" Then
            sError &= "- Please select PAYMENT ACCOUNT field!<BR>"
        End If
        If cashbanktype.SelectedValue = "BBM" Or cashbanktype.SelectedValue = "BGM" Then
            If cashbankrefno.Text = "" Then
                sError &= "- Please fill REF. NO. field!<BR>"
            End If
            If cashbankduedate.Text = "" Then
                sError &= "- Please fill DUE DATE field!<BR>"
            Else
                If Not IsValidDate(cashbankduedate.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DUE DATE is invalid. " & sErr & "<BR>"
                Else
                    If CDate(cashbankdate.Text) > CDate(cashbankduedate.Text) Then
                        sError &= "- DUE DATE must be more or equal than RECEIPT DATE<BR>"
                    End If
                End If
            End If
        ElseIf cashbanktype.SelectedValue = "BLM" Then
            If giroacctgoid.Text = "" Then
                sError &= "- Please select DP NO. field!<BR>"
            End If
        End If
        If cashbanktype.SelectedValue = "BGM" Then
            If cashbanktakegiro.Text = "" Then
                sError &= "- Please fill DATE TAKE GIRO field!<BR>"
            Else
                If Not IsValidDate(cashbanktakegiro.Text, "MM/dd/yyyy", sErr) Then
                    sError &= "- DATE TAKE GIRO is invalid. " & sErr & "<BR>"
                Else
                    If cashbankduedate.Text <> "" Then
                        If CDate(cashbanktakegiro.Text) > CDate(cashbankduedate.Text) Then
                            sError &= "- DATE TAKE GIRO must be less or equal than DUE DATE<BR>"
                        End If
                    End If
                    If cashbankdate.Text <> "" Then
                        If CDate(cashbankdate.Text) > CDate(cashbanktakegiro.Text) Then
                            sError &= "- DATE TAKE GIRO must be more or equal than RECEIPT DATE<BR>"
                        End If
                    End If
                End If
            End If
            If custoid.Text = "" Then
                sError &= "- Please select CUSTOMER field!<BR>"
            End If
        End If
        If personoid.SelectedValue = "" Then
            sError &= "- Please select PIC field!<BR>"
        End If
        If Not isLengthAccepted("cashbanktaxamt", "QL_trncashbankmst", ToDouble(cashbanktaxamt.Text), sErrReply) Then
            sError &= "- TAX AMOUNT must be less than MAX TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If cashbankothertaxamt.Text <> "" Then
            If Not isLengthAccepted("cashbankothertaxamt", "QL_trncashbankmst", ToDouble(cashbankothertaxamt.Text), sErrReply) Then
                sError &= "- OTHER TAX AMOUNT must be less than MAX OTHER TAX AMOUNT (" & sErrReply & ") allowed stored in database!<BR>"
            End If
        End If
        If ToDouble(cashbankgrandtotal.Text) < 0 Then
            sError &= "- GRAND TOTAL must be more than 0!<BR>"
        Else
            If cashbanktype.SelectedValue = "BLM" Then
                If ToDouble(cashbankgrandtotal.Text) > ToDouble(dparamt.Text) Then
                    sError &= "- GRAND TOTAL must be less than DP AMOUNT!<BR>"
                End If
            End If
        End If
        If Not isLengthAccepted("cashbankamt", "QL_trncashbankmst", ToDouble(cashbankgrandtotal.Text), sErrReply) Then
            sError &= "- GRAND TOTAL must be less than MAX GRAND TOTAL (" & sErrReply & ") allowed stored in database!<BR>"
        End If
        If Session("TblDtl") Is Nothing Then
            sError &= "- Please fill Detail Data!"
        Else
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count <= 0 Then
                sError &= "- Please fill Detail Data!"
            End If
        End If
        If sError <> "" Then
            showMessage(sError, 2)
            cashbankstatus.Text = "In Process"
            Return False
        End If
        Return True
    End Function

    Private Function GetTaxValue() As Double
        GetTaxValue = 0
        Try
            sSql = "SELECT TOP 1 gendesc FROM QL_mstgen WHERE cmpcode='XXX' AND activeflag='ACTIVE' AND gengroup='DEFAULT TAX' ORDER BY updtime DESC"
            GetTaxValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetTaxValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub CheckReceiptStatus()
        Dim nDays As Integer = 7
        sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE DATEDIFF(DAY, updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' AND cashbankgroup='RECEIPT'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("SpecialAccess")) = False Then
            sSql &= " AND createuser='" & Session("UserID") & "'"
        End If
        If ToDouble(GetStrData(sSql)) > 0 Then
            lkbReceiptInProcess.Visible = True
            lkbReceiptInProcess.Text = "You have " & GetStrData(sSql) & " In Process Cash/Bank Receipt data that not been processed more than " & nDays.ToString & " days. Please click this link to view it !"
        End If
    End Sub

    Private Sub InitALLDDL()
        ' Fill DDL Business Unit
        sSql = "SELECT divcode, divname FROM QL_mstdivision WHERE activeflag='ACTIVE'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cmpcode='" & Session("CompnyCode") & "'"
        End If
        FillDDL(DDLBusUnit, sSql)
        If DDLBusUnit.Items.Count > 0 Then
            InitDDLPerson() : InitDDLDiv()
        End If
        ' Fill DDL Currency
        sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
        FillDDL(curroid, sSql)
    End Sub

    Private Sub InitDDLDiv()
        sSql = "SELECT groupoid, groupdesc FROM QL_mstdeptgroup WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND activeflag='ACTIVE'"
        FillDDLWithAdditionalText(groupoid, sSql, "NONE", 0)
    End Sub

    Private Sub InitDDLPerson()
        ' Fill DDL PIC
        sSql = "SELECT personoid, personname FROM QL_mstperson WHERE activeflag='ACTIVE' AND cmpcode='" & DDLBusUnit.SelectedValue & "'"
        FillDDLWithNone(personoid, sSql)
        Try
            personoid.SelectedValue = GetStrData("SELECT personoid FROM QL_mstprof WHERE profoid='" & Session("UserID") & "'")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindTrnData(ByVal sSqlPlus As String)
        sSql = "SELECT cb.cmpcode, cb.cashbankoid, cb.cashbankno, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, (CASE cashbanktype WHEN 'BKM' THEN 'CASH' WHEN 'BBM' THEN 'TRANSFER' WHEN 'BLM' THEN 'DOWN PAYMENT' ELSE 'GIRO/CHEQUE' END) AS cashbanktype, ('(' + a.acctgcode + ') ' + a.acctgdesc) AS acctgdesc, ISNULL(p.personname,'None') personname, cb.cashbankstatus, cb.cashbanknote, 'False' AS checkvalue, ISNULL(custname, '') AS custname, '' AS CBType, cashbankamt FROM QL_trncashbankmst cb INNER JOIN QL_mstacctg a ON a.acctgoid=cb.acctgoid LEFT JOIN QL_mstperson p ON p.personoid=cb.personoid LEFT JOIN QL_mstcust s ON s.custoid=cb.refsuppoid WHERE cb.cashbankgroup='RECEIPT'"
        If Session("CompnyCode") <> CompnyCode Then
            sSql &= " AND cb.cmpcode='" & Session("CompnyCode") & "'"
        End If
        sSql &= sSqlPlus & " ORDER BY CONVERT(DATETIME, cb.cashbankdate) DESC, cb.cashbankoid DESC"
        Session("TblMst") = cKon.ambiltabel(sSql, "QL_trncashbankmst")
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
        lblViewInfo.Visible = False
    End Sub

    Private Sub UpdateCheckedValue()
        If Session("TblMst") IsNot Nothing Then
            Dim dt As DataTable = Session("TblMst")
            Dim dv As DataView = dt.DefaultView
            For C1 As Integer = 0 To gvTRN.Rows.Count - 1
                Dim row As System.Web.UI.WebControls.GridViewRow = gvTRN.Rows(C1)
                If (row.RowType = DataControlRowType.DataRow) Then
                    Dim cc As System.Web.UI.ControlCollection = row.Cells(gvTRN.Columns.Count - 1).Controls
                    Dim cbCheck As Boolean = False
                    Dim sOid As String = ""
                    For Each myControl As System.Web.UI.Control In cc
                        If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                            cbCheck = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        ElseIf TypeOf (myControl) Is System.Web.UI.WebControls.Label Then
                            If CType(myControl, System.Web.UI.WebControls.Label).ID = "lblOidGVMst" Then
                                sOid = CType(myControl, System.Web.UI.WebControls.Label).Text
                            End If
                        End If
                    Next
                    dv.RowFilter = "cashbankoid=" & sOid
                    If cbCheck = True Then
                        dv(0)("checkvalue") = "True"
                    Else
                        dv(0)("checkvalue") = "False"
                    End If
                    dv.RowFilter = ""
                End If
            Next
            dt.AcceptChanges()
            Session("TblMst") = dt
        End If
    End Sub

    Private Sub GenerateReceiptNo()
        If DDLBusUnit.SelectedValue <> "" Then
            Dim sErr As String = ""
            If cashbankdate.Text <> "" Then
                If IsValidDate(cashbankdate.Text, "MM/dd/yyyy", sErr) Then
                    Dim sNo As String = cashbanktype.SelectedValue & "-" & Format(CDate(cashbankdate.Text), "yyMM") & "-"
                    sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cashbankno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankno LIKE '%" & sNo & "%'"
                    If GetStrData(sSql) = "" Then
                        cashbankno.Text = GenNumberString(sNo, "", 1, DefaultFormatCounter)
                    Else
                        cashbankno.Text = GenNumberString(sNo, "", GetStrData(sSql), DefaultFormatCounter)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BindCustomerData()
        sSql = "SELECT custoid, custcode, custname, custaddr, (CASE custtaxable WHEN 0 THEN 'NON TAX' WHEN 1 THEN 'TAX' ELSE '' END) AS custtaxable FROM QL_mstcust WHERE cmpcode='" & CompnyCode & "' AND " & FilterDDLListCust.SelectedValue & " LIKE '%" & Tchar(FilterTextListCust.Text) & "%' ORDER BY custcode"
        FillGV(gvListCust, sSql, "QL_mstcust")
    End Sub

    Private Sub BindListCOA()
        FillGVAcctg(gvListCOA, "VAR_RECEIPT", DDLBusUnit.SelectedValue, "AND " & FilterDDLListCOA.SelectedValue & " LIKE '%" & Tchar(FilterTextListCOA.Text) & "%'")
    End Sub

    Private Sub ClearDetail()
        i_u2.Text = "New Detail"
        cashbankglseq.Text = "1"
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            If dt.Rows.Count > 0 Then
                cashbankglseq.Text = dt.Rows.Count + 1
                EnableHeader(False)
            Else
                EnableHeader(True)
            End If
        Else
            EnableHeader(True)
        End If
        acctgoid2.Text = ""
        acctgcode.Text = ""
        acctgdesc.Text = ""
        cashbankglamt.Text = ""
        cashbankglnote.Text = ""
        groupoid.SelectedIndex = -1
        gvDtl.SelectedIndex = -1
    End Sub

    Private Sub EnableHeader(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then
            sCss = "inpTextDisabled"
        End If
        DDLBusUnit.CssClass = sCss : DDLBusUnit.Enabled = bVal
    End Sub

    Private Sub CountTotalAmt()
        Dim dVal As Double = 0 : Dim dValMinus As Double = 0
        If Session("TblDtl") IsNot Nothing Then
            Dim dt As DataTable = Session("TblDtl")
            dVal = ToDouble(dt.Compute("SUM(cashbankglamt)", "cashbankglamt>0").ToString)
            dValMinus = ToDouble(dt.Compute("SUM(cashbankglamt)", "cashbankglamt<0").ToString)
        End If
        cashbankamt.Text = ToMaskEdit(dVal, 4)
        lblDeduction.Visible = (dValMinus < 0)
        lblDeductionSign.Visible = (dValMinus < 0)
        totaldeduction.Visible = (dValMinus < 0)
        totaldeduction.Text = ToMaskEdit(dValMinus, 4)
        If cashbanktaxtype.SelectedValue = "TAX" Then
            cashbanktaxamt.Text = ToMaskEdit((ToDouble(cashbankamt.Text) * ToDouble(cashbanktaxpct.Text)) / 100, 4)
        Else
            cashbanktaxamt.Text = ""
        End If
        cashbankgrandtotal.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(cashbanktaxamt.Text) + ToDouble(cashbankothertaxamt.Text) + ToDouble(dValMinus), 4)
    End Sub

    Private Sub FillTextBox(ByVal sOid As String, ByVal sCmpcode As String)
        Try
            sSql = "SELECT cb.cmpcode, cashbankoid, cb.periodacctg, cashbankno, cashbankdate, cashbanktype, cb.acctgoid, cb.curroid, cashbankamt, cb.personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, cb.createuser, cb.createtime, cb.upduser, cb.updtime, cashbanktakegiro, ISNULL(cb.refsuppoid, 0) AS custoid, ISNULL(custname, '') AS custname, cashbanktaxtype, cashbanktaxpct, cashbanktaxamt, cashbankothertaxamt, cashbankdpp, ISNULL(giroacctgoid, 0) AS giroacctgoid, (CASE cashbanktype WHEN 'BLM' THEN (ISNULL((SELECT dparamt - ISNULL(dp.dparaccumamt, 0.0) FROM QL_trndpar dp WHERE dp.cmpcode=cb.cmpcode AND dp.dparoid=ISNULL(cb.giroacctgoid, 0)), 0.0) + cashbankamt) ELSE 0.0 END) AS cashbankresamt FROM QL_trncashbankmst cb LEFT JOIN QL_mstcust s ON s.custoid=cb.refsuppoid WHERE cb.cashbankoid=" & sOid & "AND cb.cmpcode='" & sCmpcode & "'"
            xCmd.CommandText = sSql
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            xreader = xCmd.ExecuteReader
            While xreader.Read
                DDLBusUnit.SelectedValue = Trim(xreader("cmpcode").ToString)
                DDLBusUnit_SelectedIndexChanged(Nothing, Nothing)
                cashbankoid.Text = Trim(xreader("cashbankoid").ToString)
                periodacctg.Text = Trim(xreader("periodacctg").ToString)
                cashbankno.Text = Trim(xreader("cashbankno").ToString)
                cashbankdate.Text = Format(xreader("cashbankdate"), "MM/dd/yyyy")
                cashbanktype.SelectedValue = Trim(xreader("cashbanktype").ToString)
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
                cashbankduedate.Text = Format(xreader("cashbankduedate"), "MM/dd/yyyy")
                If cashbankduedate.Text = "01/01/1900" Then
                    cashbankduedate.Text = ""
                End If
                cashbanktakegiro.Text = Format(xreader("cashbanktakegiro"), "MM/dd/yyyy")
                If cashbanktakegiro.Text = "01/01/1900" Then
                    cashbanktakegiro.Text = ""
                End If
                cashbankrefno.Text = Trim(xreader("cashbankrefno").ToString)
                acctgoid.SelectedValue = Trim(xreader("acctgoid").ToString)
                cashbankgrandtotal.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankamt").ToString)), 4)
                personoid.SelectedValue = Trim(xreader("personoid").ToString)
                cashbanknote.Text = Trim(xreader("cashbanknote").ToString)
                cashbankstatus.Text = Trim(xreader("cashbankstatus").ToString)
                createuser.Text = Trim(xreader("createuser").ToString)
                createtime.Text = Trim(xreader("createtime").ToString)
                upduser.Text = Trim(xreader("upduser").ToString)
                updtime.Text = Trim(xreader("updtime").ToString)
                custoid.Text = Trim(xreader("custoid").ToString)
                custname.Text = Trim(xreader("custname").ToString)
                cashbanktaxtype.SelectedValue = Trim(xreader("cashbanktaxtype").ToString)
                cashbanktaxpct.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbanktaxpct").ToString)), 4)
                cashbanktaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbanktaxamt").ToString)), 4)
                cashbankothertaxamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankothertaxamt").ToString)), 4)
                cashbankamt.Text = ToMaskEdit(ToDouble(Trim(xreader("cashbankdpp").ToString)), 4)
                giroacctgoid.Text = xreader("giroacctgoid").ToString
                Session("lastdparoid") = giroacctgoid.Text
                dparamt.Text = ToMaskEdit(ToDouble(xreader("cashbankresamt").ToString), 4)
                Session("lastcashbanktype") = cashbanktype.SelectedValue
                curroid.SelectedValue = Trim(xreader("curroid").ToString)
            End While
            xreader.Close()
            conn.Close()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            btnSave.Visible = False : btnDelete.Visible = False : btnposting.Visible = False : btnShowCOA.Visible = False
            Exit Sub
        End Try
        DDLBusUnit.CssClass = "inpTextDisabled" : DDLBusUnit.Enabled = False
        btnShowCOA.Visible = False
        cashbanktype.CssClass = "inpTextDisabled" : cashbanktype.Enabled = False
        acctgoid.CssClass = "inpTextDisabled" : acctgoid.Enabled = False
        If cashbankstatus.Text = "Post" Then
            btnSave.Visible = False
            btnDelete.Visible = False
            btnposting.Visible = False
            btnShowCOA.Visible = True
            btnAddToList.Visible = False
            gvDtl.Columns(0).Visible = False
            gvDtl.Columns(gvDtl.Columns.Count - 1).Visible = False
        End If
        sSql = "SELECT 0 AS cashbankglseq, cbgl.acctgoid, a.acctgcode, a.acctgdesc, cbgl.cashbankglamt, cbgl.cashbankglnote, 0 groupoid, 'NONE' groupdesc FROM QL_trncashbankgl cbgl INNER JOIN QL_mstacctg a ON a.acctgoid=cbgl.acctgoid WHERE cbgl.cashbankoid=" & sOid & " ORDER BY cashbankgloid"
        Dim dtTable As DataTable = cKon.ambiltabel(sSql, "QL_trncashbankgl")
        For C1 As Integer = 0 To dtTable.Rows.Count - 1
            dtTable.Rows(C1)("cashbankglseq") = C1 + 1
        Next
        Session("TblDtl") = dtTable
        gvDtl.DataSource = Session("TblDtl")
        gvDtl.DataBind()
        ClearDetail() : CountTotalAmt()
        Session("lastdparamt") = ToDouble(cashbankgrandtotal.Text)
    End Sub

    Private Sub ShowReport()
        Try
            Dim sOid As String = ""
            If Session("TblMst") IsNot Nothing Then
                Dim dv As DataView = Session("TblMst").DefaultView
                dv.RowFilter = "checkvalue='True'"
                For C1 As Integer = 0 To dv.Count - 1
                    sOid &= dv(C1)("cashbankoid").ToString & ","
                Next
                dv.RowFilter = ""
            End If
            report.Load(Server.MapPath(folderReport & "rptReceipt.rpt"))
            Dim sWhere As String = "WHERE cashbankgroup='RECEIPT'"
            If Session("CompnyCode") <> CompnyCode Then
                sWhere = " AND cb.cmpcode='" & Session("CompnyCode") & "'"
            End If
            If sOid = "" Then
                sWhere &= " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
                If cbPeriode.Checked Then
                    If IsValidPeriod() Then
                        sWhere &= " AND cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
                    Else
                        Exit Sub
                    End If
                End If
                If cbStatus.Checked Then
                    sWhere &= " AND cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
                End If
                If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("SpecialAccess")) = False Then
                    sWhere &= " AND cb.createuser='" & Session("UserID") & "'"
                End If
            Else
                sOid = Left(sOid, sOid.Length - 1)
                sWhere &= " AND cb.cashbankoid IN (" & sOid & ")"
            End If
            report.SetParameterValue("sWhere", sWhere)
            cProc.SetDBLogonForReport(report)
            Response.Buffer = False
            Response.ClearContent()
            Response.ClearHeaders()
            report.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, True, "ReceiptPrintOut")
            report.Close()
            report.Dispose()
        Catch ex As Exception
            showMessage(ex.Message, 1)
            report.Close()
            report.Dispose()
        End Try
        Response.Redirect("~\Accounting\trnReceipt.aspx?awal=true")
    End Sub

    Private Sub BindDPData()
        sSql = "SELECT dp.dparoid, dp.dparno, (CONVERT(VARCHAR(10), dp.dpardate, 101)) AS dpardate, dp.acctgoid AS dparacctgoid, a.acctgdesc, dp.dparnote, (dp.dparamt - ISNULL(dp.dparaccumamt, 0.0)) AS dparamt FROM QL_trndpar dp INNER JOIN QL_mstacctg a ON a.acctgoid=dp.acctgoid LEFT JOIN QL_trncashbankmst cb ON cb.cmpcode=dp.cmpcode AND cb.cashbankoid=dp.cashbankoid WHERE dp.cmpcode='" & DDLBusUnit.SelectedValue & "' AND " & FilterDDLListDP.SelectedValue & " LIKE '%" & Tchar(FilterTextListDP.Text) & "%' AND dp.dparstatus='Post' AND dp.custoid=" & custoid.Text & " AND dp.curroid=" & curroid.SelectedValue & " AND dp.dparamt > dp.dparaccumamt ORDER BY dp.dparoid"
        FillGV(gvListDP, sSql, "QL_trndpar")
    End Sub

    Private Sub EnableCurrency(ByVal bVal As Boolean)
        Dim sCss As String = "inpText"
        If bVal = False Then sCss = "inpTextDisabled"
        curroid.Enabled = bVal : curroid.CssClass = sCss
        If bVal = True Then
            If curroid.Items.Count = 0 Then
                ' Fill DDL Currency
                sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                FillDDL(curroid, sSql)
            End If
        End If
    End Sub

    Private Sub SetCOACurrency(ByVal sAcctgOid As String)
        If sAcctgOid <> "" Then
            Dim sCurrOid As String = GetStrData("SELECT curroid FROM QL_mstacctg WHERE cmpcode='" & CompnyCode & "' AND acctgoid=" & sAcctgOid)
            If sCurrOid <> "0" Then
                If curroid.Items.Count = 0 Then
                    ' Fill DDL Currency
                    sSql = "SELECT curroid, currcode FROM QL_mstcurr WHERE cmpcode='" & CompnyCode & "' AND activeflag='ACTIVE'"
                    FillDDL(curroid, sSql)
                End If
                curroid.SelectedValue = sCurrOid
            Else
                If curroid.Enabled = False Then
                    curroid.Items.Clear()
                    showMessage("Please define Currency for selected Payment Account!", 2)
                End If
            End If
        Else
            curroid.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            Session("CompnyCode") = cmpcode
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnReceipt.aspx")
        End If
        If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Cash/Bank Receipt"
        Session("oid") = Request.QueryString("oid")
        Session("cmpcode") = Request.QueryString("cmpcode")
        If Session("oid") = "" Or Session("oid") = Nothing Then
            i_u.Text = "New Data"
        Else
            i_u.Text = "Update Data"
        End If
        btnDelete.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to DELETE this data?');")
        btnposting.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to POST this data?');")
        If Not Page.IsPostBack Then
            CheckReceiptStatus()
            createuser.Text = Session("UserID") : createtime.Text = GetServerTime().ToString
            upduser.Text = "-" : updtime.Text = "-"
            FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
            FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
            InitALLDDL()
            If Session("oid") <> Nothing And Session("oid") <> "" Then
                FillTextBox(Session("oid"), Session("cmpcode"))
                TabContainer1.ActiveTabIndex = 1
            Else
                cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                cashbankdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
                cashbankstatus.Text = "In Process"
                periodacctg.Text = GetDateToPeriodAcctg(GetServerTime())
                btnDelete.Visible = False
                btnShowCOA.Visible = False
                TabContainer1.ActiveTabIndex = 0
                cashbanktype_SelectedIndexChanged(Nothing, Nothing)
            End If
        End If
        If Not Session("TblDtl") Is Nothing Then
            Dim dt As New DataTable
            dt = Session("TblDtl")
            gvDtl.DataSource = dt
            gvDtl.DataBind()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnReceipt.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub lkbReceiptInProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbReceiptInProcess.Click
        Dim nDays As Integer = 7
        FilterText.Text = ""
        FilterDDL.SelectedIndex = -1
        cbPeriode.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = True
        FilterDDLStatus.SelectedIndex = 0
        Dim sSqlPlus As String = " AND DATEDIFF(DAY, cb.updtime, GETDATE()) > " & nDays & " AND cashbankstatus='In Process' "
        If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "' "
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim sSqlPlus As String = " AND " & FilterDDL.SelectedValue & " LIKE '%" & Tchar(FilterText.Text) & "%'"
        If cbPeriode.Checked Then
            If IsValidPeriod() Then
                sSqlPlus &= " AND cb.cashbankdate>='" & FilterPeriod1.Text & " 00:00:00' AND cb.cashbankdate<='" & FilterPeriod2.Text & " 23:59:59'"
            Else
                Exit Sub
            End If
        End If
        If cbStatus.Checked Then
            sSqlPlus &= " AND cb.cashbankstatus='" & FilterDDLStatus.SelectedValue & "'"
        End If
        If cbType.Checked Then
            sSqlPlus &= " AND cb.cashbanktype='" & ddlType.SelectedValue & "'"
        End If
        If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("SpecialAccess")) = False Then
            sSqlPlus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlPlus)
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAll.Click
        Dim sSqlplus As String = ""
        FilterDDL.SelectedIndex = -1
        FilterText.Text = ""
        cbPeriode.Checked = False
        cbType.Checked = False
        FilterPeriod1.Text = Format(GetServerTime(), "MM/01/yyyy")
        FilterPeriod2.Text = Format(GetServerTime(), "MM/dd/yyyy")
        cbStatus.Checked = False
        FilterDDLStatus.SelectedIndex = -1
        If checkPagePermission("~\Accounting\trnReceipt.aspx", Session("SpecialAccess")) = False Then
            sSqlplus &= " AND cb.createuser='" & Session("UserID") & "'"
        End If
        BindTrnData(sSqlplus)
    End Sub

    Protected Sub gvTRN_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTRN.PageIndexChanging
        UpdateCheckedValue()
        gvTRN.PageIndex = e.NewPageIndex
        gvTRN.DataSource = Session("TblMst")
        gvTRN.DataBind()
    End Sub

    Protected Sub gvTRN_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTRN.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(6).Text = ToMaskEdit(ToDouble(e.Row.Cells(6).Text), 4)
        End If
    End Sub

    Protected Sub gvTRN_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvTRN.Sorting
        UpdateCheckedValue()
        'Retrieve the table from the session object.
        Dim dt = TryCast(Session("TblMst"), DataTable)
        If dt IsNot Nothing Then
            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression, ViewState("SortExpression"), ViewState("SortDirection"))
            gvTRN.DataSource = Session("TblMst")
            gvTRN.DataBind()
        End If
    End Sub

    Protected Sub DDLBusUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLBusUnit.SelectedIndexChanged
        InitDDLPerson()
        cashbanktype_SelectedIndexChanged(Nothing, Nothing)
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
        End If
        btnClearCOA_Click(Nothing, Nothing)
        InitDDLDiv()
    End Sub

    Protected Sub cashbankdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankdate.TextChanged
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
        End If
    End Sub

    Protected Sub cashbanktype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbanktype.SelectedIndexChanged
        Dim sVar As String = "VAR_CASH"
        Dim bVal As Boolean = False
        Dim bVal2 As Boolean = True
        Dim bVal3 As Boolean = False
        Dim sCss As String = "inpText"
        Dim sText As String = "Ref. No."
        Dim sText2 As String = "Due Date"
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : cashbankduedate.Text = "" : dparamt.Text = ""
        EnableCurrency(False)
        If cashbanktype.SelectedValue = "BBM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BGM" Then
            sVar = "VAR_BANK" : bVal = True : bVal3 = True
        ElseIf cashbanktype.SelectedValue = "BLM" Then
            sVar = "VAR_DP_AR" : bVal2 = False : sCss = "inpTextDisabled" : sText = "DP No." : sText2 = "DP Amount" : bVal3 = True : EnableCurrency(True)
        End If
        FillDDLAcctg(acctgoid, sVar, DDLBusUnit.SelectedValue) : SetCOACurrency(acctgoid.SelectedValue)
        lblWarnDueDate.Visible = bVal : cashbankduedate.Visible = bVal : imbDueDate.Visible = bVal : lblInfoDueDate.Visible = bVal : cashbankduedate.Text = ""
        lblDueDate.Visible = bVal3 : lblSeptDueDate.Visible = bVal3 : lblWarnRefNo.Visible = bVal3
        acctgoid.Enabled = bVal2 : acctgoid.CssClass = sCss : btnSearchDP.Visible = Not bVal2 : btnClearDP.Visible = Not bVal2 : cashbankrefno.Enabled = bVal2 : cashbankrefno.CssClass = sCss : dparamt.Visible = Not bVal2
        lblRefNo.Text = sText : lblDueDate.Text = sText2
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
        End If
    End Sub

    Protected Sub acctgoid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles acctgoid.SelectedIndexChanged
        If i_u.Text = "New Data" Then
            GenerateReceiptNo()
        End If
        SetCOACurrency(acctgoid.SelectedValue)
    End Sub

    Protected Sub btnSearchCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCust.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, True)
    End Sub

    Protected Sub btnClearCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCust.Click
        custoid.Text = "" : custname.Text = "" : cashbanktaxtype.SelectedIndex = -1 : cashbanktaxpct.Text = "" : CountTotalAmt()
    End Sub

    Protected Sub btnFindListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCust.Click
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub btnAllListCust_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCust.Click
        FilterDDLListCust.SelectedIndex = -1 : FilterTextListCust.Text = "" : gvListCust.SelectedIndex = -1
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCust.PageIndexChanging
        gvListCust.PageIndex = e.NewPageIndex
        BindCustomerData()
        mpeListCust.Show()
    End Sub

    Protected Sub gvListCust_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCust.SelectedIndexChanged
        If custoid.Text <> gvListCust.SelectedDataKey.Item("custoid").ToString Then
            btnClearCust_Click(Nothing, Nothing)
        End If
        custoid.Text = gvListCust.SelectedDataKey.Item("custoid").ToString
        custname.Text = gvListCust.SelectedDataKey.Item("custname").ToString
        cashbanktaxtype.SelectedValue = gvListCust.SelectedDataKey.Item("custtaxable").ToString
        If cashbanktaxtype.SelectedValue = "TAX" Then
            cashbanktaxpct.Text = ToMaskEdit(GetTaxValue(), 4)
        Else
            cashbanktaxpct.Text = ""
        End If
        CountTotalAmt()
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub lkbCloseListCust_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCust.Click
        cProc.SetModalPopUpExtender(btnHideListCust, pnlListCust, mpeListCust, False)
    End Sub

    Protected Sub cashbankothertaxamt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cashbankothertaxamt.TextChanged
        cashbankgrandtotal.Text = ToMaskEdit(ToDouble(cashbankamt.Text) + ToDouble(cashbanktaxamt.Text) + ToDouble(cashbankothertaxamt.Text), 4)
    End Sub

    Protected Sub btnSearchCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchCOA.Click
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, True)
    End Sub

    Protected Sub btnClearCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearCOA.Click
        acctgoid2.Text = ""
        acctgdesc.Text = ""
        acctgcode.Text = ""
    End Sub

    Protected Sub btnFindListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListCOA.Click
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub btnAllListCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListCOA.Click
        FilterTextListCOA.Text = "" : FilterDDLListCOA.SelectedIndex = -1 : gvListCOA.SelectedIndex = -1
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListCOA.PageIndexChanging
        gvListCOA.PageIndex = e.NewPageIndex
        BindListCOA()
        mpeListCOA.Show()
    End Sub

    Protected Sub gvListCOA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListCOA.SelectedIndexChanged
        If acctgoid2.Text <> gvListCOA.SelectedDataKey.Item("acctgoid").ToString Then
            btnClearCOA_Click(Nothing, Nothing)
        End If
        acctgoid2.Text = gvListCOA.SelectedDataKey.Item("acctgoid").ToString
        acctgcode.Text = gvListCOA.SelectedDataKey.Item("acctgcode").ToString
        acctgdesc.Text = gvListCOA.SelectedDataKey.Item("acctgdesc").ToString
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub lkbCloseListCOA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListCOA.Click
        cProc.SetModalPopUpExtender(btnHideListCOA, pnlListCOA, mpeListCOA, False)
    End Sub

    Protected Sub btnAddToList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddToList.Click
        If IsDetailInputValid() Then
            If Session("TblDtl") Is Nothing Then
                Dim dtlTable As DataTable = New DataTable("TabelReceipt")
                dtlTable.Columns.Add("cashbankglseq", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("acctgcode", Type.GetType("System.String"))
                dtlTable.Columns.Add("acctgdesc", Type.GetType("System.String"))
                dtlTable.Columns.Add("cashbankglamt", Type.GetType("System.Double"))
                dtlTable.Columns.Add("cashbankglnote", Type.GetType("System.String"))
                dtlTable.Columns.Add("groupoid", Type.GetType("System.Int32"))
                dtlTable.Columns.Add("groupdesc", Type.GetType("System.String"))
                Session("TblDtl") = dtlTable
            End If
            Dim objTable As DataTable = Session("TblDtl")
            Dim dv As DataView = objTable.DefaultView
            If i_u2.Text = "New Detail" Then
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' AND groupoid=" & groupoid.SelectedValue
            Else
                dv.RowFilter = "acctgoid=" & acctgoid2.Text & " AND cashbankglnote='" & Tchar(cashbankglnote.Text) & "' AND cashbankglseq<>" & cashbankglseq.Text & " AND groupoid=" & groupoid.SelectedValue
            End If
            If dv.Count > 0 Then
                dv.RowFilter = ""
                showMessage("This Data has been added before, please check!", 2)
                Exit Sub
            End If
            dv.RowFilter = ""
            Dim objRow As DataRow
            If i_u2.Text = "New Detail" Then
                objRow = objTable.NewRow()
                cashbankglseq.Text = objTable.Rows.Count + 1
                objRow("cashbankglseq") = cashbankglseq.Text
            Else
                objRow = objTable.Rows(cashbankglseq.Text - 1)
                objRow.BeginEdit()
            End If
            objRow("acctgoid") = acctgoid2.Text
            objRow("acctgcode") = acctgcode.Text
            objRow("acctgdesc") = acctgdesc.Text
            objRow("cashbankglamt") = ToDouble(cashbankglamt.Text)
            objRow("cashbankglnote") = cashbankglnote.Text
            objRow("groupoid") = groupoid.SelectedValue
            objRow("groupdesc") = groupoid.SelectedItem.Text
            If i_u2.Text = "New Detail" Then
                objTable.Rows.Add(objRow)
            Else
                objRow.EndEdit()
            End If
            Session("TblDtl") = objTable
            gvDtl.DataSource = Session("TblDtl")
            gvDtl.DataBind()
            ClearDetail()
            CountTotalAmt()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click
        ClearDetail()
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDtl.SelectedIndexChanged
        Try
            cashbankglseq.Text = gvDtl.SelectedDataKey.Item("cashbankglseq").ToString
            If Session("TblDtl") Is Nothing = False Then
                i_u2.Text = "Update Detail"
                Dim objTable As DataTable = Session("TblDtl")
                Dim dv As DataView = objTable.DefaultView
                dv.RowFilter = "cashbankglseq=" & cashbankglseq.Text
                acctgoid2.Text = dv.Item(0).Item("acctgoid").ToString
                acctgcode.Text = dv.Item(0).Item("acctgcode").ToString
                acctgdesc.Text = dv.Item(0).Item("acctgdesc").ToString
                cashbankglamt.Text = ToMaskEdit(ToDouble(dv.Item(0).Item("cashbankglamt").ToString), 4)
                cashbankglnote.Text = dv.Item(0).Item("cashbankglnote").ToString
                groupoid.SelectedValue = dv.Item(0).Item("groupoid").ToString
                dv.RowFilter = ""
            End If
        Catch ex As Exception
            showMessage(ex.Message, 1)
        End Try
    End Sub

    Protected Sub gvDtl_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvDtl.RowDeleting
        Dim objTable As DataTable = Session("TblDtl")
        Dim objRow() As DataRow
        objRow = objTable.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
        objTable.Rows.Remove(objRow(e.RowIndex))
        For C1 As Int16 = 0 To objTable.Rows.Count - 1
            Dim dr As DataRow = objTable.Rows(C1)
            dr.BeginEdit()
            dr("cashbankglseq") = C1 + 1
            dr.EndEdit()
        Next
        Session("TblDtl") = objTable
        gvDtl.DataSource = objTable
        gvDtl.DataBind()
        ClearDetail()
        CountTotalAmt()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If IsInputValid() Then
            Dim isRegenOid As Boolean = False
            If Session("oid") = Nothing Or Session("oid") = "" Then
                sSql = "SELECT COUNT(*) FROM QL_trncashbankmst WHERE cashbankoid=" & cashbankoid.Text
                If CheckDataExists(sSql) Then
                    cashbankoid.Text = GenerateID("QL_TRNCASHBANKMST", CompnyCode)
                End If
                Dim sNo As String = cashbankno.Text
                GenerateReceiptNo()
                If sNo <> cashbankno.Text Then
                    isRegenOid = True
                End If
            Else
                Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
                If sStatusInfo <> "" Then
                    showMessage(sStatusInfo, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            End If
            cashbankgloid.Text = GenerateID("QL_TRNCASHBANKGL", CompnyCode)
            Dim iGlMstOid As Integer = GenerateID("QL_TRNGLMST", CompnyCode)
            Dim iGlDtlOid As Integer = GenerateID("QL_TRNGLDTL", CompnyCode)
            Dim cRate As New ClassRate()
            Dim iTaxAcctgOid As Integer = 0
            Dim iOtherTaxAcctgOid As Integer = 0
            Dim iGiroAcctgOid As Integer = ToInteger(giroacctgoid.Text)
            periodacctg.Text = GetDateToPeriodAcctg(CDate(cashbankdate.Text))
            Dim sDueDate As String = ""
            If cashbanktype.SelectedValue = "BKM" Or cashbanktype.SelectedValue = "BLM" Then
                sDueDate = cashbankdate.Text
            Else
                sDueDate = cashbankduedate.Text
            End If
            Dim sDTG As String = "1/1/1900"
            If cashbanktakegiro.Text <> "" Then
                sDTG = cashbanktakegiro.Text
            End If
            Dim sDate As String = cashbankdate.Text
            If cashbanktype.SelectedValue = "BBM" Then
                sDate = cashbankduedate.Text
            End If
            Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
            If cashbankstatus.Text = "Post" Then
                If isPeriodAcctgClosed(DDLBusUnit.SelectedValue, sDate) Then
                    showMessage("Cannot posting accounting data to period " & MonthName(Month(CDate(sDate))).ToUpper & " " & Year(CDate(sDate)).ToString & " anymore because the period has been closed. Please select another period!", 3) : cashbankstatus.Text = "In Process" : Exit Sub
                End If
                cRate.SetRateValue(CInt(curroid.SelectedValue), cashbankdate.Text)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
                Dim sVarErr As String = ""
                If ToDouble(cashbanktaxamt.Text) > 0 Then
                    If Not IsInterfaceExists("VAR_PPN_OUT", DDLBusUnit.SelectedValue) Then
                        sVarErr = "VAR_PPN_OUT"
                    Else
                        iTaxAcctgOid = GetAcctgOID(GetVarInterface("VAR_PPN_OUT", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If ToDouble(cashbankothertaxamt.Text) > 0 Then
                    If Not IsInterfaceExists("VAR_OTHER_TAX_RECEIPT", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_OTHER_TAX_RECEIPT"
                    Else
                        iOtherTaxAcctgOid = GetAcctgOID(GetVarInterface("VAR_OTHER_TAX_RECEIPT", DDLBusUnit.SelectedValue), CompnyCode)
                    End If
                End If
                If cashbanktype.SelectedValue = "BGM" Then
                    If Not IsInterfaceExists("VAR_GIRO_IN", DDLBusUnit.SelectedValue) Then
                        sVarErr &= IIf(sVarErr = "", "", " AND ") & "VAR_GIRO_IN"
                    End If
                End If
                If sVarErr <> "" Then
                    showMessage(GetInterfaceWarning(sVarErr, "posting"), 2)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
                If cashbanktype.SelectedValue = "BGM" Then
                    iGiroAcctgOid = GetAcctgOID(GetVarInterface("VAR_GIRO_IN", DDLBusUnit.SelectedValue), CompnyCode)
                End If
            End If
            Dim objTrans As SqlClient.SqlTransaction
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
            objTrans = conn.BeginTransaction()
            xCmd.Transaction = objTrans
            Try
                If Session("oid") = Nothing Or Session("oid") = "" Then
                    sSql = "INSERT INTO QL_trncashbankmst (cmpcode, cashbankoid, periodacctg, cashbankno, cashbankdate, cashbanktype, cashbankgroup, acctgoid, curroid, cashbankamt, cashbankamtidr, cashbankamtusd, personoid, cashbankduedate, cashbankrefno, cashbanknote, cashbankstatus, createuser, createtime, upduser, updtime, cashbanktakegiro, giroacctgoid, refsuppoid, cashbanktaxtype, cashbanktaxpct, cashbanktaxamt, cashbankothertaxamt, cashbankdpp, cashbankresamt) VALUES ('" & DDLBusUnit.SelectedValue & "', " & cashbankoid.Text & ", '" & periodacctg.Text & "', '" & cashbankno.Text & "', '" & cashbankdate.Text & "', '" & cashbanktype.SelectedValue & "', 'RECEIPT', " & acctgoid.SelectedValue & ", " & curroid.SelectedValue & ", " & ToDouble(cashbankgrandtotal.Text) & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", " & personoid.SelectedValue & ", '" & sDueDate & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & sDTG & "', " & iGiroAcctgOid & ", " & ToInteger(custoid.Text) & ", '" & cashbanktaxtype.SelectedValue & "', " & ToDouble(cashbanktaxpct.Text) & ", " & ToDouble(cashbanktaxamt.Text) & ", " & ToDouble(cashbankothertaxamt.Text) & ", " & ToDouble(cashbankamt.Text) & ", " & ToDouble(dparamt.Text) & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & cashbankoid.Text & " WHERE tablename='QL_TRNCASHBANKMST' and cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If cashbanktype.SelectedValue = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " & ToDouble(cashbankgrandtotal.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                Else
                    sSql = "UPDATE QL_trncashbankmst SET periodacctg='" & periodacctg.Text & "', cashbankno='" & cashbankno.Text & "', cashbankdate='" & cashbankdate.Text & "', cashbanktype='" & cashbanktype.SelectedValue & "', acctgoid=" & acctgoid.SelectedValue & ", curroid=" & curroid.SelectedValue & ", cashbankamt=" & ToDouble(cashbankgrandtotal.Text) & ", cashbankamtidr=" & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", cashbankamtusd=" & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", personoid=" & personoid.SelectedValue & ", cashbankduedate='" & sDueDate & "', cashbankrefno='" & Tchar(cashbankrefno.Text) & "', cashbanknote='" & Tchar(cashbanknote.Text) & "', cashbankstatus='" & cashbankstatus.Text & "', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP, cashbanktakegiro='" & sDTG & "', giroacctgoid=" & iGiroAcctgOid & ", refsuppoid=" & ToInteger(custoid.Text) & ", cashbanktaxtype='" & cashbanktaxtype.SelectedValue & "', cashbanktaxpct=" & ToDouble(cashbanktaxpct.Text) & ", cashbanktaxamt=" & ToDouble(cashbanktaxamt.Text) & ", cashbankothertaxamt=" & ToDouble(cashbankothertaxamt.Text) & ", cashbankdpp=" & ToDouble(cashbankamt.Text) & ", cashbankresamt=" & ToDouble(dparamt.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " & ToDouble(Session("lastdparamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & Session("lastdparoid")
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    If cashbanktype.SelectedValue = "BLM" Then
                        sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt + " & ToDouble(cashbankgrandtotal.Text) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & giroacctgoid.Text
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If Not Session("TblDtl") Is Nothing Then
                    Dim objTable As DataTable = Session("TblDtl")
                    For c1 As Int16 = 0 To objTable.Rows.Count - 1
                        sSql = "INSERT INTO QL_trncashbankgl (cmpcode, cashbankgloid, cashbankoid, acctgoid, cashbankglamt, cashbankglamtidr, cashbankglamtusd, cashbankglduedate, cashbankglrefno, cashbankglnote, cashbankglstatus, createuser, createtime, upduser, updtime) VALUES ('" & DDLBusUnit.SelectedValue & "', " & (c1 + CInt(cashbankgloid.Text)) & ", " & cashbankoid.Text & ", " & objTable.Rows(c1)("acctgoid").ToString & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(c1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", '" & IIf(cashbanktype.SelectedValue <> "BKM", cashbankduedate.Text, "1/1/1900") & "', '" & Tchar(cashbankrefno.Text) & "', '" & Tchar(objTable.Rows(c1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP)"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Next
                    sSql = "UPDATE QL_mstoid SET lastoid=" & (objTable.Rows.Count - 1 + CInt(cashbankgloid.Text)) & " WHERE tablename='QL_TRNCASHBANKGL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                If cashbankstatus.Text = "Post" Then
                    ' Insert Into GL Mst
                    sSql = "INSERT INTO QL_trnglmst (cmpcode, glmstoid, gldate, periodacctg, glnote, glflag, postdate, createuser, createtime, upduser, updtime, type, rateoid, rate2oid, glrateidr, glrate2idr, glrateusd, glrate2usd) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlMstOid & ", '" & sDate & "', '" & sPeriod & "', 'Cash/Bank Receipt|No=" & cashbankno.Text & "', '" & cashbankstatus.Text & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '" & Session("UserID") & "', CURRENT_TIMESTAMP, '', " & cRate.GetRateDailyOid & ", " & cRate.GetRateMonthlyOid & ", " & cRate.GetRateDailyIDRValue & ", " & cRate.GetRateMonthlyIDRValue & ", " & cRate.GetRateDailyUSDValue & ", " & cRate.GetRateMonthlyUSDValue & ")"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    ' Insert Into GL Dtl
                    ' DEBET
                    ' Detail Receipt
                    Dim iSeq As Integer = 1
                    Dim objTable As DataTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) < 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(C1)("acctgoid").ToString & ", 'D', " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) & ", '" & cashbankno.Text & "', '" & Tchar(objTable.Rows(C1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & -ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            iSeq += 1
                        End If
                    Next
                    If cashbanktype.SelectedValue = "BGM" Then
                        ' Hutang Giro
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iGiroAcctgOid & ", 'D', " & ToDouble(cashbankgrandtotal.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    Else
                        ' Kas/Bank
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & acctgoid.SelectedValue & ", 'D', " & ToDouble(cashbankgrandtotal.Text) & ", '" & cashbankno.Text & "', '" & Tchar(cashbanknote.Text) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankgrandtotal.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                    End If
                    iGlDtlOid += 1
                    iSeq += 1
                    ' CREDIT
                    objTable = Session("TblDtl")
                    For C1 As Int16 = 0 To objTable.Rows.Count - 1
                        If ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) > 0 Then
                            sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & objTable.Rows(C1)("acctgoid").ToString & ", 'C', " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) & ", '" & cashbankno.Text & "', '" & Tchar(objTable.Rows(C1)("cashbankglnote").ToString) & "', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(objTable.Rows(C1)("cashbankglamt").ToString) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "')"
                            xCmd.CommandText = sSql
                            xCmd.ExecuteNonQuery()
                            iGlDtlOid += 1
                            iSeq += 1
                        End If
                    Next
                    ' PPN Out
                    If ToDouble(cashbanktaxamt.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iTaxAcctgOid & ", 'C', " & ToDouble(cashbanktaxamt.Text) & ", '" & cashbankno.Text & "', '', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbanktaxamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbanktaxamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Tax C/B Receipt No. " & cashbankno.Text & "', 'M')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    ' Other Tax
                    If ToDouble(cashbankothertaxamt.Text) > 0 Then
                        sSql = "INSERT INTO QL_trngldtl (cmpcode, gldtloid, glseq, glmstoid, acctgoid, gldbcr, glamt, noref, glnote, glflag, upduser, updtime, glamtidr, glamtusd, glother1, glother3, glother4) VALUES ('" & DDLBusUnit.SelectedValue & "', " & iGlDtlOid & ", " & iSeq & ", " & iGlMstOid & ", " & iOtherTaxAcctgOid & ", 'C', " & ToDouble(cashbankothertaxamt.Text) & ", '" & cashbankno.Text & "', '', '" & cashbankstatus.Text & "', '" & Session("UserID") & "', CURRENT_TIMESTAMP, " & ToDouble(cashbankothertaxamt.Text) * cRate.GetRateMonthlyIDRValue & ", " & ToDouble(cashbankothertaxamt.Text) * cRate.GetRateMonthlyUSDValue & ", 'QL_trncashbankmst " & cashbankoid.Text & "', 'Other Tax C/B Receipt No. " & cashbankno.Text & "', 'M')"
                        xCmd.CommandText = sSql
                        xCmd.ExecuteNonQuery()
                        iGlDtlOid += 1
                        iSeq += 1
                    End If
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlMstOid & " WHERE tablename='QL_TRNGLMST' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGlDtlOid & " WHERE tablename='QL_TRNGLDTL' AND cmpcode='" & CompnyCode & "'"
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()
                End If
                objTrans.Commit()
                xCmd.Connection.Close()
            Catch exSql As SqlException
                objTrans.Rollback() : conn.Close()
                If ToDouble(Session("ErrorCounter")) < 5 Then
                    If exSql.Number = 2627 Then
                        Session("ErrorCounter") = ToDouble(Session("ErrorCounter")) + 1
                        btnSave_Click(sender, e)
                        Exit Sub
                    Else
                        showMessage(exSql.Message, 1)
                        cashbankstatus.Text = "In Process"
                        Exit Sub
                    End If
                Else
                    showMessage(exSql.Message, 1)
                    cashbankstatus.Text = "In Process"
                    Exit Sub
                End If
            Catch ex As Exception
                objTrans.Rollback()
                showMessage(ex.Message, 1)
                xCmd.Connection.Close()
                cashbankstatus.Text = "In Process"
                Exit Sub
            End Try
            If isRegenOid Then
                Session("SavedInfo") = "Cash/Bank No. have been regenerated because being used by another data. Your new Cash/Bank No. is " & cashbankno.Text & ".<BR>"
            End If
            If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
                showMessage(Session("SavedInfo"), 3)
            Else
                Response.Redirect("~\Accounting\trnReceipt.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("~\Accounting\trnReceipt.aspx?awal=true")
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        If cashbankoid.Text = "" Then
            showMessage("Please select Cash/Bank Receipt data first!", 2)
            Exit Sub
        Else
            Dim sStatusInfo As String = GetMultiUserStatus("QL_trncashbankmst", "cashbankoid", cashbankoid.Text, "cashbankstatus", updtime.Text, "Post")
            If sStatusInfo <> "" Then
                showMessage(sStatusInfo, 2)
                cashbankstatus.Text = "In Process"
                Exit Sub
            End If
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim objTrans As SqlClient.SqlTransaction
        objTrans = conn.BeginTransaction()
        xCmd.Transaction = objTrans
        Try
            sSql = "DELETE FROM QL_trncashbankgl WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            sSql = "DELETE FROM QL_trncashbankmst WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND cashbankoid=" & cashbankoid.Text & ""
            xCmd.CommandText = sSql
            xCmd.ExecuteNonQuery()
            If Session("lastcashbanktype") IsNot Nothing And Session("lastcashbanktype") = "BLM" Then
                sSql = "UPDATE QL_trndpar SET dparaccumamt = dparaccumamt - " & ToDouble(Session("lastdparamt").ToString) & " WHERE cmpcode='" & DDLBusUnit.SelectedValue & "' AND dparoid=" & Session("lastdparoid")
                xCmd.CommandText = sSql
                xCmd.ExecuteNonQuery()
            End If
            objTrans.Commit()
            xCmd.Connection.Close()
        Catch ex As Exception
            objTrans.Rollback()
            xCmd.Connection.Close()
            showMessage(ex.Message, 1)
            Exit Sub
        End Try
        Response.Redirect("~\Accounting\trnReceipt.aspx?awal=true")
    End Sub

    Protected Sub btnposting_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnposting.Click
        cashbankstatus.Text = "Post"
        btnSave_Click(Nothing, Nothing)
    End Sub

    Protected Sub btnShowCOA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShowCOA.Click
        DDLRateType.SelectedIndex = -1
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, True)
    End Sub

    Protected Sub lkbCloseCOAPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseCOAPosting.Click
        cProc.SetModalPopUpExtender(btnHideCOAPosting, pnlCOAPosting, mpeCOAPosting, False)
    End Sub

    Protected Sub gvCOAPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCOAPosting.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = ToMaskEdit(ToDouble(e.Row.Cells(2).Text), 4)
            e.Row.Cells(3).Text = ToMaskEdit(ToDouble(e.Row.Cells(3).Text), 4)
        End If
    End Sub

    Protected Sub DDLRateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLRateType.SelectedIndexChanged
        ShowCOAPosting(cashbankno.Text, DDLBusUnit.SelectedValue, gvCOAPosting, DDLRateType.SelectedValue, "QL_trncashbankmst " & cashbankoid.Text)
        mpeCOAPosting.Show()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrint.Click
        UpdateCheckedValue()
        ShowReport()
    End Sub

    Protected Sub btnSearchDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchDP.Click
        If DDLBusUnit.SelectedValue = "" Then
            showMessage("Please select Business Unit first!", 2)
            Exit Sub
        End If
        If curroid.SelectedValue = "" Then
            showMessage("Please select Currency first!", 2)
            Exit Sub
        End If
        If custoid.Text = "" Then
            showMessage("Please select Customer first!", 2)
            Exit Sub
        End If
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, True)
    End Sub

    Protected Sub btnClearDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearDP.Click
        giroacctgoid.Text = "" : cashbankrefno.Text = "" : acctgoid.SelectedIndex = -1 : dparamt.Text = ""
    End Sub

    Protected Sub btnFindListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListDP.Click
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub btnAllListDP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAllListDP.Click
        FilterDDLListDP.SelectedIndex = -1 : FilterTextListDP.Text = "" : gvListDP.SelectedIndex = -1
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvListDP.PageIndexChanging
        gvListDP.PageIndex = e.NewPageIndex
        BindDPData()
        mpeListDP.Show()
    End Sub

    Protected Sub gvListDP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListDP.SelectedIndexChanged
        If giroacctgoid.Text <> gvListDP.SelectedDataKey.Item("dparoid").ToString Then
            btnClearDP_Click(Nothing, Nothing)
        End If
        giroacctgoid.Text = gvListDP.SelectedDataKey.Item("dparoid").ToString
        cashbankrefno.Text = gvListDP.SelectedDataKey.Item("dparno").ToString
        acctgoid.SelectedValue = gvListDP.SelectedDataKey.Item("dparacctgoid").ToString
        dparamt.Text = ToMaskEdit(ToDouble(gvListDP.SelectedDataKey.Item("dparamt").ToString), 4)
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub

    Protected Sub lkbCloseListDP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbCloseListDP.Click
        cProc.SetModalPopUpExtender(btnHideListDP, pnlListDP, mpeListDP, False)
    End Sub
#End Region

End Class