Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO
Imports ClassFunction
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Reporting

Structure VarTransferValue
    Public Val_IDR As Double
End Structure

Partial Class Transaction_TransferConfirmation
    Inherits System.Web.UI.Page

#Region "Variables"
    Public CompnyCode As String = ConfigurationSettings.AppSettings("CompnyCode")
    Public BranchTrnCode As String = ConfigurationSettings.AppSettings("BranchTrnCode")
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
    Dim conn As New SqlConnection(ConfigurationSettings.AppSettings("QL_CONN"))
    Public DefaultFormatCounter As Int16 = ConfigurationSettings.AppSettings("DefaultFormatCounterLong")
    Dim xCmd As New SqlCommand("", conn)
    Dim xreader As SqlDataReader
    Dim sSql As String = ""
    Dim cKon As New Koneksi
    Dim cProc As New ClassProcedure
    Dim report As New ReportDocument
    Dim cRate As New ClassRate()
    Dim cRate2 As New ClassRate()
    Public folderReport As String = "~/Report/"
#End Region

#Region "Functions"
    Private Function GetDetailOid(ByRef sRef As String) As String
        Dim sReturn As String = ""
        For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
            Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
            If (row.RowType = DataControlRowType.DataRow) Then
                Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                For Each myControl As System.Web.UI.Control In cc
                    If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                        Dim cbCheck As Boolean = CType(myControl, System.Web.UI.WebControls.CheckBox).Checked
                        If cbCheck Then
                            sReturn &= CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip & ","
                            sRef &= CType(myControl, System.Web.UI.WebControls.CheckBox).Text & ","
                        End If
                    End If
                Next
            End If
        Next
        If sReturn <> "" Then
            sReturn = Left(sReturn, sReturn.Length - 1)
        End If
        If sRef <> "" Then
            sRef = Left(sRef, sRef.Length - 1)
        End If
        Return sReturn
    End Function

    Private Function GetTextBoxValue(ByVal cc As System.Web.UI.ControlCollection) As String
        Dim sReturn As String = ""
        For Each myControl As System.Web.UI.Control In cc
            If TypeOf (myControl) Is System.Web.UI.WebControls.TextBox Then
                sReturn = CType(myControl, System.Web.UI.WebControls.TextBox).Text
            End If
        Next
        Return sReturn
    End Function

    Private Function UpdateCheckedMat() As Boolean
        Dim bReturn As Boolean = False
        Dim sIRDtlOid As String = ""
        'Dim sMTDtlOid As String = GetDetailOid(sIRDtlOid)
        If Not Session("TblDtl") Is Nothing Then
            Dim dtTbl As DataTable = Session("TblDtl")
            If dtTbl.Rows.Count > 0 Then
                Dim dtView As DataView = dtTbl.DefaultView
                dtView.RowFilter = ""
                dtView.AllowEdit = True
                For C1 As Integer = 0 To gvPODtl.Rows.Count - 1
                    Dim row As System.Web.UI.WebControls.GridViewRow = gvPODtl.Rows(C1)
                    If (row.RowType = DataControlRowType.DataRow) Then
                        Dim cc As System.Web.UI.ControlCollection = row.Cells(0).Controls
                        For Each myControl As System.Web.UI.Control In cc
                            If TypeOf (myControl) Is System.Web.UI.WebControls.CheckBox Then
                                Dim cbOid As String = CType(myControl, System.Web.UI.WebControls.CheckBox).ToolTip
                                dtView.RowFilter = "oid=" & cbOid
                            End If
                        Next
                    End If
                    dtView.RowFilter = ""
                Next
                Session("TblDtl") = dtTbl
                bReturn = True
            End If
        End If
        Return bReturn
    End Function

    Private Function IsDetailInputValid() As Boolean
        Dim sError As String = ""
        If Session("TblDtl") Is Nothing Then
            sError &= "- Plese Fill detail data!"
        Else
            Dim objTbl As DataTable = Session("TblDtl")
            If objTbl.Rows.Count <= 0 Then
                sError &= "- Please fill DETAIL DATA!<BR>"
            Else
                For C1 As Integer = 0 To objTbl.Rows.Count - 1
                    If CDate(objTbl.Rows(C1)("cashbankdate").ToString) > CDate(confirmdate.Text) Then
                        sError &= "- CANCEL Date must More or equal than Giro Date !<BR>"
                    End If
                Next
            End If
        End If

        If sError <> "" Then
            showMessage(sError, 2)
            Exit Function
            Return False
        End If
        Return True
    End Function

    Private Function GetAmtByGroup(ByVal sOid As String) As DataTable
        GetAmtByGroup = Nothing
        sSql = "SELECT SUM(bd.mtvalue*mtqty) AS iramt, g.genother3 AS acctgoid, 0.0 AS amtbelinettowithtax FROM QL_trnmattransdtl bd INNER JOIN QL_mstmat m ON m.cmpcode=bd.cmpcode AND m.matoid=bd.matoid INNER JOIN QL_mstgen g ON g.cmpcode=m.cmpcode AND g.genoid=m.matsubcategoryoid WHERE bd.cmpcode='" & Session("CompnyCode") & "' AND bd.mtmstoid=" & sOid & " GROUP BY g.genother3"
        Dim dtAcctg As DataTable = cKon.ambiltabel(sSql, "QL_trnirdtl")
        Return dtAcctg
    End Function

    Private Function GetStockValue(ByVal sOid As String, ByVal sRef As String) As Double
        Dim sPeriod As String = GetDateToPeriodAcctg(GetServerTime())
        Try
            sSql = "SELECT SUM(ISNULL(stockqty, 0) * ISNULL(stockvalue, 0)) / SUM(ISNULL(stockqty, 0)) FROM QL_stockvalue WHERE cmpcode='" & Session("CompnyCode") & "' AND periodacctg IN ('" & sPeriod & "') AND refoid=" & sOid & " AND refname='" & sRef & "' AND closeflag=''"
            GetStockValue = ToDouble(GetStrData(sSql))
        Catch ex As Exception
            GetStockValue = 0
        End Try
    End Function
#End Region

#Region "Procedures"
    Private Sub showMessage(ByVal sMessage As String, ByVal iType As Integer)
        Dim strCaption As String = CompnyName
        If iType = 1 Then ' Error
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        ElseIf iType = 2 Then ' Warning
            imIcon.ImageUrl = "~/images/warn.png" : strCaption &= " - WARNING"
        ElseIf iType = 3 Then ' Information
            imIcon.ImageUrl = "~/images/information.png" : strCaption &= " - INFORMATION"
        Else
            imIcon.ImageUrl = "~/images/error.jpg" : strCaption &= " - ERROR"
        End If
        lblCaption.Text = strCaption : lblPopUpMsg.Text = sMessage
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, True)
    End Sub

    Private Sub BindListMT()
        sSql = "SELECT mtm.mtmstoid, mtm.mtdate AS sortdate, mtm.mtno, CONVERT(VARCHAR(10), mtm.mtdate, 101) AS mtdate, g.gendesc AS warehouse, mtm.mtmstnote, mtm.mtwhoid AS mtrwhoid FROM QL_trnmattransmst mtm INNER JOIN QL_mstgen g ON mtm.cmpcode=g.cmpcode AND mtm.mtwhoid=g.genoid WHERE mtm.cmpcode='" & Session("CompnyCode") & "' AND ISNULL(mtm.mtmstflag,'')='' AND mtmststatus IN ('Post', 'Closed') AND " & FilterDDLListPO.SelectedValue & " LIKE '%" & Tchar(FilterTextListPO.Text) & "%'"
        sSql &= " ORDER BY sortdate DESC"
        FillGV(gvListPO, sSql, "QL_trnmattransmst")
    End Sub

    Private Sub ViewDetailMT(ByVal iOid As Integer)
        sSql = "SELECT DISTINCT mtd.mtdtloid, mtm.mtmstoid, mtm.mtno, mtd.irdtloid, mtd.mtdtlseq, m.matcode, m.matshortdesc, mtd.mtqty, g.gendesc AS mtunit, irm.irno, mtd.mtdtlnote, irm.irdate, mtd.mtvalue, m.matoid,  mtd.mtqty AS confirmqty, 0.0 AS balanceqty, '' AS reason, ISNULL(gx.genother3,0) AS stockacctgoid FROM QL_trnmattransdtl mtd INNER JOIN QL_trnmattransmst mtm ON mtm.cmpcode=mtd.cmpcode AND mtm.mtmstoid=mtd.mtmstoid INNER JOIN QL_mstmat m ON m.cmpcode=mtd.cmpcode AND m.matoid=mtd.matoid INNER JOIN QL_mstgen g ON g.cmpcode=mtd.cmpcode AND g.genoid=mtd.mtunitoid INNER JOIN QL_trnirmst irm ON irm.cmpcode=mtd.cmpcode AND irm.irmstoid=mtd.irmstoid LEFT JOIN QL_mstgen gx ON gx.genoid=m.matsubcategoryoid AND gx.gengroup='SUB CATEGORY' WHERE mtd.cmpcode='" & Session("CompnyCode") & "' AND mtd.mtmstoid=" & iOid & " AND ISNULL(mtd.mtdtlflag,'')='' "
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_trnmattransdtl")
        Session("TblDtl") = dt
        gvPODtl.DataSource = Session("TblDtl")
        gvPODtl.DataBind()
    End Sub

    Private Sub InitAllDDL()
        sSql = "SELECT genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='MTR LOCATION' AND genactiveflag='ACTIVE' /*AND ISNULL(genother1,'')='" & BranchTrnCode & "'*/ ORDER BY gendesc"
        FillDDL(mtrwhoid, sSql)

        sSql = "SELECT TOP 1 genoid, gendesc FROM QL_mstgen WHERE cmpcode='" & Session("CompnyCode") & "' AND gengroup='MTR LOCATION' AND genactiveflag='ACTIVE' AND gencode='MTRLOCOTW'"
        FillDDL(mtrwhfrom, sSql)
    End Sub

    Private Sub SetUsageValue(ByRef objVal As VarTransferValue)
        objVal.Val_IDR = 0
        Dim dt As DataTable = Session("TblDtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            Dim dValIDR As Double = GetStockValue(dt.Rows(C1)("matoid").ToString, "MATERIAL")
            dt.Rows(C1)("mtvalue") = dValIDR
            objVal.Val_IDR += dValIDR
        Next
        Session("TblDtl") = dt
    End Sub

    Private Sub BindGiroData()
        sSql = "SELECT * FROM ( "
        sSql &= "SELECT 0 AS nmr, cb.cashbankoid, cb.cashbankno, cb.cashbankrefno, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN '' ELSE s.suppname END) AS suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cb.cashbanktakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) AS cashbankduedate, cashbankamt, cb.createuser, cb.cashbankamtidr, cb.cashbankamtusd, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN cb.personoid ELSE s.suppoid END) AS suppoid, cb.cashbanknote, cb.giroacctgoid, cb.acctgoid, cb.curroid, '' AS giroflagnote, 'QL_trncashbankmst' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbankres2' AS fieldnote, 'cashbanktakegiroreal' AS fieldtakegiro, cashbankgroup FROM QL_trncashbankmst cb INNER JOIN QL_mstsupp s ON s.suppoid=(CASE cb.cashbankgroup WHEN 'EXPENSE' THEN cb.refsuppoid ELSE cb.personoid END) WHERE cb.cmpcode='" & Session("CompnyCode") & "' AND cb.cashbanktype='BGK' AND cashbankgroup IN ('AP') AND cb.cashbankstatus='Post' AND ISNULL(cb.cashbankres1, '')='' /*AND cb.cashbankdate<=CONVERT(DATETIME, '" & confirmdate.Text & " 23:59:59')*/ UNION ALL "
        sSql &= "SELECT 0 AS nmr, cb.cashbankoid, cb.cashbankno, cb.cashbankrefno, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN '' ELSE s.custname END) AS suppname, CONVERT(VARCHAR(10), cb.cashbankdate, 101) AS cashbankdate, CONVERT(VARCHAR(10), cb.cashbanktakegiro, 101) AS cashbanktakegiro, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) AS cashbankduedate, cashbankamt, cb.createuser, cb.cashbankamtidr, cb.cashbankamtusd, (CASE cb.cashbankgroup WHEN 'MUTATION' THEN cb.personoid ELSE s.custoid END) AS suppoid, cb.cashbanknote, cb.giroacctgoid, cb.acctgoid, cb.curroid, '' AS giroflagnote, 'QL_trncashbankmst' AS tblname, '' AS girostatus, '' AS postingcashbankno, 'cashbankoid' AS fieldoid, 'cashbankres1' AS fieldstatus, 'cashbankres2' AS fieldnote, 'cashbanktakegiroreal' AS fieldtakegiro, cashbankgroup FROM QL_trncashbankmst cb INNER JOIN QL_mstcust s ON s.custoid=(CASE cb.cashbankgroup WHEN 'RECEIPT' THEN cb.refsuppoid ELSE cb.personoid END) WHERE cb.cmpcode='" & Session("CompnyCode") & "' AND cb.cashbanktype='BGM' AND cashbankgroup IN ('AR') AND cb.cashbankstatus='Post' AND ISNULL(cb.cashbankres1, '')='' /*AND cb.cashbankdate<=CONVERT(DATETIME, '" & confirmdate.Text & " 23:59:59')*/ "
        sSql &= ") AS QL_girodetail "
        sSql &= "WHERE (cashbankno LIKE '%" & Tchar(mtno.Text) & "%' OR suppname LIKE '%" & Tchar(mtno.Text) & "%' OR cashbankrefno LIKE '%" & Tchar(mtno.Text) & "%') "
        sSql &= "ORDER BY CONVERT(DATETIME, cashbanktakegiro) DESC, cashbankno DESC"
        Dim dt As DataTable = cKon.ambiltabel(sSql, "QL_girodetail")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("nmr") = C1 + 1
        Next
        dt.AcceptChanges()
        Session("TblDataGiro") = dt
        gvDtl.DataSource = Session("TblDataGiro")
        gvDtl.DataBind()
    End Sub

    Private Sub BindDetailGiro()
        sSql = "SELECT * FROM ( "
        If cashbankgroup.Text.ToUpper = "AP" Then
            sSql &= "SELECT 0 seq, cb.cmpcode, pay.payapoid oid, cb.cashbankoid, cashbankno, CONVERT(VARCHAR(10), cb.cashbankdate, 101) cashbankdate, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) cashbankduedate, cashbankrefno, bm.trnbelino invoiceno, pay.payapamtidr payamtidr, pay.payapamtusd payamtusd, pay.reftype, con.refoid, pay.suppoid cust_supp_oid, cb.curroid, pay.acctgoid refacctgoid, 'AP' reftype2, con.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayap pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conap con ON con.payrefoid=pay.payapoid INNER JOIN QL_trnbelimst bm ON bm.cmpcode=pay.cmpcode AND bm.trnbelimstoid=pay.refoid WHERE pay.reftype='QL_trnbelimst' AND ISNULL(pay.payapres1,'')<>'Lebih Bayar'"
        Else
            sSql &= "SELECT 0 seq, cb.cmpcode, cb.cashbankoid, pay.payaroid oid, cashbankno, CONVERT(VARCHAR(10), cb.cashbankdate, 101) cashbankdate, CONVERT(VARCHAR(10), cb.cashbankduedate, 101) cashbankduedate, cashbankrefno, bm.trnjualno invoiceno, pay.payaramtidr payamtidr, pay.payaramtusd payamtusd, pay.reftype, con.refoid, pay.custoid cust_supp_oid, cb.curroid, pay.acctgoid refacctgoid, 'AR' reftype2, con.acctgoid FROM QL_trncashbankmst cb INNER JOIN QL_trnpayar pay ON pay.cmpcode=cb.cmpcode AND pay.cashbankoid=cb.cashbankoid INNER JOIN QL_conar con ON con.payrefoid=pay.payaroid INNER JOIN QL_trnjualmst bm ON bm.cmpcode=pay.cmpcode AND bm.trnjualmstoid=pay.refoid WHERE pay.reftype='QL_trnjualmst' AND ISNULL(pay.payarres1,'')<>'Lebih Bayar' "
        End If
        sSql &= ") AS Tb_Dtl WHERE reftype2='" & cashbankgroup.Text & "' AND cashbankoid=" & cashbankoid.Text
        Dim dt As DataTable = cKon.ambiltabel(sSql, "Giro_dtl")
        For C1 As Integer = 0 To dt.Rows.Count - 1
            dt.Rows(C1)("seq") = C1 + 1
        Next
        dt.AcceptChanges()
        Session("TblDtl") = dt
        gvPODtl.DataSource = Session("TblDtl")
        gvPODtl.DataBind()
    End Sub

    Private Sub GenerateCNNo()
        Dim sNo As String = "GC" & cashbankgroup.Text.ToUpper & "-" & Format(CDate(confirmdate.Text), "yyMM") & "-"
        sSql = "SELECT ISNULL(MAX(CAST(RIGHT(cnno, 4) AS INTEGER)) + 1, 1) AS IDNEW FROM QL_trngirocancel WHERE cmpcode='" & CompnyCode & "' AND cnno LIKE '%" & sNo & "%'"
        Session("cnno") = GenNumberString(sNo, "", GetStrData(sSql), 4)
    End Sub

#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserID") = "" Then
            Response.Redirect("~/Other/login.aspx")
        End If
        'Session.Timeout = 60
        If Request.QueryString("awal") = "true" Then
            ' Simpan session ke variabel temporary supaya tidak hilang
            Dim userId As String = Session("UserID")
            Dim xsetAcc As DataTable = Session("SpecialAccess")
            Dim appLimit As Decimal = Session("ApprovalLimit")
            Dim xsetRole As DataTable = Session("Role")
            Dim cmpcode As String = Session("CompnyCode")
            ' Clear all session
            Session.Clear()
            ' Insertkan lagi sesion yg sebelumnya disimpan di variabel temporary
            Session("CompnyCode") = cmpcode
            Session("UserID") = userId
            Session("SpecialAccess") = xsetAcc
            Session("ApprovalLimit") = appLimit
            Session("Role") = xsetRole
            ' Halaman ini di call lagi untuk menghilangkan parameter "awal=true"
            Response.Redirect("~\Accounting\trnGiroCancel.aspx")
        End If
        If checkPagePermission("~\Accounting\trnGiroCancel.aspx", Session("Role")) = False Then
            Response.Redirect("~\Other\NotAuthorize.aspx")
        End If
        Page.Title = CompnyName & " - Cancel Confirmation"
        imbConfirm.Attributes.Add("OnClick", "javascript:return confirm('Are you sure to CONFIRM this data?');")
        If Not Page.IsPostBack Then
            upduser.Text = Session("UserID") : updtime.Text = GetServerTime().ToString
            confirmdate.Text = Format(GetServerTime(), "MM/dd/yyyy")
            'InitAllDDL()
        End If
    End Sub

    Protected Sub imbOKPopUpMsg_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOKPopUpMsg.Click
        cProc.SetModalPopUpExtender(bePopUpMsg, pnlPopUpMsg, mpePopUpMsg, False)
        If Not Session("ShowError") Is Nothing And Session("ShowError") <> "" Then
            Session("ShowError") = Nothing
        End If
        If Not Session("SavedInfo") Is Nothing And Session("SavedInfo") <> "" Then
            If lblPopUpMsg.Text = Session("SavedInfo") Then
                Response.Redirect("~\Accounting\trnGiroCancel.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub imbFindMT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbFindMT.Click
        BindGiroData()
        pnlGiro.Visible = True
        'cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, True)
    End Sub

    Protected Sub imbEraseMT_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbEraseMT.Click
        mtmstoid.Text = ""
        mtno.Text = ""
        cashbankoid.Text = ""
        cashbankgroup.Text = ""
        curroid.Text = ""
        cProc.DisposeGridView(gvDtl)
        cProc.DisposeGridView(gvPODtl)
        mtno.Enabled = True : mtno.CssClass = "inpText"
    End Sub

    Protected Sub btnFindListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFindListPO.Click
        BindListMT()
        mpeListPO.Show()
    End Sub

    Protected Sub btnViewAllListPO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnViewAllListPO.Click
        FilterTextListPO.Text = ""
        FilterDDLListPO.SelectedIndex = -1
        BindListMT()
        mpeListPO.Show()
    End Sub

    Protected Sub gvListPO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvListPO.SelectedIndexChanged
        mtno.Text = gvListPO.SelectedDataKey.Item("mtno").ToString
        mtmstoid.Text = gvListPO.SelectedDataKey.Item("mtmstoid").ToString
        mtrwhoid.SelectedValue = gvListPO.SelectedDataKey.Item("mtrwhoid").ToString
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
        ViewDetailMT(CInt(mtmstoid.Text))
    End Sub

    Protected Sub lkbListPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lkbListPO.Click
        cProc.SetModalPopUpExtender(btnHiddenListPO, PanelListPO, mpeListPO, False)
    End Sub

    Protected Sub gvPODtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPODtl.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(7).Text = ToMaskEdit(ToDouble(e.Row.Cells(7).Text), 2)
        End If
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("~\Accounting\trnGiroCancel.aspx?awal=true")
    End Sub

    Protected Sub imbConfirm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbConfirm.Click
        If UpdateCheckedMat() Then
            If IsDetailInputValid() Then
                If cashbankoid.Text = "" Then
                    showMessage("Please select GIRO No. first!", 2)
                    Exit Sub
                End If
                Dim objTable As DataTable = Session("TblDtl")
                Dim sIRDtlOid As String = ""
                Dim sMTDtlOid As String = GetDetailOid(sIRDtlOid)
                Dim conmtroid, crdmatoid, iStockValOid, iGLMstOid, iGLDtlOid As Integer
                Dim dtStockOid As DataTable = Nothing

                conmtroid = GenerateID("QL_CONMAT", Session("CompnyCode"))
                crdmatoid = GenerateID("QL_CRDMTR", Session("CompnyCode"))

                Dim iConapOid As Integer = GenerateID("QL_CONAP", Session("CompnyCode"))
                Dim iConarOid As Integer = GenerateID("QL_CONAR", Session("CompnyCode"))
                Dim iCOADebet As Integer = GetAcctgOID(GetVarInterface("VAR_GIRO", Session("CompnyCode")), Session("CompnyCode"))
                Dim iCOACredit As Integer = GetAcctgOID(GetVarInterface("VAR_HUTANG_GIRO_TOLAKAN", Session("CompnyCode")), Session("CompnyCode"))
                If cashbankgroup.Text = "AR" Then
                    iCOADebet = GetAcctgOID(GetVarInterface("VAR_PIUTANG_GIRO_TOLAKAN", Session("CompnyCode")), Session("CompnyCode"))
                    iCOACredit = GetAcctgOID(GetVarInterface("VAR_GIRO_IN", Session("CompnyCode")), Session("CompnyCode"))
                End If
                Dim iacctgoiddebet, iacctgoidcredit As Integer
                Dim icnOid As Integer = GenerateID("QL_TRNGIROCANCEL", Session("CompnyCode"))
                periodacctg.Text = GetDateToPeriodAcctg(confirmdate.Text)
                Dim sDate As String = confirmdate.Text
                Dim sPeriod As String = GetDateToPeriodAcctg(CDate(sDate))
                Dim iSeq As Int16 = 1
                cRate.SetRateValue(CInt(curroid.Text), sDate)
                If cRate.GetRateDailyLastError <> "" Then
                    showMessage(cRate.GetRateDailyLastError, 2)
                    Exit Sub
                End If
                If cRate.GetRateMonthlyLastError <> "" Then
                    showMessage(cRate.GetRateMonthlyLastError, 2)
                    Exit Sub
                End If
                iGLMstOid = GenerateID("QL_TRNGLMST", Session("CompnyCode"))
                iGLDtlOid = GenerateID("QL_TRNGLDTL", Session("CompnyCode"))
                iStockValOid = GenerateID("QL_STOCKVALUE", Session("CompnyCode"))
                'dtStockOid = GetAmtByGroup(mtmstoid.Text)
                If isPeriodAcctgClosed(Session("CompnyCode"), sDate) Then
                    showMessage("- This periode hass been CLOSED before, Please CANCEL this transaction and try again!", 2)
                    Exit Sub
                End If
                If sMTDtlOid = "" Then
                    showMessage("Please select Detail Material Data first!", 2)
                    Exit Sub
                End If
                Dim dv As DataView = objTable.DefaultView
                Dim iCheck As Integer = dv.Count

                ' Define Tabel Strucure utk Auto Jurnal 
                sSql = "SELECT 0 acctgoid, 0.0 debet, 0.0 credit, '' refno, '' note, '' AS glother1 "
                Dim tbPostGL As DataTable = cKon.ambiltabel(sSql, "PostGL")
                tbPostGL.Rows.Clear()
                Dim objTrans As SqlClient.SqlTransaction
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
                objTrans = conn.BeginTransaction()
                xCmd.Transaction = objTrans
                Try
                    sSql = "UPDATE QL_trncashbankmst SET cashbankres1='Cancel', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & Session("CompnyCode") & "' AND cashbankoid=" & cashbankoid.Text
                    xCmd.CommandText = sSql
                    xCmd.ExecuteNonQuery()

                    For C1 As Int16 = 0 To dv.Count - 1
                        GenerateCNNo()
                        sSql = "INSERT INTO QL_trngirocancel (cmpcode,cnoid,cnno,cndate,reftype,refoid,suppcustoid,dbacctgoid,cracctgoid,curroid,rateoid,rate2oid,cnamt,cnamtidr,cnamtusd, cntaxtype,cntaxamt,cntaxamtidr,cntaxamtusd,cntotal,cntotalidr,cntotalusd,cnnote,cnstatus,createuser,createtime,upduser,updtime, cashbankno, cashbankoid) VALUES ('" & Session("CompnyCode") & "'," & icnOid & ",'" & Session("cnno") & "','" & confirmdate.Text & "','" & cashbankgroup.Text & "'," & dv(C1).Item("refoid") & "," & dv(C1).Item("cust_supp_oid") & "," & iCOADebet & "," & iCOACredit & "," & dv(C1).Item("curroid") & ",0,0," & ToDouble(dv(C1).Item("payamtidr")) & "," & ToDouble(dv(C1).Item("payamtidr")) & "," & ToDouble(dv(C1).Item("payamtusd")) & ",'',0,0,0," & ToDouble(dv(C1).Item("payamtidr")) & "," & ToDouble(dv(C1).Item("payamtidr")) & "," & ToDouble(dv(C1).Item("payamtusd")) & ",'','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP, '" & mtno.Text & "', " & cashbankoid.Text & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        If cashbankgroup.Text = "AP" Then
                            iacctgoiddebet = iCOADebet
                            iacctgoidcredit = dv(C1).Item("acctgoid")
                            sSql = "INSERT INTO QL_conap (cmpcode,conapoid,reftype,refoid,payrefoid,suppoid,acctgoid,trnapstatus,trnaptype,trnapdate,periodacctg,paymentacctgoid,paymentdate,payrefno, paybankoid,payduedate,amttrans,amtbayar,trnapnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES ('" & Session("CompnyCode") & "'," & iConapOid & ",'" & dv(C1).Item("reftype") & "'," & dv(C1).Item("refoid") & "," & icnOid & "," & dv(C1).Item("cust_supp_oid") & ", " & dv(C1).Item("acctgoid") & ",'Post','GC" & cashbankgroup.Text.ToUpper & "','1/1/1900','" & sPeriod & "'," & iCOADebet & ",'" & sDate & "', '" & Session("cnno") & "',0,'" & sDate & "',0," & (ToDouble(dv(C1).Item("payamtidr"))) * -1 & ",'GC No. " & Session("cnno") & " for A/P No. " & dv(C1).Item("invoiceno") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(dv(C1).Item("payamtidr"))) * -1 & ", 0," & (ToDouble(dv(C1).Item("payamtusd"))) * -1 & ",0)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = " UPDATE QL_TRNBELIMST SET trnbelimststatus='Approved', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & Session("CompnyCode") & "' AND trnbelimstoid=" & dv(C1).Item("refoid")
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        Else
                            iacctgoiddebet = dv(C1).Item("acctgoid")
                            iacctgoidcredit = iCOACredit
                            sSql = "INSERT INTO QL_conar (cmpcode,conaroid,reftype,refoid,payrefoid,custoid,acctgoid,trnarstatus,trnartype,trnardate,periodacctg,paymentacctgoid,paymentdate,payrefno,paybankoid,payduedate,amttrans,amtbayar,trnarnote,createuser,createtime,upduser,updtime,amttransidr,amtbayaridr,amttransusd,amtbayarusd,amtbayarother) VALUES ('" & Session("CompnyCode") & "'," & iConarOid & ",'" & dv(C1).Item("reftype") & "'," & dv(C1).Item("refoid") & "," & icnOid & "," & dv(C1).Item("cust_supp_oid") & ", " & dv(C1).Item("acctgoid") & ",'Post','GC" & cashbankgroup.Text.ToUpper & "','1/1/1900','" & sPeriod & "', " & iCOADebet & ",'" & sDate & "','" & Session("cnno") & "',0,'" & sDate & "',0," & (ToDouble(dv(C1).Item("payamtidr"))) * -1 & ",'GC No. " & Session("cnno") & " for A/R No. " & dv(C1).Item("invoiceno") & "','" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,0," & (ToDouble(dv(C1).Item("payamtidr"))) * -1 & ",0," & (ToDouble(dv(C1).Item("payamtusd"))) * -1 & ",0)"
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                            sSql = " UPDATE QL_trnjualmst SET trnjualstatus='Post', upduser='" & Session("UserID") & "', updtime=CURRENT_TIMESTAMP WHERE cmpcode='" & Session("CompnyCode") & "' AND trnjualmstoid=" & dv(C1).Item("refoid")
                            xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        End If

                        sSql = "INSERT INTO QL_trnglmst (cmpcode,glmstoid,gldate,periodacctg,glnote,glflag,postdate,createuser,createtime,upduser,updtime,type,rateoid,rate2oid,glrateidr,glrate2idr,glrateusd,glrate2usd) VALUES ('" & Session("CompnyCode") & "'," & iGLMstOid & ",'" & sDate & "','" & sPeriod & "','GC No. " & Session("cnno") & " for " & cashbankgroup.Text & " No. " & dv(C1).Item("invoiceno") & "','Post',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,'" & Session("UserID") & "',CURRENT_TIMESTAMP,''," & cRate.GetRateDailyOid & "," & cRate.GetRateMonthlyOid & "," & cRate.GetRateDailyIDRValue & "," & cRate.GetRateMonthlyIDRValue & "," & cRate.GetRateDailyUSDValue & "," & cRate.GetRateMonthlyUSDValue & ")"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1) VALUES ('" & Session("CompnyCode") & "'," & iGLDtlOid & ", " & iSeq & "," & iGLMstOid & ", " & iacctgoiddebet & ",'D'," & ToDouble(dv(C1).Item("payamtidr")) & ",'" & Session("cnno") & "','GC No. " & Session("cnno") & " for " & cashbankgroup.Text & " No. " & dv(C1).Item("invoiceno") & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & ToDouble(dv(C1).Item("payamtidr")) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(dv(C1).Item("payamtidr")) * cRate.GetRateMonthlyUSDValue & ",'QL_trngirocancel " & icnOid & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                        iGLDtlOid += 1
                        iSeq += 1

                        sSql = "INSERT INTO QL_trngldtl (cmpcode,gldtloid,glseq,glmstoid,acctgoid,gldbcr,glamt,noref,glnote,glflag,upduser,updtime,glamtidr,glamtusd,glother1) VALUES ('" & Session("CompnyCode") & "'," & iGLDtlOid & ", " & iSeq & "," & iGLMstOid & "," & iacctgoidcredit & ",'C'," & ToDouble(dv(C1).Item("payamtidr")) & ",'" & Session("cnno") & "','GC No. " & Session("cnno") & " for " & cashbankgroup.Text & " No. " & dv(C1).Item("invoiceno") & "','Post','" & Session("UserID") & "',CURRENT_TIMESTAMP," & ToDouble(dv(C1).Item("payamtidr")) * cRate.GetRateMonthlyIDRValue & "," & ToDouble(dv(C1).Item("payamtidr")) * cRate.GetRateMonthlyUSDValue & ",'QL_trngirocancel " & icnOid & "')"
                        xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                        iGLDtlOid += 1
                        iGLMstOid += 1
                        icnOid += 1
                        iConapOid += 1
                        iConarOid += 1
                    Next

                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConapOid - 1 & " WHERE tablename='QL_CONAP' and cmpcode='" & Session("CompnyCode") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iConarOid - 1 & " WHERE tablename='QL_CONAR' and cmpcode='" & Session("CompnyCode") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLDtlOid - 1 & " WHERE tablename='QL_TRNGLDTL' and cmpcode='" & Session("CompnyCode") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & iGLMstOid - 1 & " WHERE tablename='QL_TRNGLMST' and cmpcode='" & Session("CompnyCode") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()
                    sSql = "UPDATE QL_mstoid SET lastoid=" & icnOid - 1 & " WHERE tablename='QL_TRNGIROCANCEL' and cmpcode='" & Session("CompnyCode") & "'"
                    xCmd.CommandText = sSql : xCmd.ExecuteNonQuery()

                    objTrans.Commit()
                    conn.Close()
                Catch ex As Exception
                    objTrans.Rollback()
                    conn.Close()
                    showMessage(ex.Message, 1)
                    Exit Sub
                End Try
                Response.Redirect("~\Accounting\trnGiroCancel.aspx?awal=true")
            End If
        End If
    End Sub

    Protected Sub confirmdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'confirmdate.Text = Format(Now.Date, "MM/dd/yyyy")
    End Sub

    Protected Sub gvDtl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        mtno.Text = gvDtl.SelectedDataKey.Item("cashbankno").ToString
        cashbankoid.Text = gvDtl.SelectedDataKey.Item("cashbankoid").ToString
        cashbankgroup.Text = gvDtl.SelectedDataKey.Item("cashbankgroup").ToString
        curroid.Text = gvDtl.SelectedDataKey.Item("curroid").ToString
        If cashbankoid.Text <> "" Then
            BindDetailGiro()
        End If
        pnlGiro.Visible = False : mtno.Enabled = False : mtno.CssClass = "inpTextDisabled"
    End Sub

    Protected Sub gvDtl_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(8).Text = ToMaskEdit(ToDouble(e.Row.Cells(8).Text), 2)
        End If
    End Sub

    Protected Sub gvDtl_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        gvDtl.PageIndex = e.NewPageIndex
        gvDtl.DataSource = Session("TblDataGiro")
        gvDtl.DataBind()
        pnlGiro.Visible = True
    End Sub
#End Region

End Class