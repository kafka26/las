Imports System.Data

Partial Class MasterUser
    Inherits System.Web.UI.MasterPage

#Region "Variables"
    Dim dv As DataView
    Public CompnyName As String = ConfigurationSettings.AppSettings("CompanyName")
#End Region

#Region "Procedures"
    Private Sub CreateSeparator(ByVal parentItem As MenuItem, ByVal cSep As Char, ByVal iCount As Integer)
        ' Create a new MenuItem object.
        Dim sepItem As New MenuItem() : sepItem.Selectable = False
        Dim sSepText As String = ""
        For C1 As Integer = 1 To iCount
            sSepText &= cSep
        Next
        sepItem.Text = sSepText
        parentItem.ChildItems.Add(sepItem)
    End Sub

    Private Sub BuildMenu()
        dv = Session("Role").defaultview
        Dim homeMenu As MenuItem = CreateMenuItem("-: HOME :-", "~/other/menu.aspx?awal=true", "HOME")

        ' ======> SETUP MENU <======
        Dim SetupMenu As MenuItem = CreateParentItem("SETUP", "", "SETUP")
        Dim SetupChildMenu As MenuItem
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        For C1 As Integer = 0 To dv.Count - 1
            SetupChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
            SetupMenu.ChildItems.Add(SetupChildMenu)
        Next
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        For C1 As Integer = 0 To dv.Count - 1
            SetupChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
            SetupMenu.ChildItems.Add(SetupChildMenu)
        Next
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='SETUP' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        For C1 As Integer = 0 To dv.Count - 1
            SetupChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
            SetupMenu.ChildItems.Add(SetupChildMenu)
        Next
        ' ======> END OF SETUP MENU <======

        ' ======> PURCHASING MENU <======
        Dim PurchaseMenu As MenuItem = CreateParentItem("PURCHASING", "", "PURCHASING")
        Dim PurchaseChildMenu As MenuItem
        ' ==========> PURCHASING > MASTER MENU <==========
        Dim MstPurchaseMenu As MenuItem = CreateParentItem("MASTER", "", "PURCHASING - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                PurchaseChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstPurchaseMenu.ChildItems.Add(PurchaseChildMenu)
            Next
            AddingMenu(PurchaseMenu, MstPurchaseMenu)
        End If
        ' ==========> PURCHASING > TRANSACTION MENU <==========
        Dim TrnPurchaseMenu As MenuItem = CreateParentItem("TRANSACTION", "", "PURCHASING - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                PurchaseChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnPurchaseMenu.ChildItems.Add(PurchaseChildMenu)
            Next
            AddingMenu(PurchaseMenu, TrnPurchaseMenu)
        End If
        ' ==========> PURCHASING > REPORT MENU <==========
        Dim RptPurchaseMenu As MenuItem = CreateParentItem("REPORT", "", "PURCHASING - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PURCHASING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                PurchaseChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptPurchaseMenu.ChildItems.Add(PurchaseChildMenu)
            Next
            AddingMenu(PurchaseMenu, RptPurchaseMenu)
        End If
        ' ======> END OF PURCHASING MENU <======

        ' ======> MARKETING MENU <======
        Dim SalesMenu As MenuItem = CreateParentItem("MARKETING", "", "MARKETING")
        Dim SalesChildMenu As MenuItem
        ' ==========> MARKETING > MASTER MENU <==========
        Dim MstSalesMenu As MenuItem = CreateParentItem("MASTER", "", "MARKETING - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                SalesChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstSalesMenu.ChildItems.Add(SalesChildMenu)
            Next
            AddingMenu(SalesMenu, MstSalesMenu)
        End If
        ' ==========> MARKETING > TRANSACTION MENU <==========
        Dim TrnSalesMenu As MenuItem = CreateParentItem("TRANSACTION", "", "MARKETING - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                SalesChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnSalesMenu.ChildItems.Add(SalesChildMenu)
            Next
            AddingMenu(SalesMenu, TrnSalesMenu)
        End If
        ' ==========> MARKETING > REPORT MENU <==========
        Dim RptSalesMenu As MenuItem = CreateParentItem("REPORT", "", "MARKETING - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='MARKETING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                SalesChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptSalesMenu.ChildItems.Add(SalesChildMenu)
            Next
            AddingMenu(SalesMenu, RptSalesMenu)
        End If
        ' ======> END OF MARKETING MENU <======

        ' ======> INVENTORY MENU <======
        Dim InventoryMenu As MenuItem = CreateParentItem("INVENTORY", "", "INVENTORY")
        Dim InventoryChildMenu As MenuItem
        ' ==========> INVENTORY > MASTER MENU <==========
        Dim MstInventoryMenu As MenuItem = CreateParentItem("MASTER", "", "INVENTORY - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                InventoryChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstInventoryMenu.ChildItems.Add(InventoryChildMenu)
            Next
            AddingMenu(InventoryMenu, MstInventoryMenu)
        End If
        ' ==========> INVENTORY > TRANSACTION MENU <==========
        Dim TrnInventoryMenu As MenuItem = CreateParentItem("TRANSACTION", "", "INVENTORY - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                InventoryChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnInventoryMenu.ChildItems.Add(InventoryChildMenu)
            Next
            AddingMenu(InventoryMenu, TrnInventoryMenu)
        End If
        ' ==========> INVENTORY > REPORT MENU <==========
        Dim RptInventoryMenu As MenuItem = CreateParentItem("REPORT", "", "INVENTORY - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='INVENTORY' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                InventoryChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptInventoryMenu.ChildItems.Add(InventoryChildMenu)
            Next
            AddingMenu(InventoryMenu, RptInventoryMenu)
        End If
        ' ======> END OF INVENTORY MENU <======

        ' ======> PRODUCTION MENU <======
        Dim ProductionMenu As MenuItem = CreateParentItem("PRODUCTION", "", "PRODUCTION")
        Dim ProductionChildMenu As MenuItem
        ' ==========> PRODUCTION > MASTER MENU <==========
        Dim MstProductionMenu As MenuItem = CreateParentItem("MASTER", "", "PRODUCTION - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                ProductionChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstProductionMenu.ChildItems.Add(ProductionChildMenu)
            Next
            AddingMenu(ProductionMenu, MstProductionMenu)
        End If
        ' ==========> PRODUCTION > TRANSACTION MENU <==========
        Dim TrnProductionMenu As MenuItem = CreateParentItem("TRANSACTION", "", "PRODUCTION - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                ProductionChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnProductionMenu.ChildItems.Add(ProductionChildMenu)
            Next
            AddingMenu(ProductionMenu, TrnProductionMenu)
        End If
        ' ==========> PRODUCTION > REPORT MENU <==========
        Dim RptProductionMenu As MenuItem = CreateParentItem("REPORT", "", "PRODUCTION - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='PRODUCTION' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                ProductionChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptProductionMenu.ChildItems.Add(ProductionChildMenu)
            Next
            AddingMenu(ProductionMenu, RptProductionMenu)
        End If
        ' ======> END OF PRODUCTION MENU <======

        ' ======> ACCOUNTING MENU <======
        Dim AccountingMenu As MenuItem = CreateParentItem("ACCOUNTING", "", "ACCOUNTING")
        Dim AccountingChildMenu As MenuItem
        ' ==========> ACCOUNTING > MASTER MENU <==========
        Dim MstAccountingMenu As MenuItem = CreateParentItem("MASTER", "", "ACCOUNTING - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AccountingChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstAccountingMenu.ChildItems.Add(AccountingChildMenu)
            Next
            AddingMenu(AccountingMenu, MstAccountingMenu)
        End If
        ' ==========> ACCOUNTING > TRANSACTION MENU <==========
        Dim TrnAccountingMenu As MenuItem = CreateParentItem("TRANSACTION", "", "ACCOUNTING - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AccountingChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnAccountingMenu.ChildItems.Add(AccountingChildMenu)
            Next
            AddingMenu(AccountingMenu, TrnAccountingMenu)
        End If
        ' ==========> ACCOUNTING > REPORT MENU <==========
        Dim RptAccountingMenu As MenuItem = CreateParentItem("REPORT", "", "ACCOUNTING - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='ACCOUNTING' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AccountingChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptAccountingMenu.ChildItems.Add(AccountingChildMenu)
            Next
            AddingMenu(AccountingMenu, RptAccountingMenu)
        End If
        ' ======> END OF ACCOUNTING MENU <======

        ' ======> WIP LOG MENU <======
        Dim WIPLogMenu As MenuItem = CreateParentItem("WIP LOG", "", "WIP LOG")
        Dim WIPLogChildMenu As MenuItem
        ' ==========> WIP LOG > MASTER MENU <==========
        Dim MstWIPLogMenu As MenuItem = CreateParentItem("MASTER", "", "WIP LOG - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                WIPLogChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstWIPLogMenu.ChildItems.Add(WIPLogChildMenu)
            Next
            AddingMenu(WIPLogMenu, MstWIPLogMenu)
        End If
        ' ==========> WIP LOG > TRANSACTION MENU <==========
        Dim TrnWIPLogMenu As MenuItem = CreateParentItem("TRANSACTION", "", "WIP LOG - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                WIPLogChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnWIPLogMenu.ChildItems.Add(WIPLogChildMenu)
            Next
            AddingMenu(WIPLogMenu, TrnWIPLogMenu)
        End If
        ' ==========> WIP LOG > REPORT MENU <==========
        Dim RptWIPLogMenu As MenuItem = CreateParentItem("REPORT", "", "WIP LOG - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='WIP LOG' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                WIPLogChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptWIPLogMenu.ChildItems.Add(WIPLogChildMenu)
            Next
            AddingMenu(WIPLogMenu, RptWIPLogMenu)
        End If
        ' ======> END OF WIP LOG MENU <======

        ' ======> FIXED ASSETS MENU <======
        Dim AssetMenu As MenuItem = CreateParentItem("FIXED ASSETS", "", "FIXED ASSETS")
        Dim AssetChildMenu As MenuItem
        ' ==========> FIXED ASSETS > MASTER MENU <==========
        Dim MstAssetMenu As MenuItem = CreateParentItem("MASTER", "", "FIXED ASSETS - MASTER")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='MASTER'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AssetChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                MstAssetMenu.ChildItems.Add(AssetChildMenu)
            Next
            AddingMenu(AssetMenu, MstAssetMenu)
        End If
        ' ==========> FIXED ASSETS > TRANSACTION MENU <==========
        Dim TrnAssetMenu As MenuItem = CreateParentItem("TRANSACTION", "", "FIXED ASSETS - TRANSACTION")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='TRANSACTION'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AssetChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                TrnAssetMenu.ChildItems.Add(AssetChildMenu)
            Next
            AddingMenu(AssetMenu, TrnAssetMenu)
        End If
        ' ==========> FIXED ASSET > REPORT MENU <==========
        Dim RptAssetMenu As MenuItem = CreateParentItem("REPORT", "", "FIXED ASSETS - REPORT")
        dv.RowFilter = ""
        dv.RowFilter = "FORMMODULE='FIXED ASSETS' AND FORMTYPE='REPORT'"
        dv.Sort = "FORMNUMBER"
        If dv.Count > 0 Then
            For C1 As Integer = 0 To dv.Count - 1
                AssetChildMenu = CreateMenuItem(dv(C1).Item(5).ToString.Trim.ToUpper, "~/" & dv(C1).Item(2).ToString.Trim & "?awal=true", dv(C1).Item(1).ToString.Trim.ToUpper)
                RptAssetMenu.ChildItems.Add(AssetChildMenu)
            Next
            AddingMenu(AssetMenu, RptAssetMenu)
        End If
        ' ======> END OF FIXED ASSETS MENU <======

        Menu1.Items.Add(homeMenu)
        AddingMenu(Menu1, SetupMenu)
        AddingMenu(Menu1, PurchaseMenu)
        AddingMenu(Menu1, SalesMenu)
        AddingMenu(Menu1, InventoryMenu)
        AddingMenu(Menu1, ProductionMenu)
        AddingMenu(Menu1, AccountingMenu)
        AddingMenu(Menu1, WIPLogMenu)
        AddingMenu(Menu1, AssetMenu)
    End Sub

    Private Sub AddingMenu(ByVal parent As Menu, ByVal child As MenuItem)
        If child.ChildItems.Count > 0 Then
            parent.Items.Add(child)
        End If
    End Sub

    Private Sub AddingMenu(ByVal parent As MenuItem, ByVal child As MenuItem)
        If child.ChildItems.Count > 0 Then
            parent.ChildItems.Add(child)
        End If
    End Sub
#End Region

#Region "Function"
    Private Function CreateMenuItem(ByVal text As String, ByVal url As String, ByVal toolTip As String) As MenuItem
        ' Create a new MenuItem object.
        Dim menuItem As New MenuItem()
        ' Set the properties of the MenuItem object using
        ' the specified parameters.
        menuItem.Text = text.Replace("REPORT", "").Replace("ACCOUNTING", "")
        menuItem.NavigateUrl = url
        menuItem.ToolTip = toolTip
        Return menuItem
    End Function

    Private Function CreateParentItem(ByVal text As String, ByVal sNothing As String, ByVal toolTip As String) As MenuItem
        ' Create a new MenuItem object.
        Dim menuItem As New MenuItem()
        ' Set the properties of the MenuItem object using the specified parameters.
        menuItem.Text = text : menuItem.Selectable = False
        menuItem.ToolTip = toolTip
        Return menuItem
    End Function
#End Region

#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("UserID") Is Nothing Or Session("UserID") <> "" Then
            lblUser.Text = "ID : " & Session("UserID")
            lblLogOut.Text = "Log Out"
            lblSep.Text = "|"
        Else
            lblUser.Text = ""
            lblLogOut.Text = ""
            lblSep.Text = ""
        End If
        lblToday1.Text = Format(Today, "dd")
        lblToday2.Text = Format(Today, "dddd")
        lblToday3.Text = Format(Today, "MMMM")
        lblToday4.Text = Format(Today, "yyyy")
        If Not IsPostBack Then
            If Not Session("UserID") Is Nothing Or Session("UserID") <> "" Then
                BuildMenu()
            End If
        End If
    End Sub
#End Region

End Class